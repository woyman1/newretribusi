							<div class="col-md-12">
                                <div class="bg-theme-dark m-b-15">
									<div class="row weather p-20">
                                        <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 m-t-40">
                                            <h3>&nbsp;</h3>
                                            <h1><?php echo date('H:i');?><sup><?php echo $time_zone;?></sup></h1>
                                            <p class="text-white">
											<?php 
										$jam = date('H');
										if ($jam > 8 AND $jam < 17) {
										 echo "Semangat Bekerja :D";
										}
										else if ($jam >17 OR $jam < 8) {
											echo "Saatnya Beristirahat Sejenak :)";
										}
										?></p>
                                        </div>
                                        <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 text-right">
										<?php 
										$jam = date('H');
										if ($jam < 17) {
										 echo "<i class='wi wi-day-cloudy-high'></i>";
										}
										else if ($jam >17) {
											echo "<i class='wi wi-night-alt-cloudy-gusts'></i>";
										}
										?>										
                                            <br/>
                                            <br/>
                                            <b class="text-white"><?php echo $day;?></b>
                                            <p class="w-title-sub"><?php echo date('d-M-y');?></p>
                                        </div>
                                    </div>
								</div>
							</div>