<?php
include ("../inc/inc.session.admin.php");
include ("../../config/db.connect.php");
include ("../../config/site.config.php");
include ("../../inc/inc.variable.php");

	if(isset($_POST['create']))
		{
			//menyimpan data dari form tambah costumer
			$query = mysqli_query($re_connect, "INSERT INTO
				re_costumer(
					cost_name,
					cost_address,
					cost_prov,
					cost_kab,
					cost_kec,
					cost_tmp_lahir,
					cost_tgl_lahir,
					cost_type_id,
					cost_desa_id,
					cost_date,
					cost_status,
					cost_user_id,
					cost_jadwal_id,
					cost_lang,
					cost_latt )

				VALUES(
					'$cost_name',
					'$cost_address',
					'$cost_prov',
					'$cost_kab',
					'$cost_kec',
					'$cost_tmplahir',
					'$cost_tgllahir',
					'$cost_typeid',
					'$cost_desaid',
					'$date',
					'active',
					'$cost_op',
					'$cost_jdwal',
					'$cost_lang',
					'$cost_lat' ) ") or die(mysqli_error($re_connect));

			$kd = mysqli_query($re_connect, "SELECT type_code FROM re_type WHERE type_id = '$cost_typeid' ");
			$k = mysqli_fetch_assoc($kd);
			$typekd = $k['type_code'];

			//mencari panjang id_costumer
			$q = mysqli_query($re_connect, "SELECT cost_id FROM re_costumer ORDER BY cost_id DESC limit 1 ");
			$i = mysqli_fetch_assoc($q);
			$id = $i['cost_id'];

			$lenght = strlen((string)abs($id));

			// for untuk menentukan berapa nol yang akan ditaruh
			for($a = 0; $a<4 - $lenght; $a++)
			{
				$zero = $zero."0";
			}

			$id_result =  $zero.$id;

			//mencari panjang id_desa
			$lenght_desa = strlen((string)abs($cost_desaid));
			if($lenght_desa < 2)
			{
				$idDesa = "0".$cost_desaid;
			}
			else
			{
				$idDesa = $cost_desaid;
			}

			//$id_pel siap untuk disimpan di database
			$id_pel = $typekd.$idDesa.$id_result;

			$save_id_pel = mysqli_query($re_connect, "UPDATE re_costumer SET cost_id_pel='$id_pel' WHERE cost_id = '$id'  ");

			header("location: ../index.php?page=viewcostumer&id=$id ");
		}
	else if($actionGet == 'delete')
		{
			$query = mysqli_query($re_connect, "DELETE FROM re_costumer WHERE cost_id = '$idGet' ");
			header("location: ../index.php?page=listcostumer&message=1");
		}
	else if($actionPost == 'update')
		{
			$query = mysqli_query($re_connect, "UPDATE re_costumer
				SET cost_name='$cost_name',
					cost_address='$cost_address',
					cost_prov='$cost_prov',
					cost_kab='$cost_kab',
					cost_kec='$cost_kec',
					cost_desa_id='$cost_desaid',
					cost_type_id='$cost_typeid',
					cost_tmp_lahir='$cost_tmplahir',
					cost_user_id = '$cost_op',
					cost_jadwal_id = '$cost_jdwal',
					cost_lang = '$cost_lang',
					cost_latt = '$cost_lat',
					cost_tgl_lahir = '$cost_tgllahir'
				WHERE cost_id = '$idPost'  ");
			header("location: ../index.php?page=listcostumer&message=1");
		}
	else if($actionPost == 'upload_arsip' )
		{
			$arsip_val = "cost_".$Carsip;

			$q = mysqli_query($re_connect, "SELECT $arsip_val FROM re_costumer WHERE cost_id = '$idPost' ");
			$type = mysqli_fetch_assoc($q);


			if($type[$arsip_val])
			{
				unlink("../../uploads/images/arsip/".$type[$arsip_val]);
			}

			$pic_blob = $_FILES['pic']['tmp_name'];
			move_uploaded_file($pic_blob, '../../uploads/images/arsip/'.$pic );

			$query = mysqli_query($re_connect, "UPDATE re_costumer SET $arsip_val = '$pic' WHERE cost_id = '$idPost' " );

			header("location: ../index.php?page=upload_arsip_cost&id=$idPost&message=1");
		}
	else if($actionGet == 'delete_arsip')
		{
			$arsip_val = "cost_".$Garsip;

			$q = mysqli_query($re_connect, "UPDATE re_costumer SET $arsip_val = null WHERE cost_id = '$idGet' ");

			$qq = mysqli_query($re_connect, "SELECT $arsip_val FROM re_costumer WHERE cost_id = '$idPost' ");
			$type = mysqli_fetch_assoc($qq);

			unlink("../../uploads/images/arsip/".$type[$arsip_val]);

			header("location: ../index.php?page=upload_arsip_cost&id=$idGet&message=1");

		}

?>
