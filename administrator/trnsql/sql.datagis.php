<?php
include ("../inc/inc.session.admin.php");
include ("../../config/db.connect.php");
include ("../../config/site.config.php");
include ("../../inc/inc.variable.php");

if(isset($_POST['create'])) // create
{
	$query = mysqli_query($re_connect, "INSERT INTO 
			 re_datagis( data_name, 
			 			 data_type,
			 			 data_desc,
			 			 data_lon,
			 			 data_lat) 

			 VALUES('$gis_name', 
			 		'$gis_tipe',
			 		'$gis_desk',
			 		'$gis_lang',
			 		'$gis_latt' )") or die(mysqli_error($re_connect));

	if($_FILES['pic']){

		$pic_blob = $_FILES['pic']['tmp_name'];
		move_uploaded_file($pic_blob, '../../uploads/images/datagis/'.$pic );

		$i = mysqli_query($re_connect, "SELECT data_id FROM re_datagis ORDER BY data_id DESC"); $d = mysqli_fetch_assoc($i);
		$idData = $d['data_id'];

		$qgambar = mysqli_query($re_connect, "INSERT INTO re_gallery(gallery_file, gallery_datagis_id) VALUES('$pic', '$idData')  ") or die(mysqli_error($re_connect));
	}
	
	header("location: ../index.php?page=listdatagis&message=1");
}
else if(isset($_POST['create_tipe']))
{
	$query = mysqli_query($re_connect,"INSERT INTO re_datagis_type(typegis_name) VALUES('$tipe_name') ");
	
	if($query)
	{
		header("location: ../index.php?page=listtipedatagis&message=1");
	}	
}


else if($actionGet == 'delete') //hapus
{
	$query = mysqli_query($re_connect,"DELETE FROM re_datagis WHERE data_id = '$idGet' ");

	if($query)
	{
		header("location: ../index.php?page=listdatagis&message=1");
	}

}
else if($actionGet == 'delete_tipe')
{
	$query = mysqli_query($re_connect,"DELETE FROM re_datagis_type WHERE typegis_id = '$idGet' ");

	if($query)
	{
		header("location: ../index.php?page=listtipedatagis&message=1");
	}	
}


else if($actionPost == 'edit') //edit
{
	$query = mysqli_query($re_connect, "UPDATE re_datagis SET 
												data_name = '$gis_name',
												data_type = '$gis_tipe',
												data_desc = '$gis_desk',
												data_lang = '$gis_lang',
												data_latt = '$gis_latt' WHERE data_id = '$idPost' ") or die(mysqli_error($re_connect));
	if($query)
	{
		header("location: ../index.php?page=listdatagis&message=1");
	}
}
else if($actionPost == 'edittipegis')
{
	$query = mysqli_query($re_connect,"UPDATE re_datagis_type SET typegis_name = '$tipe_name' WHERE typegis_id = '$idPost' ");

	if($query)
	{
		header("location: ../index.php?page=listtipedatagis&message=1");
	}
}
else if($actionGet =="delete_fto")
{
	$dataid = $_GET['dataid'];
	$query = mysqli_query($re_connect,"DELETE FROM re_gallery WHERE gallery_id = '$idGet' ");

	header("location: ../index.php?page=datagis_galery&id=$dataid&message=1");	
}
else if($actionPost =="up_galfoto")
{
	$loop = count($_FILES['data_gal']['tmp_name']);

	for($a = 0; $a<$loop; $a++){

		$blob = $_FILES['data_gal']['tmp_name'][$a];
		$blob_name = uniqid($_FILES['data_gal']['name'][$a]);
		move_uploaded_file($blob, '../../uploads/images/datagis/'.$blob_name);

		$query = mysqli_query($re_connect,"INSERT INTO re_gallery(gallery_file, gallery_datagis_id) VALUES('$blob_name', '$idPost') ");
	}
	
	header("location: ../index.php?page=datagis_galery&id=$idPost&message=1");

}

?>