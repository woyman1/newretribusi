<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Apps Admin PHP ">
    <meta name="author" content="TechnoGIS Indonesia">
    <link rel="icon" type="image/png" sizes="16x16" href="../uploads/images/favicon.png">
   <title><?php if (!$pageName) { echo "Dashboard"; } else { echo "$pageName";}?> - <?php echo $site_name; ?></title>
   <!--  CSS Start Here-->
    <!-- Bootstrap Core CSS -->
    <link href="../assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />

    <!-- animation CSS -->
    <link href="../assets/css/animate.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../assets/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="../assets/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- custome plugin -->
    <link href="../plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!--js script start here-->
    <!-- Bootstrap Core JavaScript -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../assets/autocomplate/jquery-ui.js"></script>
    <script src="../assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="../assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="../assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../assets/js/custom.min.js"></script>
    <script src="../plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!-- Toast message JavaScript -->
    <script src="../plugins/bower_components/toast-master/js/jquery.toast.js"></script>

    <link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/autocomplate/jquery-ui.css">
       <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .radio_z {
                visibility: hidden;
                position: absolute;
        }

       .radio+z + svg{
            cursor:pointer;
              border:2px solid transparent;
        }

        input:checked + svg {
            border:2px solid blue;
            border-radius: 25px;
            box-shadow: 0.5px 0px blue;

        }

        .home{
            margin: auto;
        }

        .backIm{
            margin: auto;
            height: 70vh;

            background-image: url("../assets/frontend/img/518169-backgrounds.jpg");

        }

        .back{
        text-align: center;
        }

        .textHome{
            background: white;

            margin-right: -8px;
      }
    </style>

</head>
<body>
    <!-- Preloader-->
 <!--div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div-->
