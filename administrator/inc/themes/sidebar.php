        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">

                    <li> <a href="index.php?page=home" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </span></a>
                    </li>



                     <li> <a href="javascript:void(0)" class="waves-effect"><i class="icon-people"></i> <span class="hide-menu">Costumer<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=listcostumer">Daftar Costumer</a> </li>

                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>
                            <li> <a href="index.php?page=addcostumer"><i class="linea-icon linea-basic" data-icon=""></i>Tambah Costumer Baru</a> </li>
                            <?php endif ?>

                        </ul>
                    </li>


                    <?php if ($levelUser != 5) : ?>
                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="ti-home"></i> <span class="hide-menu">Kelurahan<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=listdesa">Daftar Kelurahan</a> </li>
                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>
                            <li> <a href="index.php?page=adddesa"><i class="linea-icon linea-basic" data-icon=""></i>Tambahkan Kelurahan Baru</a> </li>
                            <?php endif ?>
                        </ul>
                    </li>



                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="ti-home"></i> <span class="hide-menu">Kecamatan<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=listkec">Daftar Kecamatan</a> </li>
                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                            <li> <a href="index.php?page=addkec"><i class="linea-icon linea-basic" data-icon=""></i>Tambahkan Kecamatan Baru</a> </li>  <?php endif ?>
                        </ul>
                    </li>

                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="icon-layers"></i> <span class="hide-menu">Tipe<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=listtipe">Daftar Tipe</a> </li>
                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                            <li> <a href="index.php?page=addtipe"><i class="linea-icon linea-basic" data-icon=""></i>Tambah Tipe Baru</a> </li><?php endif ?>
                        </ul>

                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="ti-image"></i> <span class="hide-menu">Gallery<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="trnsql/sql.gallery.php?action=filterIt&filter=all">Lihat Galery</a> </li>
                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                            <li> <a href="index.php?page=addgallery"><i class="linea-icon linea-basic"></i>Tambah Foto Gallery Baru</a> </li><?php endif ?>
                        </ul>
                    </li>

                     <li> <a href="javascript:void(0)" class="waves-effect"> <i class="ti-list"></i> <span class="hide-menu">Jadwal Sampah<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=listjadwal">Jadwal Pengambilan Sampah</a> </li>
                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                            <li> <a href="index.php?page=addjadwal"><i class="linea-icon linea-basic" data-icon=""></i>Tambahkan Jadwal Baru</a> </li> <?php endif ?>
                        </ul>
                    </li>

                    <?php if ($levelUser == 1) : ?>

                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="icon-user"></i> <span class="hide-menu">Petugas<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=listuser">Daftar Petugas</a> </li>
                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                            <li> <a href="index.php?page=adduser"><i class="linea-icon linea-basic" data-icon=""></i>Tambahkan Petugas Baru</a> </li><?php endif ?>
                        </ul>
                    </li>

                    <?php endif ?>

                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="ti-exchange-vertical"></i> <span class="hide-menu">Transaksi<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=listpembayaran">Daftar Transaksi</a> </li>
                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                            <li> <a href="index.php?page=addtransaksi"><i class="linea-icon linea-basic" data-icon=""></i>Tambahkan Transaksi Baru</a> </li> <?php endif ?>

                        </ul>
                    </li>

                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="ti-briefcase"></i> <span class="hide-menu">Data Gis<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=listdatagis">Daftar Data Gis</a> </li>
                            <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                            <li> <a href="index.php?page=adddatagis"><i class="linea-icon linea-basic" data-icon=""></i>Tambahkan Data Gis Baru</a> </li> <?php endif ?>
                            <li> <a href="javascript:void(0)" class="waves-effect">Tipe Data Gis<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li> <a href="index.php?page=listtipedatagis"> Daftar Tipe Data Gis</a></li>
                                    <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                                    <li> <a href="index.php?page=addtipegis">Tambahkan Tipe Data Gis</a></li><?php endif ?>
                                </ul>
                        </ul>
                    </li>

                    <?php endif ?>



                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="ti-bar-chart"></i> <span class="hide-menu">Laporan<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=regular_report">Laporan</a> </li>
                        </ul>
                    </li>

                    <?php if ($levelUser != 5) : ?>

                    <li> <a href="javascript:void(0)" class="waves-effect"> <i class="fa fa-gear"></i> <span class="hide-menu">Pengaturan<span class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                 <li> <a href="index.php?page=peng_umum">Umum</a> </li>
                <li> <a href="index.php?page=peng_editlogo">Edit Logo</a> </li>
                <li> <a href="index.php?page=peng_about">About</a> </li>
                <li> <a href="index.php?page=peng_lain">Lain-lain</a> </li>
            </ul>
        </li>

        <?php endif ?>

                </ul>
            </div>
        </div>
