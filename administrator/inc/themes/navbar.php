<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <!--div class="top-left-part"><a class="logo" href="index.php"><b><img src="" alt="home" /></b><span class="hidden-xs"><strong>iSURVEY</strong>Admin</span></a></div-->
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    <li>
                        <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <!-- <img src="../uploads/images/users/<?php echo $picUser;?>" alt="user-img" width="36" class="img-circle"> --><b class="hidden-xs"><?php echo $nameUser;?></b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="index.php?page=profileuser&id=<?php echo $idUser;?>"><i class="ti-user"></i>  My Profile</a></li>
                            <li><a href="index.php?page=logout"><i class="fa fa-power-off"></i>  Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav> 