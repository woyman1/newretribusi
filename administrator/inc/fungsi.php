<?php 


function date_day_name($day){
	$name_day = date("D", mktime(0,0,0, date("m"), $day, date("Y") ));
	switch ($name_day) {
	case 'Sun' : $hari = "Minggu"; break;
	case 'Mon' : $hari = "Senin"; break;
	case 'Tue' : $hari = "Selasa"; break;
	case 'Wed' : $hari = "Rabu"; break;
	case 'Thu' : $hari = "Kamis"; break;
	case 'Fri' : $hari = "Jum'at"; break;
	case 'Sat' : $hari = "Sabtu"; break;
	default : $hari = "Kiamat";
	}
	return $hari;
}

function date_month_name($month){
	$month_name = date("m", mktime(0,0,0, $month, 1, date("Y") ));
	switch ($month_name) {
	case '01' : $hari = "Januari"; break;
	case '02' : $hari = "Februari"; break;
	case '03' : $hari = "Maret"; break;
	case '04' : $hari = "April"; break;
	case '05' : $hari = "Mei"; break;
	case '06' : $hari = "Juni"; break;
	case '07' : $hari = "Juli"; break;
	case '08' : $hari = "Agustus"; break;
	case '09' : $hari = "September"; break;
	case '10' : $hari = "Oktober"; break;
	case '11' : $hari = "November"; break;
	case '12' : $hari = "Desember"; break;

	default : $hari = "Kiamat";
	}
	return $hari;
}

function terbilang($feed) {
    //mendefinisikan satuan 
    $units = array('','ribu ','juta','milyar','triliun','biliun');
    //Mendefinisikan bilangan
    $angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan");
    //membuat function untuk memecah bilangan menjadi array beranggota 3 digit
  
  //menginisiasi luaran
  $output = '';
  if (strlen($feed) % 3 > 0) {
    $feed = substr('00', 0, (3 - strlen($feed) % 3)) . (string) $feed;
    //1234 akan diubah menjadi 001234
  }
 $segment3 = str_split($feed, 3);
 //Membilang setiap segmen 3 digit
 foreach($segment3 as $key => $val){
  	//memecah 3 digit menjadi arrau 1 digit
     
    $digit = str_split($val, 1);
    //menentukan nilai ratusan
    if($digit[0] == '1'){
    	$output .= 'seratus ';
    }else if($digit[0] != '0'){
   		$output .= $angka[$digit[0]] . ' ratus ';  	
    }
    //menentukan nilai puluhan
    if($digit[1] == '1'){
    	if($digit[2] == '0'){
      	$output .= 'sepuluh ';
      }else{
      	$output .= $angka[$digit[2]] . 'belas '; 
      }          
    }else if($digit[1] != '0'){
    	$output .= $angka[$digit[1]] . ' puluh ' . $angka[$digit[2]] . ' ';
    }else{
    	if($digit[0] == '0' && $digit[1]=='0' && $digit[2]=='1'){
      	$output .= 'se';
      }else{
      	$output .= $angka[$digit[2]] . ' ';
      }
    }
    $output .= $units[count($segment3)-$key-1] . ' ';
    $out = ucfirst($output);
 }
  return $out." rupiah" ;
}



?>