<?php if (!$page) {header("location:index.php=404");} else {}; ;?>
<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $pageName;?></h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                         <ol class="breadcrumb">
                            <li><a href="index.php?page=home">Dashboard</a></li>
                            <li class="active"><?php echo $pageName;?></li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->
                <div class="row">
					<?php 
						$query = mysqli_query($re_connect,"SELECT * FROM re_users INNER JOIN re_users_level ON re_users.re_user_level = re_users_level.level_id WHERE re_user_id = '$idGet' ") or die(mysqli_error($re_connect));
						$data=mysqli_fetch_array($query);

                          if(!$data) {
                                 header("location:index.php?page=404");
                             }else{}
                               
                          
					?>
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="../uploads/images/large/img0.jpg">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="index.php?page=editfoto"><img src="../uploads/images/users/<?php echo $data["users_pic"];?>" class="thumb-lg img-circle" alt="img"></a>
                                        <h4 class="text-white"><?php echo $data["re_user_name"];?></h4>
                                        <h5 class="text-white"><?php echo $data["re_user_email"];?></h5>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav customtab nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item"><a href="#profile" class="nav-link active" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profile</span></a></li>
                                <?php if ($data["re_user_name"]==$_SESSION["users_name"]) {
                                    echo 
                                "<li role='presentation' class='nav-item'><a href='#edit' class='nav-link' aria-controls='profile' role='tab' data-toggle='tab' aria-expanded='false'><span class='visible-xs'><i class='fa fa-user'></i></span> <span class='hidden-xs'>Edit Profile</span></a></li>
                                <li role='presentation' class='nav-item'><a href='#password' class='nav-link' aria-controls='profile' role='tab' data-toggle='tab' aria-expanded='false'><span class='visible-xs'><i class='fa fa-user'></i></span> <span class='hidden-xs'>Ganti Password</span></a></li>";

                                } else {

                                }
                                ?>
                            </ul>
                            <div class="tab-content">                   
                                <div class="tab-pane active" id="profile">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Nama Lengkap</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $data["re_user_name"];?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>No Hp</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $data["re_user_phone"];?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $data["re_user_email"];?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6"> <strong>Status Level</strong>
                                            <br>
                                            <p class="text-muted"><?php echo $data["level_name"]; ?></p>
                                        </div>
                                    </div>
                                    <hr>
                                <p><?php echo $data["re_user_address"];?></p>
                                </div>

                                <div class="tab-pane " id="edit">
                                    <div class="row">
                                        <form class="form-horizontal" action="trnsql/sql.profile.php" method="post">
                                        <input type="hidden" name="action" value="edit">
                                        <input type="hidden" name="id" value="<?php echo $data["re_user_id"];?>">
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Username*</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="username" class="form-control" id="inputEmail3" placeholder="Username" value="<?php echo $data["re_user_username"];?>" disabled="disabled" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Email*</label>
                                                <div class="col-sm-9">
                                                    <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email" value="<?php echo $data["re_user_email"];?>"  required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama*</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Nama" value="<?php echo $data["re_user_name"];?>"  required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Alamat*</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="address" class="form-control" id="inputEmail3" placeholder="Alamat" value="<?php echo $data["re_user_address"];?>"  required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">No Hp*</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="phone" class="form-control" id="inputEmail3" placeholder="Phone" value="<?php echo $data["re_user_phone"];?>"  required>
                                                </div>
                                            </div>                                                   
                                            <div class="form-group m-b-0">
                                                <div class="offset-sm-3 col-sm-9">
                                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <hr>
                                </div>
                                <div class="tab-pane " id="password">
                                    <div class="row">
                                        <form class="form-horizontal" data-toggle="validator" action="trnsql/sql.profile.php" method="post">
                                        <input type="hidden" name="action" value="resetpassword">
                                        <input type="hidden" name="id" value="<?php echo $data["re_user_id"];?>">
                                            <div class="form-group row">
                                                <label for="inputPassword3"class="col-sm-3 control-label col-form-label">Password Lama*</label>
                                                <div class="col-sm-9">
                                                    <input type="password"  name="oldpassword" class="form-control" id="oldPassword" placeholder="Masukan Password Lama" required>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group row">
                                                <label for="inputPassword3" class="col-sm-3 control-label col-form-label">Password Baru*</label>
                                                <div class="col-sm-9">
                                                    <input type="password"  name="newpassword" class="form-control" id="inputPassword3" placeholder="Password Baru" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword4" class="col-sm-3 control-label col-form-label">Ulangi Password Baru*</label>
                                                <div class="col-sm-9">
                                                    <input type="password" class="form-control" name="repassword" data-match="#inputPassword3" data-match-error="Password not match!" id="inputPassword4" placeholder="Ulangi Password Baru" required>
                                                </div>
                                            </div>
                                            <div class="form-group m-b-0">
                                                <div class="offset-sm-3 col-sm-9">
                                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Ganti Password</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

        <script src="../assets/js/validator.js"></script>
   