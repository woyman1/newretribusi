<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">    
<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>
<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
<?php 
    $query = mysqli_query($re_connect, "SELECT * FROM re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type = re_datagis_type.typegis_id WHERE data_id = '$idGet' ");
    while($data = mysqli_fetch_array($query)){
?> 

        <div class="row">
        	<div class="col-lg-12 white-box"> 

                <div class="col-sm-12 m-b-15">
                        <a href="index.php?page=listdatagis"><button class="btn btn-info"> < List Datagis</button></a>
                    </div>

        		<h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>
        		
        			  	
                            <input type="hidden" name="id" value="<?php echo $idGet; ?>">
                            <input type="hidden" name="action" value="edit">

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama Data Gis</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_datagis" value="<?php echo $data['data_name']; ?>" disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tipe Data Gis</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipe_datagis" disabled> 
                                        <option value="<?php echo $data['typegis_id'] ?>"><?php echo $data['typegis_name'] ?></option>
                                
                                </select>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Deskripsi</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="4" name="des_datagis" disabled><?php echo $data['data_desc']; ?></textarea>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Langitude</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="lang_datagis" value="<?php echo $data['data_lon']; ?>" disabled> 
                            </div>  </div>
                            
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Lattitude</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="latt_datagis" value="<?php echo $data['data_lat']; ?>" disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label class="col-sm-3 control-label col-form-label">Foto <?php echo $data['data_name']; ?></label>
                            <div class="white-box col-md-12" style="background: #dee1e5;">   
                                <?php
                                    $qFotoDatagis = mysqli_query($re_connect,"SELECT * FROM re_gallery WHERE gallery_datagis_id = '$idGet' ");
                                    while($foto = mysqli_fetch_array($qFotoDatagis)){
                                 ?>

                            
                                <div class="col-md-2" style="padding-top: 10px; padding-bottom: 10px; margin-right: 10px; float: left; margin-bottom: 10px; height: 230px; ">
                                    <a class="image-popup-no-margins" href="../uploads/images/datagis/<?php echo $foto['gallery_file']; ?>" >
                                        <img style="height: 165px;" src="../uploads/images/datagis/<?php echo $foto['gallery_file']; ?>" class="img-responsive"  />
                                    </a>
                                    
                                </div>


                                 <?php } ?>
                            </div>  </div>

                           
                                    <div class="offset-sm-3 col-sm-9">
                                        <a href="index.php?page=editdatagis&id=<?php echo $idGet; ?>"><button class="btn btn-info waves-effect waves-light m-t-10">Edit</button></a>

                                         <a href="index.php?page=datagis_galery&id=<?php echo $idGet; ?>"><button class="btn btn-info waves-effect waves-light m-t-10">Edit Foto</button></a>
                                    </div>
                      
        	</div>
        </div>
<?php } ?>
	</div>
</div>         

<script src="../plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="../plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>