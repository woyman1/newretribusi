
<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>
<?php if (!$page) {header("location:index.php=404");} else {}; ?>
<?php if (!$_SESSION['bulan']) {header("location:index.php?page=addtransaksi");} else {}; ?>



<style type="text/css">
    .left{
        float: left;
        margin-left: 50px;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">
                    <div class="text-left"><a href="index.php?page=addtrans-steps-2&id=<?php echo $idGet; ?>"><button type="button" name="create" class="btn btn-info waves-effect waves-light m-t-10">< Kembali</button></a></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">From Add Transaksi</h3>
                            <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>
                            <div class="row line-steps">
                                <div class="col-md-3 column-step  active">
                                    <a href="index.php?page=addtransaksi"><div class="step-number">1</div></a>
                                    <div class="step-title">ID Pelanggan</div>
                                    <div class="step-info">Input Id Pelanggan</div>
                                </div>
                                <div class="col-md-3 column-step active ">
                                    <a href="index.php?page=addtrans-steps-2&id=<?php echo $idGet; ?>"><div class="step-number">2</div></a>
                                    <div class="step-title">Bayar Bulanan</div>
                                    <div class="step-info">Bayar Bulanan</div>
                                </div>
                                <div class="col-md-3 column-step active ">
                                    <div class="step-number">3</div>
                                    <div class="step-title">Add On</div>
                                    <div class="step-info">Add On</div>
                                </div>
                                <div class="col-md-3 column-step ">
                                    <div class="step-number">4</div>
                                    <div class="step-title">SELESAI</div>
                                    <div class="step-info">Selesai</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



        	<?php
                $query = mysqli_query($re_connect, "SELECT * FROM re_costumer INNER JOIN re_type ON re_costumer.cost_type_id = re_type.type_id WHERE cost_id='$idGet' ");
                $data = mysqli_fetch_array($query);
            ?>
        			<form action="../administrator/trnsql/sql.transaksi.php" method="post" class="form-horizontal" enctype="multipart/form-data">

                        <div class="form-body">

                        <div class="panel panel-info col-sm-12">

                            <div class="form-group col-sm-12">
                                <label class="control-label col-form-label m-l-10">Bulan Yang Akan Dibayar</label>

                                <?php

                                function lastBayar($re_connect){
                                  $idCostumer = $_SESSION['id_costumer_bayar'];
                                  $query = "SELECT cost_periode_terakhir
                                  FROM `re_costumer`
                                  WHERE cost_id = $idCostumer";

                                  $result = mysqli_query($re_connect, $query);

                                  if ($result){
                                    $data = mysqli_fetch_array($result);
                                    return $data['cost_periode_terakhir'];

                                  }else{
                                    $data = mysqli_error($re_connect);
                                    return mysqli_error($re_connect);
                                  }
                                }



                                $maxB = $_SESSION['bulan'];

                                $bulan = array(
                                        '01' => 'JANUARI',
                                        '02' => 'FEBRUARI',
                                        '03' => 'MARET',
                                        '04' => 'APRIL',
                                        '05' => 'MEI',
                                        '06' => 'JUNI',
                                        '07' => 'JULI',
                                        '08' => 'AGUSTUS',
                                        '09' => 'SEPTEMBER',
                                        '10' => 'OKTOBER',
                                        '11' => 'NOVEMBER',
                                        '12' => 'DESEMBER',
                                );
                                  for ($i=1; $i <=$maxB ; $i++) {
                                    $months[] = $bulan [date("m", strtotime( lastBayar($re_connect)." $i months"))]." ".date("Y", strtotime( lastBayar($re_connect)." $i months"));
                                  }

                                ?>

                                <ul>
                                    <?php foreach ($months as $month) :
                                      # code...
                                    ?>
                                    <li><?=$month?></li>
                                  <?php endforeach ?>
                                </ul>
                            </div>


                            <div class="form-group col-sm-6">
                                <label class="control-label col-form-label m-l-10">Jumlah Nominal Retribusi </label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="nominal" value="<?= $_SESSION['total'] ?>"  readonly>
                            </div>  </div>

                            <div class="form-group col-sm-6">
                                <label class="control-label col-form-label m-l-10">Tipe Bangunan</label>
                            <div class="col-sm-12">
                                 <p class="form-control-static"> <?php echo $data['type_name'] ?> </p>

                            </div>  </div>

                        </div>

                        <div class="panel panel-info col-sm-12">

                            <div class="form-group input_fields_wrap row">
                                 <label class="control-label col-form-label m-l-10">Apakah ada tambahan?</label>
                                 <div class="col-sm-1 m-t-5"><input type="checkbox" id="enableAdd"><label for="enableAdd">Ya</label></div>
                                    <div class="col-sm-12" >
                                        <button class="add_field_button btn btn-success m-r-10 add" id="addButton" disabled> <i class="fa fa-plus"></i>Add More </button> <p> </p>
                                            <div class="col-sm-7">
                                             <label>Nama Tambahan</label>
                                            <input type="text" name="nama_add[]" class="form-control add" id="addNama" required disabled>
                                            <label>Nominal Tambahan</label>
                                            <input type="text" name="nominal_add[]" class="form-control add" id="price" placeholder="Rp. 999.999.999" required disabled>
                                            </div>
                                    </div>
                            </div>

                        </div>



                       <!-- <textarea class="form-control" rows="10"><?php print_r($_SESSION); ?></textarea> -->
                       <input type="hidden" name="id" value="<?php echo $idGet; ?> ">
                           <div class="form-group m-r-10 text-right">
                                        <button type="submit" name="step3" class="btn btn-info waves-effect waves-light m-t-10">Tambahkan</button>
                            </div>
                        </div>
                    </form>
        	</div>
        </div>

	</div>
</div>

<script src="../assets/curreny-format/jquery.priceformat.js"></script>

<script type="text/javascript">




    (function() {

        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });

    })();

       $(document).ready(function() {

    $('#price').priceFormat({
        prefix: 'Rp. ',
        thousandsSeparator: ',',
        centsLimit: 0,
        clearOnEmpty: true
    });

    $('#enableAdd').change(function() {
        if(this.checked) {
            $("#addNama, #price, #addButton, .add").removeAttr("disabled");
        }else{
            $("#addNama, #price, #addButton, .add").attr("disabled",true);
        }
    });

    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append("<div class='col-sm-7'><label>Nama Tambahan</label><input type='text' name='nama_add[]' class='form-control add' required><label>Nominal Tambahan</label><input type='text' name='nominal_add[]' class='form-control add' id='price-"+ x +"' placeholder='Rp. 999.999.999' required><a href='#' class='remove_field'>Remove</a></div>"); //add input box

                $('#price-'+x).priceFormat({
                prefix: 'Rp. ',
                thousandsSeparator: ',',
                centsLimit: 0,
                clearOnEmpty: true
                });

        }



    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
    }); //end of add field button



</script>
