<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>    
<?php if (!$_GET['idKey']) {header("location:index.php=404");} else {}; ;?>    
<?php if (!$page) {header("location:index.php=404");} else {}; 
include("inc/fungsi.php");
$key = $_GET['idKey'];?>

 <link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
    <!-- animation CSS -->


<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
<?php 
    $mysqli = mysqli_query($re_connect, "SELECT * FROM re_pembayaran INNER JOIN re_costumer ON re_pembayaran.bayar_cost_id = re_costumer.cost_id WHERE bayar_key = '$key' ");
    $data = mysqli_fetch_assoc($mysqli);
 
?> 
        <div class="row">
            <div class="col-md-12 white-box">
                <a href="index.php?page=listpembayaran"><button class="btn btn-info" ><span>< List Pembayaran</span></button></a>
                <div class="text-right">
                    
                    <button class="btn btn-info btn-outline" id="print"><span><i class="fa fa-print"></i> Print Kwitansi</span></button>
                    <button class="btn btn-info" ><span><i class="fa fa-file"></i> Kartu Anggota</span></button>
                </div>
            </div>
        </div>

        <div class="row">
        	<div class="col-lg-12 white-box printableArea">    

                <div class="col-sm-12" style="background: #61e375; padding-top: 5px; padding-bottom: 5px;"><center><h2>BUKTI BAYAR KWITANSI</h2></center></div>
                
                <center>

                <div class="align-center col-md-12">
                    <table><div class="text-center col-sm-12 " style="margin-top: 30px; "><h3><b>KWITANSI</b></h3></div></table>


                    <table width="1000px">
                        <tr>
                            <td width="180">Dinas</font></td>
                            <td width="10"> : </td>
                            <td>Dinas Lingkungan Hidup Kabupaten Merauke</td>
                        </tr>
                        <tr>
                            <td>No. Kwitansi</font></td>
                            <td> : </td>
                            <td><?php echo $key; ?> </td>
                        </tr>
                         <tr>
                            <td>Sudah terima dari </td>
                            <td> : </td>
                            <td><?php echo $data['cost_name']; ?></td>
                        </tr>
                        <tr>
                            <td>Terbilang</td>
                            <td> : </td>
                            <td><?php echo terbilang($data['bayar_total']); ?></td>
                        </tr>
                        <tr>
                            <td valign="top">Buat Pembayaran</td>
                            <td valign="top"> : </td>
                            <td>
                                    <table width="100%">
                                        <tr>
                                            <td colspan="3">Pembayaran Pelanggan Sampah</td>
                                        </tr>
                                        <tr>
                                            <td>No Id Pel.</td>
                                            <td> : </td>
                                            <td> <?php echo $data['cost_id_pel'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama </td>
                                            <td> : </td>
                                            <td> <?php echo $data['cost_name'] ?> </td>
                                        </tr>
                                        <tr>
                                            <td>Alamat </td>
                                            <td> : </td>
                                            <td> <?php echo $data['cost_address'] ?><td>
                                        </tr>
                                        <tr>
                                            <td valign="top">Detail</td>
                                            <td valign="top"> : </td>
                                            <td>
                                                <?php 
                                                    $idType = $data['cost_type_id'];
                                                    $t = mysqli_query($re_connect,"SELECT type_name, type_harga FROM re_type WHERE type_id = '$idType' ");
                                                    $tipe = mysqli_fetch_assoc($t);
                                                 ?>

                                                Tipe Bangunan: <br>
                                                <?= $tipe['type_name']; ?>
                                                <br><br>

                                                Pembayaran Bulan : 
                                                <table width="230px">
                                                    <?php $qu =mysqli_query($re_connect, "SELECT bayar_bulan FROM re_pembayaran WHERE bayar_key = '$key' ");
                                                    while($bulan = mysqli_fetch_assoc($qu)){?>
                                                    <tr>
                                                        <td> - <?php $bln = $bulan['bayar_bulan'];  echo date_month_name($bln); ?></td>
                                                        <td>Rp. <?= number_format($tipe['type_harga'], 0,',','.')?></td>
                                                    </tr>
                                                <?php }?>
                                                </table><br>

                                                Tambahan : <br> 
                                                <table width="230px">
                                                    <?php 
                                                        $tm = mysqli_query($re_connect,"SELECT add_name, add_nominal FROM re_pembayaran_add WHERE add_byr_key = '$key' "); $count = mysqli_num_rows($tm);
                                                        if($count>0){
                                                        while($add = mysqli_fetch_assoc($tm)){?>
                                                        <tr>
                                                            <td>- <?= $add['add_name']; ?></td>
                                                            <td>Rp. <?= number_format($add['add_nominal'], 0,',','.');?></td>
                                                        </tr>                                                            
                                                    <?php }
                                                    }else { echo "Tidak tambahan";} ?>
                                                                                
                                                </table>

                                            </td>
                                        </tr>

                                    </table>
                            </td>
                        </tr>
                        <tr height="50px">
                            <td>Jumlah Total</td>
                            <td> : </td>
                            <td>Rp <?php echo number_format($data['bayar_total'], 0,',','.').",00"; ?>  </td>
                        </tr>
                    </table>

                    <table width="800px">
                         <tr height="50px">
                            <td></td>
                            <td width="300px"></td>
                            <td align="right">
                            <?php $byr_date = explode(" ",$data['bayar_date']);
                                  $tgl = explode("-", $byr_date[0]);
                             ?>
                            Merauke, <?= date_day_name($tgl[2]); ?> <?= $tgl[2]." ".date_month_name($tgl[1]);?>  <?= $tgl[0]; ?></td>
                        </tr>
                        <tr>
                            <td align="center">Yang Menyerahkan</td>
                            <td></td>
                            <td align="right"><center>Yang Menerima <br>Petugas Retribusi Sampah</center></div></td>
                        </tr>
                        <tr height="80px">
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td align="center">.....................................................</td>
                            <td></td>
                            <td align="right">
                                
                                     
                                 
                            <center><b><u><?= $_SESSION['users_name'];?></u></b><br>NIP:<?= $_SESSION['users_nip'];?></center></div></td>
                        </tr>
                    </table>
                
                </div></center>
                

            </div>
        </div>
    

    </div>
</div>

<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
 <script src="../assets/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    <script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>



