

<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
<?php 
    $query = mysqli_query($re_connect, "SELECT * FROM re_type WHERE type_id = '$idGet' ");
    while($data = mysqli_fetch_array($query)){
?>

        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h3 class="box-title m-b-0">Form <?php echo $pageName." ".$data['type_name']; ?> </h3>
                <p class="text-muted m-b-30 font-13"> *Not required to be filled. Maximum size 50kb or 50 pixels and format supported is .png </p>
                

        			  	<form action="../administrator/trnsql/sql.tipe.php" method="post" id="fileForm" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="gantiicon">
                            <input type="hidden" name="id" value="<?php echo $idGet; ?>">

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Icon Before</label>
                            <div class="col-sm-9 m-t-5">
                                <?php if($data['type_icon']){?>
                                    <img src="../uploads/images/type/<?php echo $data['type_icon']; ?>" style="max-height: 50px;">
                                <?php 
                                        }else{
                                
                                echo "<div class='col-sm-9 m-t-5 text-danger'>Icon belum di upload</div>";
                                                                        } 
                                ?>
                            </div>  </div>


                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Icon Tipe </label>
                            <div class="col-sm-9 m-t-5">
                                <input type="file" class="form-control dropify" name="pic" > 
                            </div>  </div>



                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Tambahkan</button>
                                    </div>
                                </div>
                      	</form>
        	</div>
        </div>
        <?php } ?>
	</div>
</div>          

<script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
 <link rel="stylesheet" href="../plugins/bower_components/dropify/dist/css/dropify.min.css">

<script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
