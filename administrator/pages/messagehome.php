<?php if (!$page) {header("location:../index.php?page=404"); exit();} else {}; ;?>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $pageName;?></h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="index.php?page=home">Dashboard</a></li>
                            <li class="active"><?php echo $pageName;?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>
                            <p class="text-muted m-b-30 font-13"> Lengkapi Form Dengan Benar </p>
                            <form class="form-horizontal" action="trnsql/sql.setting.php" method="post">
                                <input type="hidden" name="action" value="homemassage">
								<div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Home Message*</label>
                                    <div class="col-sm-9">
									<div class="form-group">
									<textarea name="home_message" class="textarea_editor form-control" rows="15" placeholder="Enter text ..." ><?php echo $home_message;?></textarea>                                      
									</div>  
                                    </div>
                                </div>
                                
                                
                                <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
              
            </div>
            <!-- /.container-fluid -->
            <?php include("inc/themes/footer.php");?>
        </div>
        <!-- /#page-wrapper -->
    </div>
        <!-- /#wrapper -->
    <!-- start - This is for js and css additional only -->
    <!-- wysuhtml5 Plugin CSS -->
    <link href="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" rel="stylesheet"/>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="../plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.textarea_editor ').wysihtml5();

    });
    </script>
    