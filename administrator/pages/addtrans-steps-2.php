
<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>
<?php if (!$_SESSION['id_costumer_bayar']) {header("location:index.php?page=addtransaksi");} else {}; ;?>
<?php if (!$page) {header("location:index.php=404");} else {};

 ?>


<?php



  function selisihBulan($re_connect){
    $idCostumer = $_SESSION['id_costumer_bayar'];
    $query = "SELECT TIMESTAMPDIFF(MONTH, cost_periode_terakhir, NOW() ) as selisih
    FROM `re_costumer`
    WHERE cost_id = $idCostumer";

    $result = mysqli_query($re_connect, $query);

    if ($result){
      $data = mysqli_fetch_array($result);
      return $data['selisih'];

    }else{
      $data = mysqli_error($re_connect);
      return mysqli_error($re_connect);
    }
  }
?>


<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">
                    <div class="text-left"><a href="index.php?page=addtransaksi"><button type="button" class="btn btn-info waves-effect waves-light m-t-10">< Kembali</button></a></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">From Add Transaksi</h3>
                            <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>
                            <div class="row line-steps">
                                <div class="col-md-3 column-step  active">
                                    <a href="index.php?page=addtransaksi"><div class="step-number">1</div></a>
                                    <div class="step-title">ID Pelanggan</div>
                                    <div class="step-info">Input Id Pelanggan</div>
                                </div>
                                <div class="col-md-3 column-step active ">
                                    <div class="step-number">2</div>
                                    <div class="step-title">Bayar Bulanan</div>
                                    <div class="step-info">Bayar Bulanan</div>
                                </div>
                                <div class="col-md-3 column-step ">
                                    <div class="step-number">3</div>
                                    <div class="step-title">Add On</div>
                                    <div class="step-info">Add On</div>
                                </div>
                                <div class="col-md-3 column-step ">
                                    <div class="step-number">4</div>
                                    <div class="step-title">SELESAI</div>
                                    <div class="step-info">Selesai</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php
                $query = mysqli_query($re_connect, "SELECT * FROM re_costumer INNER JOIN re_type ON re_costumer.cost_type_id = re_type.type_id WHERE cost_id='$idGet' ");
                $data = mysqli_fetch_array($query);

            ?>




        	</div>
        </div>


        <div class="row">
          <div class="col-lg-12 white-box">


            <div class="row">
              <div class="col-md-4">
                <label for="Nama">Nama Pelanggan</label>
              </div>
              <div class="col-md-4">
                <?= $data['cost_name'] ?>
              </div>
            </div>

            <?php
              if (selisihBulan($re_connect) > 0) :

             ?>

            <div class="row">
              <div class="col-md-4">
                <label for="Nama">Jumlah periode yang harus dibayar hingga saat ini</label>
              </div>
              <div class="col-md-4">
                <?=  abs(selisihBulan($re_connect))?> Bulan <span class="label label-warning">tunggakan</span>
              </div>
            </div>

          <?php else : ?>
            <div class="row">
              <div class="col-md-4">
                <label for="Nama">Anda telah membayar untuk :</label>
              </div>
              <div class="col-md-4">
                <?=  abs(selisihBulan($re_connect))?> Bulan ke depan <span class="label label-info">sudah bayar di muka</span>
              </div>
            </div>

          <?php endif; ?>

            <form class="form-horizontal" action="../administrator/trnsql/sql.transaksi.php" method="post">
              <input type="hidden" name="id" value="<?php echo $idGet; ?>">
              <input type="hidden" name="total" id="nominal_bayar" value="" required>
              <div class="form-group">
                <div class="col-md-4">
                  <label for="Nama">Periode yang akan dibayar </label>
                </div>
                <div class="col-md-4">
                  <input type="number" id="periodeBayar" class="form-control" name="inputPeriode" value="1" min="1">
                </div>
                <div class="col-md-4">
                    <span style="font-size:200%">Bulan</span>
                    <button id="btnPlus" class="btn btn-info waves-effect waves-light m-r-20" type="button" name="button"><i class=" fa fa-plus"></i></button>
                    <button id="btnMin" class="btn btn-info waves-effect waves-light m-r-20" type="button" name="button"><i class=" fa fa-minus"></i></button>
                </div>

              </div>

              <div class="form-group">
                   <button type="submit" id="hitung" name="step2" class="btn btn-info waves-effect waves-light m-r-20">Next </button>
              </div>

              <div id="alertwarning" class="alert alert-warning" style="color:black"> <i class="fa  fa-warning"></i> Jumlah periode yang harus dibayar hingga saat ini adalah <?=  abs(selisihBulan($re_connect))?> bulan </div>

            </form>

          </div>

        </div>

	</div>
</div>
<link rel="stylesheet" href="../assets/autocomplate/jquery-ui.css">
<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<script src="../plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="../plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>

<script type="text/javascript">
$('#alertwarning').hide();

    $( document ).ready(function() {

      $('#btnPlus').click(function(event) {
        var valueBulan = parseInt($('#periodeBayar').val());
        $('#periodeBayar').val(valueBulan + 1)

        var valueBulan = $('#periodeBayar').val();
        var tunggakan = <?=  selisihBulan($re_connect)?>;

        if (valueBulan < tunggakan) {
          $('#alertwarning').show();
        }else{
          $('#alertwarning').hide();
        }
      });

      $('#btnMin').click(function(event) {
        var valueBulan = parseInt($('#periodeBayar').val());

        if (valueBulan>0){
          $('#periodeBayar').val(valueBulan - 1)

          var valueBulan = $('#periodeBayar').val();
          var tunggakan = <?=  selisihBulan($re_connect)?>;

          if (valueBulan < tunggakan) {
            $('#alertwarning').show();
          }else{
            $('#alertwarning').hide();
          }
        }

      });



        $("#hitung").click(function(){ //untuk menghitung checkbox yang telah dicek

                var total = $("#periodeBayar").val();
                $("#nominal_bayar").val('Rp. ' + total * <?= $data['type_harga']; ?>);
        });

        $('#periodeBayar').on('change, keyup, click', function(event) {
          var valueBulan = $('#periodeBayar').val();
          var tunggakan = <?=  selisihBulan($re_connect)?>;

          if (valueBulan < tunggakan) {
            $('#alertwarning').show();
          }else{
            $('#alertwarning').hide();
          }

        });


            $('#pilih').change(function() { //fungsi untuk disable dan enable checkbox
                <?php $i = 0; ?>
                if(this.checked) {
                    <?php if($_SESSION['bulan'])
                            {
                                $r = count($_SESSION['bulan']);?>
                                $("<?php for($w=0; $w<$r; $w++){ echo"#bulan-".$w.","; } ?>#bulan-"+<?= $r; ?>).removeAttr("disabled");
                            <?php }else{ ?>
                                $("#bulan-" + <?= $i; ?>).removeAttr("disabled");
                           <?php } ?>

                }else{

                    <?php if($_SESSION['bulan']){

                                $r = count($_SESSION['bulan']);?>
                                $("<?php for($w=0; $w<$r; $w++){ echo"#bulan-".$w.","; } ?>#bulan-"+<?= $r; ?>).attr("disabled", true);

                    <?php }else{ ?>

                                $(
                                   "<?php
                                    for($x = $i; $x <= 11; $x++){
                                        echo "#bulan-".$x.",";
                                    }
                                   ?>#bulan-12 "
                                 ).attr("disabled",true);

                                $("<?php
                                    for($x = $i; $x <= 11; $x++){
                                        echo "#bulan-".$x.",";
                                    }
                                   ?>#bulan-12 "
                                  ).attr('checked', false);
                    <?php } ?>



                }
            });


        <?php
            for($a = 0; $a<12; $a++){ //untuk membuat perulangan checkbox
         ?>
            $('#bulan-'+<?= $a ?>).change(function() {
                <?php $i = $a+1; ?>
                if(this.checked) {
                    $("#bulan-" + <?= $i; ?>).removeAttr("disabled");
                }else{
                    $(
                       "<?php
                        for($x = $i; $x <= 11; $x++){
                            echo "#bulan-".$x.",";
                        }
                       ?>#bulan-12 "
                     ).attr("disabled",true);

                    $("<?php
                        for($x = $i; $x <= 11; $x++){
                            echo "#bulan-".$x.",";
                        }
                       ?>#bulan-12 "
                      ).attr('checked', false);
                }
            });

        <?php } ?>

    });

</script>


    <script src="../assets/autocomplate/jquery-ui.js"></script>
