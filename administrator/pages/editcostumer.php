
<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>
<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?php echo $pageName;?></h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
<?php
    $query = mysqli_query($re_connect, "SELECT * FROM re_costumer WHERE cost_id = '$idGet' ");
    while($data = mysqli_fetch_array($query)){
      $lat = $data['cost_latt'];
      $lon = $data['cost_lang'];
      $nameCost = $data['cost_name'];
?>

        <div class="row">
            <div class="col-lg-12 white-box">
                <h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>


                        <form action="../administrator/trnsql/sql.costumer.php" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="update" >
                            <input type="hidden" name="id" value="<?php echo $idGet; ?>" >
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">ID Costumer</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_cost" value="<?php echo $data['cost_id_pel']; ?>"  disabled>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_cost" value="<?php echo $data['cost_name']; ?>"  required>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea name="alamat_cost" class="form-control" rows="4" required><?php echo $data['cost_address']; ?> </textarea>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Provinsi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="prov_cost" value="<?php echo $data['cost_prov']; ?>" required>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kabupaten</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="kab_cost" value="<?php echo $data['cost_kab']; ?>" required>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kecamatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="kec_cost" value="<?php echo $data['cost_kec']; ?>" required>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Desa </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="desaid_cost" required>
                                <?php
                                    $kd_desa = $data['cost_desa_id'];
                                    $query = mysqli_query($re_connect, "SELECT desa_name FROM re_desa WHERE desa_id = '$kd_desa' ");
                                    while($tipe = mysqli_fetch_array($query)){
                                 ?>
                                    <option value="<?php echo $data['cost_desa_id']; ?>"><?php echo $tipe['desa_name']; ?></option>
                                <?php } ?>

                               <?php
                                    $query = mysqli_query($re_connect, "SELECT * FROM re_desa");
                                    while($tipe = mysqli_fetch_array($query)){
                                 ?>
                                    <option value="<?php echo $tipe['desa_id']; ?>"><?php echo $tipe['desa_name']; ?></option>
                                <?php } ?>
                                </select>
                            </div>  </div>

                             <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Type </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="typeid_cost" required>
                                <?php
                                    $kd_tipe = $data['cost_type_id'];
                                    $query = mysqli_query($re_connect, "SELECT type_name, type_code FROM re_type WHERE type_id = '$kd_tipe' ");
                                    $tipe = mysqli_fetch_array($query);
                                 ?>
                                    <option value="<?php echo $data['cost_type_id']; ?>"><?php echo $tipe['type_name']." - ".$tipe['type_code']; ?></option>

                                <?php
                                    $query = mysqli_query($re_connect, "SELECT * FROM re_type");
                                    while($tipe = mysqli_fetch_array($query)){
                                 ?>
                                    <option value="<?php echo $tipe['type_id']; ?>"><?php echo $tipe['type_name']; ?></option>
                                <?php } ?>

                                </select>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tempat Lahir</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="tmplahir_cost" value="<?php echo $data['cost_tmp_lahir']; ?>" required>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tanggal Lahir</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" name="tgllahir_cost" value="<?php echo $data['cost_tgl_lahir']; ?>" required>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Pengangkut</label>
                            <div class="col-sm-9" >
                                <select name="operator" class="form-control" required>
                                    <?php
                                    $idUser= $data['cost_user_id'];
                                    $qp = mysqli_query($re_connect,"SELECT re_user_name FROM re_users WHERE re_user_id = '$idUser' ");
                                    while($pengangkut = mysqli_fetch_assoc($qp)){ ?>
                                  <option value="<?php echo $data['cost_user_id']; ?>"> <?php echo $pengangkut['re_user_name']; ?></option>

                                    <?php }

                                      $qop = mysqli_query($re_connect, "SELECT re_user_id, re_user_name FROM re_users WHERE re_user_level = 5 ");
                                      while($op = mysqli_fetch_array($qop)){?>
                                          <option value="<?php echo $op['re_user_id']; ?>"><?php echo $op['re_user_name']; ?> </option>
                                      <?php } ?>

                                </select>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Jadwal Angkut Sampah</label>
                            <div class="col-sm-9" >
                                <select name="jadwal" class="form-control" required>
                                <?php $jdwl = $data['cost_jadwal_id'];
                                      $qj = mysqli_query($re_connect, "SELECT jadwal_name FROM re_jdw_sampah WHERE jadwal_id = '$jdwl' ") or die(mysqli_error($re_connect));
                                      while($jadwal = mysqli_fetch_assoc($qj)){  ?>

                                            <option value="<?php echo $data['cost_jadwal_id']; ?>"> <?php echo $jadwal['jadwal_name']; ?></option>

                                    <?php }

                                      $qop = mysqli_query($re_connect, "SELECT * FROM re_jdw_sampah ");
                                      while($op = mysqli_fetch_array($qop)){?>
                                          <option value="<?php echo $op['jadwal_id']; ?>"><?php echo $op['jadwal_name']; ?> </option>
                                      <?php } ?>

                                </select>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Ambil Coordinate</label>
                            <div class="col-sm-9">
                              <input id="pac-input" class="controls form-control" type="text" placeholder="Cari tempat...">
                              <div id="map" style="height:400px;border: 2px solid #3872ac; margin-bottom:0px;"></div>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Longitude</label>
                            <div class="col-sm-9">
                                <input id="lon" type="text" class="form-control" name="long" value="<?php echo $data['cost_lang']; ?>" required>
                            </div>  </div>

                             <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Latitude</label>
                            <div class="col-sm-9">
                                <input id="lat" type="text" class="form-control" name="lat" value="<?php echo $data['cost_latt']; ?>" required>
                            </div>  </div>

                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Simpan</button>
                                    </div>
                                </div>

            </div>
        </div>
        <?php } ?>

    </div>
</div>

<script>
  function initMap() {
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -8.4991, lng: 140.404984},
      zoom: 13,
      fullscreenControl: false,
      mapTypeControl: false
    });

    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    function markerCoords(markerobject){
      google.maps.event.addListener(markerobject, 'dragend', function(evt){
          console.log("lat: "+evt.latLng.lat().toFixed(3)+" lon: "+evt.latLng.lng().toFixed(3));
      });

      google.maps.event.addListener(markerobject, 'drag', function(evt){
          console.log("marker is being dragged");
          console.log("lat: "+evt.latLng.lat().toFixed(3)+" lon: "+evt.latLng.lng().toFixed(3));

          $('#lat').val(evt.latLng.lat().toFixed(6));
          $('#lon').val(evt.latLng.lng().toFixed(6));
      });
    }

    <?php
      if (is_numeric($lat) && is_numeric($lon)):
     ?>

     var lon = <?=$lon?>;
     var lat = <?=$lat?>;

     <?php else : ?>


     var lon = 140.404984;
     var lat = -8.4991;

     <?php endif ?>

    var marker = new google.maps.Marker({
      position: {lat: lat, lng: lon},
      map: map,
      title: '<?=$nameCost?>',
      draggable:true,
    });

    markerCoords(marker);

    google.maps.event.addListener(map, 'click', function(evt) {
       console.log("lat: "+evt.latLng.lat().toFixed(3)+" lon: "+evt.latLng.lng().toFixed(3));
        var latlng = new google.maps.LatLng(evt.latLng.lat().toFixed(6), evt.latLng.lng().toFixed(6));
        marker.setPosition(latlng);
        $('#lat').val(evt.latLng.lat().toFixed(6));
        $('#lon').val(evt.latLng.lng().toFixed(6));
        });

      searchBox.addListener('places_changed', function() {
       var places = searchBox.getPlaces();

       if (places.length == 0) {
         return;
       }

       places.forEach(function(place) {
        marker.setPosition(place.geometry.location);
        $('#lat').val(place.geometry.location.lat().toFixed(6));
        $('#lon').val(place.geometry.location.lng().toFixed(6));
        map.setCenter(marker.getPosition());
       });

     });

     $("#pac-input").click(function(event) {
       $(this).val('');
     });

  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwx2LQQM6zmHj7oqZfI_oDrAuuXXN3tBk&callback=initMap&libraries=places"></script>
