<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h2>Daftar <?php echo $pageName;?></h2>
        		      
                    <div class="table-responsive">
                    <h3 class="box-title m-b-0">Data Export</h3>
                            
                        <table id="myTable" class="table table-striped">                
                            <thead>
                                <tr>
                                    <th>Nama Kecamatan</th>
                                    <th>Kode Kecamatan</th>
                                    <th>Action</th>
                      
                                </tr>
                            </thead>
                                    <?php 
                                        $query = mysqli_query($re_connect, "SELECT * FROM re_kecamatan ORDER BY idKec DESC");
                                        while($data = mysqli_fetch_array($query)){
                                    ?>
                                <tr>
                                    <td><?php echo $data['namaKec']; ?></td>
                                    <td><?php echo $data['kodeKec']; ?></td>
                                    <td>
                                         <a href="index.php?page=viewkec&id=<?php echo $data['idKec'];?>" data-toggle="tooltip" data-original-title="Lihat Kecamatan <?php echo$data['namaKec']; ?>"> <i class="fa fa-search text-info m-r-10"></i></a>
                                         <?php if ($levelUser == 1 || $levelUser ==2) : ?>
                                         
                                         <a href="index.php?page=editkec&id=<?php echo $data['idKec'];?>" data-toggle="tooltip" data-original-title="Edit Kecamatan <?php echo$data['namaKec']; ?>"> <i class="fa fa-pencil text-success m-r-10"></i></a>
                                        <a href="trnsql/sql.kec.php?action=delete&id=<?php echo $data['idKec'];?>" data-toggle="tooltip" data-original-title="Delete Kecamatan <?php echo$data['namaKec']; ?>"> <i class="fa fa-close text-danger m-r-10"></i></a>
<?php endif?>
                                    </td>
                                </tr>
                               <?php } ?>
                            </thead>
                        </table>
                    </div>
        			  	
        	</div>
        </div>

	</div>
</div>  


<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <!-- start - This is for export functionality only -->
<script src="../assets/js/dataTables.buttons.min.js"></script>       

<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                    [2, 'desc']
            ],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
          });
        
    });
  
    </script>

<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>