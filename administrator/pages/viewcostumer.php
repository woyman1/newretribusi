<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>
<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol> 
            </div>
        </div>
<?php 
    $query = mysqli_query($re_connect, "SELECT * FROM re_costumer WHERE cost_id = '$idGet' ");
    while($data = mysqli_fetch_array($query)){
?>

        <div class="row">
        	<div class="col-lg-12 white-box">

                <div class="col-sm-12 m-b-15">
                        <a href="index.php?page=listcostumer"><button class="btn btn-info"> < List Costumer</button></a>
                    </div>
        		<h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">ID Costumer</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_cost" value="<?php echo $data['cost_id_pel']; ?>"  disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_cost" value="<?php echo $data['cost_name']; ?>"  disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea name="alamat_cost" class="form-control" rows="4" disabled><?php echo $data['cost_address']; ?> </textarea>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Provinsi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="prov_cost" value="<?php echo $data['cost_prov']; ?>" disabled>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kabupaten</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="kab_cost" value="<?php echo $data['cost_kab']; ?>" disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kecamatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="kec_cost" value="<?php echo $data['cost_kec']; ?>" disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kelurahan/Desa </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="desaid_cost" disabled> 
                                <?php 
                                    $kd_desa = $data['cost_desa_id'];
                                    $query = mysqli_query($re_connect, "SELECT desa_name FROM re_desa WHERE desa_id = '$kd_desa' ");
                                    while($tipe = mysqli_fetch_array($query)){
                                 ?>
                                    <option value="<?php echo $data['cost_desa_id']; ?>"><?php echo $tipe['desa_name']; ?></option>
                                <?php } ?>
                                </select>
                            </div>  </div>

                             <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Type </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="typeid_cost" disabled>   
                                <?php 
                                    $kd_tipe = $data['cost_type_id'];
                                    $query = mysqli_query($re_connect, "SELECT type_name, type_code FROM re_type WHERE type_id = '$kd_tipe' ");
                                    $tipe = mysqli_fetch_array($query);
                                 ?>
                                    <option value="<?php echo $data['cost_type_id']; ?>"><?php echo $tipe['type_name']." - ".$tipe['type_code']; ?></option>
                                </select>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tempat Lahir</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="tmplahir_cost" value="<?php echo $data['cost_tmp_lahir']; ?>" disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tanggal Lahir</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" name="tgllahir_cost" value="<?php echo $data['cost_tgl_lahir']; ?>"  disabled> 
                            </div>  </div>

                             <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Pengangkut</label>
                            <div class="col-sm-9" >
                                <select name="operator" class="form-control" disabled>
                                    <?php 
                                    $idUser= $data['cost_user_id'];
                                    $qp = mysqli_query($re_connect,"SELECT re_user_name FROM re_users WHERE re_user_id = '$idUser' ");
                                    while($pengangkut = mysqli_fetch_assoc($qp)){ ?>
                                  <option value="<?php echo $data['cost_user_id']; ?>"> <?php echo $pengangkut['re_user_name']; ?></option>

                                    <?php }?>

                                </select>
                            </div>  </div>

                             <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Jadwal Angkut Sampah</label>
                            <div class="col-sm-9" >
                                <select name="operator" class="form-control" disabled>
                                <?php $jdwl = $data['cost_jadwal_id'];
                                      $qj = mysqli_query($re_connect, "SELECT jadwal_name FROM re_jdw_sampah WHERE jadwal_id = '$jdwl' ") or die(mysqli_error($re_connect));
                                      while($jadwal = mysqli_fetch_assoc($qj)){  ?>
                            
                                            <option value="<?php echo $data['cost_jadwal_id']; ?>"> <?php echo $jadwal['jadwal_name']; ?></option>

                                    <?php } ?>

                                </select>
                            </div>  </div>

                             <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Longitude</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="long" value="<?php echo $data['cost_lang']; ?>" disabled> 
                            </div>  </div>

                             <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Latitude</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="lat" value="<?php echo $data['cost_latt']; ?>" disabled> 
                            </div>  </div>

                             <div class="panel panel-info">
                                <div class="panel-heading"> Arsip dan Foto
                                </div>
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        
                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <div class="white-box">
                                                    <h3 class="box-title">Foto Formal 3x4</h3>
                                                    <?php 
                                                        if($data['cost_foto']){
                                                       ?>                                   
                                                        <div class="col-sm-12">
                                                            <a class="image-popup-no-margins" href="../uploads/images/arsip/<?php echo $data['cost_foto']; ?>" title="Foto 3x4">
                                                                <img src="../uploads/images/arsip/<?php echo $data['cost_foto']; ?>" class="img-responsive" />
                                                            </a>
                                                           
                                                        </div>
                                                    <?php }else {?>
                                                            <div class="col-sm-12">
                                                                <div class="panel-body">
                                                                    <div class="text-danger "><b>Belum Upload</b></div>
                                                                </div>
                                                            </div>
                                                     <?php }?>                                         
                                                    
                                                </div> 
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <div class="white-box">
                                                    <h3 class="box-title">Foto Rumah/Lokasi</h3>
                                                      <?php 
                                                        if($data['cost_sk']){
                                                       ?>                                
                                                        <div class="col-sm-12">
                                                            <a class="image-popup-no-margins" href="../uploads/images/arsip/<?php echo $data['cost_sk']; ?>" title="Surat Keterangan">
                                                                <img src="../uploads/images/arsip/<?php echo $data['cost_sk']; ?>" class="img-responsive" />
                                                            </a>
                                                           
                                                        </div>
                                                    <?php }else {?>
                                                            <div class="col-sm-12">
                                                                <div class="panel-body">
                                                                    <div class="text-danger"><b>Belum Upload</b></div>
                                                                </div>
                                                            </div>
                                                    <?php }?>                            
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <div class="white-box">
                                                    <h3 class="box-title">Denah</h3>
                                                     <?php 
                                                     if($data['cost_sk']){
                                                     ?>      
                                                       <div class="col-sm-12">
                                                            <a class="image-popup-no-margins" href="../uploads/images/arsip/<?php echo $data['cost_denah']; ?>" title="Denah">
                                                                <img src="../uploads/images/arsip/<?php echo $data['cost_denah']; ?>" class="img-responsive" />
                                                            </a>
                                                           
                                                        </div>
                                                     <?php }else {?>
                                                            <div class="col-sm-12">
                                                                <div class="panel-body">
                                                                    <div class="text-danger"><b>Belum Upload</b></div>
                                                                </div>
                                                            </div>
                                                     <?php }?>            

                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <div class="white-box">
                                                    <h3 class="box-title">KTP</h3>
                                                    <?php 
                                                     if($data['cost_ktp']){
                                                     ?>   
                                                       <div class="col-sm-12">
                                                            <a class="image-popup-no-margins" href="../uploads/images/arsip/<?php echo $data['cost_ktp']; ?>" title="Kartu Tanda Penduduk">
                                                                <img src="../uploads/images/arsip/<?php echo $data['cost_ktp']; ?>" class="img-responsive" />
                                                            </a>
                                                            <a href="trnsql/sql.costumer.php?action=delete_arsip&arsip=ktp&id=<?php echo $idGet; ?>"><button class="btn btn-info waves-effect waves-light m-t-10">Delete</button></a>
                                                        </div>
                                                    <?php }else {?>
                                                            <div class="col-sm-12">
                                                                <div class="panel-body">
                                                                    <div class="text-danger"><b>Belum Upload</b></div>
                                                                </div>
                                                            </div>
                                                     <?php }?>         

                                                </div>
                                            </div>

                                    </div>
                                </div>
                            </div>

                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <a href="index.php?page=editcostumer&id=<?php echo $data['cost_id']; ?>"> <button class="btn btn-info waves-effect waves-light m-t-10">Edit</button></a>
                                        <a href="index.php?page=upload_arsip_cost&id=<?php echo $data['cost_id'];?>"> <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Edit Gambar</button></a>
                                    </div>
                                </div>
                  
        	</div>
        </div>
        <?php } ?>

	</div>
</div>          

<script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="../plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="../plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>

