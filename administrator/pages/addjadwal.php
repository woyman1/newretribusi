<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>
                <p class="text-muted m-b-30 font-13"> *Pilih Jadwal Hari Pengambilan Sampah </p>
        		
        			  	<form action="../administrator/trnsql/sql.jadwal_angkut.php" method="post" class="form-horizontal">

                            <div class="form-group row">
                                <input type="radio" id="allDay" name="jadwalPilihan" class="m-t-10" value="SetiapHari" required><label for="allDay" class="control-label col-form-label m-l-15">Setiap Hari</label>
                            <div class="col-sm-9">
                               <input type="hidden" name="jadwal_value" value="Setiap Hari">
                            </div>  </div>

                            <div class="form-group row">
                               <input type="radio" id="2day" name="jadwalPilihan" class="m-t-10" value="2Hari" ><label for="2day" class="control-label col-form-label m-l-15">2 Kali Seminggu</label>
                            <div class="col-sm-12 row">
                              <div class="col-sm-4">
                                <select name="jadwal_value" class="form-control 2hari" disabled required>
                                    <option value="" >-- Pilih Hari Pertama ---</option>
                                    <option value="Senin">Senin</option>
                                    <option value="Selasa">Selasa</option>
                                    <option value="Rabu">Rabu</option>
                                    <option value="Kamis">Kamis</option>
                                    <option value="Jumat">Jumat</option>
                                    <option value="Sabtu">Sabtu</option>
                                    <option value="Minggu">Minggu</option>
                                </select>
                              </div>

                              <div class="col-sm-4">
                               <select name="jadwal_value2" class="form-control 2hari"  disabled required>
                                    <option value="">-- Pilih Hari Kedua ---</option>
                                    <option value="Senin">Senin</option>
                                    <option value="Selasa">Selasa</option>
                                    <option value="Rabu">Rabu</option>
                                    <option value="Kamis">Kamis</option>
                                    <option value="Jumat">Jumat</option>
                                    <option value="Sabtu">Sabtu</option>
                                    <option value="Minggu">Minggu</option>
                                </select>
                              </div>
                            </div>  </div>

                            <div class="form-group row">
                               <input type="radio" id="1day" name="jadwalPilihan" class="m-t-10" value="SatuHari"><label for="1day" class="control-label col-form-label m-l-15">1 Kali Seminggu</label>
                            <div class="col-sm-12">
                              <div class="col-sm-4">
                                <select name="jadwal_value" class="form-control" id="hari" disabled required>
                                    <option value="">-- Pilih Hari ---</option>
                                    <option value="Senin">Senin</option>
                                    <option value="Selasa">Selasa</option>
                                    <option value="Rabu">Rabu</option>
                                    <option value="Kamis">Kamis</option>
                                    <option value="Jumat">Jumat</option>
                                    <option value="Sabtu">Sabtu</option>
                                    <option value="Minggu">Minggu</option>
                                </select>
                              </div>
                            </div>  </div>

                           <div class="form-group m-b-0">
                                    <div class="col-sm-12">
                                        <button type="submit" name="create" class="btn btn-info waves-effect waves-light pull-right m-t-10">Tambahkan</button>
                                    </div>
                                </div>
                      	</form>
        	</div>
        </div>

	</div>
</div>          

<script type="text/javascript">

$(document).ready(function() { 

    // $('#2day').change(function(){
    //     if(this.checked) {
    //         $(".2hari").removeAttr("disabled");
    //     }else{
    //         $(".2hari").attr("disabled",true);
    //     }
    // });

    // $('#1day').change(function(){
    //     if(this.checked) {
    //         $("#hari").removeAttr("disabled");
    //     }else{
    //         $("#hari").attr("disabled",true);
    //     }
    // });

    $('input[type="radio"][name="jadwalPilihan"]').change(function(){
        if (this.value == '2Hari'){
            $(".2hari").removeAttr("disabled");
        }else{
            $(".2hari").attr("disabled",true);
        }

        if (this.value == 'SatuHari'){
            $("#hari").removeAttr("disabled");
        }else{
            $("#hari").attr("disabled",true);
        }
    });

});


</script>