<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

  <?php
    $q_setting = "SELECT setting_about_page FROM re_settingsite";
    $r_setting = $re_connect->query($q_setting);

    if ($r_setting->num_rows >0) {
        $row = $r_setting->fetch_assoc();
    }
  ?>

<div id="page-wrapper">
  <div class="container-fluid">

    <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">Pengaturan About Page</h4>
    </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
              <ol class="breadcrumb">
        <li><a href="index.php?page=home">Dashboard</a></li>
                  <li class="active"><?php echo $pageName;?></li>
              </ol>
          </div>
      </div>

      <div class="row">
        <div class="col-lg-12 white-box">
          <div class="form-group">
            <textarea class="textarea_editor form-control" id="aboutPage" rows="15" placeholder="Enter text ..."><?php echo $row['setting_about_page'] ?></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 white-box">
          <div class="form-group">

            <button class="btn btn-info btn-outline tomSave" id="tomSave" data-toggle="tooltip" data-original-title="Save"
              type="button" editId="doser" id="save_doser"><i class="fa fa-save"></i> Save</button>
          </div>
        </div>
      </div>


  </div>
</div>

<script src="../plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
   <script src="../plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
   <script src="../plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
   <script>
       $(document).ready(function() {

           $('.textarea_editor').wysihtml5();

           $('#tomSave').click(function(event) {
            var value = $('#aboutPage').val();
            var column = "setting_about_page";

            $.ajax({
              url: '../administrator/trnsql/sql.setting_admin.php',
              type: 'POST',
              data: {
                  column:column,
                  value:value
              },
            })
            .done(function() {
              $.toast({
                    heading: 'Data disimpan',
                    text: 'Data Berhasil Disimpan',
                    position: 'mid-center',
                    loaderBg: 'white',
                    icon: 'info',
                    hideAfter: 1000,
                    stack: 6
              });
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });

           });

       });
       </script>
