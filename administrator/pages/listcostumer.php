<link href="../plugins/select2/css/select2.min.css" rel="stylesheet">
    <?php if (!$page) {header("location:index.php=404");} else {}; 

    $query_keluarahan = "SELECT * FROM re_desa ORDER BY desa_id desc ";

    $qkel = mysqli_query($re_connect, $query_keluarahan);
    

    ?>

    <div id="page-wrapper">
        <div class="container-fluid">
        	<div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title"><?php echo $pageName;?></h4>
    			</div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
    					<li><a href="index.php?page=home">Dashboard</a></li>
                        <li class="active"><?php echo $pageName;?></li>
                    </ol>
                </div>
            </div>


            <div class="row">
            	<div class="col-lg-12 white-box">

            	     <div class="col-sm-12"> 
                        
                            <div class="pull-right row col-5">
                                <div class="m-t-10 col-6">
                                    <?php if($_SESSION['users_level'] == 1){ ?>
                                    <form id="sort_by_desa">
                                       <div class="form-group"> 
                                            <select class="form-control js-example-basic-single js-states col-md-3" name="layers" id="sort_desa">
                                                <option></option>
                                                <option value="0">Semua Kelurahan</option>
                                              <?php while($desa = mysqli_fetch_assoc($qkel)){ ?>  
                                                <option value="<?= $desa['desa_id']; ?>"> <?= $desa['desa_name']; ?> </option>
                                              <?php } ?>           
                                            </select>
                                          </div>
                                    </form>
                                    <?php } ?>
                                </div>

                                <div class="col-6 m-r-0">
                                    <a href="index.php?page=addcostumer"><button type="button" class="btn btn-info waves-effect waves-light m-t-10" ><i class="icon-plus m-t-5"> </i>Tambah Costumer Baru</button></a> 
                                </div>
                            </div>
                            
                     </div>


            		<h2>Daftar <?php echo $pageName;?></h2>
                       <div id="content">  
                        <div class="table-responsive" id="oldTable">
                            <table id="myTable" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID Costumer</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>Desa/Kelurahan</th>
                                        <th>Tipe</th>
                                        <th>Status</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                        <?php

                                            if ($levelUser != 5)
                                              $query = mysqli_query($re_connect, "SELECT * FROM re_costumer ORDER BY cost_date DESC");
                                            else
                                              $query = mysqli_query($re_connect, "SELECT * FROM re_costumer WHERE cost_user_id = $_SESSION[users_id] ORDER BY cost_date DESC");

                                              if (!$query) echo mysqli_error($re_connect);

                                            while($data = mysqli_fetch_array($query)){
                                        ?>
                                    <tr>
                                        <td><?php echo $data['cost_id_pel']; ?></td>
                                        <td><?php echo $data['cost_name']; ?></td>
                                        <td> <?php echo $data['cost_address']; ?> </td>
                                        <td>
                                        <?php
                                              $tmp_desa = $data['cost_desa_id'];
                                              $qdesa = mysqli_query($re_connect, "SELECT desa_name FROM re_desa WHERE desa_id = '$tmp_desa' ");
                                              $desa = mysqli_fetch_assoc($qdesa); echo $desa['desa_name'];
                                         ?>
                                            </td>
                                        <td>
                                        <?php
                                            $tmp_type = $data['cost_type_id'];

                                              $tmp_desa = $data['cost_desa_id'];
                                              $qt = mysqli_query($re_connect, "SELECT type_name FROM re_type WHERE type_id = '$tmp_type' ");
                                              $type = mysqli_fetch_assoc($qt); echo $type['type_name'];
                                        ?></td>
                                        <td><?php echo $data['cost_status']; ?></td>
                                        <td>

                                        <a href="index.php?page=viewcostumer&id=<?php echo $data['cost_id'];?>" data-toggle="tooltip" data-original-title="Lihat Costumer <?php echo $data['cost_name']; ?>"> <i class="fa fa-search text-info m-r-10"></i></a>

                                        <?php if ($levelUser == 1 || $levelUser ==2) : ?>

                                        <a href="index.php?page=editcostumer&id=<?php echo $data['cost_id'];?>" data-toggle="tooltip" data-original-title="Edit <?php echo $data['cost_name']; ?>"> <i class="fa fa-pencil text-success m-r-10"></i></a>

                                        <a href="index.php?page=upload_arsip_cost&id=<?php echo $data['cost_id'];?>" data-toggle="tooltip" data-original-title="Upload Arsip <?php echo $data['cost_name']; ?>"> <i class="fa fa-photo text-info m-r-10"></i></a>

                                         <a href="index.php?page=cetak_kartu&id=<?php echo $data['cost_id'];?>" data-toggle="tooltip" data-original-title="Cetak Kartu Milik <?php echo $data['cost_name']; ?>"> <i class="fa fa-file-text text-success m-r-10"></i></a>


                                        <a href="trnsql/sql.costumer.php?action=delete&id=<?php echo $data['cost_id'];?>" data-toggle="tooltip" data-original-title="Delete <?php echo $data['cost_name']; ?>"> <i class="fa fa-close text-danger m-r-10"></i></a>

                                        <?php endif ?>

                                            </td>

                                    </tr>
                                   <?php } ?>
                                </thead>
                            </table>
                        </div>
                    </div>

            	</div>
            </div>

    	</div>
    </div>


    <script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

        <!-- start - This is for export functionality only -->
    <script src="../assets/js/dataTables.buttons.min.js"></script>
    <script src="../plugins/select2/js/select2.full.min.js"></script>

    <script>
       
$(document).ready(function() {

    $('.js-example-basic-single').select2({
        placeholder: "Pilih kelurahan",
        allowClear: true
    }); 

    $('#myTable').DataTable({
        "order": [
        [7, 'desc']
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        });


    $('#sort_desa').change(function(event) {
    
    var idDesa = $(this).val();
    
    if( idDesa == 0)
    {
            location.reload();
    }else{

        $.ajax({
          url: 'inc/ajax.sort_desa.php',
          type: 'POST',
          data: {id_desa: idDesa},
        })
        .done(function(data) {
          console.log("success");

          $('#oldTable').remove();

          $('#content').html(data);

          $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
          });
          
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });

    }

  });

});


        </script>


    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
