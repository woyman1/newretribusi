
<?php if (!$idGet) {header("location:index.php=404");} else {}; ?>
<?php if (!$page) {header("location:index.php=404");} else {}; ?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
 
 <?php 
    $mysql = mysqli_query($re_connect,"SELECT * FROM re_users WHERE re_user_id = '$idGet' ");
    while($data = mysqli_fetch_array($mysql)){
?>
        <div class="row">
        	<div class="col-lg-12 white-box">
                <div class="text-left"><a href="index.php?page=listuser"><button type="button" class="btn btn-info waves-effect waves-light m-b-10">< Kembali</button></a></div>
                <p></p>
        		<h3 class="box-title m-b-0">Form <?php echo $pageName." ".$data['re_user_name'];?></h3>     
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>        		

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Username*</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $data['re_user_username']; ?>" disabled>
                                        <span class="help-block with-errors"></span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Email*</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" value="<?php echo $data['re_user_email']; ?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" class="form-control" value="<?php echo $data['re_user_name']; ?>" disabled>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">NIP*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" class="form-control" value="<?php echo $data['re_user_nip']; ?>" disabled>
                                    </div>
                                </div>

                                  <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-3 control-label col-form-label">Password*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="password" class="form-control" value="<?php echo $data['re_user_password']; ?>" disabled> 
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Alamat*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="address" class="form-control" value="<?php echo $data['re_user_address']; ?>" disabled>
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">No Hp*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="phone" class="form-control" value="<?php echo $data['re_user_phone']; ?>" disabled>
                                    </div>
                                </div>
                               
                                <div class="form-group row">
                                      <label for="inputPassword4" class="col-sm-3 control-label col-form-label">Level*</label>
                                      <div class="col-sm-9">
                                         <select class="form-control" name="level" disabled>
                                                    <?php 
                                                        $idLevel = $data['re_user_level'];
                                                        $query = mysqli_query($re_connect, "SELECT * FROM re_users_level WHERE level_id = '$idLevel' ");
                                                        while($level = mysqli_fetch_array($query)){ ?>
                                                        
                                                          <option value="<?php echo $level['level_id']; ?>"><?php echo $level['level_name']; ?></option>  
                                                        
                                                    <?php } ?>
                                         </select>
                                        </div>      
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tempat Lahir</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="tmplahir" value="<?php echo $data['re_user_tmlahir'] ?>" disabled> 
                                </div>  </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" name="tgllahir" value="<?php echo $data['re_user_tglahir'] ?>" disabled> 
                                </div>  </div>

                                <div class="panel panel-info">
                                    <div class="panel-heading"> Arsip User</div>
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                                        
                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <div class="white-box">
                                                    <h3 class="box-title">Foto Formal 3x4</h3>
                                                    <?php 
                                                        if($data['re_user_foto']){
                                                       ?>                                   
                                                        <div class="col-sm-12">
                                                            <a class="image-popup-no-margins" href="../uploads/images/arsip/<?php echo $data['re_user_foto']; ?>" title="Foto 3x4">
                                                                <img src="../uploads/images/arsip/<?php echo $data['re_user_foto']; ?>" class="img-responsive" />
                                                            </a>
                                                           
                                                        </div>
                                                    <?php }else {?>
                                                            <div class="col-sm-12">
                                                                <div class="panel-body">
                                                                    <div class="text-danger "><b>Belum Upload</b></div>
                                                                </div>
                                                            </div>
                                                     <?php }?>                                         
                                                    
                                                </div> 
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <div class="white-box">
                                                    <h3 class="box-title">KTP</h3>
                                                      <?php 
                                                        if($data['re_user_ktp']){
                                                       ?>                                
                                                        <div class="col-sm-12">
                                                            <a class="image-popup-no-margins" href="../uploads/images/arsip/<?php echo $data['re_user_ktp']; ?>" title="Surat Keterangan">
                                                                <img src="../uploads/images/arsip/<?php echo $data['re_user_ktp']; ?>" class="img-responsive" />
                                                            </a>
                                                           
                                                        </div>
                                                    <?php }else {?>
                                                            <div class="col-sm-12">
                                                                <div class="panel-body">
                                                                    <div class="text-danger"><b>Belum Upload</b></div>
                                                                </div>
                                                            </div>
                                                    <?php }?>                            
                                                </div>
                                            </div>
                                          
                                    </div>
                                </div>
                            </div>

                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <a href="index.php?page=edituser&id=<?php echo $data['re_user_id'];?>"><button type="button" name="create" class="btn btn-info waves-effect waves-light m-l-10">Edit</button></a>
                                        <a href="index.php?page=upload_arsip_user&id=<?php echo $data['re_user_id'];?>"><button type="button" name="create" class="btn btn-info waves-effect waves-light m-l-10">Upload Arsip User</button></a>
                                    </div>
                                </div>
        	</div>
        </div>
<?php } ?>
	</div>
</div>        
