    
<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>
<?php if (!$_GET['idKey']) {header("location:index.php=404");} else {}; ;
$idKey = $_GET['idKey'];
?>
<?php if (!$page) {header("location:index.php=404");} else {}; ?>

<style type="text/css">
    .left{
        float: left;
        margin-left: 50px; 
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
 

        <div class="row">
        	<div class="col-lg-12 white-box"> 
                    <div class="text-left"><a href="index.php?page=listpembayaran"><button type="button" name="create" class="btn btn-info waves-effect waves-light m-t-10 m-b-10">< Kembali</button></a></div>

                <div class="form-body m-t-10">
                   
                        <div class="panel panel-info col-sm-12 m-t-25">
                            <div class="row">
                            
                             <div class="form-group col-sm-3">
                                <label class="control-label col-form-label m-l-10">Nomer Kwitansi</label>
                                 <div class="col-sm-12">
                                    <b class="m-l-10"><?php echo $idKey; ?></b></div>
                             </div>
                                <div class="form-group col-sm-12">
                                    <label class="control-label col-form-label m-l-10">Bulan Yang Dibayar</label>
                                    <ul>  
                                        <?php
                                            $l = 0; 
                                            $query = mysqli_query($re_connect,"SELECT bayar_bulan FROM re_pembayaran WHERE bayar_key = '$idKey' AND bayar_cost_id = '$idGet' ") or die(mysqli_error($re_connect));
                                            while($namaBln = mysqli_fetch_assoc($query)){
                                                $bulan[] = $namaBln['bayar_bulan'];
                                                $l++;
                                            }

                                            for($a = 0; $a<$l; $a++)
                                            {   ?>

                                                <li class="left"><?php echo date_month_name($bulan[$a]); ?></li>        

                                        <?php    } ?>
                                    </ul>
                                </div>

                        
                            <div class="form-group col-sm-3"> 
                                <label class="control-label col-form-label m-l-10">Jumlah Retribusi</label>
                            <div class="col-sm-12">
                                <?php $nom = mysqli_query($re_connect, "SELECT bayar_nominal FROM re_pembayaran WHERE bayar_key = '$idKey' AND bayar_cost_id = '$idGet' ");
                                        $nominal = mysqli_fetch_assoc($nom);
                                ?>
                                <p class="form-control-static"> Rp. <?php echo number_format($nominal['bayar_nominal'], 0,',','.'); ?> </p>
                            </div>  </div>

                            <div class="form-group col-sm-3"> 
                                <label class="control-label col-form-label m-l-10">Tipe Bangunan</label>
                            <div class="col-sm-12">
                            <?php $bangunan = mysqli_query($re_connect, "SELECT * FROM re_costumer INNER JOIN re_type ON re_costumer.cost_type_id = re_type.type_id WHERE cost_id = '$idGet' ");
                                    $type = mysqli_fetch_assoc($bangunan);
                            ?>

                                 <p class="form-control-static"> <?php echo $type['type_name'] ?> </p>
                            </div> </div>

                        <?php 

                            $tmbh = mysqli_query($re_connect,"SELECT * FROM re_pembayaran_add WHERE add_byr_key = '$idKey' ");

                        ?>

                            <div class="form-group col-sm-12"> 
                                <label class="control-label col-form-label m-l-10">Tambahan</label>
                            <div class="col-sm-12">
                                <table class="col-sm-4">
                                    <?php 
                                    if(mysqli_num_rows($tmbh) > 0){
                                    
                                    while($tambahan = mysqli_fetch_array($tmbh)){ ?>
                                            <tr>
                                                <td><?php echo $tambahan['add_name']; ?> </td>
                                                <td><?php echo "Rp. ".number_format($tambahan['add_nominal'], 0,',','.'); ?></td>
                                            </tr>

                                    <?php } 

                                    }else{ ?>
                                            <label class="control-label col-form-label m-l-10 text-danger">Tidak ada tambahan</label>
                                    <?php } ?>
                                </table>
                            </div> </div>

                             <div class="form-group col-sm-12"> 
                                <label class="control-label col-form-label m-l-10">Total Jumlah</label>
                            <div class="col-sm-12">
                               
                                    <?php 
                                        $tot = mysqli_query($re_connect,"SELECT bayar_total FROM re_pembayaran WHERE bayar_key = '$idKey' AND  bayar_cost_id = '$idGet' ") or die(mysqli_error($re_connect));
                                        $total = mysqli_fetch_assoc($tot); 
                                        echo "Rp. ".number_format($total['bayar_total'], 0,',','.');
                                     ?>
                            </div> </div>


                            
                        </div>
                    </div>      
                </div>  

                    </form>
        	</div>
        </div>

	</div>
</div>         
