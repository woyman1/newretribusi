

<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>

<?php 
    $query = mysqli_query($re_connect, "SELECT * FROM re_type WHERE type_id = '$idGet' ");
    while($data = mysqli_fetch_array($query)){
?>
        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h3 class="box-title m-b-0">Lihat <?php echo $data['type_name'];?></h3>
                <p class="text-muted m-b-30 font-13"> </p>
        		
                    <form action="../administrator/trnsql/sql.tipe.php" method="post" id="fileForm" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="edit">
                            <input type="hidden" name="id" value="<?php echo $idGet; ?>">

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_tipe" value="<?php echo $data['type_name'] ?>" required> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kode Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="kode_tipe" value="<?php echo $data['type_code'] ?>" required> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Harga Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="harga_tipe" value="<?php echo $data['type_harga'] ?>" required> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Icon Tipe </label>

                            
                                <?php include ("../inc/inc.pilih_ikon.php"); ?>
                                
                                <div class="col-sm-9 m-t-5">

                                <?php 
                                    $queryIcon = "SELECT type_icon FROM re_type";
                                    $hasil = $re_connect->query($queryIcon);

                                    $dataIcons = array();
                                    while ($baris = $hasil->fetch_assoc()){
                                        $dataIcons[] = $baris['type_icon'];
                                    }

                                    foreach ($icon as $key => $value) :
                                ?>
                                    <label>
                                      <input type="radio" name="fb" value="<?=$value?>" class="radio_z" <?php if ($value == $data['type_icon']) : ?> checked <?php endif?>

                                        <?php
                                            if (in_array($value, $dataIcons) && $value != $data['type_icon']) {
                                                echo "disabled";
                                            }
                                        ?>
                                      />
                                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 50" height="24px" width="50px" 
                                        <?php
                                            if (!in_array($value, $dataIcons) || $value == $data['type_icon']) :
                                        ?>
                                        style="fill: #000000"
                                        <?php else : ?>
                                            style="fill: #808080" data-toggle="tooltip" data-placement="right" title="Ikon sudah dipilih untuk data lain!"
                                        <?php endif ?>
                                      >
                                            <path d="<?=$value?>"></path>
                                      </svg>
                                    </label>
                                <?php endforeach ?>
                                </div>
                            </div>

                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Simpan</button>
                                    </div>
                                </div>

                    </form>
                
        	</div>
        </div>

        <?php } ?>

	</div>
</div>          

