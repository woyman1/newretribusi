

<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
 

        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>     
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>
        		
        			  	<form action="trnsql/sql.users.php" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Username*</label>
                                    <div class="col-sm-9">
                                        <input type="text" data-toggle="validator" data-error="minimal 3 characters!" pattern="[a-zA-Z0-9]{3,}" name="username" class="form-control"  placeholder="Username" required>
                                        <span class="help-block with-errors"></span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Email*</label>
                                    <div class="col-sm-9">
                                        <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3,4}$" class="form-control" id="inputEmail3" placeholder="Email" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Nama" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">NIP*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="nip" pattern="[0-9]{10,}" data-error="NIP harus berupa angka! Min 10 karakter!" class="form-control" id="nip" placeholder="NIP " required>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Alamat*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="address" class="form-control" id="alamat" placeholder="Alamat" required>
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">No Hp*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="phone" class="form-control" placeholder="081234567891"  pattern="[0-9]{11,}"  id="noHp" placeholder="No Hp" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword3" class="col-sm-3 control-label col-form-label">Password*</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password" data-toggle="validator" data-minlength="6" data-error="Minimal 6 characters!" id="psw" class="form-control" placeholder="Password" required> <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword4" class="col-sm-3 control-label col-form-label">Ulangi Password*</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="psw2" data-match="#psw" data-match-error="Password not match!" placeholder="Ulangi Password" required>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                </div>
                                <div class="form-group row">
                                      <label for="inputPassword4" class="col-sm-3 control-label col-form-label">Level*</label>
                                      <div class="col-sm-9">
                                         <select class="form-control" name="level" required>
                                                <option value="">Pilih Level Pengguna</option>
                                                    <?php 
                                                        $query = mysqli_query($re_connect, "SELECT * FROM re_users_level");
                                                        while($level = mysqli_fetch_array($query)){ ?>
                                                        
                                                          <option value="<?php echo $level['level_id']; ?>"><?php echo $level['level_name']; ?></option>  
                                                        
                                                    <?php } ?>
                                         </select>
                                        </div>      
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tempat Lahir</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="tmplahir" required> 
                                </div>  </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" name="tgllahir" required> 
                                </div>  </div>

                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" name="create" class="btn btn-info waves-effect waves-light m-t-10">Tambahkan</button>
                                    </div>
                                </div>
                      	</form>
        	</div>
        </div>

	</div>
</div>        

<script src="../assets/js/mask.js"></script>
<script src="../assets/js/validator.js"></script>
