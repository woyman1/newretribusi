<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>    
<?php if (!$page) {header("location:index.php=404");} else {}; ?>


 <link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
    <!-- animation CSS -->
<style type="text/css">
    .f2{
        font-size: 11px;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
<?php 
    $mysqli = mysqli_query($re_connect, "SELECT * FROM re_costumer WHERE cost_id = '$idGet' ");
    $data = mysqli_fetch_array($mysqli);
 
?> 
        <div class="row">
            <div class="col-md-12 white-box">
                 <a href="index.php?page=listcostumer"><button class="btn btn-info" ><span>< List Costumer</span></button></a>
                <div class="text-right">
                    
                    <button class="btn btn-info btn-outline" id="print"><span><i class="fa fa-print"></i> Print Kartu</span></button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 white-box printableArea">    

                    <div class="text-center col-sm-12"><h3><b>KARTU RETRIBUSI SAMPAH</b></h3></div>

                <div class="row">
                        <?php $type = $data['cost_type_id'];
                            $qType = mysqli_query($re_connect,"SELECT type_harga FROM re_type where type_id = '$type' "); 
                            $type = mysqli_fetch_assoc($qType); 
                        ?>                    
            <div>
                <table class="f2" width="1000px">
                    <tr >
                        <td width="150px" >No. Pelanggan</td>
                        <td widht="130px" >: <?php echo $data['cost_id_pel']; ?></td>
                        <td width="150px" >Jadwal Angkut</td>
                        <td class="f2">: <?php $jw = $data['cost_jadwal_id'];
                                $jd = mysqli_query($re_connect, "SELECT * FROM re_jdw_sampah WHERE jadwal_id = '$jw' ");
                                $jadwal = mysqli_fetch_assoc($jd);
                                echo $jadwal['jadwal_name'];
                                ?></td>

                    </tr>
                    <tr>
                        <td >Nama Pelanggan</td>
                        <td >: <?php echo $data["cost_name"]; ?></td>
                        <td >Pengangkut</td>
                        <td >: <?php $pgk = $data['cost_user_id'];
                                    $pg = mysqli_query($re_connect, "SELECT re_user_name, re_user_phone FROM re_users WHERE re_user_id = '$pgk' ");
                                    $pengangkut = mysqli_fetch_assoc($pg);
                                    echo $pengangkut['re_user_name']."(".$pengangkut['re_user_phone'].")";
                        ?> </td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td colspan="4">: <?php echo $data["cost_address"]; ?></td>
                    </tr>
                     <tr>
                        <td>Retribusi/Bulan</td>
                        <td>: Rp.<?php echo number_format($type['type_harga'], 0,',','.');?></td>
                    </tr>
                </table>
            </div>

                    <div class="col-md-12 m-b-10"> <strong class="pull-right"><?php echo "Tahun ".date("Y"); ?></strong></div>

                <table class="tablesaw table-bordered table-hover tablesaw f2">
                    <thead>
                     <tr>
                        <th>No.</th>
                        <th>Retribusi Bulan</th>
                        <th>Tanggal Pembayaran</th>
                        <th>Total Pembayaran</th>
                        <th>Petugas</th>
                        <th>Keterangan</th>
                     </tr>
                    </thead>

                    <tbody>
                <?php


                $masuk_bulan = substr($data['cost_date'], 5,2);
                $masuk_tahun = substr($data['cost_date'], 0,4);
                if($masuk_bulan<10 && date('Y') == $masuk_tahun ) //apakah tanggal masuk tahun ini? jika ya maka cetak mulai bulan masuk
                {
                    $masuk_bulan = substr($masuk_bulan, 1);
                }
                else{
                    $masuk_bulan = 1;
                }

                
                $d = $masuk_bulan; 
                $qTransDate = mysqli_query($re_connect,"SELECT * FROM re_pembayaran WHERE bayar_cost_id = '$idGet' ORDER BY bayar_id ASC ");                

                while($dataPembayaran = mysqli_fetch_assoc($qTransDate)){
                    $tmpUser = $dataPembayaran['bayar_users_id'];
                    $qTmpUser = mysqli_query($re_connect,"SELECT re_user_name FROM re_users WHERE re_user_id = '$tmpUser' ");
                    $usr = mysqli_fetch_assoc($qTmpUser);

                    if($dataPembayaran['bayar_bulan'] > 1)
                    {
                        for($entah = 1; $entah<=$dataPembayaran['bayar_bulan']; $entah++){
                            $dateBayar[$d] = $dataPembayaran['bayar_date'];                                         //array untuk tanggal pembayaran mulai bulan masuk
                            $nominalBayar[$d] = "Rp. ".number_format($dataPembayaran['bayar_total'], 0,',','.');    //array untuk jumlah pembayaran mulai bulan masuk
                            $userBayar[$d] = $usr['re_user_name'];                                                  //array untuk petugas
                                                                               
                            $d++;        
                        }
                    }else{
                        $dateBayar[$d] = $dataPembayaran['bayar_date'];                                             //array untuk tanggal pembayaran
                        $nominalBayar[$d] = "Rp. ".number_format($dataPembayaran['bayar_total'], 0,',','.');        //array untuk tanggal pembayaran
                        $userBayar[$d] = $usr['re_user_name'];                                                      //array untuk petugas
                        
                        $d++;
                    }
                    
                }

                //query untuk petugas 
                $ad=$masuk_bulan;
                $qTransUser = mysqli_query($re_connect,"SELECT bayar_users_id FROM re_pembayaran WHERE bayar_cost_id = '$idGet' ORDER BY bayar_id ASC ");
                while($bayarUsers = mysqli_fetch_assoc($qTransUser)){

                    $tmpUser = $bayarUsers['bayar_users_id'];

                        
                        

                    $userBayar[$ad] = $usr['re_user_name'];
                    $ad++;
                }

              
                //perulangan untuk perbulannya 
                for($a = $masuk_bulan; $a<=12; $a++){ ?>
                     <tr>
                        <td><?php echo $a; ?></td>
                        <td><?php $b = date("m", mktime(0,0,0, $a, 1, date("Y")) ); echo date_month_name($b); ?></td>
                        <td><?php echo $dateBayar[$a]; ?></td>
                        <td><?php echo $nominalBayar[$a]; ?></td>
                        <td><?php echo $userBayar[$a]; ?></td>  
                        <td><?php   
                                
                            $bln =  date("n", mktime(0,0,0, $a, 1, date("Y"))); 
                                $qKet = mysqli_query($re_connect, "SELECT bayar_key FROM re_pembayaran WHERE bayar_cost_id = '$idGet' AND bayar_bulan = '$bln' "); 
                                $qk = mysqli_fetch_assoc($qKet); 
                                $transKet = $qk['bayar_key'];
                            
                            $qAdd = mysqli_query($re_connect, "SELECT add_name, add_nominal FROM re_pembayaran_add WHERE add_byr_key = '$transKet' ");
                            while($byr_add = mysqli_fetch_assoc($qAdd)){
                                echo "Tambahan ".$byr_add['add_name']." : Rp. ".number_format($byr_add['add_nominal'], 0,',','.')."<br>";
                            }
                                    ?></td>
                    </tr>
                <?php } ?>  
                    </tbody>
                </table>
                
                <div class="clearfix"></div>
                    <hr>
                

                </div>  
            </div>
        </div>

    </div>
</div>         

<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
 <script src="../assets/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    <script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>



