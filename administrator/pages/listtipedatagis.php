<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h2>Daftar <?php echo $pageName;?></h2>
        		      
                    <div class="table-responsive">
                        <table id="myTable" class="table table-striped">                
                            <thead>
                                <tr>
                                    <th>Id tipe</th>
                                    <th>Nama Tipe Datagis</th>
                                    <th>Action</th>
                      
                                </tr>
                            </thead>
                                    <?php 
                                        $query = mysqli_query($re_connect, "SELECT * FROM re_datagis_type ORDER BY typegis_id DESC");
                                        while($data = mysqli_fetch_array($query)){
                                    ?>
                                <tr>
                                    <td><?php echo $data['typegis_id']; ?></td>
                                    <td><?php echo $data['typegis_name']; ?></td>
                                    <td>
                                         <a href="index.php?page=edittipegis&id=<?php echo $data['typegis_id'];?>" data-toggle="tooltip" data-original-title="Edit Tipe <?php echo$data['typegis_name']; ?>"> <i class="fa fa-pencil text-success m-r-10"></i></a>
                                         <a href="trnsql/sql.datagis.php?action=delete_tipe&id=<?php echo $data['typegis_id'];?>" data-toggle="tooltip" data-original-title="Delete Tipe <?php echo$data['typegis_name']; ?>"> <i class="fa fa-close text-danger m-r-10"></i></a>

                                    </td>
                                </tr>
                               <?php } ?>
                            </thead>
                        </table>
                    </div>
        			  	
        	</div>
        </div>

	</div>
</div>  


<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <!-- start - This is for export functionality only -->
<script src="../assets/js/dataTables.buttons.min.js"></script>       

<script>
    $(document).ready(function() {
        $('#myTable').DataTable(
          {
            "order": [
                    [0, 'desc']
                ],
          });
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 1
                }],
                "order": [
                    [1, 'desc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([1, 'asc']).draw();
                } else {
                    table.order([1, 'desc']).draw();
                }
            });
        });
    });
  
    </script>