
<?php if (!$idGet) {header("location:index.php=404");} else {}; ;?>
<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
 <?php 
    $mysqli = mysqli_query($re_connect,"SELECT * FROM re_users WHERE re_user_id = '$idGet' ");
    while($data = mysqli_fetch_array($mysqli)){
 ?>

        <div class="row">
        	<div class="col-lg-12 white-box">
        	    
        	    <div class="col-sm-12"><div class="m-b-10"><a href="index.php?page=listuser"><button class="btn btn-info waves-effect waves-light"> < List Petugas </button></a></div></div>
        	    
        		<h3 class="box-title m-b-0">Form <?php echo $pageName." ".$data['re_user_name'];?></h3>     
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>
        		
        			  	<form action="trnsql/sql.users.php" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="update">
                            <input type="hidden" name="id" value="<?php echo $idGet; ?>">

                                <div class="form-group row">
                                    <label for="username" class="col-sm-3 control-label col-form-label">Username*</label>
                                    <div class="col-sm-9">
                                        <input type="text" data-toggle="validator" data-error="minimal 3 characters!" pattern="[a-zA-Z0-9]{3,}" name="username" class="form-control" value="<?php echo $data['re_user_username']; ?>"  placeholder="Username" required>
                                        <span class="help-block with-errors"></span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 control-label col-form-label">Email*</label>
                                    <div class="col-sm-9">
                                        <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3,4}$" class="form-control" id="inputEmail3" placeholder="Email" value="<?php echo $data['re_user_email']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 control-label col-form-label">Nama*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Nama" value="<?php echo $data['re_user_name']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nip" class="col-sm-3 control-label col-form-label"><input type="checkbox" id="enableNIP"> NIP*</label>
                                    <div class="col-sm-9">
                                    <input type="text" name="nip" pattern="[0-9]{10,}" data-error="NIP harus berupa angka! Min 10 karakter!" class="form-control" id="nip" placeholder="NIP" value="<?php echo $data['re_user_nip'];?>" required readonly>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="address" class="col-sm-3 control-label col-form-label">Alamat*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="address" class="form-control" id="alamat" placeholder="Alamat" >
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label for="phone" class="col-sm-3 control-label col-form-label">No Hp*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="phone" class="form-control" placeholder="081234567891" data-error="Only number!"  pattern="[0-9]{11,}"  id="noHp" placeholder="No Hp" value="<?php echo $data['re_user_phone']; ?>"  required><span class="help-block with-errors"></span>
                                    </div>
                                </div>
                               
                                <div class="form-group row">
                                      <label for="level" class="col-sm-3 control-label col-form-label">Level*</label>
                                      <div class="col-sm-9">
                                         <select class="form-control" name="level" required>
                                                    <?php 
                                                        $idLevel = $data['re_user_level'];
                                                        $le = mysqli_query($re_connect, "SELECT * FROM re_users_level WHERE level_id='$idLevel' ");
                                                        while($lvel = mysqli_fetch_array($le)){ ?>
                                                            <option value="<?php echo $lvel['level_id']; ?>"><?php echo $lvel['level_name'] ?></option>
                                                    <?php } ?>
                                                    <?php 
                                                        $query = mysqli_query($re_connect, "SELECT * FROM re_users_level");
                                                        while($level = mysqli_fetch_array($query)){ ?>
                                                        
                                                          <option value="<?php echo $level['level_id']; ?>"><?php echo $level['level_name']; ?></option>  
                                                        
                                                    <?php } ?>
                                         </select>
                                        </div>      
                                </div>

                                <div class="form-group row">
                                    <label for="tmplahir" class="col-sm-3 control-label col-form-label">Tempat Lahir</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="tmplahir" value="<?php echo $data['re_user_tmlahir']; ?>" required> 
                                </div>  </div>

                                <div class="form-group row">
                                    <label for="tgllahir" class="col-sm-3 control-label col-form-label">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" name="tgllahir" value="<?php echo $data['re_user_tglahir']; ?>" required> 
                                </div>  </div>

                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Update</button>
                                    </div>
                                </div>
                      	</form>
        	</div>
        </div>
        <?php } ?>

	</div>
</div>        

<script src="../assets/js/mask.js"></script>
<script src="../assets/js/validator.js"></script>
<script type="text/javascript">
   $(document).ready(function() { 
        
        var nip =  $("#nip").val();
        
        $('#enableNIP').change(function() {
        if(this.checked) {
            $("#nip").removeAttr("readonly");
        }else{
            $("#nip").attr("readonly",true);
            $("#nip").val(nip);
            
        }
        
    });
         
   });
</script>
