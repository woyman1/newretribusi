
<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>

        <div class="row">

               <div class="white-box col-md-12">
                    <h3 class="box-title">General</h3>
                <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="row row-in">
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-8 col-sm-8 col-xs-8"> <i class="icon-people"></i>
                                            <h5 class="text-muted vb">Semua Pelanggan</h5>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <h3 class="counter text-right m-t-15 text-danger">
                                                <?php $qcost = mysqli_query($re_connect, "SELECT count(cost_id) as all_cost FROM re_costumer");
                                                      $cost = mysqli_fetch_array($qcost); $allCost = $cost['all_cost'];
                                                      echo $allCost;
                                                 ?> 
                                            </h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?= $allCost;?>%">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                    <div class="col-in row">
                                        <div class="col-md-8 col-sm-8 col-xs-8"> <i class="icon-user"></i>
                                            <h5 class="text-muted vb">Semua Petugas</h5>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <h3 class="counter text-right m-t-15 text-megna">
                                                <?php $quser = mysqli_query($re_connect, "SELECT count(re_user_id) as all_user FROM re_users");
                                                      $user = mysqli_fetch_array($quser); $allUser = $user['all_user'];
                                                      echo $allUser;
                                                 ?> 
                                            </h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-megna" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?=$allUser;?>%">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-8 col-sm-8 col-xs-8"> <i class="ti-home"></i>
                                            <h5 class="text-muted vb">Jumlah Kelurahan</h5>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <h3 class="counter text-right m-t-15 text-primary">
                                                <?php $qdesa = mysqli_query($re_connect, "SELECT count(desa_id) as all_desa FROM re_desa");
                                                      $desa = mysqli_fetch_array($qdesa); $allDesa = $desa['all_desa'];
                                                      echo $allDesa;
                                                 ?> 
                                            </h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?=$allDesa;?>%"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6  b-0">
                                    <div class="col-in row">
                                        <div class="col-md-8 col-sm-8 col-xs-8"> <i class="ti-home"></i>
                                            <h5 class="text-muted vb">Jumlah Kecamatan</h5>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <h3 class="counter text-right m-t-15 text-success">
                                            <?php $qkec = mysqli_query($re_connect, "SELECT count(idKec) as all_kec FROM re_kecamatan");
                                                      $kec = mysqli_fetch_array($qkec); $allKec = $kec['all_kec'];
                                                      echo $allKec;
                                                 ?> 
                                            </h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?=$allKec;?>%"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                   </div>
                

                <div class="white-box col-md-8">
                    
                    <h3 class="box-title">Jumlah Pelanggan Berdasarkan Tipe</h3>
                    
                    <div class="col-md-12"> 
                        <table class="col-md-12"  style="border-collapse: collapse;">
                            <tr>
                                <th><center>Icon</center></td>
                                <th>Nama Icon</td>
                                <th>Jumah Pelanggan</td>
                            </tr>
                            <?php $qtipe = mysqli_query($re_connect,"SELECT * FROM re_type");
                                  
                                  while($tipe = mysqli_fetch_assoc($qtipe))
                                  {?>
                                        <tr>
                                            <td>
                                                 <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 10 50" height="24px" width="50px" style="fill: #808080">
                                                        <path d="<?=$tipe['type_icon']?>"></path>
                                                  </svg>
                                            </td>                                           
                                            <td>
                                                <?= $tipe['type_name']?>
                                            </td>
                                            <td> : <?php 
                                                    $tipeId = $tipe['type_id'];
                                                    $cTipe = mysqli_query($re_connect,"SELECT COUNT(cost_id) as jml_pel FROM re_costumer WHERE cost_type_id ='$tipeId' ");
                                                    $jmlPel = mysqli_fetch_assoc($cTipe); echo "  ".$jmlPel['jml_pel']." Pelanggan"; ?>
                                                        
                                            </td>
                                        </tr>            
                             <?php

                                }

                             ?>
                        </table>
                    </div>

                </div>


                <div class="white-box col-md-12">
                        <div class="col-lg-12 col-sm-12 row-in-br">
                            <div class="col-in row">
                                <div class="col-md-8 col-sm-8 col-xs-8"> <i class="ti-exchange-vertical"></i>
                                    <h5 class="text-muted vb">Progress Pencapaian Target Pemasukan</h5>
                                </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <h4 class="counter text-right m-t-15 text-info">
                                                <?php $qjummlah = mysqli_query($re_connect, "SELECT bayar_total as total FROM re_pembayaran GROUP BY bayar_key");
                                                      while($jml = mysqli_fetch_array($qjummlah)){
                                                      $total = $total + $jml['total'];
                                                      }
                                                      echo "Rp. ".number_format($total,0,',','.');
                                                 ?> 
                                        </h4>
                                    </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="progress">
                                        <?php $q_target = mysqli_query($re_connect,"SELECT setting_target_pemasukan FROM re_settingsite WHERE setting_id = 1 ");
                                                $trg = mysqli_fetch_assoc($q_target); $target = $trg['setting_target_pemasukan'];
                                                $persentase = ($total/$target)*100;
                                         ?>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?= round($persentase,2);?>%">  </div>

                                </div>
                                <div class="col-md-12"><div class="col-sm-12"><?= round($persentase,2)."%";?><h5 class="pull-right text-right text-info"><?="Target Rp. ".number_format($target,0,',','.');?></h5></div>  </div>
                            </div>
                            </div>
                        </div>
                </div>
                
                <!-- <textarea class="form-control" rows="10"> <?php  echo round($persentase,2)." ".$total." ".$target; ?> </textarea> -->

                <script src="../plugins/code/highcharts.js"></script>
                <script src="../plugins/code/modules/exporting.js"></script>

                <div class="col-md-12 white-box" id="container" style="margin: 0 auto; height: auto;" ></div>
        </div>


        </div>

    </div>

<script type="text/javascript">

Highcharts.chart('container', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Jumlah Pembarayan dan Total Pemasukan Tiap Bulan Tahun <?= date("Y");?>.'
    },
    xAxis: [{
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun',
            'Jul', 'Agus', 'Sept', 'Okt', 'Nov', 'Des'],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}',
            style: {
                color: '#c11d1b'
            }
        },
        title: {
            text: 'Pembayaran',
            style: {
                color: '#c11d1b'
            }
        }
    }, { // Secondary yAxis
        title: {
            text: 'Pemasukan',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: 'Rp. {value}',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        x: 120,
        verticalAlign: 'top',
        y: 100,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    series: [{
        name: 'Pemasukan',
        type: 'column',
        yAxis: 1,
         tooltip: {
            valuePrefix: 'Rp. '
        },
        data: [<?php 
                //total pemasukan sebelum bulan 12
                for($a = 1; $a<12; $a++){
                    $y = date('Y');
                $qmasukan = mysqli_query($re_connect, "SELECT bayar_total as total_masuk FROM re_pembayaran WHERE MONTH(bayar_date) ='$a' AND YEAR(bayar_date) = '$y' GROUP BY bayar_key");
                $num_qpemasukan = mysqli_num_rows($qmasukan);
                    
                    if($num_qpemasukan > 0 )
                    { 
                        while($msk =mysqli_fetch_assoc($qmasukan)){
                            $tMasukBulan = $tMasukBulan + $msk['total_masuk'];
                        }
                        echo $tMasukBulan.', ';
                    }
                    else
                    {
                        echo '0, ';
                    }
                }
                //total pemasukan pada bulan 12 
                $qmasukan12 = mysqli_query($re_connect,"SELECT bayar_total as total_masuk FROM re_pembayaran WHERE MONTH(bayar_date) = 12 AND YEAR(bayar_date) ='$y' GROUP BY bayar_key ");
                $num_qpemasukan12 = mysqli_num_rows($qmasukan12);
                    
                    if($num_qpemasukan12>0)
                    {
                        while($msk12 = mysqli_fetch_assoc($qmasukan12))
                        {
                            $tMasukBulan12 = $tMasukBulan12 + $msk12['total_masuk'];
                        }
                        echo $tMasukBulan12;
                    }else{
                        echo '0';
                    }
             ?>]
       

    }, {
        name: 'Pembayaran',
        type: 'spline',
        color: '#c11d1b',
        tooltip: {
            valueSuffix: ' Pembayaran'
        },
        data:  [<?php 
                //total pemasukan sebelum bulan 12
                for($a = 1; $a<12; $a++){
                $qpembayaran = mysqli_query($re_connect, "SELECT COUNT(DISTINCT(bayar_key)) as jumlah_pembayaran FROM re_pembayaran WHERE MONTH(bayar_date) ='$a' AND YEAR(bayar_date) ='$y' ");
                $num_qpembayaran = mysqli_num_rows($qpembayaran);
                    
                    if($num_qpembayaran > 0 )
                    { 
                        $byr =mysqli_fetch_assoc($qpembayaran);
                        echo $byr['jumlah_pembayaran'].', ';
                    }
                    else
                    {
                        echo '0, ';
                    }
                }
                //total pemasukan pada bulan 12 
                $qpembayaran12 = mysqli_query($re_connect, "SELECT COUNT(DISTINCT(bayar_key)) as jumlah_pembayaran FROM re_pembayaran WHERE MONTH(bayar_date) = 12 AND YEAR(bayar_date) ='$y' ");
                $num_qpembayaran12 = mysqli_num_rows($qpembayaran12);
                    
                    if($num_qpembayaran12 > 0 )
                    { 
                        $byr12 =mysqli_fetch_assoc($qpembayaran12);
                        echo $byr12['jumlah_pembayaran'];
                    }
                    else
                    {
                        echo '0 ';
                    }
             ?>]
        
    }]
});
        </script>
    <!-- <textarea class="form-control" rows="10"><?= print_r($qmasukan);?></textarea> -->
