<style type="text/css">
    
    .radio_cate {
                visibility: hidden; 
        }
        
    .radio_label{
        border:2px solid #49bc49;
        border-radius: 25px; 
        box-shadow: 0.5px 0px #49bc49;
    }   
        .radio_label:hover{
        border:2px solid #49bc49;
        background: #49bc49;
        color: #fff;
        border-radius: 25px; 
        box-shadow: 0.5px 0px blue;
        }
 
    input[type="radio"]:checked + label{
        border:2px solid #49bc49;
        background: #49bc49;
        color: #fff;
        border-radius: 25px; 
        box-shadow: 0.5px 0px blue;
    }
</style>

<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>

        <div class="row">
         <div class="col-md-6 white-box">
             <h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>     
                <p class="text-muted m-b-30 font-13"> *Choose your photo wishly</p>

                <form action="trnsql/sql.gallery.php" method="post" class="form-horizontal" data-toggle="" enctype="multipart/form-data">

                    <div class="form-group row">
                    <label class="col-sm-12 m-l-15">Multiple Upload Here</label>
                        <div class="col-sm-10">
                            <input type="file" name="pic[]" class="form-control" multiple required>
                        </div>
                    </div>

                    <div class="form-group row">
                    <label class="col-sm-12 m-l-15">Category Here</label>
                        <div class="col-sm-10">
                            <input type="text" name="cate_name" class="form-control" value="" id="cate_here" required readonly>
                            <input type="hidden" name="cate_hidden" value="" id="cate_hidden">
                        </div>
                    </div>

                    <div class="form-group row">
                        <button type="submit" name="create" class="btn btn-info waves-effect waves-light m-l-15">Upload</button>
                    </div>
                    
                </form>

         </div>

         <div class="col-md-6">
          <div class="white-box">
            <form class="form-horizontal" action="trnsql/sql.gallery.php" method="post"></form>

            <div class="form-group row">
                    <label class="col-sm-12 m-l-15">Add New Category Here</label>
                        <div class="col-sm-10">
                            <input type="hidden" name="action" value="newCategory">
                            <input type="text" name="newcategory" class="form-control" id="kategori">
                            <button type="button" class="btn btn-info waves-light waves-effect m-t-10" id="simpan_kat">Add Category</button>
                        </div>
            </div>

            <div class="col-sm-10">
            <label class="col-sm-12">Select your category here!</label>
            <div class="fadeOut">
                <?php 
                    $op = mysqli_query($re_connect,"SELECT * FROM re_gallery_category");
                    while($cate = mysqli_fetch_assoc($op)){ ?>

                    <input type="radio" name="fb" id="<?= $cate['galcat_id'];?>" value="<?= $cate['galcat_name'];?>" class="radio_cate m-l-10" />
                    <label for="<?= $cate['galcat_id'];?>" class="radio_label">
                    <span class="m-l-10"><?= $cate['galcat_name'];?></span>
                    <a href="javascript:void(0)" class="clear" id_x="<?= $cate['galcat_id']; ?>"><i class="fa fa-times m-r-10"></i></a>
                    </label>

                <?php } ?>
            </div>
            </div>
             
         </div>

         </div>

        </div>


    </div>
</div>
 
<script src="../plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script type="text/javascript">
     $(document).ready(function(){
        
        var action = "apalah";

        $('#simpan_kat').click(function(){
            $.ajax({
                url: "trnsql/sql.gallery.php", 
                method: "POST",
                data: {
                    item:$('#kategori').val(),
                    action:action,
                }
            }).done(function(){
                 window.location.reload(true);
            });
        });


        $('.apalah').addClass('fadeIn animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            
        });

        $('.radio_cate').each(function(){
            $(this).on("click", function(e){    
                var value = $(this).val();
                var id = $(this).attr("id");

                if(this.checked) {
                    
                    $("#cate_here").val(value);
                    $("#cate_hidden").val(id); 

                }
            });
                
        });  

        $(".clear").each(function(){
            $(this).on("click", function(e){
                e.preventDefault();
                var id_x = $(this).attr("id_x");

                $.ajax({
                    url: "trnsql/sql.gallery.php", 
                    method: "POST",
                    data: {
                        id_x:$(this).attr("id_x"),
                        action:"delete_x",
                    }
                }).done(function(){
                     window.location.reload(true);
                })

            })
        });          

    });

      
 </script>