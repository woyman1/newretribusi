<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>
        		
        			  	<form action="../administrator/trnsql/sql.desa.php" method="post" class="form-horizontal">

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kecamatan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="namaKec" name="namaKec" required> 
                                <input type="hidden" class="form-control" id="idKec" name="idKec" required> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama Desa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_desa" required> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kode Desa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="kode_desa" required> 
                            </div>  </div>

                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" name="create" class="btn btn-info waves-effect waves-light m-t-10">Tambahkan</button>
                                    </div>
                                </div>
                      	</form>
        	</div>
        </div>

	</div>
</div>       

<script src="../assets/autocomplate/jquery-ui.js"></script>   
<script type="text/javascript">
    $(document).ready(function(){
        $("#namaKec").autocomplete({
                source: "trnsql/autocomplete_adddesa.php",
                select: function(event, ui) {
                    $('#namaKec').val(''+ui.item.coba);
                    $('#idKec').val('' + ui.item.coba); // menampilkan nama di field nama 
                }
        });
    });
</script>