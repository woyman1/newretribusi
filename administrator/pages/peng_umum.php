<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title">Pengaturan Umum</h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">

            <?php
                $q_setting = "SELECT * FROM re_settingsite";
                $r_setting = $re_connect->query($q_setting);

                if ($r_setting->num_rows >0) {
                    $row = $r_setting->fetch_assoc();
                }
            ?>
                <div class="form-group row">
                    <div class="col-lg-4">Setting TimeZone</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_timezone" type="text" class="form-control" value="<?=$row['setting_timezone']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="setting_timezone" id="edit_setting_timezone"><i class="fa fa-edit"></i> Edit</button>    
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save" type="button" editId="setting_timezone" id="save_setting_timezone"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Setting Site Name</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_site_name" type="text" class="form-control" value="<?=$row['setting_site_name']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="setting_site_name" id="edit_setting_site_name"><i class="fa fa-edit"></i> Edit</button>    
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save" 
                        type="button" editId="setting_site_name" id="save_setting_site_name"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Setting Site Deskription</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_site_desc" type="text" class="form-control" value="<?=$row['setting_site_desc']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="setting_site_desc" id="edit_setting_site_desc"><i class="fa fa-edit"></i> Edit</button>    
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save" 
                        type="button" editId="setting_site_desc" id="save_setting_site_desc"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Setting E-mail</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_email" type="text" class="form-control" value="<?=$row['setting_email']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="setting_email" id="edit_setting_email"><i class="fa fa-edit"></i> Edit</button>    
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save" 
                        type="button" editId="setting_email" id="save_setting_email"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-lg-4">Setting Phone</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_phone" type="textarea" class="form-control" value="<?=$row['setting_phone']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="setting_phone" id="edit_setting_phone"><i class="fa fa-edit"></i> Edit</button>    
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save" 
                        type="button" editId="setting_phone" id="save_setting_phone"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Setting Mobile</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_mobile" type="text" class="form-control" value="<?=$row['setting_mobile']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" type="button" editId="setting_mobile" id="edit_setting_mobile">
                            <i class="fa fa-edit"></i> Edit
                        </button>    

                        <button class="btn btn-info tomSave" type="button" editId="setting_mobile" id="save_setting_mobile">
                            <i class="fa fa-save"></i> Save
                        </button>
                        
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Setting Home Message</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_home_message" type="text" class="form-control" value="<?=$row['setting_home_message']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" type="button" editId="setting_home_message" id="edit_setting_home_message">
                            <i class="fa fa-edit"></i> Edit
                        </button>    

                        <button class="btn btn-info tomSave" type="button" editId="setting_home_message" id="save_setting_home_message">
                            <i class="fa fa-save"></i> Save
                        </button>
                        
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Setting Target Pemasukan</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_target_pemasukan" type="text" class="form-control" value="<?=$row['setting_target_pemasukan']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" type="button" editId="setting_target_pemasukan" id="edit_setting_target_pemasukan">
                            <i class="fa fa-edit"></i> Edit
                        </button>    

                        <button class="btn btn-info tomSave" type="button" editId="setting_target_pemasukan" id="save_setting_target_pemasukan">
                            <i class="fa fa-save"></i> Save
                        </button>
                        
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">No WA Call Center</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="setting_no_wa_call_center" type="text" class="form-control" value="<?=$row['setting_no_wa_call_center']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" type="button" editId="setting_no_wa_call_center" id="edit_setting_no_wa_call_center">
                            <i class="fa fa-edit"></i> Edit
                        </button>    

                        <button class="btn btn-success tomSave" type="button" editId="setting_no_wa_call_center" id="save_setting_no_wa_call_center">
                            <i class="fa fa-save"></i> Save
                        </button>
                        
                    </div>
                </div>

         
                <!-- end here -->

        	</div>
        </div>

	</div>
</div> 

<script type="text/javascript">

$(document).ready(function(){
    $(".tomSave").hide().attr({
        "data-toggle": "tooltip",
        "data-original-title": "save",
        "class": "btn btn-success tomSave"
    });

    $(".tomEdit").attr({
       "data-toggle": "tooltip",
        "data-original-title": "Edit"
    });

    $(".form-control").css("color","gray");


    $(".tomEdit").each(function(i){
        $(this).on("click", function(){
            var editValue = $(this).attr("editId");
            $("#"+editValue).attr({
                "readonly": false,
                
            }).css({
                "box-shadow":"10px 10px 5px #888888",
                "border": "0.5px solid black",
                "color": "black"
            }), 
            $("#save_"+editValue).show();
            $(this).hide();
            console.log(editValue);
        });
    });

    $(".tomSave").each(function(i){
        $(this).on("click", function(){
            var editValue = $(this).attr("editId");

            var column = editValue;
            var value = $("#"+editValue).val();

            $.ajax({
                type: "POST",
                data: {
                    column:column,
                    value:value
                },
                url: "../administrator/trnsql/sql.setting_admin.php",
                success: function(response){
                    $.toast({
                        heading: 'Data disimpan',
			            text: 'Data Berhasil Disimpan',
			            position: 'mid-center',
			            loaderBg: 'white',
			            icon: 'info',
			            hideAfter: 1000,
			            stack: 6
                    });
                }
            });


            
            $("#"+editValue).attr("readonly", true).css({"box-shadow" : "", "border": ""});
            $("#edit_"+editValue).show();
            $(this).hide();
            console.log(editValue);
            $(".form-control").css("color","gray");
        });
    });
})
    
</script>