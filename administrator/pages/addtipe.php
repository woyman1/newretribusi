

<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>

        <?php include ("../inc/inc.pilih_ikon.php"); ?>

        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>
        		
        			  	<form action="../administrator/trnsql/sql.tipe.php" method="post" id="fileForm" class="form-horizontal" enctype="multipart/form-data">

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_tipe" required> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kode Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="kode_tipe" required> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Harga Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="harga_tipe" required> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Icon Tipe </label>

                            
                                
                            <div class="col-sm-9 m-t-5">

                            <?php
                                $queryIcon = "SELECT type_icon FROM re_type";
                                $hasil = $re_connect->query($queryIcon);

                                $dataIcons = array();
                                while ($baris = $hasil->fetch_assoc()){
                                    $dataIcons[] = $baris['type_icon'];
                                }
                            
                                foreach ($icon as $ikon) :
                            ?>
                                <label>
                                  <input type="radio" name="fb" value="<?=$ikon?>" class="radio_z" 
                                    <?php
                                        if (in_array($ikon, $dataIcons)) {
                                            echo "disabled";
                                        }
                                    ?>

                                   />

                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 50" height="24px" width="50px" 
                                    <?php
                                        if (!in_array($ikon, $dataIcons)) :
                                    ?>

                                    style="fill: #000000"
                                    <?php else : ?>

                                    style="fill: #808080" data-toggle="tooltip" data-placement="right" title="Ikon sudah dipilih untuk data lain!"

                                    <?php endif ?>

                                  >
                                        <path d="<?=$ikon?>"></path>
                                  </svg>
                                </label>
                            <?php endforeach ?>
                            </div>
                            



                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" name="create" class="btn btn-info waves-effect waves-light m-t-10">Tambahkan</button>
                                    </div>
                                </div>
                      	</form>


        	</div>
        </div>

	</div>
    <pre>

    </pre>
</div>          

<script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
 <link rel="stylesheet" href="../plugins/bower_components/dropify/dist/css/dropify.min.css">

<script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

</script>
