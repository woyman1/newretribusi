<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">

        	    <div class="col-sm-12"> <div class="pull-right"><a href="index.php?page=addtransaksi"><button type="button" class="btn btn-info waves-effect waves-light m-t-10" ><i class="icon-plus m-t-5"> </i>Masukkan Pembayaran</button></a></div> </div>

        		<h2>Daftar <?php echo $pageName;?></h2>

                    <div class="table-responsive">
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Id Pembayaran</th>
                                    <th>Tanggal</th>
                                    <th>Nominal</th>
                                    <th>Bulan</th>
                                    <th>Tahun</th>
                                    <th>Total</th>
                                    <th>Costumer</th>
                                    <th>Petugas</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                                    <?php
                                        $query = mysqli_query($re_connect, "SELECT * FROM re_pembayaran GROUP BY bayar_key ORDER BY bayar_date DESC");
                                        while($data = mysqli_fetch_array($query)){
                                    ?>
                                <tr>
                                    <td><?php echo $data['bayar_key'] ?></td>
                                    <td><?php echo $data['bayar_date']; ?></td>
                                    <td>Rp. <?php echo number_format($data['bayar_nominal'], 0,',','.'); ?></td>
                                    <td><?php echo $data['bayar_bulan']; ?></td>
                                    <td><?php echo $data['bayar_tahun']; ?></td>
                                    <td>Rp. <?php echo number_format($data['bayar_total'], 0,',','.'); ?></td>
                                    <td><?php
                                        $costId = $data['bayar_cost_id'];
                                        $cost = mysqli_query($re_connect, "SELECT cost_name FROM re_costumer WHERE cost_id = '$costId' ");
                                        $s = mysqli_fetch_assoc($cost); echo $s['cost_name'];
                                     ?></td>
                                    <td><?php
                                        $costId = $data['bayar_users_id'];
                                        $cost = mysqli_query($re_connect, "SELECT re_user_username FROM re_users WHERE re_user_id = '$costId' ");
                                        $s = mysqli_fetch_assoc($cost); echo $s['re_user_username'];
                                     ?></td>
                                    <td>

                                      <?php

                                        $cost_id_x = $data['bayar_cost_id'];

                                        $max_id_q = mysqli_query($re_connect, "SELECT MAX(bayar_date) as bayar, bayar_cost_id FROM `re_pembayaran` WHERE bayar_cost_id = $cost_id_x");
                                        $max_id = mysqli_fetch_assoc($max_id_q);

                                      ?>

                                    <a href="index.php?page=viewpembayaran&id=<?php echo $data['bayar_cost_id'];?>&idKey=<?php echo $data['bayar_key'];?>" data-toggle="tooltip" data-original-title="Lihat Pembayaran"> <i class="fa fa-search text-info m-r-10"></i></a>

                                    <?php if ($levelUser == 1 or $levelUser==2) : ?>




                                    <a href="index.php?page=kwitansi&id=<?php echo $data['bayar_cost_id'];?>&idKey=<?php echo $data['bayar_key'];?>" data-toggle="tooltip" data-original-title="Lihat Kwitansi"> <i class="fa fa-file-text text-success m-r-10"></i></a>

                                    <?php if ($max_id['bayar_cost_id'] == $data['bayar_cost_id']) : ?>

                                    <?php if ($max_id['bayar'] == $data['bayar_date']) : ?>

                                    <a href="index.php?page=edittransaksi&bulan=<?php echo $data['bayar_bulan']; ?>&id=<?php echo $data['bayar_cost_id'];?>&idKey=<?php echo $data['bayar_key'];?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-info m-r-10"></i></a>

                                    <a href="trnsql/sql.transaksi.php?action=delete&id=<?php echo $data['bayar_key'] ?>" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger m-r-10"></i></a>

                                    <?php endif ?>
                                    <?php endif ?>

                                    <?php endif ?>
                                        </td>

                                </tr>
                               <?php } ?>
                            </thead>
                        </table>
                    </div>

        	</div>
        </div>

	</div>
</div>


<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <!-- start - This is for export functionality only -->
<script src="../assets/js/dataTables.buttons.min.js"></script>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable(
          {
            "order": [
                    [1, 'desc']
                ],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
          });
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 1
                }],
                "order": [
                    [1, 'desc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([1, 'asc']).draw();
                } else {
                    table.order([1, 'desc']).draw();
                }
            });
        });
    });

    </script>

    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
