    

<?php if (!$page) {header("location:index.php=404");} else {}; 
session_start();
    
    unset($_SESSION['bayar_key']);
    unset($_SESSION['total']);
    unset($_SESSION['bulan']);
    unset($_SESSION['id_costumer_bayar']);

?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>
 

        <div class="row">
        	<div class="col-lg-12 white-box"> 

                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">From Add Transaksi</h3>
                            <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>
                            <div class="row line-steps">
                                <div class="col-md-3 column-step  active">
                                    <div class="step-number">1</div>
                                    <div class="step-title">ID Pelanggan</div>
                                    <div class="step-info">Input Id Pelanggan</div>
                                </div>
                                <div class="col-md-3 column-step">
                                    <div class="step-number">2</div>
                                    <div class="step-title">Bayar Bulanan</div>
                                    <div class="step-info">Bayar Bulanan</div>
                                </div>
                                <div class="col-md-3 column-step ">
                                    <div class="step-number">3</div>
                                    <div class="step-title">Add On</div>
                                    <div class="step-info">Add On</div>
                                </div>
                                <div class="col-md-3 column-step ">
                                    <div class="step-number">4</div>
                                    <div class="step-title">SELESAI</div>
                                    <div class="step-info">Selesai</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        		<h3 class="box-title m-b-0">Masukkan ID Pelanggan</h3>
                
        		
        			<form action="../administrator/trnsql/sql.transaksi.php" method="post" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-body">

                            <div class="row">
                                <div class="form-group col-sm-4"> 
                                    <label class="control-label col-form-label m-l-10">ID Costumer</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="id_costumer_bayar" id="id_cost" required> 
                                </div>  </div>

                                <div class="form-group col-sm-4"> 
                                    <label class="control-label col-form-label m-l-10">Nama Costumer</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="nama_costumer" name="trans_cost_name"> 
                                </div>  </div>

                                <div class="form-group col-sm-2"> 
                                    <label class="control-label col-form-label m-l-10">Foto</label>
                                <div class="col-sm-12">
                                    <img src="" class="img-responsive" id="pic" alt="Tidak ada gambar">
                                </div>  </div>

                            </div>
                            
                           <div class="form-group text-right">
                                <button type="submit" name="step1" class="btn btn-info waves-effect waves-light m-r-20">Selanjutnya</button>
                            </div>
                        </div>
                    </form>
        	</div>
        </div>

	</div>
</div>         
<link rel="stylesheet" href="../assets/autocomplate/jquery-ui.css">

<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<script src="../plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="../plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>

<script type="text/javascript">
    
    $( document ).ready(function() {

        $(function(){
            $("#id_cost").autocomplete({
                source: "trnsql/autocomplete_addtransaksi.php",
                select: function(event, ui) {
                    $('#nama_costumer').val('' + ui.item.coba); // menampilkan nama di field nama 
                    $('#pic').attr('src', '../uploads/images/arsip/' + ui.item.foto);
                }

            });

        });

        $(".count").click(function(){

            var total = 0;
            $(".count:checked").each(function() {
                total += 1;
            }); 

            $("#nominal_bayar").val(total);

        });



        // $(function() {
        //   enable_cb();
        //   $("#group1").click(enable_cb);
        // });

          
                        
    });

</script>


    <script src="../assets/autocomplate/jquery-ui.js"></script>

