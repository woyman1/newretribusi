<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h2>Daftar <?php echo $pageName;?></h2>
        		      
                    <div class="table-responsive">
                        <table id="myTable" class="table table-striped">                
                            <thead>
                                <tr>
                                    <th>Nama Desa</th>
                                    <th>Kode Desa</th>
                                    <th>Kecamatan</th>
                                    <th>Action</th>
                      
                                </tr>
                            </thead>
                                    <?php 
                                        $query = mysqli_query($re_connect, "SELECT * FROM re_desa ORDER BY desa_id DESC");
                                        while($data = mysqli_fetch_array($query)){
                                    ?>
                                <tr>
                                    <td><?php echo $data['desa_name']; ?></td>
                                    <td><?php echo $data['desa_code']; ?></td>
                                    <td>
                                        <?php
                                            $idKec = $data['idKec'];
                                            $q_nmKec = "SELECT namaKec FROM re_kecamatan WHERE idKec = '$idKec'";

                                            $r_nmKec = $re_connect->query($q_nmKec);
                                        ?>
                                        <?php print_r($r_nmKec->fetch_assoc()['namaKec']);?>
                                    </td>
                                    <td>
                                         <a href="index.php?page=viewdesa&id=<?php echo $data['desa_id'];?>" data-toggle="tooltip" data-original-title="Lihat Desa <?php echo$data['desa_name']; ?>"> <i class="fa fa-search text-info m-r-10"></i></a>
                                         <?php if ($levelUser == 1 || $levelUser ==2) : ?>
                                         
                                         <a href="index.php?page=editdesa&id=<?php echo $data['desa_id'];?>&idKec=<?=$idKec?>" data-toggle="tooltip" data-original-title="Edit Desa <?php echo$data['desa_name']; ?>"> <i class="fa fa-pencil text-success m-r-10"></i></a>
                                        <a href="trnsql/sql.desa.php?action=delete&id=<?php echo $data['desa_id'];?>" data-toggle="tooltip" data-original-title="Delete Desa <?php echo$data['desa_name']; ?>"> <i class="fa fa-close text-danger m-r-10"></i></a>
<?php endif?>
                                    </td>
                                </tr>
                               <?php } ?>
                            </thead>
                        </table>
                    </div>
        			  	
        	</div>
        </div>

	</div>
</div>  


<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <!-- start - This is for export functionality only -->
<script src="../assets/js/dataTables.buttons.min.js"></script>       

<script>
    $(document).ready(function() {
        $('#myTable').DataTable(
          {
            "order": [
                    [2, 'desc']
                ],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
          });
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 1
                }],
                "order": [
                    [1, 'desc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([1, 'asc']).draw();
                } else {
                    table.order([1, 'desc']).draw();
                }
            });
        });
    });
  
    </script>
    
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>