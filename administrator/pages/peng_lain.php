<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Pengaturan Lain</h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">

            <?php
                $q_setting = "SELECT * FROM re_settingsite";
                $r_setting = $re_connect->query($q_setting);

                if ($r_setting->num_rows >0) {
                    $row = $r_setting->fetch_assoc();
                }
            ?>
                <div class="form-group row">
                    <div class="col-lg-4">Kendaraan Dump Truck</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="dump_truck" type="text" class="form-control" value="<?=$row['dump_truck']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="dump_truck" id="edit_dump_truck"><i class="fa fa-edit"></i> Edit</button>
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save" type="button" editId="dump_truck" id="save_dump_truck"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Kendaraan Amroll</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="amroll" type="text" class="form-control" value="<?=$row['amroll']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="amroll" id="edit_amroll"><i class="fa fa-edit"></i> Edit</button>
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save"
                        type="button" editId="amroll" id="save_amroll"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Kendaraan Roda 3</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="roda_tiga" type="text" class="form-control" value="<?=$row['roda_tiga']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="roda_tiga" id="edit_roda_tiga"><i class="fa fa-edit"></i> Edit</button>
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save"
                        type="button" editId="roda_tiga" id="save_roda_tiga"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Kendaraan Excavator</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="excavator" type="text" class="form-control" value="<?=$row['excavator']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="excavator" id="edit_excavator"><i class="fa fa-edit"></i> Edit</button>
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save"
                        type="button" editId="excavator" id="save_excavator"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">Kendaraan Doser</div>
                    <div class="col-lg-6">
                        <div class="field">
                            <input id="doser" type="textarea" class="form-control" value="<?=$row['doser']?>" readonly>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-info tomEdit" data-toggle="tooltip" data-original-title="Edit" type="button" editId="doser" id="edit_doser"><i class="fa fa-edit"></i> Edit</button>
                        <button class="btn btn-info tomSave" data-toggle="tooltip" data-original-title="Save"
                        type="button" editId="doser" id="save_doser"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>

                <!-- end here -->

        	</div>
        </div>

	</div>
</div>

<script type="text/javascript">

$(document).ready(function(){
    $(".tomSave").hide().attr({
        "data-toggle": "tooltip",
        "data-original-title": "save",
        "class": "btn btn-success tomSave"
    });

    $(".tomEdit").attr({
       "data-toggle": "tooltip",
        "data-original-title": "Edit"
    });

    $(".form-control").css("color","gray");


    $(".tomEdit").each(function(i){
        $(this).on("click", function(){
            var editValue = $(this).attr("editId");
            $("#"+editValue).attr({
                "readonly": false,

            }).css({
                "box-shadow":"10px 10px 5px #888888",
                "border": "0.5px solid black",
                "color": "black"
            }),
            $("#save_"+editValue).show();
            $(this).hide();
            console.log(editValue);
        });
    });

    $(".tomSave").each(function(i){
        $(this).on("click", function(){
            var editValue = $(this).attr("editId");

            var column = editValue;
            var value = $("#"+editValue).val();

            $.ajax({
                type: "POST",
                data: {
                    column:column,
                    value:value
                },
                url: "../administrator/trnsql/sql.setting_admin.php",
                success: function(response){
                    $.toast({
                        heading: 'Data disimpan',
			            text: 'Data Berhasil Disimpan',
			            position: 'mid-center',
			            loaderBg: 'white',
			            icon: 'info',
			            hideAfter: 1000,
			            stack: 6
                    });
                }
            });



            $("#"+editValue).attr("readonly", true).css({"box-shadow" : "", "border": ""});
            $("#edit_"+editValue).show();
            $(this).hide();
            console.log(editValue);
            $(".form-control").css("color","gray");
        });
    });
})

</script>
