

<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"> 
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>

<?php 
    $query = mysqli_query($re_connect, "SELECT * FROM re_type WHERE type_id = '$idGet' ");
    while($data = mysqli_fetch_array($query)){
?>
        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h3 class="box-title m-b-0">Lihat <?php echo $data['type_name'];?></h3>
                <p class="text-muted m-b-30 font-13"> </p>
        		

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_tipe" value="<?php echo $data['type_name'] ?>" disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Kode Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="kode_tipe" value="<?php echo $data['type_code'] ?>" disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Harga Tipe</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="harga_tipe" value="<?php echo $data['type_harga'] ?>" disabled> 
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Icon Tipe </label>
                                
                            <div class="col-sm-9 m-t-5">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 50" height="24px" width="50px" style="fill: #808080">
                                        <path d="<?=$data['type_icon']?>"></path>
                                  </svg>
                            </div>
                            
                            </div>


                            <?php if ($levelUser == 1 or $levelUser==2) : ?>
                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <a href="index.php?page=edittipe&id=<?=$idGet?>"><button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Edit</button></a>
                                    </div>
                                </div>
                                <?php endif ?>
                
        	</div>
        </div>

        <?php } ?>

	</div>
</div>          

