

<?php if (!$page) {header("location:index.php=404");} else {}; ;?>

<div id="page-wrapper">
    <div class="container-fluid">
    	<div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?php echo $pageName;?></h4>
			</div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
					<li><a href="index.php?page=home">Dashboard</a></li>
                    <li class="active"><?php echo $pageName;?></li>
                </ol>
            </div>
        </div>


        <div class="row">
        	<div class="col-lg-12 white-box">
        		<h3 class="box-title m-b-0">Form <?php echo $pageName;?></h3>
                <p class="text-muted m-b-30 font-13"> *Lengkapi Form Dengan Benar </p>

        			  	<form action="../administrator/trnsql/sql.datagis.php" method="post" class="form-horizontal" enctype="multipart/form-data">

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Nama Data Gis</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nama_datagis" required>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Tipe Data Gis</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipe_datagis" required>
                                        <option value="">-- Tipe --</option>
                                <?php $query = mysqli_query($re_connect, "SELECT * FROM re_datagis_type");
                                      while($type = mysqli_fetch_array($query)){ ?>
                                        <option value="<?php echo $type['typegis_id']?>"><?php echo $type['typegis_name'] ?></option>
                                <?php } ?>
                                </select>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Deskripsi</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="4" name="des_datagis"></textarea>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Ambil Coordinate</label>
                            <div class="col-sm-9">
                              <input id="pac-input" class="controls form-control" type="text" placeholder="Cari tempat...">
                              <div id="map" style="height:400px;border: 2px solid #3872ac; margin-bottom:0px;"></div>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Langitude</label>
                            <div class="col-sm-9">
                                <input type="text" id="lon" class="form-control" name="lang_datagis" placeholder="999.9999999999" required>
                            </div>  </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label col-form-label">Lattitude</label>
                            <div class="col-sm-9">
                                <input type="text" id="lat" class="form-control" name="latt_datagis" placeholder="999.9999999999" required>
                            </div>  </div>

                           <div class="form-group m-b-0">
                                    <div class="offset-sm-3 col-sm-9">
                                        <button type="submit" name="create" class="btn btn-info waves-effect waves-light m-t-10">Tambahkan</button>
                                    </div>
                                </div>
                      	</form>
        	</div>
        </div>

	</div>
</div>

<script>
  function initMap() {
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -8.4991, lng: 140.404984},
      zoom: 13,
      fullscreenControl: false,
      mapTypeControl: false
    });

    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    function markerCoords(markerobject){
      google.maps.event.addListener(markerobject, 'dragend', function(evt){
          console.log("lat: "+evt.latLng.lat().toFixed(3)+" lon: "+evt.latLng.lng().toFixed(3));
      });

      google.maps.event.addListener(markerobject, 'drag', function(evt){
          console.log("marker is being dragged");
          console.log("lat: "+evt.latLng.lat().toFixed(3)+" lon: "+evt.latLng.lng().toFixed(3));

          $('#lat').val(evt.latLng.lat().toFixed(6));
          $('#lon').val(evt.latLng.lng().toFixed(6));
      });
    }

    var marker = new google.maps.Marker({
      position: {lat: -8.4991, lng: 140.404984},
      map: map,
      title: 'Pilih Lokasi!',
      draggable:true,
    });

    markerCoords(marker);

    google.maps.event.addListener(map, 'click', function(evt) {
       console.log("lat: "+evt.latLng.lat().toFixed(3)+" lon: "+evt.latLng.lng().toFixed(3));
        var latlng = new google.maps.LatLng(evt.latLng.lat().toFixed(6), evt.latLng.lng().toFixed(6));
        marker.setPosition(latlng);
        $('#lat').val(evt.latLng.lat().toFixed(6));
        $('#lon').val(evt.latLng.lng().toFixed(6));
        });

      searchBox.addListener('places_changed', function() {
       var places = searchBox.getPlaces();

       if (places.length == 0) {
         return;
       }

       places.forEach(function(place) {
        marker.setPosition(place.geometry.location);
        $('#lat').val(place.geometry.location.lat().toFixed(6));
        $('#lon').val(place.geometry.location.lng().toFixed(6));
        map.setCenter(marker.getPosition());
       });

     });

     $("#pac-input").click(function(event) {
       $(this).val('');
     });

  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwx2LQQM6zmHj7oqZfI_oDrAuuXXN3tBk&callback=initMap&libraries=places"></script>
