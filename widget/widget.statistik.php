<!-- masukkan ini di head:

        <link href="assets/frontend/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        
        <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        
        <link href="assets/frontend/css/animate.css" rel="stylesheet">
        
        <link href="assets/frontend/css/style.css" rel="stylesheet">
        <link href="assets/frontend/css/colors/blue.css" id="theme" rel="stylesheet">
 -->

<div class="container">
    <?php
        include ("config/db.connect.php");

        //data bulan ini

        $q_pemasukan_bulan_ini = "SELECT SUM(bayar_total)as jumlah FROM re_pembayaran WHERE MONTH(bayar_date) = MONTH(CURRENT_DATE()) AND YEAR(bayar_date) = YEAR(CURRENT_DATE())";
         
        $r_pemasukan_bulan_ini = $re_connect->query($q_pemasukan_bulan_ini);
         
        if ($r_pemasukan_bulan_ini->num_rows >0) {
            $pemasukan_bulan_ini = $r_pemasukan_bulan_ini->fetch_assoc();
        }

        //data bulan lalu

        $bulan = date('m');
        $tahun = date('Y');
        
        if ($bulan == 1){
            $bulan_past = 12;
            $tahun_past = $tahun-1;
        }else{
            $bulan_past = $bulan - 1;
            $tahun_past = $tahun;
        }

        $q_pemasukan_bulan_lalu = "SELECT SUM(bayar_total)as jumlah FROM re_pembayaran WHERE MONTH(bayar_date) = $bulan_past AND YEAR(bayar_date) = $tahun_past";

        $r_pemasukan_bulan_lalu = $re_connect->query($q_pemasukan_bulan_lalu);

        if ($r_pemasukan_bulan_lalu->num_rows >0) {
            $pemasukan_bulan_lalu = $r_pemasukan_bulan_lalu->fetch_assoc();
        }

        //data tahun ini

        $q_pemasukan_tahun_ini = "SELECT SUM(bayar_total)as jumlah FROM re_pembayaran WHERE YEAR(bayar_date) = YEAR(CURRENT_DATE())";

        $r_pemasukan_tahun_ini = $re_connect->query($q_pemasukan_tahun_ini);

        if ($r_pemasukan_tahun_ini->num_rows >0) {
            $pemasukan_tahun_ini = $r_pemasukan_tahun_ini->fetch_assoc();
        }

        //target
        $q_target = "SELECT setting_target_pemasukan FROM re_settingsite WHERE setting_id = 1";
        $r_target = $re_connect->query($q_target);

        if ($r_target->num_rows >0) {
            $target = $r_target->fetch_assoc();
        }

        $persentase = ($pemasukan_tahun_ini['jumlah']/$target['setting_target_pemasukan'])*100;

    ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">Pemasukan Bulan Ini <?=$bulan?></div>
                    <div class="col-lg-8"><?php echo ''.($pemasukan_bulan_ini['jumlah'] != NULL) ? 'Rp '.number_format($pemasukan_bulan_ini['jumlah'], 0, ',', '.') : '0' ?></div>
                </div>

                <div class="row">
                    <div class="col-lg-4">Pemasukan Bulan Kemarin</div>
                    <div class="col-lg-8"><?php echo ''.($pemasukan_bulan_lalu['jumlah'] != NULL) ? 'Rp '.number_format($pemasukan_bulan_lalu['jumlah'], 0, ',', '.') : '0' ?></div>
                </div>

                <div class="row">
                    <div class="col-lg-4">Pemasukan Tahun Ini</div>
                    <div class="col-lg-8"><?php echo ''.($pemasukan_tahun_ini['jumlah'] != NULL) ? 'Rp '.number_format($pemasukan_tahun_ini['jumlah'], 0, ',','.') : '0' ?></div>
                </div>

                <div class="row">
                    <div class="col-lg-4">Target</div>
                    <div class="col-lg-8"><?php echo ''.($target['setting_target_pemasukan'] != NULL) ? 'Rp '.number_format($target['setting_target_pemasukan'], 0, ',', '.') : '0' ?></div>
                </div>

                <div class="row">
                    <div class="col-lg-4">Progress</div>
                    <div class="col-lg-8">
                        <div class="progress progress-lg">
                            <div class="progress-bar progress-bar-success active progress-bar-striped" style="width: <?=round($persentase)?>%;" role="progressbar"><?=round($persentase,2)?>%</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


        <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="assets/frontend/bootstrap/dist/js/tether.min.js"></script>
        <script src="assets/frontend/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Sidebar menu plugin JavaScript -->
        <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--Slimscroll JavaScript For custom scroll-->
        <script src="assets/frontend/js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="assets/frontend/js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="assets/frontend/js/custom.min.js"></script>
        <script src="assets/frontend/js/MeasureTool.js"></script>