<script type="text/javascript">
        $(document).ready(function () {
                    $("#sembunyi").click(function () {
                        $('#legend').toggle("slow");
                    });

                    $("#legend").hide();

                    var icon3 = {
                        path: "M7.167 615l620-608q8-7 18.5-7t17.5 7l608 608q8 8 5.5 13t-12.5 5h-175v575q0 10-7.5 17.5t-17.5 7.5h-250q-10 0-17.5-7.5t-7.5-17.5V833h-300v375q0 10-7.5 17.5t-17.5 7.5h-250q-10 0-17.5-7.5t-7.5-17.5V633h-175q-10 0-12.5-5t5.5-13z",
                        fillColor: '#000000',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.014,
                        ask: "assets/icon/home.svg"
                    }

                    var tps = {
                        path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                        fillColor: '#0000ff',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.5,
                        ask: "assets/icon/trash.svg"
                    }

                    var tpa = {
                        path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                        fillColor: '#ff0066',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.5,
                        ask: "assets/icon/trash.svg"
                    }

                    var kontainer = {
                        path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                        fillColor: '#ffa500',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.5,
                        ask: "assets/icon/trash.svg"
                    }

                    var sampahLiar = {
                        path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                        fillColor: '#000000',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.5,
                        ask: "assets/icon/trash.svg"
                    }

            

            <?php
                $query_type = mysqli_query($re_connect, "SELECT * FROM re_type");
                while($data_type = mysqli_fetch_array($query_type)) :  
            ?>
            
            var <?=$data_type["type_name"]?> = {
                path: "<?=$data_type["type_icon"]?>",
                fillColor: '#008000',
                fillOpacity: 1,
                anchor: new google.maps.Point(0,0),
                strokeWeight: 0,
                scale: 0.5,
            }
            <?php endwhile ?>

        var myCenter = new google.maps.LatLng(-4.2699,138.0804);

        var places={

            <?php
                $query_cost = mysqli_query($re_connect, "SELECT * FROM re_type");

                while($data_cost = mysqli_fetch_array($query_cost)) :
            ?>
                <?php
                    $type_name = $data_cost["type_name"];
                ?>
                <?=$data_cost["type_name"]?> : {
                    label : '<?=$data_cost["type_name"]?>',
                    checked: true,
                    icon:<?=$data_cost["type_name"]?>,
                    items:[
                         <?php
                        $people = mysqli_query($re_connect, "SELECT * FROM re_costumer inner join re_type on re_costumer.cost_type_id=re_type.type_id where re_type.type_name = '$type_name'");

                        while($data_people = mysqli_fetch_array($people)) : ?>
                            ['<?=$data_people["cost_name"]?>' , <?=$data_people["cost_latt"]?>  , <?=$data_people["cost_lang"]?> ],

                         <?php endwhile ?>
                    ]   
                },

            <?php endwhile ?>
 
            tps:{
                label:' TPS',
                checked:true,
                icon:tps,
                items:[
                    <?php $query = mysqli_query($re_connect, "SELECT * from re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type=re_datagis_type.typegis_id WHERE data_type = 1");
                        while($data = mysqli_fetch_array($query)) :?>                     

                        ['<?=$data["data_name"]?>' , <?=$data["data_lat"]?>  , <?=$data["data_lon"]?> ],

                    <?php endwhile ?>
                ]
            },

            tpa:{
                label:' TPA',
                checked:true,
                icon:tpa,
                items:[
                    <?php $query = mysqli_query($re_connect, "SELECT * from re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type=re_datagis_type.typegis_id WHERE data_type = 2");
                        while($data = mysqli_fetch_array($query)) :?>                     
                           
                        ['<?=$data["data_name"]?>' , <?=$data["data_lat"]?>  , <?=$data["data_lon"]?> ],

                    <?php endwhile ?>
                ]
            },

            kontainer:{
                label:' kontainer',
                checked:true,
                icon:kontainer,
                items:[
                    <?php $query = mysqli_query($re_connect, "SELECT * from re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type=re_datagis_type.typegis_id WHERE data_type = 3");
                    while($data = mysqli_fetch_array($query)) :?>                     
                       
                    ['<?=$data["data_name"]?>' , <?=$data["data_lat"]?>  , <?=$data["data_lon"]?> ],

                <?php endwhile ?>
                ]
            },

            sampahLiar:{
                label:' Sampah Liar',
                checked:true,
                icon:sampahLiar,
                items:[
                    <?php $query = mysqli_query($re_connect, "SELECT * from re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type=re_datagis_type.typegis_id WHERE data_type = 4");
                    while($data = mysqli_fetch_array($query)) :?>                     
                       
                    ['<a class="btn btn-primary" href="index.php?page=gis"><?=$data["data_name"]?></a>' , <?=$data["data_lat"]?>  , <?=$data["data_lon"]?> ],

                <?php endwhile ?>
                ]
            }
        };

        var mapProp = {
            center: new google.maps.LatLng(-3.5355560513505457, 133.64749157187498),
            zoom: 7,
            mapTypeControl: true,
            overviewMapControl: true,
            overviewMapControlOptions: {
                opened: false,
            },

            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },

            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            fullscreenControl: false
        };

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        infowindow = new google.maps.InfoWindow();

        // control area
        ctrl = $('<div/>').css({
            background: '#fff',
            border: '1px solid #000',
            padding: '4px',
            margin: '2px',
            textAlign: 'center',
            display: 'none',
        }).attr('id', 'markerz');

        ctrl.append($('<input>', {
                type: 'button',
                value: 'Tampilkan semua',
                class: 'btn btn-info btn-sm'
            })
            .click(function () {
                $(this).parent().find('input[type="checkbox"]')
                    .prop('checked', true).trigger('change');
            }));
        ctrl.append($('<br/><br/>'));

        //clear all-button
        ctrl.append($('<input>', {
                type: 'button',
                value: 'Sembunyikan semua',
                class: 'btn btn-info btn-sm'
            })
            .click(function () {
                $(this).parent().find('input[type="checkbox"]')
                    .prop('checked', false).trigger('change');
            }));

        ctrl.append($('<br/><br/>'));


        $.each(places, function (c, category) {

            //a checkbox fo the category
            var cat = $('<input>', {
                    type: 'checkbox'
                }).change(function () {
                    $(this).data('goo').set('map', (this.checked) ? map : null);
                })
                //create a data-property with a google.maps.MVCObject
                //this MVC-object will do all the show/hide for the category 
                .data('goo', new google.maps.MVCObject)
                .prop('checked', !!category.checked)

                //this will initialize the map-property of the MVCObject
                .trigger('change')

                //label for the checkbox
                .appendTo($('<div/>').css({
                    whiteSpace: 'nowrap',
                    textAlign: 'left'
                }).appendTo(ctrl))
                .after(category.label);

            //loop over the items(markers)
            $.each(category.items, function (m, item) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(item[1], item[2]),
                    title: item[0],
                    icon: category.icon
                });

                //bind the map-property of the marker to the map-property
                //of the MVCObject that has been stored as checkbox-data 
                marker.bindTo('map', cat.data('goo'), 'map');
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent(item[0]);
                    infowindow.open(map, this);
                });
            });

        });

        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(ctrl[0]);
        // end control area


        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(control);
        //legend area
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);
        //end legend area

        measureTool = new MeasureTool(map, {
            unit: MeasureTool.UnitTypeId.METRIC
        });

        $("#sembunyi_marker").click(function () {
            $('#markerz').toggle("slow");
        });


        $("#markerz").hide();
        });
 
    </script>    