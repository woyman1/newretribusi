<?php

function tpa_count() {
  global $re_connect;
  $query = "SELECT COUNT(data_name) as jumlah from re_datagis WHERE data_type = 2";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      return $row['jumlah'];
    }
  }else{
    return "0";
  }
}

function tpa_items(){
  global $re_connect;
  $query = "SELECT data_name from re_datagis WHERE data_type = 2";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;
  $item = array();

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      $item[] = $row['data_name'];
    }
    return implode(", ", $item);
  }else{
    return "Tidak ada";
  }
}

function tps_count(){
  global $re_connect;
  $query = "SELECT COUNT(data_name) as jumlah from re_datagis WHERE data_type = 1";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      return $row['jumlah'];
    }
  } else{
    return "0";
  }
}
function tps_items(){
  global $re_connect;
  $query = "SELECT data_name from re_datagis WHERE data_type = 1";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;
  $item = array();

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      $item[] = $row['data_name'];
    }
    return implode(", ", $item);
  }else{
    return "Tidak ada";
  }
}

function kont_count(){
  global $re_connect;
  $query = "SELECT COUNT(data_name) as jumlah from re_datagis WHERE data_type = 3";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      return $row['jumlah'];
    }
  }else{
    return "0";
  }
}

function kont_items(){
  global $re_connect;
  $query = "SELECT data_name from re_datagis WHERE data_type = 3";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;
  $item = array();

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      $item[] = $row['data_name'];
    }
    return implode(", ", $item);
  }else{
    return "Tidak ada";
  }
}
 ?>
