<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="index.php?page=home" class="waves-effect">
                    <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                    <span class="hide-menu">Home
                        <span class="fa arrow"></span>
                    </span>
                </a>

            </li>
            <li>
                <a href="index.php?page=about" class="waves-effect">
                    <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                    <span class="hide-menu">About
                        <span class="fa arrow"></span>
                    </span>
                </a>

            </li>
            <li>
                <a href="index.php?page=gis" class="waves-effect">
                    <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                    <span class="hide-menu">GIS
                        <span class="fa arrow"></span>
                    </span>
                </a>

            </li>
            <li>
                <a href="index.php?page=contact" class="waves-effect">
                    <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                    <span class="hide-menu">Kontak
                        <span class="fa arrow"></span>
                    </span>
                </a>

            </li>
            <li>
                <a href="index.php?page=sarpras" class="waves-effect">
                    <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                    <span class="hide-menu">Data Lain
                        <span class="fa arrow"></span>
                    </span>
                </a>

            </li>
            <li>
                <a href="index.php?page=sarpras" class="waves-effect">
                    <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                    <span class="hide-menu">Galeri
                        <span class="fa arrow"></span>
                    </span>
                </a>

            </li>
               
                </ul>
            </div>
        </div>