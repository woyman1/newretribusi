<div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="index.php?page=home"><b><img src="uploads/images/assets/<?php echo $logo;?>" alt="home" /></b><span class="hidden-xs"><img src="uploads/images/assets/<?php echo $logo_text;?>" alt="home" /></span></a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    <li>
                        <form role="search" class="app-search hidden-xs" action="index.php" method="get">
                            <input type="hidden" name="page" value="search">
                            <input type="text" placeholder="Cari..." class="form-control" name="keywords">
                            <a href="index.php?page=search"><i class="fa fa-search"></i></a>
                        </form>
                    </li> 
                </ul>  
                <ul class="nav navbar-top-links navbar-right pull-right">
                  
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="uploads/images/users/<?php echo $_SESSION['users_pic'];?>" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo $_SESSION['users_name'];?></b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="index.php?page=profile&id=<?php echo $_SESSION['users_id'];?>"><i class="ti-user"></i> My Profile</a></li>
                           
                            <li role="separator" class="divider"></li>
                            
                            <li><a href="index.php?page=logout"><i class="fa fa-power-off"></i> Logout</a></li> 
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown --> 
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu-horizontal">
                    
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="../uploads/images/users/<?php echo $picUser;?>" alt="user-img" class="img-circle"> <span class="hide-menu"><?php echo $nameUser;?>
                            <span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="index.php?page=profile&id=<?php echo $idUser;?>"><i class="ti-user"></i> My Profile</a></li>   
                            <li><a href="../index.php?page=logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>
                    <li> <a href="index.php?page=home" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </span></a></li>
                 
                    <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="&#xe009;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Data Survey<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=data">List Data</a> </li>
                            <li> <a href="index.php?page=maps">Map Data</a> </li>
                        </ul>
                    </li>
                     <!--li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="&#xe00d;" class="linea-icon linea-elaborate fa-fw"></i> <span class="hide-menu">Responden<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=responden">List Responden</a> </li>
                           
                        </ul>
                    </li-->
                    <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="O" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Gallery<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=gallery">List Gallery</a> </li>
                            
                           
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><i class="icon-people p-r-10"></i> <span class="hide-menu"> Users <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=users">List Users</a> </li>
                            <li> <a href="index.php?page=addusers">Add Users</a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="waves-effect"><i class="icon-chart p-r-10"></i> <span class="hide-menu"> Reports <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="index.php?page=generalreport">General Report</a></li>
                            <li> <a href="index.php?page=respondenreport">Responden Report</a></li>
                            <li> <a href="index.php?page=usersreport">Users Report</a></li>
                        </ul>
                    </li>
                    <li> <a href="#" class="waves-effect"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Setting<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="index.php?page=setting">General Setting</a></li>
                            <li><a href="index.php?page=messagehome">Dashboard Message</a></li>
                            <li><a href="index.php?page=logo">Logo & Banner</a></li>
                            </ul>
                    </li>
                     
                </ul>
            </div>
        </div>
</div>
