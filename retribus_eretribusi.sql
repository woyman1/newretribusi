-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 12, 2018 at 12:13 AM
-- Server version: 10.0.31-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `retribus_eretribusi`
--

-- --------------------------------------------------------

--
-- Table structure for table `re_costumer`
--

CREATE TABLE `re_costumer` (
  `cost_id` int(15) NOT NULL,
  `cost_id_pel` varchar(15) NOT NULL,
  `cost_name` varchar(100) NOT NULL,
  `cost_address` text NOT NULL,
  `cost_prov` varchar(50) NOT NULL,
  `cost_kab` varchar(50) NOT NULL,
  `cost_kec` varchar(50) NOT NULL,
  `cost_status` varchar(150) NOT NULL,
  `cost_date` datetime NOT NULL,
  `cost_periode_terakhir` date DEFAULT NULL,
  `cost_tmp_lahir` varchar(50) NOT NULL,
  `cost_tgl_lahir` date NOT NULL,
  `cost_phone` varchar(30) DEFAULT NULL,
  `cost_foto` varchar(255) DEFAULT NULL,
  `cost_lokasi` varchar(255) DEFAULT NULL,
  `cost_denah` varchar(255) DEFAULT NULL,
  `cost_ktp` varchar(255) DEFAULT NULL,
  `cost_lang` double DEFAULT NULL,
  `cost_latt` double DEFAULT NULL,
  `cost_type_id` int(15) NOT NULL,
  `cost_desa_id` int(15) NOT NULL,
  `cost_user_id` int(15) NOT NULL,
  `cost_jadwal_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_costumer`
--

INSERT INTO `re_costumer` (`cost_id`, `cost_id_pel`, `cost_name`, `cost_address`, `cost_prov`, `cost_kab`, `cost_kec`, `cost_status`, `cost_date`, `cost_periode_terakhir`, `cost_tmp_lahir`, `cost_tgl_lahir`, `cost_phone`, `cost_foto`, `cost_lokasi`, `cost_denah`, `cost_ktp`, `cost_lang`, `cost_latt`, `cost_type_id`, `cost_desa_id`, `cost_user_id`, `cost_jadwal_id`) VALUES
(12, 'RT040012', 'SANITA', ' JL. TERNATE, Gg. OKABA 2', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-09 19:25:49', '2018-12-09', 'MERAUKE', '1970-08-01', NULL, NULL, NULL, NULL, NULL, 140.38941, -8.501182, 1, 4, 12, 4),
(13, 'RT030013', 'SUYUDI', ' JL. GAK, Gg. SAYUR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-09 19:46:32', '2018-12-09', 'MERAUKE', '1970-08-01', NULL, NULL, NULL, NULL, NULL, 140.3975, -8.507899, 1, 3, 0, 4),
(14, 'RT120014', 'DARIUS LEWAR', ' JL. RAYA MANDALA Gg. KAWANUA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-09 19:55:23', '2018-12-09', 'MERAUKE', '1970-08-01', NULL, NULL, NULL, NULL, NULL, 140.41016, -8.519141, 1, 12, 0, 2),
(15, 'RT040015', 'RAMIADI', ' JL. TERNATE, Gg. H. KASIM (LINGKUP Gg. DEPAN MASJID)', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-09 20:12:49', '2018-12-09', 'MERAUKE', '1965-08-01', NULL, NULL, NULL, NULL, NULL, 140.383473, -8.496747, 1, 4, 0, 4),
(16, 'TS080016', 'TOKO SAGITA FORNITURE', ' JL. TMP', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-09 20:21:10', '2018-02-09', 'MERAUKE', '1970-08-01', NULL, NULL, NULL, NULL, NULL, 140.401363, -8.492534, 3, 8, 0, 0),
(17, 'RT120017', 'PRIYONO', ' JL.. RAYA MANDALA SPADEM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-09 21:48:39', '2018-05-09', 'MERAUKE', '1967-08-01', NULL, NULL, NULL, NULL, NULL, 140.411582, -8.520335, 1, 12, 28, 2),
(18, 'RT080018', 'MARYATI', ' JL. PEMBANGUNAN, Gg GEMPOL', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-09 23:41:52', '2018-12-09', 'MERAUKE', '1998-12-02', NULL, NULL, NULL, NULL, NULL, 140.414565, -8.502869, 1, 8, 0, 0),
(19, 'RT040019', 'H.ANWAR (37KK)', ' JL. RAYA MANDALA, Gg.H.JALIS', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-09 23:53:52', '2018-02-09', 'MERAUKE', '1994-03-01', NULL, NULL, NULL, NULL, NULL, 140.391433, -8.491579, 1, 4, 0, 0),
(20, 'RT090020', 'YONAS TANGDIESAK', ' Jl. Pompa Air, Gg. Batu Karang', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-10 06:03:28', '2018-01-10', 'MERAUKE', '1970-01-01', NULL, NULL, NULL, NULL, NULL, 140.408452, -8.512345, 1, 9, 0, 0),
(21, 'RT040021', 'NUR PITA', ' JL. TERNATE', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-11 00:23:54', '2019-02-11', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.389631, -8.501278, 1, 4, 0, 0),
(22, 'RT120022', 'RUDIANTO BOWO SASONGKO', ' JL. RAYA MANDALA SPADEM,Gg. Saner', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-11 00:35:02', '2017-12-11', 'merauke', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.411304, -8.520486, 1, 12, 0, 0),
(23, 'BGL040023', 'BENGKEL KASNEL', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-11 00:42:57', '2017-12-11', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.392456, -8.498674, 11, 4, 0, 0),
(24, 'RM120024', 'Warung Makan ERVINA', ' JL. RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-11 02:31:13', '2018-06-11', 'MERAUKE', '2018-11-01', NULL, NULL, NULL, NULL, NULL, 140.406257, -8.514966, 4, 12, 0, 0),
(25, 'RT090025', 'KARIAMAN', ' JL. ONGGATMIT', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-11 06:19:23', '2018-12-11', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.399035, -8.509885, 1, 9, 0, 0),
(27, 'RT070027', 'ALEXANDER LINIARDI', ' JL. POLDER III', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-11 06:26:33', '2018-09-11', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.400451, -8.484949, 1, 7, 0, 0),
(28, 'KI070028', 'AGUS LIFIN', ' JL. TMP POLDER', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-11 06:30:49', '2018-10-11', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.396568, -8.482982, 6, 7, 0, 0),
(29, 'RT090029', 'SUKARJO', ' JL. ONGGATMIT', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-11 06:52:04', '2018-12-11', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398467, -8.509838, 1, 9, 0, 0),
(30, 'RT120030', 'TOTOK SUDARMANTO', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-11 06:56:39', '2018-12-11', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402927, -8.511895, 1, 12, 0, 0),
(31, 'RT060031', 'BERLINDA E. OHOITIMUR', ' JL. RADIO, Gg. METRO', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 00:01:53', '2018-10-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383962, -8.485693, 1, 6, 0, 0),
(32, 'RM120032', 'WARUNG KETAPANG', ' JL. RAYA MANDALA SPADEM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 00:12:04', '2018-12-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.411551, -8.520841, 4, 12, 0, 0),
(33, 'RT040033', 'MUHAMAD ASHLAM', 'JL. IRIAN SERINGGU ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 00:17:55', '2018-12-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.388646, -8.500486, 1, 4, 0, 0),
(34, 'RT030034', 'ALIN NUR HAMIDAH (5 KK)', ' JL. GAK Gg. SAYUR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 00:28:45', '2018-02-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398388, -8.508266, 1, 3, 0, 0),
(35, 'RT040035', 'PURWATI', 'JL. TERNATE, Gg. OKABA II ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-12 00:38:21', '2018-01-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.38738, -8.500335, 1, 4, 0, 0),
(36, 'RT080036', 'FRANSISKA MARDIANA (5KK)', ' JL. PRAJURIT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-12 00:43:48', '2018-01-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.397305, -8.491087, 1, 8, 0, 0),
(37, 'RT040037', 'GABRIEL LETSON', ' JL. SERINGGU', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 01:27:39', '2018-12-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.393502, -8.498104, 1, 4, 0, 0),
(38, 'RT040038', 'MODESTA MITAKDA', 'JL. KAMPUNG TIMUR, GG. MITAK', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 01:34:12', '2018-12-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.392822, -8.496293, 1, 4, 0, 0),
(39, 'RT060039', 'EUVEMIA M.O. RESUBUN', ' JL. NUSA BARONG,Gg. ASPAL BARU ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 01:42:29', '2018-12-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.386941, -8.491313, 1, 6, 0, 0),
(40, 'RT020040', 'FATHUROHMAN (5 KK)', ' JL. AHMAD YANI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 01:56:10', '2018-05-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.407719, -8.510556, 1, 2, 0, 0),
(41, 'RT120041', 'NUR ASIYAH', ' JL. RAYA MANDALA, Gg. DARMO', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 02:09:07', '2018-06-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.409582, -8.519702, 1, 12, 0, 0),
(44, 'TS080044', 'TOKO NOBEL', ' JL. PRAJURIT', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 02:27:01', '2018-04-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.393705, -8.492698, 3, 8, 0, 0),
(45, 'RS070045', 'RUMAH SAKIT TNI-ANGKATAN LAUT', ' JL. TRIKORA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 02:34:02', '2018-03-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.391739, -8.484623, 10, 7, 0, 0),
(46, 'RT020046', 'NETTY P', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 02:46:59', '2018-06-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398384, -8.502307, 1, 2, 0, 0),
(47, 'RT020047', 'SONI L.', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 02:54:01', '2018-06-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.400604, -8.506278, 1, 2, 0, 0),
(48, 'RT020048', 'RINI TRI HAPSARI', ' JL.MARTADINATA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 03:32:59', '2018-09-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.410136, -8.514927, 1, 2, 0, 0),
(49, 'RT070049', 'PAK TAUFIK', ' JL. KIMAAM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 03:52:52', '2018-12-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.391561, -8.485591, 1, 7, 0, 0),
(50, 'RT070050', 'HAIRUDIN', ' JL. ASPOL', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 04:56:02', '2018-12-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.394492, -8.491472, 1, 7, 0, 0),
(51, 'RT060051', 'LIS HARMIASIH', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 05:04:17', '2018-06-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.387933, -8.491258, 1, 6, 0, 0),
(52, 'RT060052', 'SUKITO', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 05:08:36', '2018-08-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.387812, -8.491074, 1, 6, 0, 0),
(53, 'RT060053', 'KRISTINA WERMASUBUN', 'JL. NATUNA, BELAKANG  ASRAMA BRIMOB', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 06:33:42', '2018-03-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.386718, -8.489175, 1, 6, 0, 0),
(54, 'RT060054', 'SUPRIANTI', ' JL. NATUNA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-12 06:43:16', '2018-06-12', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.386728, -8.489079, 1, 6, 0, 0),
(55, 'RT060055', 'MANGGAR SUTOPO', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 03:02:48', '2018-12-13', 'MERAUKE', '2018-01-10', NULL, NULL, NULL, NULL, NULL, 140.387965, -8.491638, 1, 6, 0, 0),
(56, 'RT060056', 'MUHAMAD DAWI DIFINUBIN', 'JL. NUSA BARONG, Gg. ASPAL BARU ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 03:10:11', '2018-12-13', 'MERAUKE', '2018-01-10', NULL, NULL, NULL, NULL, NULL, 140.386765, -8.491784, 1, 6, 0, 0),
(57, 'TS020057', 'SULASTRI', ' JL. PENDIDIKAN ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-13 03:15:51', '2017-12-13', 'MERAUKE', '2018-01-10', NULL, NULL, NULL, NULL, NULL, 140.401379, -8.500752, 6, 2, 23, 1),
(58, 'RT080058', 'BEATA LETSOIN', 'JL. BRAWIJAYA ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 03:16:08', '2018-06-13', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.40794, -8.500778, 1, 8, 0, 0),
(59, 'RT030059', 'RAMDANI/RUMAH SEWABINTANG PUTRA (5KK) ', 'JL. GAK(DEPAN KELURAHAN BAMPEL)', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 03:46:04', '2018-03-13', 'MERAUKE', '1970-08-01', NULL, NULL, NULL, NULL, NULL, 140.393131, -8.504774, 1, 3, 0, 0),
(60, 'RT090060', 'ALBERD RUMLAWANG', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 03:52:14', '2018-05-13', 'MERAUKE', '1970-08-01', NULL, NULL, NULL, NULL, NULL, 140.417027, -8.507453, 1, 9, 0, 0),
(61, 'RT020061', 'TANTI YUA LESTARI', ' JL. RAYA MANDALA, GG. SOSKA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 04:05:42', '2018-03-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.392193, -8.493517, 1, 2, 0, 0),
(62, 'RT040062', 'RIFAI A. HAMDJA', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 04:14:51', '2018-04-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.391085, -8.499766, 1, 4, 0, 0),
(63, 'RT120063', 'SRI SUPATMI', ' JL. RAYA MANDALA, Gg. DIRGANTARA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 04:20:12', '2018-12-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.407563, -8.518035, 1, 12, 0, 0),
(64, 'KI020064', 'HAMZAH/KIOS', ' JL. AHMAD YANI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-13 04:26:57', '2017-12-13', 'MERAUKE', '1970-08-01', NULL, NULL, NULL, NULL, NULL, 140.408163, -8.510371, 6, 2, 0, 0),
(65, 'RT040065', 'SITTI A. K. LAITUPA', ' JL. TIDORE', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 04:43:47', '2018-06-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.387397, -8.49309, 1, 4, 0, 0),
(66, 'RT040066', 'FAHRIAH TAWAKAL', ' JL. RAYA MANDALA, Gg, KELINCI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 04:54:44', '2018-03-13', 'merauke', '1970-08-01', NULL, NULL, NULL, NULL, NULL, 140.389531, -8.488487, 1, 4, 0, 0),
(68, 'TS040068', 'MUHAMAD UMAR', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-13 05:27:01', '2018-01-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.391375, -8.499809, 3, 4, 0, 0),
(69, 'RT070069', 'YANUARIUS HARBELUBUN', ' JL. MARIND ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 05:33:38', '2018-03-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.391819, -8.484574, 1, 7, 23, 1),
(71, 'KI090071', 'SUHARI', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 05:44:18', '2018-06-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.420888, -8.519432, 6, 9, 0, 0),
(72, 'RT090072', 'ALAM', ' JL. RAYA MANDALA, GG. HINDUN', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 05:48:02', '2018-12-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.403125, -8.509497, 1, 9, 0, 0),
(73, 'RT040073', 'LIDIA SEYAWATI', 'JL. TERNATE ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 05:53:20', '2018-12-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.387941, -8.499469, 1, 4, 0, 0),
(74, 'RT040074', 'JULIATI (3 KK)', ' JL. TIMOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 05:56:12', '2018-06-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.391484, -8.492813, 1, 4, 0, 0),
(75, 'RT090075', 'H. HERNADI (LAYANAN RUMAH TANGGA)', ' JL. YOBAR 1', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:01:18', '2018-04-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.401526, -8.510095, 1, 9, 0, 0),
(76, 'KI090076', 'H. HERNADI (KIOS)', ' JL. YOBAR 1', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:05:00', '2018-04-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.401484, -8.510103, 6, 9, 0, 0),
(77, 'RT020077', 'H. HERNADI (LAYANAN RUMAH TANGGA II)', ' JL. BIAK', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:08:16', '2018-04-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.400395, -8.501547, 1, 2, 0, 0),
(78, 'KI120078', 'MUTMAINAH', ' JL. GARUDA SPADEM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:13:10', '2018-12-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.412275, -8.521611, 6, 12, 0, 0),
(79, 'KI090079', 'MUTMAINAH II', ' JL. GARUDA SPADEM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:17:58', '2018-12-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.412307, -8.521601, 6, 9, 0, 0),
(80, 'RT060080', 'KRISTANTO/ BOIRAH (5KK)', 'JL. NUSA BARONG, RMH SEWA YANG MASUK GANG DEPAN KIOS SAYUR PAKDE IMAM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:26:18', '2018-12-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.385651, -8.492564, 1, 6, 0, 0),
(81, 'RT020081', 'RAHMAT V. WINERUNGAN', ' JL. PARAKOMANDO', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:32:43', '2018-03-13', 'MERAUKE', '1970-01-08', NULL, NULL, NULL, NULL, NULL, 140.398911, -8.495625, 1, 2, 0, 0),
(82, 'KI020082', 'BUDI WIDODO', 'JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:41:18', '2018-12-13', 'MERAUKE', '1976-01-08', NULL, NULL, NULL, NULL, NULL, 140.396619, -8.501094, 6, 2, 0, 0),
(83, 'RT080083', 'IMELDA', ' JL. TRIKORA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 06:45:01', '2018-12-13', 'MERAUKE', '1965-01-08', NULL, NULL, NULL, NULL, NULL, 140.400856, -8.491093, 1, 8, 0, 0),
(84, 'RT040084', 'MUHAMAD UMAR (7 KK)', ' JL. TERNATE, Gg. MAWAR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-13 06:53:47', '2018-01-13', 'MERAUKE', '1975-01-08', NULL, NULL, NULL, NULL, NULL, 140.385859, -8.499936, 1, 4, 0, 0),
(85, 'RT020085', 'IRWAN RUSLI (4 KK)', ' JL. PENDIDIKAN', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 07:19:11', '2018-04-13', 'MERAUKE', '1977-01-08', NULL, NULL, NULL, NULL, NULL, 140.402583, -8.50101, 1, 2, 0, 0),
(87, 'RT060087', 'ZAINUDIN', ' JL. RADIO', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 07:31:02', '2018-06-13', 'MERAUKE', '1976-01-08', NULL, NULL, NULL, NULL, NULL, 140.382609, -8.486307, 1, 6, 0, 0),
(88, 'RT040088', 'SRI SUNARTI', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 07:33:35', '2018-12-13', 'MERAUKE', '1977-01-08', NULL, NULL, NULL, NULL, NULL, 140.390956, -8.50017, 1, 4, 0, 0),
(89, 'RT060089', 'HARI SLAMET WIBOWO', 'JL. TERNATE ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 07:43:09', '2018-12-13', 'MERAUKE', '1960-01-08', NULL, NULL, NULL, NULL, NULL, 140.381819, -8.492873, 1, 6, 0, 0),
(90, 'RT040090', 'SUTIMAN', ' JL. TIDORE', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 07:45:56', '2018-12-13', 'MERAUKE', '1977-01-08', NULL, NULL, NULL, NULL, NULL, 140.388963, -8.495233, 1, 4, 0, 0),
(91, 'KI020091', 'KIOS AULIA (MUSDALIFAH', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 07:48:25', '2018-02-13', 'MERAUKE', '1967-01-08', NULL, NULL, NULL, NULL, NULL, 140.399161, -8.50466, 6, 2, 0, 0),
(92, 'TS070092', 'TOKO SERBA LISTRIK', ' JL. ARU ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 07:52:51', '2018-02-13', 'MERAUKE', '1967-01-08', NULL, NULL, NULL, NULL, NULL, 140.393807, -8.492353, 3, 7, 23, 1),
(93, 'KI060093', 'PANGKAS RAMBUT NOARI', ' JL. NOARI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-13 07:56:49', '2018-01-13', 'MERAUKE', '1977-01-08', NULL, NULL, NULL, NULL, NULL, 140.382839, -8.488004, 6, 6, 0, 0),
(94, 'RT070094', 'SULAINAH', ' JL.MUTING POLDER', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 08:03:59', '2018-05-13', 'MERAUKE', '1977-01-08', NULL, NULL, NULL, NULL, NULL, 140.397157, -8.481659, 1, 7, 0, 0),
(95, 'RT040095', 'AHMAD MADAMAR', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-13 08:07:59', '2018-05-13', 'MERAUKE', '1979-01-08', NULL, NULL, NULL, NULL, NULL, 140.391714, -8.50198, 1, 4, 0, 0),
(96, 'RT060096', 'MARWANTO', ' JL. NATUNA G. LABOBAR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 00:49:00', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.382984, -8.49056, 1, 6, 0, 0),
(97, 'RT060097', 'EMILIANA RAHAIL', ' JL. NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 00:58:32', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.385835, -8.489913, 1, 6, 0, 0),
(98, 'RT060098', 'MUHAMAD ILHAM SARDI', ' JL. NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:05:34', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.384994, -8.489805, 1, 6, 0, 0),
(99, 'RT030099', 'ERWIN', 'JL. RAYA MANDALA, BAMPEL, Gg. SAYUR ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:10:21', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.398399, -8.508015, 1, 3, 0, 0),
(100, 'RT120100', 'ERNAWATI', ' JL. RAYA MANDALA, Gg. KPKN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:15:31', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.407111, -8.518966, 1, 12, 0, 0),
(101, 'KI060101', 'ROMLAN SUPARLAN', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:28:36', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.385516, -8.492683, 6, 6, 0, 0),
(102, 'RT060102', 'YULIANA Y. YOLMEN', ' JL. Gg. RADIO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:35:05', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.382885, -8.485955, 1, 6, 0, 0),
(103, 'RT040103', 'MUSTAKIM', ' JL. TERNATE, Gg. KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:42:24', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.382577, -8.494869, 1, 4, 0, 0),
(104, 'RT040104', 'RAMI ADI CICIK', ' JL. TERNATE, Gg. KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:48:35', '2017-12-15', 'MERAUKE', '1969-12-01', NULL, NULL, NULL, NULL, NULL, 140.38252, -8.494975, 1, 4, 0, 0),
(105, 'KI070105', 'KIOS HIDAYAH', ' MAYOR WIRATNO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:52:36', '2017-12-15', 'MERAUKE', '1999-12-01', NULL, NULL, NULL, NULL, NULL, 140.393391, -8.478197, 6, 7, 0, 0),
(106, 'RT040106', 'SUPARDI', ' JL. KAMPUNG TIMUR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 01:56:54', '2017-12-15', 'MERAUKE', '1999-12-01', NULL, NULL, NULL, NULL, NULL, 140.392573, -8.497107, 1, 4, 0, 0),
(107, 'RT030107', 'IIS IMAWATI', 'JL. IRIAN SERINGGU, Gg. MUMU ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:01:59', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.392777, -8.500429, 1, 3, 0, 0),
(108, 'RT030108', 'LINAWATI', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:06:47', '2017-12-15', 'MERAUKE', '1975-12-01', NULL, NULL, NULL, NULL, NULL, 140.39271, -8.500418, 1, 3, 0, 0),
(109, 'RT020109', 'JONO', ' JL. ARU, Gg. ARU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:11:29', '2017-12-15', 'MERAUKE', '1974-12-01', NULL, NULL, NULL, NULL, NULL, 140.393903, -8.491977, 1, 2, 0, 0),
(110, 'KI040110', 'KIOS  VEBRI /MISRO', ' JL. KANGGURU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:17:04', '2017-12-15', 'MERAUKE', '1976-12-01', NULL, NULL, NULL, NULL, NULL, 140.391039, -8.499803, 6, 4, 0, 0),
(111, 'RT040111', 'MUJANI (6KK)', ' JL. TERNATE, Gg. PAPUA I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:22:21', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.387971, -8.500652, 1, 4, 0, 0),
(112, 'KI040112', 'KIOS AGUNG/MUJANI', ' JL. TERNATE, Gg. PAPUA I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:26:34', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.387885, -8.500715, 6, 4, 0, 0),
(113, 'RT040113', 'SYAHRUDIN M. ARIF', ' JL. TERNATE, OKABA 2', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:29:48', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.388885, -8.499453, 1, 4, 0, 0),
(114, 'RT120114', 'SYAMSUL BAHRI', 'JL. RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:45:16', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.403778, -8.511915, 1, 12, 0, 0),
(115, 'RT030115', 'JASMIN BASIR', ' JL. GAK, DEKAT HOTEL ROYAL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:50:11', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.390663, -8.502859, 1, 3, 0, 0),
(116, 'RT020116', 'RICHART NAINGGOLAN', ' JL. PARAKOMANDO, Gg. CENDERAWASIH', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 02:57:05', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.399104, -8.496728, 1, 2, 0, 0),
(117, 'TS070117', 'PT. WEDU', ' JL. TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 03:21:49', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.393325, -8.481886, 3, 7, 0, 0),
(118, 'RT030118', 'CHRISTINA SUTIYEM', ' JL. SASATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 03:32:54', '2017-12-15', 'MEARUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.396039, -8.504482, 1, 3, 0, 0),
(119, 'RT030119', 'EDMUNDUS TAKERUBUN', ' JL. SASATE, RUMAH DEKAR Dr. DAVID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 03:40:58', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.396058, -8.504475, 1, 3, 0, 0),
(120, 'RT040120', 'SUPRAPTI (15 KK)', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 03:45:32', '2017-12-15', 'MERAUKE', '1967-12-01', NULL, NULL, NULL, NULL, NULL, 140.392046, -8.502466, 1, 4, 0, 0),
(121, 'RT040121', 'KARYONO', ' JL. SERINGGU, Gg. AT-TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 03:52:26', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.391397, -8.501283, 1, 4, 0, 0),
(122, 'RS070122', 'BENYAMIN SIMATUPANG', ' JL . TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 04:04:42', '2017-12-15', 'MERAUKE', '1965-11-01', NULL, NULL, NULL, NULL, NULL, 140.39344, -8.481929, 10, 7, 0, 0),
(124, 'TS060124', 'UD. BERKAT/ ATIK RAHMAWATI', 'JL. NUSA BARONG ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 04:27:27', '2017-12-15', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.38653, -8.492473, 3, 6, 0, 0),
(125, 'RT040125', 'BAKRI UMBALAK', ' JL. IRIAN SERINGGU, Gg. AT-TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 05:40:46', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.392348, -8.502211, 1, 4, 0, 0),
(126, 'RT040126', 'ASMAUN UMBALAK', ' JL. IRIAN SERINGGU, Gg. AT-TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 05:43:34', '2017-12-15', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.392996, -8.502768, 1, 4, 0, 0),
(127, 'RT040127', 'ABDUL LATIF UMBALAK', ' JL. IRIAN SERINGGU, Gg. AT- TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 05:50:44', '2017-12-15', 'MERAUKE', '1969-12-01', NULL, NULL, NULL, NULL, NULL, 140.393131, -8.503081, 1, 4, 0, 0),
(128, 'RT020128', 'JUSUF MARJUN', 'JL. BIAK ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 06:24:20', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.400527, -8.50228, 1, 2, 0, 0),
(129, 'RT120129', 'RUFINUS BUDI YOLMEN', ' JL. GARUDA SPADEM   ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 06:30:20', '2017-12-15', 'MERAUKE', '1978-12-01', NULL, NULL, NULL, NULL, NULL, 140.416023, -8.524218, 1, 12, 23, 1),
(130, 'RT020130', 'HENGKI T.', ' JL. RAYA MANDALA, Gg. SOSKA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 23:41:04', '2017-12-15', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.397852, -8.502092, 1, 2, 0, 0),
(131, 'RT040131', 'SUGITO', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 23:45:06', '2017-12-15', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.395042, -8.490415, 1, 4, 0, 0),
(132, 'RT090132', 'KAMARUDIN', ' JL. PEMBANGUNAN, Gg. GEMPOL ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-15 23:51:56', '2017-12-15', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.420974, -8.517819, 1, 9, 23, 1),
(133, 'RT060133', 'KRISENSIA RESUBUN', ' JL. TERNATE, SAMPING KM-TV', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 00:23:26', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.404469, -8.508438, 1, 6, 0, 0),
(134, 'RT040134', 'CIMAHI MEKAR (EDDY SUSWANTO)', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 00:35:52', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.388805, -8.503132, 1, 4, 0, 0),
(135, 'RT060135', 'AMIN SUSANTI ', 'JL. TERNATE, Gg. H. KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 00:51:41', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.381853, -8.499058, 1, 6, 0, 0),
(136, 'PRSH060136', 'UD. GAYA PESONA (AMIN SUSANTI)', ' JL. TERNATE, Gg. H. KASIM ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:02:26', '2017-12-16', 'MERAUKE', '1978-12-01', NULL, NULL, NULL, NULL, NULL, 140.382212, -8.49463, 12, 6, 23, 1),
(137, 'RT040137', 'ANDI RUKAYAH', ' JL. TIDORE ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:12:10', '2017-12-16', 'MERAUKE', '1974-12-01', NULL, NULL, NULL, NULL, NULL, 140.388835, -8.493154, 1, 4, 23, 1),
(138, 'RT060138', 'MUTRIAH', ' JL. TERNATE, Gg. H. KASIM(LINGKUP Gg. DEPAN MASJID)', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:27:17', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.385243, -8.497148, 1, 6, 0, 0),
(139, 'RT060139', 'SUYATI', ' JL. TERNATE, Gg. H. KASIM(LINGKUP Gg. DEPAN MASJID) ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:30:51', '2017-12-16', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.38234, -8.494418, 1, 6, 23, 1),
(140, 'RT090140', 'MARFLY', ' JL. SUTAN SYAHRIL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:34:13', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.408246, -8.497912, 1, 9, 0, 0),
(141, 'PRSH080141', 'SENTRA BELANJA MERAUKE', ' JL. BRAWIJAYA MERAUKE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:38:15', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383054, -8.496553, 12, 8, 0, 0),
(142, 'RT120142', 'RUMAH SEWA AINI (4KK)', ' JL. RAYA MANDALA SPADEM , Gg. SANER', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:43:01', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.385243, -8.492166, 1, 12, 0, 0),
(143, 'RT080143', 'ARIFIN SULAIMAN', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:49:12', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398718, -8.498845, 1, 8, 0, 0),
(144, 'RT070144', 'FAUSTINUS RENGIL (9 KK)', ' JL., ALIARKAM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 01:58:35', '2017-12-16', 'MERAUJKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.396058, -8.488574, 1, 7, 0, 0),
(145, 'RT090145', 'HARI MAMBRASAR', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:09:32', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.403267, -8.512342, 1, 9, 0, 0),
(146, 'RT060146', 'MUH. DAWI DIFINUBUN', ' JL. NUSA BARONG,Gg. ASPAL BARU ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:22:55', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.400349, -8.502665, 1, 6, 0, 0),
(147, 'TB060147', 'SMA NEGERI II MERAUKE', ' JL.  NOWARI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:27:35', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.404469, -8.503684, 2, 6, 0, 0),
(148, 'RT070148', ' RIRIN PURNAWANDARI', 'JL. ASPOL MERAUKE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:33:18', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.400006, -8.501647, 1, 7, 0, 0),
(149, 'RT020149', 'HANNA', ' JL. RAYA MANDALA, Gg. HAJI JALIS', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:38:54', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.391878, -8.507223, 1, 2, 0, 0),
(150, 'RT080150', 'NATALIA M,. R', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:43:38', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.404469, -8.499058, 1, 8, 0, 0),
(151, 'PRSH060151', 'PT. SUMBER CIPTA MULTI NIAGA', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:47:23', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402581, -8.501816, 12, 6, 0, 0),
(152, 'TS020152', 'PANTI ASUHAN ABBA', ' JL. MISSI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:50:29', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398461, -8.503005, 3, 2, 0, 0),
(153, 'RT030153', 'YANUARIUS RESUBUN (3KK)', ' JL. SASATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 02:56:22', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.395199, -8.503684, 1, 3, 0, 0),
(154, 'RT020154', 'MIRANDA', ' JL. RAYA MANDALA, Gg. PMI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 03:00:33', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.38799, -8.496553, 1, 2, 0, 0),
(155, 'RSTR030155', 'RESTAURANT PINANG SIRI', ' JL. SASATE,/ GALAXI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 03:06:04', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.396358, -8.50309, 5, 3, 0, 0),
(156, 'RT070156', 'MARKUS LOLO', ' JALAN BAKTI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:16:51', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.385243, -8.496553, 1, 7, 0, 0),
(157, 'RT060157', 'MUHAMAD RAMLI', ' JL. NATUNA, JALUR Gg. DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:22:22', '2017-12-16', 'MERAUKE', '1977-01-16', NULL, NULL, NULL, NULL, NULL, 140.387088, -8.493073, 1, 6, 0, 0),
(158, 'RT060158', 'SUNARTI', ' JLNATUNA JALUR Gg DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:26:11', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.404469, -8.492166, 1, 6, 0, 0),
(159, 'RT060159', 'NGATISAH', ' JL. NATUNA JALUR Gg DEKAT MASJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:29:35', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.381853, -8.503684, 1, 6, 0, 0),
(160, 'RT060160', 'SUINTIANA', ' JL. NATUNA, JALUR Gg. DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:30:40', '2017-12-16', 'MERAUKE', '1977-01-16', NULL, NULL, NULL, NULL, NULL, 140.382711, -8.489529, 1, 6, 0, 0),
(161, 'RT060161', 'AHMAD SAHRONI', ' JL. NATUNA, JALUR Gg DEKAT MASJID ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:33:38', '2017-12-16', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.382848, -8.490606, 1, 6, 23, 1),
(162, 'RT060162', 'SUKESI', ' JL. NATUNA, JALUR Gg. DEKATR MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:36:06', '2017-12-16', 'MERAUKE', '1977-01-16', NULL, NULL, NULL, NULL, NULL, 140.382926, -8.489338, 1, 6, 0, 0),
(163, 'RT060163', 'NANIK', ' JL.NATUNA JALUR Gg DEKAT MASJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:37:32', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.382677, -8.489492, 1, 6, 0, 0),
(164, 'RT060164', 'AYU YULIANTI', ' JL. NATUNA, JALUR Gg. DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:40:10', '2017-12-16', 'MERAUKE', '1977-01-16', NULL, NULL, NULL, NULL, NULL, 140.382926, -8.489338, 1, 6, 0, 0),
(165, 'RT060165', 'HARTATI HS', ' JL.NATUNA JALUR Gg DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:43:58', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383536, -8.489556, 1, 6, 0, 0),
(166, 'RT060166', 'SIGIT WIJAKSONO', ' JL. NATUNA JALUR Gg DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:52:20', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383236, -8.490374, 1, 6, 0, 0),
(167, 'RT060167', 'RINANI KURNIATI', ' JL.NATUNA JALUR Gg DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:54:56', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383555, -8.490358, 1, 6, 0, 0),
(168, 'RT060168', 'DEWI HANDAYANI', ' JL.NATUNA JALUR Gg DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 05:57:38', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383384, -8.489747, 1, 6, 0, 0),
(169, 'RT060169', 'RITA', ' JALAN NATUNA JALUR Gg.DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 06:00:15', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383384, -8.490519, 1, 6, 0, 0),
(170, 'RT060170', 'ROIKE DANU DJOE', ' JALAN NATUNA JALUR Gg.DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 06:02:33', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.382955, -8.49018, 1, 6, 0, 0),
(171, 'RT060171', 'BERNADETHA RUMFAAN', ' JL.NATUNA JALUR Gg DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 06:04:57', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383298, -8.490103, 1, 6, 0, 0),
(172, 'RT060172', 'AHMAD SAHRONI', ' JL NATUNA JALUR Gg DEKAT MASJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 06:07:37', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383298, -8.490086, 1, 6, 0, 0),
(173, 'RT060173', 'SUNARTI 1', ' JL NATUNA JALUR Gg DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 06:14:30', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383212, -8.490265, 1, 6, 0, 0),
(174, 'RT060174', 'SUDARMIN', ' JL.NATUNA JALUR Gg.DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 06:16:39', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383212, -8.490252, 1, 6, 0, 0),
(175, 'RT060175', 'WAKIAH - BPK AHMAD YANI', ' JL. NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 06:36:16', '2017-12-16', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.383298, -8.490086, 1, 6, 0, 0),
(176, 'RT020176', 'ERIS HERLAMBANG', ' JL. PENDIDIKAN, RUMAH NO 2 PAGAR HIJAU+KUNING', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-16 23:57:48', '2017-12-16', 'MERAUKE', '1998-12-01', NULL, NULL, NULL, NULL, NULL, 140.401907, -8.501317, 1, 2, 0, 0),
(177, 'RT120177', 'YUSUF PAYUNG ALLO', ' JL. HUSEN PALELA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:04:14', '2017-12-17', 'MERAUKE', '1999-12-01', NULL, NULL, NULL, NULL, NULL, 140.404981, -8.51696, 1, 12, 0, 0),
(178, 'RT020178', 'YOHANIS RAHANG METAN', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:07:34', '2017-12-17', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.397897, -8.501766, 1, 2, 0, 0),
(179, 'RT040179', 'MARTINA MANGIRI', ' JALAN TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:08:58', '2017-12-17', 'MERAUKE', '2018-01-16', NULL, NULL, NULL, NULL, NULL, 140.388891, -8.499902, 1, 4, 0, 0),
(180, 'RT040180', 'MARTINA MANGIRI', ' JL. TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:13:19', '2017-12-17', 'MERAUKE', '1956-01-17', NULL, NULL, NULL, NULL, NULL, 140.389159, -8.500183, 1, 4, 0, 0),
(181, 'RT080181', 'SINATAN', ' JL. CEMARA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:14:53', '2017-12-17', 'MERAUKE', '1970-09-01', NULL, NULL, NULL, NULL, NULL, 140.404989, -8.496597, 1, 8, 0, 0),
(182, 'RT040182', 'IMSIBAH RAHMAN', ' JL.TERNATE Gg. EVANDEKAI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:17:12', '2017-12-17', 'MERAUKE', '1966-01-17', NULL, NULL, NULL, NULL, NULL, 140.389674, -8.500183, 1, 4, 0, 0),
(183, 'RT020183', 'GANI WASER', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:24:41', '2017-12-17', 'MERAUKE', '1955-01-17', NULL, NULL, NULL, NULL, NULL, 140.394083, -8.495149, 1, 2, 0, 0),
(184, 'RT020184', 'ANDREW EODORA', ' JL. RAYA MANDALA, Gg. H. JALIS', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:26:57', '2017-12-17', 'MERAUKE', '1975-12-01', NULL, NULL, NULL, NULL, NULL, 140.39146, -8.491664, 1, 2, 0, 0),
(185, 'RT040185', 'RENOLD JULIANUS RAHAWARIN', ' JL. SESATE, Gg. A- TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:31:58', '2017-12-17', 'MERAUKE', '1978-12-01', NULL, NULL, NULL, NULL, NULL, 140.39425, -8.503485, 1, 4, 0, 0),
(186, 'RT040186', 'BAMBANG', ' JL. TERNATE, Gg. HAJI KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:36:02', '2017-12-17', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.381466, -8.496099, 1, 4, 0, 0),
(187, 'RT060187', 'NUNUNG TRI RATNASARI', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:39:47', '2017-12-17', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.384406, -8.493158, 1, 6, 0, 0),
(188, 'KI060188', 'ARHAM CELL', ' JL. JAWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:47:32', '2017-12-17', 'MERAUKE', '1997-09-01', NULL, NULL, NULL, NULL, NULL, 140.387365, -8.488699, 6, 6, 0, 0),
(189, 'RT020189', 'WODROU WILSON LEREBULAN', ' JL. RAYA MANDALA ASPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 00:47:44', '2017-12-17', 'MERAUKE', '1975-01-17', NULL, NULL, NULL, NULL, NULL, 140.390757, -8.488046, 1, 2, 0, 0),
(190, 'RT040190', 'VIA ANDRI', ' VIA ANDRI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 01:05:59', '2017-12-17', 'MERAUKE', '1970-10-01', NULL, NULL, NULL, NULL, NULL, 140.38903, -8.499864, 1, 4, 0, 0),
(191, 'RT040191', 'DANI ABDUL AZIS', ' JL. TERNATE, Gg. TIGA TANJUNG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 01:08:57', '2017-12-17', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.3891, -8.499941, 1, 4, 0, 0),
(192, 'RT070192', 'AMBO TANG', ' JALAN AMPERA 1 NO. 12', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 01:10:22', '2017-12-17', 'MERAUKE', '1966-01-17', NULL, NULL, NULL, NULL, NULL, 140.396611, -8.484833, 1, 7, 0, 0),
(193, 'KI060193', 'KIOS BERKAT USAHA', ' JL. NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 01:32:32', '2017-12-17', 'MERAUKE', '1997-12-01', NULL, NULL, NULL, NULL, NULL, 140.386874, -8.487513, 6, 6, 0, 0),
(194, 'KI060194', 'MUH. ARIFIN', ' JL. NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 01:35:49', '2017-12-17', 'MERAUKE', '1972-12-01', NULL, NULL, NULL, NULL, NULL, 140.385541, -8.489046, 6, 6, 0, 0),
(195, 'RT060195', 'TITIN SOFIANA', ' JALAN NUSA BARONG ( SEBELAH KANAN RUMAH IBU IMEL )', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 01:56:01', '2017-12-17', 'MERAUKE', '1967-01-17', NULL, NULL, NULL, NULL, NULL, 140.399378, -8.501227, 1, 6, 0, 0),
(196, 'RT040196', 'TIAN SUPANDI', 'KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 01:56:29', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.383993, -8.502546, 1, 4, 0, 0),
(197, 'RT040197', 'TOMMY SARUNGGU', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:00:04', '2017-12-17', 'MERAUKE', '1975-12-01', NULL, NULL, NULL, NULL, NULL, 140.384251, -8.501931, 1, 4, 0, 0),
(198, 'TB050198', 'SMK NEGERI III MERAUKE', ' JALAN KAMIZAUN MOPAH LAMA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:01:47', '2017-12-17', 'MERAUKE', '1977-01-17', NULL, NULL, NULL, NULL, NULL, 140.390038, -8.496923, 2, 5, 0, 0),
(199, 'RT120199', 'HABEL WARIJO', ' JL. Gg. GARUDA I SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:05:38', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.41234, -8.522619, 1, 12, 0, 0),
(200, 'RT040200', 'BUDI CAHYONO', 'JL. TERNATE, Gg. MAWAR ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:14:38', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.386917, -8.499567, 1, 4, 0, 0),
(201, 'RT090201', 'KUSDI KASMIN', ' JL. ONGGATMIT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:18:06', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.402367, -8.513668, 1, 9, 0, 0),
(202, 'RT060202', 'MUCH. YUNUS ANIS', ' JL. NATUNA Gg. 3', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:22:43', '2017-12-17', 'MERAUKE', '1969-12-01', NULL, NULL, NULL, NULL, NULL, 140.384221, -8.489863, 1, 6, 0, 0),
(203, 'RM040203', 'BAKBO PRIMA', ' JALAN IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:25:33', '2017-12-17', 'MERAUKE', '1966-01-17', NULL, NULL, NULL, NULL, NULL, 140.38955, -8.502882, 4, 4, 0, 0),
(204, 'RT060204', 'HIKMAWATI I', ' JL. NATUNA, Gg. 3', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:28:00', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.384318, -8.489905, 1, 6, 0, 0),
(205, 'RT060205', 'HIKMAWATI II', ' JL. NATUNA, Gg. 3', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:30:28', '2017-12-17', 'MERAUKE', '0977-12-01', NULL, NULL, NULL, NULL, NULL, 140.383813, -8.49016, 1, 6, 0, 0),
(206, 'RT060206', 'HIKMAWATI III', ' JL. NATUNA, Gg. 3', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:32:56', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.383803, -8.490202, 1, 6, 0, 0),
(207, 'RT060207', 'HIKMAWATI IV', ' JL. NATUNA, Gg,. 3', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:35:09', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.384264, -8.489916, 1, 6, 0, 0),
(208, 'RT060208', 'HIKMAWATI V', '  JL. NATUNA, Gg,. 3', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:37:41', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.383803, -8.490149, 1, 6, 0, 0),
(209, 'TB020209', 'KULINER PMI', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:39:21', '2017-12-17', 'MERAUKE', '1980-01-17', NULL, NULL, NULL, NULL, NULL, 140.392115, -8.49168, 2, 2, 0, 0),
(210, 'RT060210', 'HIKMAWATI VI', '  JL. NATUNA, Gg,. 3', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:40:07', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.383803, -8.490181, 1, 6, 0, 0),
(211, 'KNTR020211', 'KANTOR PALANG MERAH INDONESIA (PMI)', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 02:49:22', '2017-12-17', 'MERAUKE', '1966-01-17', NULL, NULL, NULL, NULL, NULL, 140.391921, -8.491903, 7, 2, 0, 0),
(212, 'RT070212', 'PERUMAHAN KOMPLEKS TNI -AL (37 KK)', ' JL. TMP. POLDER', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 03:18:11', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.397731, -8.483629, 1, 7, 0, 0),
(213, 'RT030213', 'AGUSTINUS', ' JALAN SESATE GANG BALUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 03:31:04', '2017-12-17', 'MERAUKE', '1956-12-01', NULL, NULL, NULL, NULL, NULL, 140.39497, -8.502827, 1, 3, 0, 0),
(214, 'RT070214', 'NURIONO', ' JALAN ALIARKAM, GANG BUNTU ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 04:09:55', '2017-12-17', 'MERAUKE', '1956-01-17', NULL, NULL, NULL, NULL, NULL, 140.394136, -8.484216, 1, 7, 23, 1),
(215, 'RT070215', 'NURIONO', ' JALAN ALIARKAM GANG BUNTU ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 04:15:44', '2017-12-17', 'MERAUKE', '1967-01-17', NULL, NULL, NULL, NULL, NULL, 140.394082, -8.484046, 1, 7, 23, 1),
(216, 'RT040216', 'MARKUS TENAU', ' JALAN TERNATE KOMPLEKS PERUM KEHUTANAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 04:23:50', '2017-12-17', 'MERAUKE', '1973-01-17', NULL, NULL, NULL, NULL, NULL, 140.383102, -8.493201, 1, 4, 0, 0),
(217, 'RT060217', 'KARSIDI', ' JL. NATUNA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 05:21:08', '2017-12-17', 'MERAUKE', '1970-12-01', NULL, NULL, NULL, NULL, NULL, 140.38414, -8.48986, 1, 6, 0, 0),
(218, 'RT040218', 'YONATAN IMERTO PAMUTU', 'KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-01-17 05:36:15', '2018-03-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.384192, -8.501923, 1, 4, 0, 0),
(219, 'RT080219', 'SUMBER ISHARTONO', ' JL. CEMARA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 05:44:47', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.405273, -8.498045, 1, 8, 0, 0),
(220, 'RT030220', 'MUBAYAH', ' JL. GAK Gg. MESJID AL-BAROKAH', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 05:54:37', '2017-12-17', 'MERAUKE', '1999-12-01', NULL, NULL, NULL, NULL, NULL, 140.398537, -8.507764, 1, 3, 0, 0),
(221, 'RT030221', 'NUR ROHMAD', ' JL. GAK, Gg. MESJID AL-BAROKAH', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:00:08', '2017-12-17', 'MERAUKE', '1995-12-01', NULL, NULL, NULL, NULL, NULL, 140.398434, -8.507411, 1, 3, 0, 0),
(222, 'RT030222', 'SUPENO', ' JL. GAK, Gg. MESJID AL-BAROKAH', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:05:36', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.39885, -8.507198, 1, 3, 0, 0),
(223, 'RT060223', 'BUDIYONO', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:07:54', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.385484, -8.493111, 1, 6, 0, 0),
(224, 'RT070224', 'SUBAKIR', ' JL. KIMAAM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:09:28', '2017-12-17', 'MERAUKE', '1976-12-01', NULL, NULL, NULL, NULL, NULL, 140.391189, -8.484802, 1, 7, 0, 0),
(225, 'RT030225', 'MESJID AL- BAROKAH', ' JL. GAK.', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:15:53', '2017-12-17', 'MERAUKE', '1999-12-01', NULL, NULL, NULL, NULL, NULL, 140.398236, -8.507849, 1, 3, 0, 0),
(226, 'RT020226', 'SUARTI', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:21:08', '2017-12-17', 'MERAUKE', '1967-12-01', NULL, NULL, NULL, NULL, NULL, 140.39768, -8.489828, 1, 2, 0, 0),
(227, 'KI020227', 'SUARTI', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:24:21', '2017-12-17', 'MERAUKE', '1996-12-01', NULL, NULL, NULL, NULL, NULL, 140.397715, -8.489499, 6, 2, 0, 0),
(228, 'KI020228', 'HASNA', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:28:13', '2017-12-17', 'MERAUKE', '1967-12-01', NULL, NULL, NULL, NULL, NULL, 140.397755, -8.489383, 6, 2, 0, 0),
(229, 'KI020229', 'SANISAH', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:31:22', '2017-12-17', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.397809, -8.489436, 6, 2, 0, 0),
(230, 'KI020230', 'SAWIYEM', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-17 06:34:21', '2017-12-17', 'MERAUKE', '1976-12-01', NULL, NULL, NULL, NULL, NULL, 140.39783, -8.489436, 6, 2, 0, 0),
(231, 'RT090231', 'ASRORY', ' JALAN PEMBANGUNAN, DEPAN BRI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 00:12:33', '2017-12-18', 'MERAUKE', '1955-01-18', NULL, NULL, NULL, NULL, NULL, 140.416914, -8.507607, 1, 9, 0, 0),
(232, 'KI090232', 'HERMAN EFFENDI', ' JALAN SULTAN SYAHRIR DEPAN MESJID AL.HIDAYAH', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 00:16:42', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.415294, -8.507365, 6, 9, 0, 0),
(233, 'RT040233', 'HJ. ILHAM FIRDAUS', ' JL.GANG LABAN FELUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 00:21:32', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.388943, -8.493526, 1, 4, 0, 0);
INSERT INTO `re_costumer` (`cost_id`, `cost_id_pel`, `cost_name`, `cost_address`, `cost_prov`, `cost_kab`, `cost_kec`, `cost_status`, `cost_date`, `cost_periode_terakhir`, `cost_tmp_lahir`, `cost_tgl_lahir`, `cost_phone`, `cost_foto`, `cost_lokasi`, `cost_denah`, `cost_ktp`, `cost_lang`, `cost_latt`, `cost_type_id`, `cost_desa_id`, `cost_user_id`, `cost_jadwal_id`) VALUES
(234, 'RT030234', 'ADE KUSLIANTO A', ' JALAN GAK GANG SAYUR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 00:31:14', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.39846, -8.505286, 1, 3, 0, 0),
(235, 'RT040235', 'SUHARTO', ' JALAN TIDORE ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 00:53:08', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.388743, -8.495313, 1, 4, 23, 1),
(236, 'RT040236', 'SUMARNO', ' JALAN TERNATE GANG MAWAR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 00:58:47', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.388643, -8.498074, 1, 4, 0, 0),
(237, 'PRSH070237', 'KANTOR PERTAMINA ', ' JALAN GUDANG ARANG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 01:08:39', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.391475, -8.48009, 12, 7, 0, 0),
(238, 'RT120238', 'DANIEL HIAREJ', ' JALAN POMPA AIR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 01:11:43', '2017-12-18', 'MERAUKE', '1967-01-18', NULL, NULL, NULL, NULL, NULL, 140.382926, -8.500183, 1, 12, 0, 0),
(239, 'RM120239', 'ANGGAI SAI', ' JALAN RAYA MANDALA MULI ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 01:20:15', '2017-12-18', 'MERAUKE', '1988-01-18', NULL, NULL, NULL, NULL, NULL, 140.404887, -8.512355, 4, 12, 23, 1),
(240, 'KI020240', 'IRIANTO PENJAHIT', ' JALAN GANG ARU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 01:28:11', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.394414, -8.492649, 6, 2, 0, 0),
(241, 'RT120241', 'IZAK PIKARIMA', ' JALAN RAYA MANDALA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 02:01:01', '2017-12-18', 'MERAUKE', '1956-01-18', NULL, NULL, NULL, NULL, NULL, 140.412268, -8.520686, 1, 12, 0, 0),
(242, 'RT120242', 'IZAK PIKARIMA', ' JALAN RAYA MANDALA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 02:02:58', '2017-12-18', 'MERAUKE', '1956-01-18', NULL, NULL, NULL, NULL, NULL, 140.411194, -8.52011, 1, 12, 0, 0),
(243, 'RT120243', 'ERLIN MARLIANA LAODE', ' JALAN RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 02:13:08', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.40581, -8.51347, 1, 12, 0, 0),
(244, 'RT030244', 'SUSANTO', ' JALAN GAK GANG MASJID AL BAROKAH', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 02:26:48', '2017-12-18', 'MERAUKE', '1977-01-18', NULL, NULL, NULL, NULL, NULL, 140.382926, -8.489338, 1, 3, 0, 0),
(245, 'RM040245', 'WARUNG MAKAN IMELDA', ' JALAN NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 02:38:28', '2018-01-18', 'MERAUKE', '1990-01-18', NULL, NULL, NULL, NULL, NULL, 140.386766, -8.491896, 4, 4, 0, 0),
(246, 'RT040246', 'SADIKIN', ' JALAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 02:42:57', '2017-12-18', 'MERAUKE', '1977-01-18', NULL, NULL, NULL, NULL, NULL, 140.388664, -8.503135, 1, 4, 0, 0),
(247, 'RT060247', 'IMELDA 5 KK', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 02:49:03', '2017-12-18', 'MERAUKE', '2018-01-18', NULL, NULL, NULL, NULL, NULL, 140.385757, -8.492839, 1, 6, 0, 0),
(248, 'RT060248', 'WAHYU KAFIAR', ' JALAN ASRAMA PELAYARAN BARU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 03:47:31', '2017-12-18', 'MERAUKE', '1956-01-18', NULL, NULL, NULL, NULL, NULL, 140.384947, -8.486505, 1, 6, 0, 0),
(249, 'RT060249', 'WAHYU KAFIAR', ' JALAN ASRAMA PELAYARAN BARU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 03:49:53', '2017-12-18', 'MERAUKE', '1967-01-18', NULL, NULL, NULL, NULL, NULL, 140.385087, -8.486541, 1, 6, 0, 0),
(250, 'RT120250', 'ROLIN MARAMIS', ' JALAN RAYA MANDALA Gg DIRGANTARA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 04:53:23', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.416344, -8.523907, 1, 12, 23, 1),
(251, 'RT040251', 'FADLI ZAID', ' JALAN TERNATE GANG H. KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 05:55:13', '2017-12-18', 'MERAUKE', '1966-01-18', NULL, NULL, NULL, NULL, NULL, 140.384384, -8.493424, 1, 4, 0, 0),
(253, 'RT120253', 'BOBY ', ' JL. RAYA MANDALA MULI SAMPING KIRI KANTOR TRAKINDO Tbk ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-18 14:31:26', '2018-01-18', 'JAYAPURA', '1978-02-06', NULL, NULL, NULL, NULL, NULL, 140.40669, -8.514, 1, 12, 23, 1),
(254, 'RT060254', 'LANDEP MELANY (3 KK)', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 00:47:55', '2017-12-19', 'MERAUKE', '1977-09-01', NULL, NULL, NULL, NULL, NULL, 140.386353, -8.491541, 1, 6, 0, 0),
(255, 'RT080255', 'ABDUL RAUF (7KK)', ' JL. CEMARA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 00:55:28', '2017-12-19', 'MERAUKE', '1974-09-01', NULL, NULL, NULL, NULL, NULL, 140.404898, -8.495998, 1, 8, 0, 0),
(256, 'TS030256', 'ERIC WANDANA', ' JLK. SASATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 01:02:10', '2017-12-19', 'MERAUKE', '1972-12-01', NULL, NULL, NULL, NULL, NULL, 140.397105, -8.502849, 3, 3, 0, 0),
(257, 'TS070257', 'TOKO MMS', ' JL ERMASSU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 01:07:09', '2017-12-19', 'MERAUKE', '1969-09-01', NULL, NULL, NULL, NULL, NULL, 140.397613, -8.495484, 3, 7, 0, 0),
(258, 'RT040258', 'NUR FITA', ' JL. TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 01:11:56', '2017-12-19', 'MERAUKE', '1972-12-01', NULL, NULL, NULL, NULL, NULL, 140.387635, -8.499257, 1, 4, 0, 0),
(259, 'RT040259', 'CRISTINA AJAWAILA (4 KK)', ' JL. KAMPUNG TIMUR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 01:17:07', '2017-12-19', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.390013, -8.493043, 1, 4, 0, 0),
(260, 'RT030260', 'SUPARTO (2KK )', ' JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 01:34:24', '2017-12-19', 'MERAUKE', '1977-09-01', NULL, NULL, NULL, NULL, NULL, 140.393746, -8.501416, 1, 3, 0, 0),
(261, 'KI030261', 'SUPARTO (KIOS)', ' JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 01:37:51', '2017-12-19', 'MERAUKE', '1975-09-01', NULL, NULL, NULL, NULL, NULL, 140.394128, -8.501418, 6, 3, 0, 0),
(262, 'TS080262', 'KIOS ALAMANDA (Bpk. LANTANG)', ' JL. SUTAN SYAHRIL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 01:44:06', '2017-12-19', 'MERAUKE', '1977-09-01', NULL, NULL, NULL, NULL, NULL, 140.41628, -8.507819, 3, 8, 0, 0),
(263, 'TS060263', 'ALOYSIUS DUMATUBUN,SH', ' JALAN SUMATERA NO.19', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 02:04:03', '2017-12-19', 'MERAUKE', '1956-01-19', NULL, NULL, NULL, NULL, NULL, 140.38962, -8.490194, 3, 6, 0, 0),
(264, 'RT060264', 'SETI FEBRIANTI SAUSA', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 02:07:48', '2017-12-19', 'MERAUKE', '1977-09-01', NULL, NULL, NULL, NULL, NULL, 140.386686, -8.492411, 1, 6, 0, 0),
(265, 'KI020265', 'FOTOCOPY GAYA WARNA', ' JALAN RAYA MANDALA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 02:20:01', '2017-12-19', 'MERAUKE', '1999-01-19', NULL, NULL, NULL, NULL, NULL, 140.407097, -8.515682, 6, 2, 0, 0),
(267, 'RT090267', 'IGNATIUS SOEGIMIN', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 02:30:49', '2017-12-19', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.418429, -8.511527, 1, 9, 0, 0),
(268, 'KI090268', 'IGNATIUS SOEGIMIN / KIOS', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 02:35:42', '2017-12-19', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.418368, -8.511146, 6, 9, 0, 0),
(269, 'KNTR090269', 'IGNATIUS SOEGIMIN / KANTOR SILVA PAPUA LESTARI', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-19 02:39:12', '2017-12-19', 'MERAUKE', '1977-12-01', NULL, NULL, NULL, NULL, NULL, 140.418545, -8.511549, 7, 9, 0, 0),
(270, 'RM020270', 'RM. CITARASA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-21 23:35:58', '2017-12-21', 'MERAUKE', '1990-01-21', NULL, NULL, NULL, NULL, NULL, 140.397355, -8.500446, 4, 2, 0, 0),
(271, 'RT050271', 'WARGA KELURAHAN SAMKAI RT.007/RW.003 (M. SYAM K.N.I.) (70 KK)', '  ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-21 23:52:16', '2017-12-21', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.378148, -8.509697, 1, 5, 23, 1),
(272, 'RT020272', 'SAROJI', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-21 23:53:32', '2017-12-21', 'MERAUKE', '1956-01-21', NULL, NULL, NULL, NULL, NULL, 140.403279, -8.510621, 1, 2, 0, 0),
(273, 'RT040273', 'KILIS HARTIKO', ' JALAN  SRINGGU Gg. KANGGURU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-21 23:58:43', '2017-12-21', 'MERAUKE', '1966-01-21', NULL, NULL, NULL, NULL, NULL, 140.390681, -8.499742, 1, 4, 0, 0),
(274, 'RT030274', 'WIHELMINA JEUJANAN', ' JALAN SESATE Gg. BALUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 00:06:50', '2017-12-22', 'MERAUKE', '1966-01-21', NULL, NULL, NULL, NULL, NULL, 140.397042, -8.502677, 1, 3, 0, 0),
(275, 'RT030275', 'USUL IRMAWATI', ' JALAN SESATE Gg.BALUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 00:11:38', '2017-12-22', 'MERAUKE', '1966-01-21', NULL, NULL, NULL, NULL, NULL, 140.396959, -8.502691, 1, 3, 0, 0),
(276, 'RT040276', 'BAITI JANATI (11 KK)', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 00:13:35', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.392834, -8.50348, 1, 4, 0, 0),
(277, 'RT030277', 'JK.RUMFAAN', ' JALAN SESATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 00:15:18', '2017-12-22', 'MERAUKE', '1966-01-21', NULL, NULL, NULL, NULL, NULL, 140.396977, -8.502671, 1, 3, 0, 0),
(278, 'RT080278', 'RUMAH SEWA YOHANES TING (6 KK)', ' JL. TMP', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 00:43:12', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.40151, -8.492186, 1, 8, 0, 0),
(279, 'RT040279', 'BONAVENTURA SUDARTO HOESODO', ' JL. TERNATE, Gg. OKABA 1', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 00:50:54', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.388005, -8.500573, 1, 4, 0, 0),
(280, 'RT080280', 'YOHANES TING (6 KK)', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 00:56:42', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.407816, -8.502301, 1, 8, 0, 0),
(281, 'KI020281', 'BERNADETHA .DEPOT BAKWAN MALANG', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 01:07:19', '2017-12-22', 'MERAUKE', '1988-01-21', NULL, NULL, NULL, NULL, NULL, 140.392536, -8.492411, 6, 2, 0, 0),
(282, 'RT060282', 'ALOYSIUS DUMATUBUN', ' JL. SUMATERA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 01:35:42', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.389739, -8.490054, 1, 6, 0, 0),
(283, 'KNTR060283', 'KANTOR NOTARIS ALOYSIUS DUMATUBUN', ' JL. SUMATERA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 01:42:06', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.408761, -8.49927, 7, 6, 0, 0),
(284, 'TS020284', 'KORAN RADAR MERAUKE', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 01:56:46', '2017-12-22', 'MERAUKE', '1980-01-21', NULL, NULL, NULL, NULL, NULL, 140.399818, -8.505149, 3, 2, 0, 0),
(285, 'RT040285', 'LATIFA TAMA LERO', ' JALAN SERINGGU Gg.AT.TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:02:24', '2017-12-22', 'MERAUKE', '1966-01-21', NULL, NULL, NULL, NULL, NULL, 140.393728, -8.50305, 1, 4, 0, 0),
(286, 'RT040286', 'SITI SALAMAH DAHLAN', ' JALAN SERINGGU Gg.AT.TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:06:18', '2017-12-22', 'MERAUKE', '1980-01-21', NULL, NULL, NULL, NULL, NULL, 140.393941, -8.503422, 1, 4, 0, 0),
(287, 'RT060287', 'YENTI REFNITA', ' JL.. TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:08:14', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.387227, -8.491354, 1, 6, 0, 0),
(288, 'RT040288', 'ADE IRMA HERLIANA', ' JALAN SERINGGU Gg.AT.TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:09:17', '2017-12-22', 'MERAUKE', '1990-01-21', NULL, NULL, NULL, NULL, NULL, 140.394113, -8.503871, 1, 4, 0, 0),
(289, 'RT060289', 'MUHAMAD TAHA (3KK)', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:11:42', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.387083, -8.491498, 1, 6, 0, 0),
(290, 'RT040290', 'I PUTU MUSTIKA 4 KK', ' JALAN TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:13:24', '2017-12-22', 'MERAUKE', '1988-01-21', NULL, NULL, NULL, NULL, NULL, 140.383955, -8.494261, 1, 4, 0, 0),
(291, 'RT120291', 'AGUNG DEDI PURNOMO', ' JL. RAYA MANDALA SPADEM, BEKAS KANTOR INSPEKTORAT LAMA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:24:58', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.410301, -8.520211, 1, 12, 0, 0),
(292, 'RT040292', 'ASMARANI', ' JALAN TERNATE Gg. MUDITA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:29:29', '2017-12-22', 'MERAUKE', '1980-01-21', NULL, NULL, NULL, NULL, NULL, 140.388488, -8.500219, 1, 4, 0, 0),
(293, 'RT080293', 'ENI SETIAWATI', ' JL. TMP, Gg. JAMRUT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:31:04', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402819, -8.493459, 1, 8, 0, 0),
(294, 'RT080294', 'IDA', ' JL. TMP, Gg. JAMRUT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:34:18', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402755, -8.493332, 1, 8, 0, 0),
(295, 'RT080295', 'NOHO KABAKOPAN', ' JK. TMP, Gg. JAMRUT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:37:09', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402462, -8.493358, 1, 8, 0, 0),
(296, 'RT080296', 'RENO SUGINO', ' JL. TMP, Gg. JAMRUT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:40:11', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402497, -8.493714, 1, 8, 0, 0),
(297, 'RT080297', 'ANSELMA MARIA SAMSON', ' JL. TMP, Gg. JAMRUT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:42:48', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402519, -8.493714, 1, 8, 0, 0),
(298, 'RS020298', 'APOTEK ARITHA FARMA', ' JALAN BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:44:52', '2017-12-22', 'MERAUKE', '1980-01-21', NULL, NULL, NULL, NULL, NULL, 140.409849, -8.502301, 10, 2, 0, 0),
(299, 'TS020299', 'APOTEK ARITHA FARMA', ' JALAN BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:50:30', '2017-12-22', 'MERAUKE', '1988-01-21', NULL, NULL, NULL, NULL, NULL, 140.408491, -8.503035, 3, 2, 0, 0),
(300, 'RT020300', 'ABDULLAH AMIN SUMARNO (2KK)', ' JL. RAYA MANDALA, SAMPING TOKO SIMON-SIMON/SAKURA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 02:52:01', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.392366, -8.491438, 1, 2, 0, 0),
(301, 'RT040301', 'WARSAN', ' JALAN TERNATE Gg.OKABA 1', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 03:23:05', '2017-12-22', 'MERAUKE', '1980-01-21', NULL, NULL, NULL, NULL, NULL, 140.388643, -8.500246, 1, 4, 0, 0),
(302, 'RT040302', 'SUDARNI', ' JALAN TERNATE Gg. AT.TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 03:25:42', '2017-12-22', 'MERAUKE', '1988-01-21', NULL, NULL, NULL, NULL, NULL, 140.393942, -8.503599, 1, 4, 0, 0),
(303, 'KI040303', 'SUDARNI 1', ' JALAN SERINGGU Gg. AT.TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 03:28:11', '2017-12-22', 'MERAUKE', '2000-01-21', NULL, NULL, NULL, NULL, NULL, 140.394713, -8.504625, 6, 4, 0, 0),
(304, 'RT070304', 'AGUS SYARIFUDIN', ' JALAN TMP POLDER', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 03:46:44', '2017-12-22', 'MERAUKE', '1955-01-21', NULL, NULL, NULL, NULL, NULL, 140.401003, -8.491658, 1, 7, 0, 0),
(305, 'RT090305', 'ANONSIATA RITA HARTANTO', ' JL. HUSEN PALELA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 05:11:50', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.40556, -8.515962, 1, 9, 0, 0),
(306, 'KNTR090306', 'KANTOR OTORITAS BANDARA MOPAH', ' JALAN MOPAH  ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 05:18:15', '2017-12-22', 'MERAUKE', '1955-01-21', NULL, NULL, NULL, NULL, NULL, 140.414833, -8.520344, 7, 9, 23, 1),
(307, 'RT030307', 'NAUMI', ' JL. SASATE,  ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 05:29:43', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.394147, -8.505728, 1, 3, 23, 1),
(308, 'RT020308', 'HENDRI PUJI ASTUTI', 'JL. RAYA MANDALA, GG. SOSKA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 05:37:57', '2017-12-22', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.392345, -8.493705, 1, 2, 0, 0),
(309, 'RT040309', 'MUSTAFUL LAELA', ' JALAN NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:33:17', '2017-12-22', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.388139, -8.491321, 1, 4, 0, 0),
(310, 'KI040310', 'KIOS MUSTAFUL LAELA', ' JALAN NUSA BARONG\r\n', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:38:08', '2017-12-22', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.388043, -8.491343, 6, 4, 0, 0),
(311, 'BGL040311', 'BENGKEL RAMAYANA STAR', ' JL. IRIAN SERINGGU ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:38:56', '2017-12-22', 'MERAUKE', '1977-02-01', NULL, NULL, NULL, NULL, NULL, 140.391364, -8.499904, 11, 4, 0, 0),
(312, 'TB120312', 'AIR MINUM AVIVA', ' JL. GARUDA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:43:31', '2017-12-22', 'MERAUKE', '1977-02-01', NULL, NULL, NULL, NULL, NULL, 140.411818, -8.52148, 2, 12, 0, 0),
(313, 'TB120313', 'TOKO JOTUN', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:48:21', '2017-12-22', 'MERAUKE', '1970-02-03', NULL, NULL, NULL, NULL, NULL, 140.404235, -8.512281, 2, 12, 0, 0),
(314, 'TB120314', 'APOTIK K24', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:50:53', '2017-12-22', 'MERAUKE', '1977-02-01', NULL, NULL, NULL, NULL, NULL, 140.404085, -8.512111, 2, 12, 0, 0),
(315, 'TB020315', 'TOKO DAYROCK', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:53:30', '2017-12-22', 'MERAUKE', '1970-02-01', NULL, NULL, NULL, NULL, NULL, 140.396884, -8.50098, 2, 2, 0, 0),
(316, 'TB030316', 'TOKO ARFAN', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:55:54', '2017-12-22', 'MERAUKE', '1977-02-01', NULL, NULL, NULL, NULL, NULL, 140.401009, -8.50671, 2, 3, 0, 0),
(317, 'TB020317', 'SETIA WIJAYA', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-22 23:58:56', '2017-12-22', 'MERAUKE', '1977-02-01', NULL, NULL, NULL, NULL, NULL, 140.395835, -8.498781, 2, 2, 0, 0),
(318, 'RT030318', 'ARDIANSYAH', ' JALAN SESATE GANG MUGITO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:00:26', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.397012, -8.502631, 1, 3, 0, 0),
(319, 'TB080319', 'WARUNG 2000', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:02:29', '2017-12-23', 'MERAUKE', '1970-02-03', NULL, NULL, NULL, NULL, NULL, 140.408439, -8.502163, 2, 8, 0, 0),
(320, 'TB080320', 'TOKO MITRA TERNAK', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:06:04', '2017-12-23', 'MERAUKE', '1970-02-23', NULL, NULL, NULL, NULL, NULL, 140.402473, -8.495956, 2, 8, 0, 0),
(321, 'TB080321', 'TOKO SIMBANG RAYA', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:09:09', '2017-12-23', 'MERAUKE', '1970-02-03', NULL, NULL, NULL, NULL, NULL, 140.402366, -8.495913, 2, 8, 0, 0),
(322, 'RM020322', 'WARUNG MAKAN PURNAMA', 'JALAN RAYA MANDALA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:18:33', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.391833, -8.490399, 4, 2, 0, 0),
(323, 'RT060323', 'TEGO', ' JL. NUSA BARONG, Gg. ASPAL BARU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:21:14', '2017-12-23', 'MERAUKE', '1970-02-03', NULL, NULL, NULL, NULL, NULL, 140.386836, -8.491053, 1, 6, 0, 0),
(324, 'RT040324', 'PETRONELANIS', ' JALAN NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:22:21', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.388343, -8.491174, 1, 4, 0, 0),
(325, 'RT060325', 'MERSI IRIYANTI', ' JALAN NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:29:11', '2017-12-23', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.381691, -8.491438, 1, 6, 0, 0),
(326, 'KNTR080326', 'KANTOR PENGADILAN AGAMA', ' JL. TMP', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:32:04', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.401317, -8.492865, 7, 8, 0, 0),
(327, 'RT090327', 'LASARUS', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:36:57', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.421124, -8.515803, 1, 9, 0, 0),
(328, 'RT040328', 'YULASTRI KARIM', ' JALAN NUSA BARONG, GANG BAITULRAHMAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:38:13', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.388134, -8.492192, 1, 4, 0, 0),
(329, 'RM120329', 'RUMAH MAKAN SEDERHANA', ' JL. RAYA MANDALA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:41:01', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.407698, -8.517499, 4, 12, 0, 0),
(330, 'RM040330', 'CAFE BOLA', ' JL. TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:44:17', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.388263, -8.500254, 4, 4, 0, 0),
(331, 'RT030331', 'BASUNI', ' JL. GAK, Gg. MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:46:28', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398665, -8.50787, 1, 3, 0, 0),
(332, 'PRSH080332', 'PT. P L N PERSERO', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 00:50:16', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.409233, -8.502471, 12, 8, 0, 0),
(333, 'TB070333', 'SMA JHON 23', ' JALAN KIMAAM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:04:50', '2017-12-23', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.391138, -8.485254, 2, 7, 0, 0),
(334, 'RT090334', 'PERUMAHAN OTORITAS BANDARA (11 KK)', 'JL. SUTAN SYAHRIL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:06:03', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.415, -8.507444, 1, 9, 0, 0),
(335, 'PRSH080335', 'KANTOR P L T D', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:14:11', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.409233, -8.502513, 12, 8, 0, 0),
(336, 'PRSH020336', 'PT. BUSAN AUTO FINANCE (BAF)', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:16:57', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.401874, -8.508029, 12, 2, 0, 0),
(337, 'PRSH020337', 'BANK MUAMALAT', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:19:26', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.401931, -8.508098, 12, 2, 0, 0),
(338, 'PRSH120338', 'BANK MEGA', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:21:57', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.406953, -8.516058, 12, 12, 0, 0),
(339, 'PRSH020339', 'PT. ANGKASA GITA SARANA (AGS)', 'JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:24:18', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.399765, -8.505683, 12, 2, 0, 0),
(340, 'PRSH120340', 'BANK PERKREDITAN RAKYAT IRIAN SENTOSA (BPR)', ' JL. RAYA MANDALA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:26:27', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.409851, -8.519213, 12, 12, 0, 0),
(341, 'PRSH030341', 'PT. PRIVINDO AKWILA', ' JL. RAYA MANDALA BAMPEL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:30:03', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.401326, -8.507365, 12, 3, 0, 0),
(342, 'PRSH020342', 'PT. JAMSOSTEK', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:32:40', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402474, -8.508974, 12, 2, 0, 0),
(343, 'PRSH080343', 'PT. PAPUA MODERN MOTOR', ' JL.. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:34:33', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.408798, -8.503315, 12, 8, 0, 0),
(344, 'PRSH020344', 'PT. WIKUTAMA SARANA (POM BENSIN AHMAD YANI)', ' JL. AHMAD YANI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:36:43', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.406455, -8.511435, 12, 2, 0, 0),
(345, 'PRSH070345', 'PT. MAROLINS', ' JL. ALIARKAM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:38:39', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.393001, -8.483484, 12, 7, 0, 0),
(346, 'PRSH030346', 'PT. RODA GEMILANG', ' JL.. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:42:28', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.400738, -8.50624, 12, 3, 0, 0),
(347, 'RT040347', 'TUTY YUHARIANTI (8 KK)', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:52:32', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.390951, -8.500721, 1, 4, 0, 0),
(348, 'HTL070348', 'HOTEL NAKORO', ' JALAN ERMASU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:52:54', '2017-12-23', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.397955, -8.495912, 15, 7, 0, 0),
(349, 'PRSH060349', 'CV. BERKAT BAROKAH JAYA', ' JL . SUMATERA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:57:47', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.390549, -8.489534, 12, 6, 0, 0),
(350, 'HTL020350', 'HOTEL AKAT', ' JALAN PRAJURIT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 01:58:38', '2017-12-23', 'MERAUKE', '1966-01-23', NULL, NULL, NULL, NULL, NULL, 140.399381, -8.494439, 15, 2, 0, 0),
(351, 'PRSH020351', 'S. HENDRI LEONARD/ AGEN TIKET', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:01:49', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.390795, -8.489154, 12, 2, 0, 0),
(352, 'KNTR090352', 'STASIUN KIPM MERAUKE', ' JALAN GARUDA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:04:56', '2017-12-23', 'MERAUKE', '1955-01-23', NULL, NULL, NULL, NULL, NULL, 140.416345, -8.525054, 7, 9, 0, 0),
(353, 'PRSH080353', 'TOKO BILBA', ' JL. TMP', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:05:54', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.400312, -8.490011, 12, 8, 0, 0),
(354, 'RT030354', 'SUMAJI', ' JL. RAYA MANDALA BAMPEL, Gg. SAYUR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:08:04', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.397618, -8.506617, 1, 3, 0, 0),
(355, 'RT020355', 'DEDI MURDOKO', ' JALAN BIAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:11:28', '2017-12-23', 'MERAUKE', '1995-01-23', NULL, NULL, NULL, NULL, NULL, 140.399297, -8.499916, 1, 2, 0, 0),
(356, 'TS080356', 'TOKO SINAR JAYA', ' JL. TMP TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:12:30', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.400742, -8.49172, 3, 8, 0, 0),
(357, 'TS080357', 'TOKO ANUGRAH', ' JL. TMP TRKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:15:56', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398415, -8.487114, 3, 8, 0, 0),
(358, 'HTL020358', 'HOTEL FLORA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:17:16', '2017-12-23', 'MERAUKE', '2018-01-23', NULL, NULL, NULL, NULL, NULL, 140.402613, -8.509087, 15, 2, 0, 0),
(359, 'RT040359', 'ANTONIUS SUNARDI', ' JL. TERNATE, Gg. OKABA 2', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:18:02', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.388542, -8.500228, 1, 4, 0, 0),
(360, 'HTL020360', 'HOTEL MARINA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:21:01', '2017-12-23', 'MERAUKE', '1966-01-23', NULL, NULL, NULL, NULL, NULL, 140.396615, -8.499033, 15, 2, 0, 0),
(361, 'TS080361', 'GUDANG TITI', ' JL. TMP', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:21:56', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.397863, -8.485904, 3, 8, 0, 0),
(362, 'TS080362', 'FARIDA SALON', ' JL. TMP TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:24:11', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398853, -8.48814, 3, 8, 0, 0),
(363, 'TS080363', 'PURNAMA INDAH', ' JL. TMP POLDER', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:26:02', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.398842, -8.488177, 3, 8, 0, 0),
(364, 'TS080364', 'TOKO EMAS UTAMA', ' JL. TMP TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:29:34', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.397723, -8.485622, 3, 8, 0, 0),
(365, 'RT040365', 'APSARI P.AMUNGKASI', ' JALAN GAK GANG AT-TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:30:26', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.393685, -8.50322, 1, 4, 0, 0),
(366, 'TS080366', 'UD. NABILA', ' JL. TMP TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:31:10', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.39827, -8.487108, 3, 8, 0, 0),
(367, 'TS070367', 'UMAR  (2 KK)', ' JL. AMPERA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:34:22', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.396875, -8.485367, 3, 7, 0, 0),
(368, 'HTL070368', 'HOTEL ASMAT', ' JALAN TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:35:22', '2017-12-23', 'MERAUKE', '1970-01-23', NULL, NULL, NULL, NULL, NULL, 140.390467, -8.48237, 15, 7, 0, 0),
(369, 'HTL020369', 'HOTEL NIRMALA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:41:40', '2017-12-23', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.391578, -8.489826, 15, 2, 0, 0),
(370, 'RM020370', 'RUMAH MAKAN PADANG PARIAMAN', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:45:12', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.403374, -8.510376, 4, 2, 0, 0),
(371, 'RT070371', 'IGNASIUS', ' JALAN PERIKANAN KOMP. BTN MARO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:50:21', '2017-12-23', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.411871, -8.501347, 1, 7, 0, 0),
(372, 'KI020372', 'MOHHADI KIOS FADIL', ' JALAN MISSI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:53:12', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.406002, -8.505865, 6, 2, 0, 0),
(373, 'RT020373', 'YOHANNES', ' JALAN PARAKOMANDO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 02:57:00', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.399415, -8.495747, 1, 2, 0, 0),
(374, 'RM120374', 'RUMAH MAKAN MAMA ETA', 'JALAN RAYA MANDALA MULI ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:04:55', '2017-12-23', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.407518, -8.517175, 4, 12, 0, 0),
(375, 'RM040375', 'WARUNG MAKAN MAMAKE', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:05:21', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.387566, -8.503483, 4, 4, 0, 0),
(376, 'RM020376', 'RUMAH MAKAN SAIYO BARU', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:07:46', '2017-12-23', 'MERAUKE', '1977-01-23', NULL, NULL, NULL, NULL, NULL, 140.392678, -8.493096, 4, 2, 0, 0),
(377, 'RM020377', 'RUMAH MAKAN PADANG JAYA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:10:17', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.392538, -8.492736, 4, 2, 0, 0),
(378, 'RM020378', 'RUMAH MAKAN SEDERHANA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:14:43', '2017-12-23', 'MERAUKE', '1978-01-23', NULL, NULL, NULL, NULL, NULL, 140.400885, -8.506815, 4, 2, 0, 0),
(379, 'KI120379', 'MUSTARI', ' JL. RAYA MANDALA MULI, DEPAN GEREJA PETRA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:19:59', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.408091, -8.516847, 6, 12, 0, 0),
(380, 'RM020380', 'RUMAH MAKAN MIE AYAM', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:31:41', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.39795, -8.501999, 4, 2, 0, 0),
(381, 'RM020381', 'RUMAH MAKAN ZORRO', ' JL.RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:36:03', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.398048, -8.502126, 4, 2, 0, 0),
(382, 'RM020382', 'RUMAH MAKAN ANGIN MAMIRI', ' JL RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:39:26', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.399056, -8.503587, 4, 2, 0, 0),
(383, 'RM020383', 'RUMAH MAKAN SOP SAUDARA', ' JL RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:52:02', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.40103, -8.506903, 4, 2, 0, 0),
(384, 'KNTR020384', 'KANTOR GEREJA KATEDRAL', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:56:40', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.390468, -8.486613, 7, 2, 0, 0),
(385, 'KNTR020385', 'KANTOR GEREJA KARMEL', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 03:59:52', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.390575, -8.487589, 7, 2, 0, 0),
(386, 'KNTR020386', 'KANTOR SAT LANTAS MERAUKE', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 04:03:48', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.390757, -8.488046, 7, 2, 0, 0),
(387, 'KNTR020387', 'KANTOR BANK BTPN', ' JL.RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 04:09:57', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.402425, -8.508875, 7, 2, 0, 0),
(388, 'KNTR020388', 'KANTOR GEREJA IMANUEL KODIM', ' JL.RYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 04:14:13', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.394099, -8.495213, 7, 2, 0, 0),
(389, 'KNTR080389', 'KANTOR DPRD MERAUKE', ' JL.BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 04:18:17', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.409592, -8.506758, 7, 8, 0, 0),
(390, 'KNTR040390', 'KANTOR MASJID AT-TAQWA', ' JL.IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 04:31:33', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.390902, -8.500775, 7, 4, 0, 0),
(391, 'KNTR030391', 'KANTOR KOPERASI KASWARI', ' JL. SESATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 04:35:45', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.396449, -8.503351, 7, 3, 0, 0),
(392, 'KNTR020392', 'KANTOR GRATIA', 'JL.BIAK ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 04:39:35', '2017-12-23', 'MERAUKE', '2018-01-22', NULL, NULL, NULL, NULL, NULL, 140.402473, -8.506315, 7, 2, 0, 0),
(393, 'RT120393', 'SARKO', ' JL. RAYA MANDALA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 05:39:16', '2017-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.41706, -8.525181, 1, 12, 0, 0),
(394, 'KNTR090394', 'KANTOR DINAS LINGKUNGAN HIDUP', ' JL. PETERNAKAN MOPAH LAMA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 06:09:03', '2018-12-23', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.432935, -8.535015, 7, 9, 0, 0),
(395, 'BGL040395', 'BENGKEL RAHAYU NAHAYU MOTOR', ' JALAN IRIAN SERINGGU DEPAN DEALER KAWASAKI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 06:33:47', '2017-12-23', 'MERAUKE', '2018-01-23', NULL, NULL, NULL, NULL, NULL, 140.393445, -8.498319, 11, 4, 0, 0),
(396, 'KI040396', 'KURNIAWATI', ' JALAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 23:19:48', '2017-12-23', 'MERAUKE', '1988-01-23', NULL, NULL, NULL, NULL, NULL, 140.389351, -8.502949, 6, 4, 0, 0),
(397, 'RT060397', 'MESTIANA SIAGIAN', ' JALAN NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 23:24:36', '2017-12-23', 'MERAUKE', '1966-01-24', NULL, NULL, NULL, NULL, NULL, 140.38138, -8.491481, 1, 6, 0, 0),
(398, 'RT020398', 'RIDWAN BADARUDIN', ' JALAN Gg. HINDUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-23 23:28:52', '2017-12-23', 'MERAUKE', '1988-01-24', NULL, NULL, NULL, NULL, NULL, 140.403556, -8.509337, 1, 2, 0, 0),
(399, 'RT040399', 'YUSUP', ' JALAN TERNATE Gg.KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 00:15:30', '2017-12-24', 'M', '1988-01-24', NULL, NULL, NULL, NULL, NULL, 140.383762, -8.493854, 1, 4, 0, 0),
(400, 'RT040400', 'JARI', ' JALAN TERNATE GANG KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 00:19:14', '2017-12-24', 'MERAUKE', '1966-01-24', NULL, NULL, NULL, NULL, NULL, 140.384025, -8.493723, 1, 4, 0, 0),
(401, 'RT020401', 'JEFFRY LAMBU ALLORERUNG', ' JALAN RAYA MANDALA GANG HINDUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 00:29:03', '2017-12-24', 'MERAUKE', '1977-01-24', NULL, NULL, NULL, NULL, NULL, 140.403599, -8.509503, 1, 2, 0, 0),
(402, 'RT090402', 'SUMARNO', ' JALAN PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 00:57:19', '2017-12-24', 'MERAUKE', '1977-01-24', NULL, NULL, NULL, NULL, NULL, 140.417472, -8.506826, 1, 9, 0, 0),
(403, 'RT040403', 'FARIDA MADI', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 01:00:53', '2017-12-24', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.39149, -8.500339, 1, 4, 0, 0),
(404, 'TB020404', 'WARUNG BAKSO PRIMA', ' JALAN AHMAD YANI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 01:27:54', '2017-12-24', 'MERAUKE', '1967-01-24', NULL, NULL, NULL, NULL, NULL, 140.408154, -8.510476, 2, 2, 0, 0),
(405, 'RT060405', 'WARSITO', ' JL. NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 01:33:55', '2017-12-24', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.386415, -8.489951, 1, 6, 0, 0),
(406, 'KNTR040406', 'ALAMSYAH/TEMPAT PENITIPAN ANAK', ' JALAN IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 01:42:58', '2017-12-24', 'MERAUKE', '1988-01-24', NULL, NULL, NULL, NULL, NULL, 140.390022, -8.502332, 7, 4, 0, 0),
(407, 'KI040407', 'AMIRULLAH', ' JALAN IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 01:46:57', '2017-12-24', 'MERAUKE', '1966-01-24', NULL, NULL, NULL, NULL, NULL, 140.386518, -8.504196, 6, 4, 0, 0),
(408, 'KI040408', 'FAISAL', ' JALAN IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 01:48:49', '2017-12-24', 'MERAUKE', '1977-01-24', NULL, NULL, NULL, NULL, NULL, 140.38669, -8.504154, 6, 4, 0, 0),
(409, 'KI080409', 'SIMON PETRUS YAMLEAN', 'JALAN BRAWIJAYA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 02:10:00', '2017-12-24', 'MERAUKE', '1978-01-25', NULL, NULL, NULL, NULL, NULL, 140.408792, -8.503374, 6, 8, 0, 0),
(410, 'RT020410', 'SARA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 02:15:45', '2017-12-24', 'MERAUKE', '1988-01-24', NULL, NULL, NULL, NULL, NULL, 140.403642, -8.509536, 1, 2, 0, 0),
(411, 'RM030411', 'RUMAH MAKAN PA DE GONDRONG / ALIYAS N', ' JALAN RAYA MANDALA BAMPEL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 02:50:08', '2017-12-24', 'MERAUKE', '1955-01-24', NULL, NULL, NULL, NULL, NULL, 140.401539, -8.507645, 4, 3, 0, 0),
(412, 'RT020412', 'AHMAD SANUSI', ' JALAN RAYA MANDALA, BELAKANG RENTAL BMD', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 02:57:42', '2017-12-24', 'MERAUKE', '1966-01-24', NULL, NULL, NULL, NULL, NULL, 140.401432, -8.507449, 1, 2, 0, 0),
(413, 'RT080413', 'N. T .SITOMPUL', ' JALAN MANGGA DUA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 03:05:28', '2017-12-24', 'MERAUKE', '1969-01-24', NULL, NULL, NULL, NULL, NULL, 140.401561, -8.488393, 1, 8, 0, 0),
(414, 'RT020414', 'KADIR ', ' JL RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 04:50:53', '2017-12-24', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.389956, -8.48577, 1, 2, 0, 0),
(415, 'RT060415', 'LEWIR', ' JL NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 04:55:18', '2017-12-24', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.383288, -8.490542, 1, 6, 0, 0),
(416, 'KI120416', 'MUSTARI', ' JL RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 05:07:16', '2017-12-24', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.408048, -8.516805, 6, 12, 0, 0),
(417, 'RT040417', 'PUJIYONO', ' JL TIDORE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 05:25:24', '2017-12-24', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.389773, -8.497457, 1, 4, 0, 0),
(418, 'KI120418', 'KIOS BERKAT JAYA/HERMINCE', ' JL. RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-24 05:50:35', '2017-12-24', 'MERAUKE', '1970-01-24', NULL, NULL, NULL, NULL, NULL, 140.406825, -8.517637, 6, 12, 0, 0),
(419, 'KNTR070419', 'SATKER PJN WILAYAH III PAPUA (TANAH MERAH)', ' JL. TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:13:56', '2017-12-25', 'MERAUKE', '2018-11-01', NULL, NULL, NULL, NULL, NULL, 140.393403, -8.481588, 7, 7, 0, 0),
(420, 'RT020420', 'NUR NINGSIH', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:18:14', '2017-12-25', 'MERAUKE', '1970-02-01', NULL, NULL, NULL, NULL, NULL, 140.39079, -8.489179, 1, 2, 0, 0),
(421, 'RT070421', 'REVY JUNIASARI', ' JALAN POLDER DALAM I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:22:39', '2017-12-25', 'MERAUKE', '1988-01-25', NULL, NULL, NULL, NULL, NULL, 140.398739, -8.483914, 1, 7, 0, 0),
(422, 'RT030422', 'RAGIL HIDAYAT', ' JL. GAK, Gg. SAYUR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:23:58', '2017-12-25', 'MERAUKE', '1975-02-01', NULL, NULL, NULL, NULL, NULL, 140.398399, -8.507315, 1, 3, 0, 0),
(423, 'RT040423', 'REVY JUNIASARI', ' JALAN TERNATE GANG EVANDEKAY', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:29:41', '2017-12-25', 'MERAUKE', '1989-01-25', NULL, NULL, NULL, NULL, NULL, 140.38521, -8.495279, 1, 4, 0, 0),
(424, 'RT080424', 'ADI RAHMAT EFFENDI', ' JL. PEMUDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:32:23', '2017-12-25', 'MERAUKE', '1970-02-01', NULL, NULL, NULL, NULL, NULL, 140.41313, -8.503559, 1, 8, 0, 0),
(425, 'RT040425', 'BAMBANG', ' JL. TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:38:48', '2017-12-25', 'MERAUKE', '1970-02-01', NULL, NULL, NULL, NULL, NULL, 140.381933, -8.494874, 1, 4, 0, 0),
(426, 'RT060426', 'SAERAH', ' JL. TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:42:38', '2017-12-25', 'MERAUKE', '1970-02-01', NULL, NULL, NULL, NULL, NULL, 140.382255, -8.494524, 1, 6, 0, 0),
(427, 'RT060427', 'A. YUMAME', ' JALAN KALIMANTAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:46:05', '2017-12-25', 'MERAUKE', '1988-01-25', NULL, NULL, NULL, NULL, NULL, 140.3883, -8.484274, 1, 6, 0, 0),
(428, 'TS020428', 'ASURANSI BUMI PUTERA', ' JL. PARAKOMANDO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:46:56', '2017-12-25', 'MERAUKE', '1973-02-01', NULL, NULL, NULL, NULL, NULL, 140.400752, -8.495126, 3, 2, 0, 0),
(429, 'RT040429', 'PATMURYATI (8 KK)', ' JL. TERNATE, Gg. MAWAR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 00:51:54', '2017-12-25', 'MERAUKE', '1973-02-04', NULL, NULL, NULL, NULL, NULL, 140.386989, -8.49902, 1, 4, 0, 0),
(430, 'RT030430', 'ISTY KOLIFAH (4 KK)', 'JL. GAK, Gg. SAYUR ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 01:03:14', '2017-12-25', 'MERAUKE', '1973-02-04', NULL, NULL, NULL, NULL, NULL, 140.398206, -8.506604, 1, 3, 23, 1),
(431, 'HTL020431', 'HOTEL ITESE', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 01:08:33', '2017-12-25', 'MERAUKE', '1970-02-01', NULL, NULL, NULL, NULL, NULL, 140.396823, -8.500617, 15, 2, 0, 0),
(432, 'RM080432', 'HADI BAKERY', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 01:11:44', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.406939, -8.499935, 4, 8, 0, 0),
(433, 'RT090433', 'KOMPI SENAPAN A ', '  JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 01:17:05', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.424011, -8.52581, 1, 9, 23, 1),
(434, 'RT060434', 'SRI LESTARI', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 01:42:48', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.384477, -8.490094, 1, 6, 0, 0),
(435, 'RT030435', 'STANISLAUS P. GEBZE', ' JALAN SESATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 01:44:53', '2017-12-25', 'MERAUKE', '1969-01-25', NULL, NULL, NULL, NULL, NULL, 140.397221, -8.502625, 1, 3, 0, 0),
(436, 'RT060436', 'DISKE JOHANA LUMINGKEWAS', 'JL. NUSA BARONG,Gg. ASPAL BARU ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 01:47:10', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.387126, -8.491185, 1, 6, 0, 0),
(437, 'RT120437', 'SITI AMINAH I (4KK)', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 01:53:24', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.407669, -8.520716, 1, 12, 0, 0),
(438, 'RT120438', 'SITI AMINAH II', ' JL. RAYA MANDALA, Gg. KPKN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 02:01:08', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.407583, -8.520928, 1, 12, 0, 0),
(439, 'RT020439', 'PETRUS ELMAS', ' JL. RAYA MANDALA,Gg. LARENTA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 02:13:53', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.400227, -8.503124, 1, 2, 0, 0),
(440, 'RT060440', 'JOKO RENYAAN', ' JALAN JAWA, ASRAMA PELAYARAN LAMA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 02:29:29', '2017-12-25', 'MERAUKE', '1955-01-25', NULL, NULL, NULL, NULL, NULL, 140.38513, -8.487052, 1, 6, 0, 0),
(441, 'RT020441', 'MARIA YOSEFINA TAMNGE', ' JL. RAYA MANDALA, Gg. LARENTA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 03:51:41', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.400224, -8.503127, 1, 2, 0, 0),
(442, 'RT020442', 'FENNY MARIA WAMAFMAA', ' JL. RAYA MANDALA, Gg. LARENTA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 03:57:35', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.399991, -8.503097, 1, 2, 0, 0),
(443, 'RT020443', 'CAROLINA MARLINA', ' JL. RAYA MANDALA, Gg. LARENTA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:01:26', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.400201, -8.503007, 1, 2, 0, 0),
(444, 'RT020444', 'ALFONS TAMNGE', ' JL. RAYA MANDALA, Gg. LARENTA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:04:50', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.400448, -8.502975, 1, 2, 0, 0),
(445, 'RT020445', 'ROBERTH MATURBONGS', ' JL. RAYA MANDALA, Gg. LARENTA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:08:45', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.400549, -8.502943, 1, 2, 0, 0),
(446, 'RT020446', 'BERNADUS FOFIED', ' JL. RAYA MANDALA, Gg. LARENTA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:11:42', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.400609, -8.502901, 1, 2, 0, 0),
(447, 'RT040447', 'MUHAMMAD JAUHARI', ' JL. TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:15:04', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.388906, -8.500382, 1, 4, 0, 0),
(448, 'RT020448', 'IBU OYO', ' JL RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:15:53', '2017-12-25', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.390577, -8.487706, 1, 2, 0, 0);
INSERT INTO `re_costumer` (`cost_id`, `cost_id_pel`, `cost_name`, `cost_address`, `cost_prov`, `cost_kab`, `cost_kec`, `cost_status`, `cost_date`, `cost_periode_terakhir`, `cost_tmp_lahir`, `cost_tgl_lahir`, `cost_phone`, `cost_foto`, `cost_lokasi`, `cost_denah`, `cost_ktp`, `cost_lang`, `cost_latt`, `cost_type_id`, `cost_desa_id`, `cost_user_id`, `cost_jadwal_id`) VALUES
(449, 'RT080449', 'VENI KAUFAN GULTOM', ' JL. PEMUDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:22:52', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.411369, -8.503611, 1, 8, 0, 0),
(450, 'RT070450', 'JUWITO', ' JL. RAYA MANDALA, ASPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:26:53', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.391497, -8.487791, 1, 7, 0, 0),
(451, 'RT080451', 'P. BUANG', ' JL.ANGKASA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:27:22', '2017-12-25', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.402456, -8.49458, 1, 8, 0, 0),
(452, 'RT070452', 'BENJAMIN SIMATUPANG', ' JALAN TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:28:32', '2017-12-25', 'MERAUKE', '1988-01-11', NULL, NULL, NULL, NULL, NULL, 140.391732, -8.481765, 1, 7, 0, 0),
(453, 'RT070453', 'SYAMSUDIN', ' JL. AMPERA III', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:30:54', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.396178, -8.486199, 1, 7, 0, 0),
(454, 'RT080454', 'MULYA RABU', ' JL.ANGKASA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:31:53', '2017-12-25', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.403133, -8.494209, 1, 8, 0, 0),
(455, 'RT020455', 'SONYA PONGSADAN', ' JL. MISI ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:33:17', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.405351, -8.500111, 1, 2, 0, 0),
(456, 'RT080456', 'RIYANTO I, II & II (3 KK)', ' JL. SUMATERA, Gg. DUMATUBUN ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:39:04', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.39006, -8.491027, 1, 8, 23, 1),
(457, 'RT070457', 'YOHANA TJIU', ' PINTU AIR GUDANG ARANG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:39:47', '2017-12-25', 'MERAUKE', '1948-01-25', NULL, NULL, NULL, NULL, NULL, 140.394614, -8.475873, 1, 7, 0, 0),
(458, 'RT080458', 'CRISTOVORUS', ' JL ANGKASA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:39:50', '2017-12-25', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.406537, -8.490047, 1, 8, 0, 0),
(459, 'RT080459', 'H JUNTAK', ' JL. ANGKASA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:44:58', '2017-12-25', 'MERAUKE', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 140.406633, -8.490334, 1, 8, 0, 0),
(460, 'RT060460', 'ADE RAGIL AGUNG WIBOWO', ' JL. MALUKU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:46:09', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.3871, -8.485406, 1, 6, 0, 0),
(461, 'RT090461', 'H. BASRI', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:50:15', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.422143, -8.521002, 1, 9, 0, 0),
(462, 'RT020462', 'MAULANA', ' JL. PRAJURIT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:53:22', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.398071, -8.490796, 1, 2, 0, 0),
(463, 'TB090463', 'TOKO SUMBER ANEKA', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:56:46', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.405476, -8.513848, 2, 9, 0, 0),
(464, 'KNTR070464', 'GPI PAPUA JEMAAT PNIEL ', ' JALAN ERMASU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 04:56:54', '2017-12-25', 'MERAUKE', '1977-01-25', NULL, NULL, NULL, NULL, NULL, 140.391986, -8.483016, 7, 7, 0, 0),
(465, 'RT040465', 'SURYA IRAWAN', ' JL.. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:04:46', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.390822, -8.50069, 1, 4, 0, 0),
(466, 'RT070466', 'JUSTINA SIANTURI', ' JALAN TRIKORA KOMPLEKS PU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:07:10', '2017-12-25', 'MERAUKE', '1966-01-25', NULL, NULL, NULL, NULL, NULL, 140.394483, -8.481031, 1, 7, 0, 0),
(467, 'RT120467', 'MARO SASMITJADI', ' JL. RAYA MANDALA, Gg. KPKN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:08:55', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.407518, -8.520578, 1, 12, 0, 0),
(468, 'RT070468', 'SINTA', ' JALAN TRIKORA KOMPLEKS PU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:10:56', '2017-12-25', 'MERAUKE', '1988-01-25', NULL, NULL, NULL, NULL, NULL, 140.394343, -8.481035, 1, 7, 0, 0),
(469, 'TB040469', 'KNS I & II (HERMINA OEY GIOK MIEN)', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:13:41', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.387281, -8.503819, 2, 4, 0, 0),
(470, 'RT040470', 'FREDERIKA TJIU', ' JALAN NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:16:58', '2017-12-25', 'MERAUKE', '1966-01-25', NULL, NULL, NULL, NULL, NULL, 140.385001, -8.493176, 1, 4, 0, 0),
(471, 'PRSH020471', 'PT. PLASMA', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:17:10', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.396989, -8.500903, 12, 2, 0, 0),
(472, 'RT040472', 'HASNI', ' JALAN SERINGGU GANG KANGGURU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:21:44', '2017-12-25', 'MERAUKE', '1988-01-25', NULL, NULL, NULL, NULL, NULL, 140.390081, -8.499073, 1, 4, 0, 0),
(473, 'RT090473', 'ARI KISWANTO', ' JL. POMPA AIR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:22:00', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.408821, -8.517157, 1, 9, 0, 0),
(474, 'RT020474', 'MUHAMAD YATIM', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:27:41', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.397255, -8.489658, 1, 2, 0, 0),
(475, 'RT070475', 'WIHELMUS WALONG', ' JALAN ERMASU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:30:11', '2017-12-25', 'MERAUKE', '1945-01-25', NULL, NULL, NULL, NULL, NULL, 140.396904, -8.493713, 1, 7, 0, 0),
(476, 'RT120476', 'SRI PRIYANTI', ' JL. RAYA MANDALA, Gg. DARMO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:31:47', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.409293, -8.519937, 1, 12, 0, 0),
(477, 'KI070477', 'KIOS RAIHAN - SYAMSURIANI', ' JALAN TMP POLDER', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:36:29', '2017-12-25', 'MERAUKE', '1980-01-25', NULL, NULL, NULL, NULL, NULL, 140.400306, -8.489591, 6, 7, 0, 0),
(478, 'KI030478', 'PAULUS', ' JL. RAYA MANDALA BAMPEL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:37:06', '2017-12-25', 'MERAUKE', '2018-01-24', NULL, NULL, NULL, NULL, NULL, 140.398161, -8.502896, 6, 3, 0, 0),
(479, 'KI030479', 'BAKSO CELEBES SAMURI', ' JALAN SERINGG GANG MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:43:01', '2017-12-25', 'MERAUKE', '1977-01-25', NULL, NULL, NULL, NULL, NULL, 140.392655, -8.500289, 6, 3, 0, 0),
(480, 'RT090480', 'VONNY EVIE TJONG (9KK)', ' JL. ONGGATMIT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:43:08', '2017-12-25', 'MERAUKE', '2018-01-09', NULL, NULL, NULL, NULL, NULL, 140.402249, -8.512761, 1, 9, 0, 0),
(481, 'RT040481', 'ROSALINA WAME BASIK BASIK', ' JAKAN NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:46:57', '2017-12-25', 'MERAUKE', '1988-01-25', NULL, NULL, NULL, NULL, NULL, 140.38697, -8.492085, 1, 4, 0, 0),
(482, 'RT040482', 'AHMAD MUNIN', ' JALAN TERNATE GANG KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 05:59:31', '2017-12-25', 'MERAUKE', '1977-01-25', NULL, NULL, NULL, NULL, NULL, 140.383708, -8.494272, 1, 4, 0, 0),
(483, 'RT040483', 'SLAMET', ' JALAN TERNATE GANG HJ.KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 06:04:22', '2017-12-25', 'MERAUKE', '1956-01-25', NULL, NULL, NULL, NULL, NULL, 140.383687, -8.494271, 1, 4, 0, 0),
(484, 'RT040484', 'HENDRIKUS MAHUZE', ' JALAN NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 06:09:36', '2017-12-25', 'MERAUKE', '1988-01-25', NULL, NULL, NULL, NULL, NULL, 140.386905, -8.492065, 1, 4, 0, 0),
(485, 'RT040485', 'IMELDA JENI', ' JALAN TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 06:14:08', '2017-12-25', 'MERAUKE', '1988-01-25', NULL, NULL, NULL, NULL, NULL, 140.387162, -8.498783, 1, 4, 0, 0),
(486, 'RT090486', 'AGUSTINUS ROY', ' JALAN YOBAR I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 06:22:04', '2017-12-25', 'MERAUKE', '2005-01-25', NULL, NULL, NULL, NULL, NULL, 140.401475, -8.510757, 1, 9, 0, 0),
(487, 'TB070487', 'PT. PELNI', ' JALAN SABANG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 06:36:43', '2017-12-25', 'MERAUKE', '1966-01-25', NULL, NULL, NULL, NULL, NULL, 140.390315, -8.479294, 2, 7, 0, 0),
(488, 'RT020488', 'AMIR HASAN', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 23:21:19', '2017-12-25', 'MERAUKE', '1956-01-25', NULL, NULL, NULL, NULL, NULL, 140.396346, -8.499184, 1, 2, 0, 0),
(490, 'KI090490', 'KIOS NUR JANAH', ' JL. PGT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 23:27:16', '2017-12-25', 'MERAUKE', '2018-01-26', NULL, NULL, NULL, NULL, NULL, 140.412878, -8.520798, 6, 9, 0, 0),
(491, 'RT080491', 'KORNELIS TAMBONOP', ' JL. TMP POLDER', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 23:33:29', '2017-12-25', 'MERAUKE', '2018-01-26', NULL, NULL, NULL, NULL, NULL, 140.397244, -8.485217, 1, 8, 0, 0),
(492, 'RT040492', 'WAHYUDI RIFAI', ' JL. IRIAN SERINGGU, Gg. AT-TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 23:44:26', '2017-12-25', 'MERAUKE', '2018-01-12', NULL, NULL, NULL, NULL, NULL, 140.393056, -8.503113, 1, 4, 0, 0),
(493, 'KNTR070493', 'KANTOR BINA MARGA TANAH MERAH', ' JALAN TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 23:52:12', '2017-12-25', 'MERAUKE', '1977-01-26', NULL, NULL, NULL, NULL, NULL, 140.393442, -8.481524, 7, 7, 0, 0),
(494, 'RT120494', 'IKHSAN ADMANTORO', ' JL. RAYA MANDALA SPADEM, Gg. KEMBAR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-25 23:53:50', '2017-12-25', 'MERAUKE', '2018-01-26', NULL, NULL, NULL, NULL, NULL, 140.409675, -8.519645, 1, 12, 0, 0),
(497, 'PRSH070497', 'PT. SURYA MEDISTRINDO', ' JL. ARUNU, Gg. ARU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-26 00:12:37', '2017-12-26', 'MERAUKE', '2018-01-26', NULL, NULL, NULL, NULL, NULL, 140.393281, -8.489818, 12, 7, 0, 0),
(498, 'RT020498', 'ESTER A NAVI', ' JALAN MARTADINATA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-26 00:40:19', '2017-12-26', 'MERAUKE', '1854-01-26', NULL, NULL, NULL, NULL, NULL, 140.407939, -8.508518, 1, 2, 0, 0),
(499, 'KNTR090499', 'KANTOR BASARNAS MERAUKE', ' JL. MARTADINATA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-26 05:58:17', '2017-12-26', 'MERAUKE', '2018-01-26', NULL, NULL, NULL, NULL, NULL, 140.413618, -8.519112, 7, 9, 0, 0),
(500, 'RT050500', 'FRANS KOBUN', ' JL. ARAFURA - BUTI Gg. ANTONIUS ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 02:15:40', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.387649, -8.514945, 1, 5, 23, 1),
(501, 'RT090501', 'ALO BAMBANG', ' JL. PEMBANGUNAN, Gg. GEMPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 02:24:42', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.423354, -8.522403, 1, 9, 0, 0),
(502, 'RT040502', 'MULYONO', ' JL. TERNATE, Gg. MUDITA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 02:32:59', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.387018, -8.501103, 1, 4, 0, 0),
(503, 'RT030503', 'WIDYO SASONGKO', ' JALAN GAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 02:39:54', '2017-12-29', 'MERAUKE', '1966-01-29', NULL, NULL, NULL, NULL, NULL, 140.391221, -8.503124, 1, 3, 0, 0),
(504, 'TB020504', 'FIONA CAKE', 'JL. PARAKOMANDO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 02:41:03', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.400126, -8.495458, 2, 2, 0, 0),
(505, 'BGL030505', 'BENGKEL RASONGKO TEKNIK WIDYO SASONGKO', ' JALAN GAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 02:44:17', '2017-12-29', 'MERAUKE', '1966-01-29', NULL, NULL, NULL, NULL, NULL, 140.396025, -8.507856, 11, 3, 0, 0),
(506, 'KNTR020506', 'KANTOR KEMENTERIAN AGAMA', 'JL. ERMASSU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 02:48:24', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.395814, -8.491417, 7, 2, 0, 0),
(507, 'RT060507', 'ANTONIA E LETSOIN', ' JALAN RADIO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 02:49:51', '2017-12-29', 'MERAUKE', '1977-01-29', NULL, NULL, NULL, NULL, NULL, 140.383722, -8.485998, 1, 6, 0, 0),
(508, 'KI090508', 'SULHAN', ' JALAN GARUDA SPADEM DEKAT MESJID', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 03:00:16', '2017-12-29', 'MERAUKE', '1956-01-29', NULL, NULL, NULL, NULL, NULL, 140.415853, -8.525067, 6, 9, 0, 0),
(509, 'KI090509', 'EDI MURYANTO', ' JALAN GARUDA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 03:03:07', '2017-12-29', 'MERAUKE', '1956-01-29', NULL, NULL, NULL, NULL, NULL, 140.415816, -8.525061, 6, 9, 0, 0),
(510, 'RT120510', 'OS. TALOHANAS 1', 'JL. RAYA MANDALA, SAMPING KANTOR TRAKINDO  ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 03:18:12', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.406325, -8.514426, 1, 12, 23, 1),
(511, 'TS070511', 'TOKO ANEKA', ' JL. ARU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 03:29:26', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.394087, -8.491871, 3, 7, 0, 0),
(512, 'RT120512', 'OS. TALOHANAS 2', ' JL. RAYA MANDALA, SAMPING KANTOR TRAKINDO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 03:34:16', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.40665, -8.51416, 1, 12, 0, 0),
(513, 'RT120513', 'OS. TALOHANAS 3', ' JL. RAYA MANDALA. SAMPING KANTOR TRAKINDO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 03:38:15', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.406711, -8.514141, 1, 12, 0, 0),
(514, 'RT040514', 'EKO SUPARTONO', ' JALAN TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 03:38:55', '2017-12-29', 'MERAUKE', '1988-01-29', NULL, NULL, NULL, NULL, NULL, 140.388496, -8.500641, 1, 4, 0, 0),
(515, 'RT120515', 'OS. TALOHANAS 4', ' JL. RAYA MANDALA, SAMPING KANTOR TRAKINDO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 03:40:50', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.406778, -8.514066, 1, 12, 0, 0),
(516, 'RT030516', 'MASAMA TARIGAN', ' JALAN SERINGGU GANG AT-TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 04:04:35', '2017-12-29', 'MERAUKE', '1945-01-29', NULL, NULL, NULL, NULL, NULL, 140.394185, -8.503676, 1, 3, 0, 0),
(517, 'RT040517', 'ABU RUMADAR', ' JL. IRIAN SERINGGU, Gg. KANGGURU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 05:20:42', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.390093, -8.499336, 1, 4, 0, 0),
(518, 'TB020518', 'YAYASAN KALAM KUDUS', ' JL. RAYA MANDALA MERAUKE ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 05:30:37', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.403394, -8.510973, 2, 2, 23, 1),
(519, 'RT020519', 'SUPRIYONO', ' JL. PENDIDIKAN (Perumahan Guru Blok B N. 2 )', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-29 06:10:16', '2017-12-29', 'MERAUKE', '2018-01-29', NULL, NULL, NULL, NULL, NULL, 140.402476, -8.501925, 1, 2, 0, 0),
(520, 'RT070520', 'MULYADHAN', ' JALAN TMP POLDER NO.4', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-30 01:23:47', '2017-12-30', 'MERAUKE', '1977-01-30', NULL, NULL, NULL, NULL, NULL, 140.396057, -8.481085, 1, 7, 0, 0),
(521, 'RS080521', 'APOTIK DLEANDRO FARMA', ' JALAN BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-30 01:50:22', '2017-12-30', 'MERAUKE', '2016-01-30', NULL, NULL, NULL, NULL, NULL, 140.407001, -8.499742, 10, 8, 0, 0),
(522, 'RT060522', 'JARED RIEPMA', ' JL. NOARI ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-30 01:51:54', '2017-12-30', 'MERAUKE', '2018-01-30', NULL, NULL, NULL, NULL, NULL, 140.384832, -8.486995, 1, 6, 23, 1),
(523, 'RT090523', 'ATMA PUTRA MERDEKA', ' JALAN PEMBANGUNAN GANG GEMPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-30 03:07:18', '2017-12-30', 'MERAUKE', '1999-01-30', NULL, NULL, NULL, NULL, NULL, 140.419317, -8.512193, 1, 9, 0, 0),
(524, 'KI040524', 'Hj SAENAB                                               ', ' JALAN TIMOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-30 05:00:47', '2017-12-30', 'MERAUKE', '1977-01-30', NULL, NULL, NULL, NULL, NULL, 140.391304, -8.492869, 6, 4, 0, 0),
(525, 'RT070525', 'SRI ENDAH PERTIWI', ' JL. PERIKANAN/ JL. MARO ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-30 23:47:47', '2017-12-30', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.411725, -8.500726, 1, 7, 0, 0),
(526, 'RT070526', 'SUNARSIH', ' JL. PERIKANAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-30 23:54:52', '2017-12-30', 'NGAWI', '1966-09-15', NULL, NULL, NULL, NULL, NULL, 140.412715, -8.500667, 1, 7, 0, 0),
(527, 'RT060527', 'FLORIDA Y. TETHOOL', ' JL NUSA BARONG, Gg. HA. KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 00:01:57', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.382392, -8.49454, 1, 6, 0, 0),
(528, 'KI090528', 'KIOS BIRU (H. SULEMAN)', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 00:08:23', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.416743, -8.507273, 6, 9, 0, 0),
(529, 'TS090529', 'KIOS BIRU (H. SULEMAN)', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 00:09:31', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.416743, -8.507273, 3, 9, 0, 0),
(530, 'RT090530', 'H. SULEMAN (6 KK)', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 00:14:23', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.416842, -8.507315, 1, 9, 0, 0),
(531, 'RM040531', 'WARUNG MAKAN NUR SAHID', ' JL. IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 00:20:27', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.391075, -8.500721, 4, 4, 0, 0),
(532, 'RT020532', 'MULA MARPAUNG', ' JL. BIAK II NO. 4 BELAKANG TOKO METRO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 00:31:09', '2017-12-31', 'TARUTUNG', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.398045, -8.500993, 1, 2, 0, 0),
(533, 'RT070533', 'FX. MARTONO', ' JL.POLDER 1  ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 02:59:46', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.399963, -8.483541, 1, 7, 23, 1),
(534, 'RT020534', 'MUHAMMAD NUR', ' JL. GOR ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 04:34:26', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.397051, -8.488244, 1, 2, 23, 1),
(535, 'KI020535', 'MUHAMMAD NUR', ' JL. GOR ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 04:39:00', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.397002, -8.488251, 6, 2, 23, 1),
(536, 'RT020536', 'MUHAMMAD NUR', ' JL. PENDIDIKAN  ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 04:48:15', '2017-12-31', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.399195, -8.497696, 1, 2, 23, 1),
(537, 'RT030537', 'SUKO SUGIARTO', ' JALAN GAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 23:45:54', '2017-12-31', 'MERAUKE', '1967-02-01', NULL, NULL, NULL, NULL, NULL, 140.394791, -8.506565, 1, 3, 0, 0),
(538, 'RM120538', 'WARUNG BESTARI / MUHTONI', ' JL. RAYA MANDALA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 23:53:42', '2017-12-31', 'PURWOREJO', '1959-07-27', NULL, NULL, NULL, NULL, NULL, 140.410335, -8.519823, 4, 12, 0, 0),
(539, 'RT120539', 'MUHTONI (3 KK)', ' JL. RAYA MANDALA SPADEM , Gg. BPJS', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-01-31 23:58:20', '2017-12-31', 'PURWOREJO', '1969-07-09', NULL, NULL, NULL, NULL, NULL, 140.412079, -8.520378, 1, 12, 0, 0),
(540, 'RT120540', 'SANIRAN', 'JL. RAYA MANDALA SPADEM , Gg. BPJS', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 00:05:13', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.412154, -8.520559, 1, 12, 0, 0),
(541, 'KI070541', 'AGUSMAN KIOS', ' JALAN ERMASU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 00:42:20', '2018-01-01', 'MERAUKE', '1967-02-01', NULL, NULL, NULL, NULL, NULL, 140.39551, -8.490833, 6, 7, 0, 0),
(542, 'RT070542', 'STEVE HANOATUBUN', ' JALAN ERMASU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 00:58:29', '2018-01-01', 'MERAUKE', '1955-02-01', NULL, NULL, NULL, NULL, NULL, 140.395569, -8.490997, 1, 7, 0, 0),
(543, 'RT040543', 'PENINA J. NAWIPA', 'KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:10:47', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.383819, -8.502351, 1, 4, 0, 0),
(544, 'RT040544', 'WIDODO', ' JALAN TERNATE GANG PAPUA 2', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:14:28', '2018-01-01', 'MERAUKE', '1977-02-01', NULL, NULL, NULL, NULL, NULL, 140.38705, -8.498525, 1, 4, 0, 0),
(545, 'RT040545', 'MASRIYAH', ' ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:16:04', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.383894, -8.502478, 1, 4, 0, 0),
(546, 'RT040546', 'TAHIR', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:18:27', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384208, -8.502788, 1, 4, 0, 0),
(547, 'RT040547', 'REGINA SARASWATI', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:20:52', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384208, -8.502745, 1, 4, 0, 0),
(548, 'RT040548', 'HENDRA', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:22:46', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.383604, -8.502065, 1, 4, 0, 0),
(549, 'TS080549', 'SOFA TALASE', ' JALAN BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:23:25', '2018-01-01', 'MERAUKE', '1988-02-01', NULL, NULL, NULL, NULL, NULL, 140.406395, -8.499491, 3, 8, 0, 0),
(550, 'RT040550', 'AKBAR/SUMARNI MALIK', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:24:40', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384337, -8.503, 1, 4, 0, 0),
(551, 'RT040551', 'SAMSUL', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:27:05', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384315, -8.502915, 1, 4, 0, 0),
(552, 'RT040552', 'AGUS RUBIYANTO', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:30:34', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384466, -8.503085, 1, 4, 0, 0),
(553, 'RT080553', 'BEACHTRIS', ' JALAN PEMUDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:31:02', '2018-01-01', 'MERAUKE', '1980-02-01', NULL, NULL, NULL, NULL, NULL, 140.409179, -8.504626, 1, 8, 0, 0),
(554, 'RT040554', 'AHMAD RAHMAN', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:32:43', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384659, -8.502873, 1, 4, 0, 0),
(555, 'RT040555', 'MUHAMAD IRWAN', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:35:09', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384616, -8.502809, 1, 4, 0, 0),
(556, 'RT040556', 'LA ODE MOANE MUISA', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:37:30', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384551, -8.502661, 1, 4, 0, 0),
(557, 'RT040557', 'AMIRUDDIN', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:39:30', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384677, -8.502892, 1, 4, 0, 0),
(558, 'RT040558', 'SALIKI', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:41:48', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384444, -8.502724, 1, 4, 0, 0),
(559, 'RT040559', 'JUNAEDI SEPTIARA', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:43:40', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384423, -8.502597, 1, 4, 0, 0),
(560, 'RT040560', 'MANSYUR', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:45:31', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384059, -8.502313, 1, 4, 0, 0),
(561, 'RT040561', 'YULIN RANTEKATA', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:47:41', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384087, -8.502266, 1, 4, 0, 0),
(562, 'RT040562', 'MIFTAH FUAD ALHAMID', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:49:17', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384058, -8.502342, 1, 4, 0, 0),
(563, 'RT040563', 'LILIS YUNA ANI', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:51:45', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384076, -8.502245, 1, 4, 0, 0),
(564, 'RT040564', 'STANLY H.D. LOPIES', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:54:50', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.391075, -8.500721, 1, 4, 0, 0),
(565, 'RT050565', 'AGUNG PRAYOGI', ' KOMP BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:58:27', '2018-01-01', 'MERAUKE', '1967-02-01', NULL, NULL, NULL, NULL, NULL, 140.384341, -8.502451, 1, 5, 0, 0),
(566, 'RT040566', 'GIMIN', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 01:59:11', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.38468, -8.502936, 1, 4, 0, 0),
(567, 'RT040567', 'MARKUS TIPAWAEL', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:01:08', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.38438, -8.502597, 1, 4, 0, 0),
(568, 'RT050568', 'ANCELINA BALAGAIZE', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:02:22', '2018-01-01', 'MERAUKE', '1945-02-01', NULL, NULL, NULL, NULL, NULL, 140.383665, -8.50212, 1, 5, 0, 0),
(569, 'RT040569', 'IRIANTO PARAPAK', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:03:18', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384723, -8.502915, 1, 4, 0, 0),
(570, 'RT050570', 'HERMAN BELA', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:05:05', '2018-01-01', 'MERAUKE', '1967-02-01', NULL, NULL, NULL, NULL, NULL, 140.383848, -8.502262, 1, 5, 0, 0),
(571, 'RT040571', 'MUHAMAD NAWIR', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:05:16', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384573, -8.502491, 1, 4, 0, 0),
(572, 'RT040572', 'MUHAMAD YUSUF', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:08:09', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.383969, -8.502234, 1, 4, 0, 0),
(573, 'RT050573', 'ESAU HOMBORE', 'KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:08:10', '2018-01-01', 'MERAUKE', '1988-02-01', NULL, NULL, NULL, NULL, NULL, 140.38388, -8.502306, 1, 5, 0, 0),
(574, 'RT040574', 'KARYA KAROL SIMBOLON', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:10:09', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.383886, -8.502236, 1, 4, 0, 0),
(575, 'RT050575', 'SLAMET WALUYO', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:10:45', '2018-01-01', 'MERAUKE', '1965-02-01', NULL, NULL, NULL, NULL, NULL, 140.383987, -8.502437, 1, 5, 0, 0),
(576, 'RT040576', 'HARSONO', ' KOMP. BTN SERINGGU PERMAI/KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:12:14', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.384122, -8.502236, 1, 4, 0, 0),
(577, 'RT050577', 'PEBRIANTO', ' KOMPLEKS BTN SERINGGU PERM,AI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:13:45', '2018-01-01', 'MERAUKE', '1978-02-01', NULL, NULL, NULL, NULL, NULL, 140.384019, -8.502468, 1, 5, 0, 0),
(578, 'RT050578', 'MUKTAR', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:17:02', '2018-01-01', 'MERAUKE', '1999-02-01', NULL, NULL, NULL, NULL, NULL, 140.384084, -8.502572, 1, 5, 0, 0),
(579, 'RT050579', 'DIRMAN AMBARURA', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:19:57', '2018-01-01', 'MERAUKE', '1956-02-01', NULL, NULL, NULL, NULL, NULL, 140.384202, -8.502661, 1, 5, 0, 0),
(580, 'RT050580', 'SITI AMANAH', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:23:04', '2018-01-01', 'MERAUKE', '1988-02-01', NULL, NULL, NULL, NULL, NULL, 140.384245, -8.502712, 1, 5, 0, 0),
(581, 'RT050581', 'PAULUS YOHANES PATIRAN', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:25:22', '2018-01-01', 'MERAUKE', '1968-02-01', NULL, NULL, NULL, NULL, NULL, 140.384341, -8.502887, 1, 5, 0, 0),
(582, 'RT090582', 'YULIANUS DANIEL', ' JL. HUSEN PALELA, Gg. SERENEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 02:25:32', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.404739, -8.516335, 1, 9, 0, 0),
(583, 'RT120583', 'IKHSAN HAMIDI', ' JL. MARTADINATA, Gg. BINA MARGA     ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 03:05:44', '2018-01-01', 'KEBUMEN', '1979-07-06', NULL, NULL, NULL, NULL, NULL, 140.411277, -8.517917, 1, 12, 23, 1),
(584, 'KNTR070584', 'KANTOR KEJAKSAAN NEGERI', ' JALAN TMP TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 03:25:49', '2018-01-01', 'MERAUKE', '1978-02-01', NULL, NULL, NULL, NULL, NULL, 140.400313, -8.49507, 7, 7, 0, 0),
(585, 'KI070585', 'SARINAH', ' JL. TMP TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 06:01:39', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.399213, -8.488439, 6, 7, 0, 0),
(586, 'KI070586', 'IRUL MOH. NURKHOLIS', ' JL. TMP TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 06:04:00', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.397561, -8.485998, 6, 7, 0, 0),
(587, 'RT120587', 'ANI', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 06:06:18', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.408134, -8.517014, 1, 12, 0, 0),
(589, 'BGL120589', 'BENGKEL NASRUDIN', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 06:11:07', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.408308, -8.517388, 11, 12, 0, 0),
(590, 'RT090590', 'SETYO HARTINI', ' JL PEMUDA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 06:37:18', '2018-01-01', 'MERAUKE', '2018-01-31', NULL, NULL, NULL, NULL, NULL, 140.412282, -8.503624, 1, 9, 23, 1),
(591, 'RT070591', 'H.B.L TOBING', ' JL TRIKORA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 07:34:23', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.394145, -8.481692, 1, 7, 0, 0),
(592, 'RT070592', 'PRASETYO', ' JL TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 07:41:33', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.393325, -8.481886, 1, 7, 0, 0),
(593, 'RT070593', 'SIMON R.C', ' JL TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 07:57:46', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.393325, -8.481886, 1, 7, 0, 0),
(594, 'RT070594', 'MOSTOFO', ' JL TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 08:05:23', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.393325, -8.481886, 1, 7, 0, 0),
(595, 'RT070595', 'SUPRIYANTO', ' JL TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 08:10:09', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.393325, -8.481886, 1, 7, 0, 0),
(596, 'RT070596', 'PUTUT H.P', ' JL TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 08:12:43', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.393325, -8.481886, 1, 7, 0, 0),
(597, 'RT070597', 'ARIS', ' JL TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 08:15:17', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.393325, -8.481886, 1, 7, 0, 0),
(598, 'RT070598', 'TUKIMIN ', ' JL TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 08:17:41', '2018-01-01', 'MERAUKE', '2018-02-01', NULL, NULL, NULL, NULL, NULL, 140.393325, -8.481886, 1, 7, 0, 0),
(599, 'RT040599', 'A. RIYANTO', ' JALAN IRIAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-01 23:58:30', '2018-01-01', 'MERAUKE', '1967-02-02', NULL, NULL, NULL, NULL, NULL, 140.390679, -8.503253, 1, 4, 0, 0),
(600, 'RT060600', 'A.A. SOMAR', ' JL. NOARI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 01:01:13', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.386543, -8.486462, 1, 6, 0, 0),
(601, 'RT080601', 'HERU SUGIANTO', ' JL. PEMUDA PERUMAHAN BANDARA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 01:20:57', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.409811, -8.503886, 1, 8, 0, 0),
(602, 'RT030602', 'SAMIYANA', ' JL. GAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 01:30:27', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.399783, -8.508005, 1, 3, 0, 0),
(603, 'KI030603', 'SAMIYANA', ' JL. GAK, DEPAN PENJUAL GALON AIR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 01:33:48', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.399687, -8.50775, 6, 3, 0, 0),
(604, 'RT070604', 'RONAL', ' JL. MUTING POLDER', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 02:09:17', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.39738, -8.480373, 1, 7, 0, 0),
(605, 'RT020605', 'YOAKIM DUMATUBUN', ' JALAN RAYA MANDALA GANG ASAHI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 03:19:12', '2018-01-02', 'MERAUKE', '1945-02-02', NULL, NULL, NULL, NULL, NULL, 140.396773, -8.501264, 1, 2, 0, 0),
(606, 'KI020606', 'AGUSTINA REGINA NURHAYATI', ' JALAN GANG MARTADINATA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 05:10:09', '2018-01-02', 'MERAUKE', '1955-02-02', NULL, NULL, NULL, NULL, NULL, 140.408925, -8.501164, 6, 2, 0, 0),
(607, 'RT020607', 'AGUSTINA REGINA NURHAYATI', ' JALAN GANG MARTADINATA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 05:13:42', '2018-01-02', 'MERAUKE', '1966-02-02', NULL, NULL, NULL, NULL, NULL, 140.408939, -8.501187, 1, 2, 0, 0),
(608, 'RT040608', 'ASHARI RADHANI', ' JALAN TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 05:48:58', '2018-01-02', 'MERAUKE', '1987-02-02', NULL, NULL, NULL, NULL, NULL, 140.382434, -8.492664, 1, 4, 0, 0),
(609, 'RT040609', 'SRI YATMI (10 KK)', ' JL. IRIAN SERINGGUGg. AT- TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 05:53:34', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.392884, -8.502986, 1, 4, 0, 0),
(610, 'RT070610', 'MUH. IMRAN POMBANG', ' JL. POLDER I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 06:06:45', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.399983, -8.483545, 1, 7, 0, 0),
(611, 'RT040611', 'MUH. ARAFAH', ' JL. POLDER 1', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 06:09:41', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.400011, -8.483632, 1, 4, 0, 0),
(612, 'TS020612', 'TALIB', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-02 06:14:28', '2018-01-02', 'MERAUKE', '2018-03-02', NULL, NULL, NULL, NULL, NULL, 140.398339, -8.488066, 3, 2, 0, 0),
(613, 'RT040613', 'NIKATUT MUJRNIATI', ' JL. TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-05 23:43:05', '2018-01-05', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.381952, -8.492332, 1, 4, 0, 0),
(614, 'RT020614', 'MURNIATI', ' JL. RAYA MANDALA, Gg. PANDAWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 00:03:45', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.404378, -8.511591, 1, 2, 0, 0),
(615, 'KI020615', 'KIOS SUL. (SULHAM HAMZAH)', ' JL. MARTADITANA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 00:07:56', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.407673, -8.507427, 6, 2, 0, 0),
(616, 'RT120616', 'ROIDAH (5KK)', ' JL. HUSEN PALELA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 00:18:01', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.403221, -8.518382, 1, 12, 0, 0),
(617, 'RT060617', 'BADARIA  BASRI', ' JL. NUSA BARONG, Gg. HAJI KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 00:23:41', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.380986, -8.495739, 1, 6, 0, 0),
(618, 'RT040618', 'SRI HANDANI', ' JL. NATUNA, Gg. H. KASIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 00:29:49', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.381139, -8.495972, 1, 4, 0, 0),
(619, 'RT020619', 'JEMIE DEKER SAGRANG', ' JL. PENDIDIKAN, PERUM GURU BLOK C NO. 19', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 00:39:28', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.401929, -8.5015, 1, 2, 0, 0),
(620, 'RT030620', 'H ANWAR SANUSI', ' JALAN GAK GANG GEREJA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 00:53:35', '2018-01-06', 'MERAUKE', '1966-02-06', NULL, NULL, NULL, NULL, NULL, 140.396389, -8.506911, 1, 3, 0, 0),
(621, 'RT060621', 'SUHARTONO', ' JALAN NATUNA GANG MESJID DEPAN KIOS BUNDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 00:59:17', '2018-01-06', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.383639, -8.490431, 1, 6, 0, 0),
(622, 'RT030622', 'MARIA G V SAINYAKIT', ' JALAN SESATE GANG ATAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:13:32', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.393085, -8.502807, 1, 3, 0, 0),
(623, 'RT030623', 'JEREMIAS S WARUOP', ' JALAN SESATE Gg ATAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:17:26', '2018-01-06', 'MERAUKE', '1976-02-06', NULL, NULL, NULL, NULL, NULL, 140.393384, -8.503098, 1, 3, 0, 0),
(624, 'RT030624', 'JAKOBUS RUMFAAN', ' JALAN SESATE GANG MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:24:42', '2018-01-06', 'MERAUKE', '1956-02-06', NULL, NULL, NULL, NULL, NULL, 140.393278, -8.500708, 1, 3, 0, 0),
(625, 'RT070625', 'AGUS AHMAD SAID', ' JALAN BHAKTI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:35:42', '2018-01-06', 'MERAUKE', '1968-02-06', NULL, NULL, NULL, NULL, NULL, 140.389951, -8.485924, 1, 7, 0, 0),
(626, 'RT060626', 'HERNING TYAS DARUMURTI', 'JL. BIAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:38:50', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.402546, -8.506151, 1, 6, 0, 0),
(627, 'RT070627', 'NUR DUHA', ' JALAN POLDER DALAM I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:39:16', '2018-01-06', 'MERAUKE', '1966-02-06', NULL, NULL, NULL, NULL, NULL, 140.398814, -8.483888, 1, 7, 0, 0),
(628, 'TS090628', 'TRI DIYANTORO', ' JALAN PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:48:35', '2018-01-06', 'MERAUKE', '2016-02-06', NULL, NULL, NULL, NULL, NULL, 140.424006, -8.530386, 3, 9, 0, 0),
(629, 'TB090629', 'TRI DIYANTORO DEALER BENELLI', ' JALAN PEMBANGUNAN NO.47', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:51:35', '2018-01-06', 'MERAUKE', '2016-02-06', NULL, NULL, NULL, NULL, NULL, 140.423748, -8.529285, 2, 9, 0, 0),
(630, 'RT120630', 'HARDI BOGE', ' JL. RAYA MANDALA, Gg. SABAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 01:54:34', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.398776, -8.505419, 1, 12, 0, 0),
(631, 'RT020631', 'PAKTINI (3KK)', ' JL. RAYA MANDALA, Gg. SABAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:02:56', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.398596, -8.505317, 1, 2, 0, 0),
(632, 'RT090632', 'TRI DIYANTORO', ' JALAN PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:04:48', '2018-01-06', 'MERAUKE', '1966-02-06', NULL, NULL, NULL, NULL, NULL, 140.423597, -8.528434, 1, 9, 0, 0),
(633, 'TB050633', 'YAYASAN KALAM KUDUS (PLAY GRUP)', ' JL. NOARI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:24:45', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.390049, -8.481663, 2, 5, 0, 0),
(634, 'RT020634', 'YAYASAN KALAM KUDUS (KEPALA SEKOLAH)', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:27:47', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.403785, -8.511804, 1, 2, 0, 0),
(635, 'RT040635', 'SULTAN', ' KOMPLEKS PERUM KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:34:33', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.384545, -8.503097, 1, 4, 0, 0),
(636, 'RT040636', 'SIANNE LAURENTJE S.', ' KOMP. BTN SERINGGU PERMAI/ KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:38:01', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.384057, -8.501893, 1, 4, 0, 0),
(637, 'RT120637', 'DENNY I DRIS AZIZ', ' JALAN RAYA MANDALA MULI GANG DIRGANTARA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:42:46', '2018-01-06', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.408248, -8.517311, 1, 12, 0, 0),
(638, 'RT040638', 'EMA LESTALUHU', ' JL. TERNATE, Gg. PAPUA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:43:46', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.389226, -8.499675, 1, 4, 0, 0),
(639, 'KNTR020639', 'UNIT TRANSFUSI DARAH', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 02:57:22', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.391921, -8.491903, 7, 2, 0, 0),
(640, 'RT040640', 'PENINA J WANIPA', ' KOMPLEKS BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 03:22:41', '2018-01-06', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.383729, -8.502436, 1, 4, 0, 0),
(641, 'RT040641', 'TAHIR', ' KOMPLEKS BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 03:26:14', '2018-01-06', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.383857, -8.502598, 1, 4, 0, 0),
(642, 'RT040642', 'BUDI SANTOSO', ' KOMPLEKS BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 03:29:59', '2018-01-06', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.384029, -8.50259, 1, 4, 0, 0),
(643, 'RT040643', 'JUNAEDY SEPTIARA', ' KOMPLEKS BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 03:33:54', '2018-01-06', 'MERAUKE', '1966-02-06', NULL, NULL, NULL, NULL, NULL, 140.383986, -8.502735, 1, 4, 0, 0),
(644, 'RT040644', 'YULIN RANTEKATA', ' KOMPLEKS BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 03:37:12', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.383857, -8.502438, 1, 4, 0, 0),
(645, 'RT040645', 'MIFTAH FUAD ALHAMID', ' KOMPLEKS BTN SERINGGU PERMAI JL. KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 03:42:39', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.384072, -8.502688, 1, 4, 0, 0),
(646, 'RT040646', 'ALAM RAHMAN', ' KOMPLEKS BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 03:50:37', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.384029, -8.502779, 1, 4, 0, 0),
(647, 'RT040647', 'MATHEIS SOUHUWAT', ' KOMPLEKS BTN SERINGGU PERMAI JL. KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 03:54:37', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.383901, -8.502504, 1, 4, 0, 0),
(648, 'RT040648', 'IMANUEL JAYA', ' KOMPLEKS BTN SERINGGU PERMAI JL.KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:06:19', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.3839, -8.502686, 1, 4, 0, 0),
(649, 'RT040649', 'SANDRA', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:09:31', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.383943, -8.502651, 1, 4, 0, 0),
(650, 'RT040650', 'CHRISTI SANDRA FORTUNA', ' KOMPLEKS BTN SERINGGU PERMAI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:14:28', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.383965, -8.502539, 1, 4, 0, 0),
(651, 'RS080651', 'RUMAH SAKIT BUNDA PENGHARAPAN', ' JALAN ANGKASA I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:19:03', '2018-01-06', 'MERAUKE', '1999-02-06', NULL, NULL, NULL, NULL, NULL, 140.404458, -8.489144, 10, 8, 0, 0),
(652, 'RT040652', 'LUSIA LUGINOP', ' JALAN SERINGGU Gg.AT-TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:21:44', '2018-01-06', 'MERAUKE', '1989-02-06', NULL, NULL, NULL, NULL, NULL, 140.392355, -8.502004, 1, 4, 0, 0),
(653, 'RT040653', 'YULI', ' KOMPLEKS BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:37:17', '2018-01-06', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.384115, -8.502774, 1, 4, 0, 0),
(654, 'RT040654', 'STENLY TUHUBALA', ' KOMPLEKD BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:40:38', '2018-01-06', 'MERAUKE', '1999-02-06', NULL, NULL, NULL, NULL, NULL, 140.384695, -8.502858, 1, 4, 0, 0),
(655, 'RT040655', 'SUTRISNO', ' KOMPLEKS BTN SERINGGU PERMAI JALAN KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:43:21', '2018-01-06', 'MERAUKE', '1999-02-06', NULL, NULL, NULL, NULL, NULL, 140.384029, -8.502246, 1, 4, 0, 0),
(656, 'RT040656', 'RIZAL', ' KOMPLEKS BTN SERINGGU PERMAI JL.KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:48:35', '2018-01-06', 'MERAUKE', '1976-02-06', NULL, NULL, NULL, NULL, NULL, 140.384415, -8.503031, 1, 4, 0, 0),
(657, 'RT020657', 'ARIE PRASETYA', ' JL. RAYA MANDALA ASPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:50:12', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.392705, -8.489088, 1, 2, 0, 0),
(658, 'RT040658', 'MARIA GORETI HARBELUBUN', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:51:02', '2018-01-06', 'MERAUKE', '1978-02-06', NULL, NULL, NULL, NULL, NULL, 140.384115, -8.502317, 1, 4, 0, 0),
(659, 'RT020659', 'SRI ASTUTI ./ INSAN', ' JL. RAYA MANDALA ASPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:53:40', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.392454, -8.487751, 1, 2, 0, 0),
(660, 'RT040660', 'DEWI ROSDIANA MARBUN', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:53:47', '2018-01-06', 'MERAUKE', '1976-02-06', NULL, NULL, NULL, NULL, NULL, 140.384415, -8.503168, 1, 4, 0, 0),
(661, 'RT020661', 'RISQI', ' JL. RAYA MANDALA ASPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:57:00', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.392102, -8.487979, 1, 2, 0, 0),
(662, 'RT020662', 'JATMIKO', ' JL. RAYA MANDALA ASPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:58:50', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.392426, -8.487801, 1, 2, 0, 0),
(663, 'RM100663', 'M ARIES', ' JALAN SULTAN SYAHRIR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 04:58:52', '2018-01-06', 'MERAUKE', '1970-02-06', NULL, NULL, NULL, NULL, NULL, 140.417204, -8.50692, 4, 10, 0, 0);
INSERT INTO `re_costumer` (`cost_id`, `cost_id_pel`, `cost_name`, `cost_address`, `cost_prov`, `cost_kab`, `cost_kec`, `cost_status`, `cost_date`, `cost_periode_terakhir`, `cost_tmp_lahir`, `cost_tgl_lahir`, `cost_phone`, `cost_foto`, `cost_lokasi`, `cost_denah`, `cost_ktp`, `cost_lang`, `cost_latt`, `cost_type_id`, `cost_desa_id`, `cost_user_id`, `cost_jadwal_id`) VALUES
(664, 'RT040664', 'MARIA GORETI EKA YUNIARTI', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 05:08:21', '2018-01-06', 'MERAUKE', '1995-02-06', NULL, NULL, NULL, NULL, NULL, 140.384158, -8.502852, 1, 4, 0, 0),
(665, 'HTL030665', 'HOTEL RAJAWALI  (YUANNA)', ' JL. GAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 05:09:52', '2018-01-06', 'MERAUKE', '2018-02-06', NULL, NULL, NULL, NULL, NULL, 140.397758, -8.508254, 15, 3, 0, 0),
(666, 'RT040666', 'SUDIRMAN', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 05:10:59', '2018-01-06', 'MERAUKE', '1955-02-06', NULL, NULL, NULL, NULL, NULL, 140.384566, -8.502744, 1, 4, 0, 0),
(667, 'RT040667', 'ALIF FRIENDY KURNIAWAN', ' JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 06:16:21', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.384329, -8.501508, 1, 4, 0, 0),
(668, 'RT040668', 'YAKOB PALINOAN', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:30:59', '2018-01-06', 'MERAUKE', '1976-02-06', NULL, NULL, NULL, NULL, NULL, 140.384138, -8.502559, 1, 4, 0, 0),
(669, 'RT040669', 'ABU BAKAR ERIK', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:33:39', '2018-01-06', 'MERAUKE', '1989-02-06', NULL, NULL, NULL, NULL, NULL, 140.384395, -8.50239, 1, 4, 0, 0),
(670, 'RT040670', 'TRI WALUYO', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:37:58', '2018-01-06', 'MERAUKE', '1987-02-06', NULL, NULL, NULL, NULL, NULL, 140.384223, -8.502475, 1, 4, 0, 0),
(671, 'RT040671', 'SITI SYUHRIATUN', ' KOMPLEKS BTN SERINGGU PERMAI KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:40:23', '2018-01-06', 'MERAUKE', '1984-02-06', NULL, NULL, NULL, NULL, NULL, 140.384653, -8.502729, 1, 4, 0, 0),
(672, 'RT040672', 'YORDAN R R BEBENA', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:42:48', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.384309, -8.502814, 1, 4, 0, 0),
(673, 'RT040673', 'EDY PURWANTO', ' KOMPLEKS BTN SERINGGU PERMAI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:44:53', '2018-01-06', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.384309, -8.50239, 1, 4, 0, 0),
(674, 'RT040674', 'YANTI', 'KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:46:28', '2018-01-06', 'MERAUKE', '0982-02-06', NULL, NULL, NULL, NULL, NULL, 140.384567, -8.502729, 1, 4, 0, 0),
(675, 'RT040675', 'CATUR WIDAYATI', 'KOMPLEKS BTN SERINGGU PERMAI ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:48:10', '2018-01-06', 'MERAUKE', '1981-02-06', NULL, NULL, NULL, NULL, NULL, 140.384395, -8.502644, 1, 4, 0, 0),
(676, 'RT040676', 'CEKLI', 'KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:50:19', '2018-01-06', 'MERAUKE', '1969-02-06', NULL, NULL, NULL, NULL, NULL, 140.384223, -8.502559, 1, 4, 0, 0),
(677, 'RT040677', 'ERNA SILESTIOWATI', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:52:00', '2018-01-06', 'MERAUKE', '1975-02-06', NULL, NULL, NULL, NULL, NULL, 140.384138, -8.502305, 1, 4, 0, 0),
(678, 'RT040678', 'NURAENI YUNUS', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:54:31', '2018-01-06', 'MERAUKE', '1974-02-06', NULL, NULL, NULL, NULL, NULL, 140.384459, -8.502865, 1, 4, 0, 0),
(679, 'RT040679', 'SUPRIYANTO', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:56:24', '2018-01-06', 'MERAUKE', '1964-02-06', NULL, NULL, NULL, NULL, NULL, 140.384223, -8.502559, 1, 4, 0, 0),
(680, 'RT040680', 'KUSWANTO', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 11:58:26', '2018-01-06', 'MERAUKE', '1966-02-06', NULL, NULL, NULL, NULL, NULL, 140.384395, -8.502475, 1, 4, 0, 0),
(681, 'RT040681', 'SUWARNO', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:00:51', '2018-01-06', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.384395, -8.502559, 1, 4, 0, 0),
(682, 'RT040682', 'ERIAWATY', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:03:10', '2018-01-06', 'MERAUKE', '1980-02-06', NULL, NULL, NULL, NULL, NULL, 140.384567, -8.502559, 1, 4, 0, 0),
(683, 'RT040683', 'FABIAN KRISTOFERUS', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:06:43', '2018-01-06', 'MERAUKE', '1989-02-06', NULL, NULL, NULL, NULL, NULL, 140.384459, -8.502695, 1, 4, 0, 0),
(684, 'RT040684', 'ENIK KUSRINI', ' KOMPLEKS SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:08:34', '2018-01-06', 'MERAUKE', '1985-02-06', NULL, NULL, NULL, NULL, NULL, 140.384309, -8.502644, 1, 4, 0, 0),
(685, 'RT040685', 'ANANG RHOMAIYOKA', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:13:24', '2018-01-06', 'MERAUKE', '1986-02-06', NULL, NULL, NULL, NULL, NULL, 140.384631, -8.502695, 1, 4, 0, 0),
(686, 'RT040686', 'EMMI', 'KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:15:28', '2018-01-06', 'MERAUKE', '1982-02-06', NULL, NULL, NULL, NULL, NULL, 140.384567, -8.502814, 1, 4, 0, 0),
(687, 'RT040687', 'GAPPAR SIGA', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:17:14', '2018-01-06', 'MERAUKE', '1976-02-06', NULL, NULL, NULL, NULL, NULL, 140.384223, -8.50205, 1, 4, 0, 0),
(688, 'RT040688', 'NURIL ANWAR', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:18:36', '2018-01-06', 'MERAUKE', '1975-02-06', NULL, NULL, NULL, NULL, NULL, 140.383966, -8.502729, 1, 4, 0, 0),
(689, 'RT040689', 'EDOWARDUS BLASIUS NGARBINGAN', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:20:13', '2018-01-06', 'MERAUKE', '1969-02-06', NULL, NULL, NULL, NULL, NULL, 140.384631, -8.502356, 1, 4, 0, 0),
(690, 'RT040690', 'SUMARDJI WAKANG', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:21:32', '2018-01-06', 'MERAUKE', '1982-02-06', NULL, NULL, NULL, NULL, NULL, 140.384052, -8.502644, 1, 4, 0, 0),
(691, 'RT040691', 'SITI ROBINGATUN ASRORIYAH', ' KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:24:08', '2018-01-06', 'MERAUKE', '1974-02-06', NULL, NULL, NULL, NULL, NULL, 140.384567, -8.502559, 1, 4, 0, 0),
(692, 'RT040692', 'RATNAWATI', 'KOMPLEKS BTN SERINGGU PERMAI JL KALIWEDA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-06 12:25:55', '2018-01-06', 'MERAUKE', '1981-02-06', NULL, NULL, NULL, NULL, NULL, 140.384395, -8.502729, 1, 4, 0, 0),
(693, 'RT060693', 'DJUNAEDI OHOIBOR', ' JALAN RADIO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:04:37', '2018-01-07', 'MERAUKE', '1955-02-07', NULL, NULL, NULL, NULL, NULL, 140.381916, -8.486726, 1, 6, 0, 0),
(694, 'KI070694', 'KIOS UMEGA(MURANI LANURU)', ' JL. MAYOR WIRATNO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:08:12', '2018-01-07', 'MERAUKE', '1952-05-27', NULL, NULL, NULL, NULL, NULL, 140.392619, -8.478511, 6, 7, 0, 0),
(695, 'RT030695', 'Dr. DAVID LIMATO WIJAYA', ' Jl. SASATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:35:19', '2018-01-07', 'MERAUKE', '2018-02-07', NULL, NULL, NULL, NULL, NULL, 140.39579, -8.504747, 1, 3, 0, 0),
(696, 'KNTR030696', 'Dr. DAVID LIMATO WIJAYA (PRAKTEK)', ' JL. SASATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:39:21', '2018-01-07', 'MERAUKE', '2018-02-07', NULL, NULL, NULL, NULL, NULL, 140.395842, -8.504699, 7, 3, 0, 0),
(697, 'TS020697', 'TOKO STARCELL', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:45:32', '2018-01-07', 'MERAUKE', '1991-05-31', NULL, NULL, NULL, NULL, NULL, 140.390396, -8.488166, 3, 2, 0, 0),
(698, 'RT030698', 'FRANSISKUS RENYAAN', ' JALAN SESATE GANG BALUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:47:29', '2018-01-07', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.39492, -8.50553, 1, 3, 0, 0),
(700, 'TS020700', 'TOKO STAR COM', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:51:47', '2018-01-07', 'MERAUKE', '1991-03-15', NULL, NULL, NULL, NULL, NULL, 140.390406, -8.488183, 3, 2, 0, 0),
(701, 'RT030701', 'MARGARETHA E P BALUBUN', ' JALAN SESATE GANG BALUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:53:48', '2018-01-07', 'MERAUKE', '1966-02-07', NULL, NULL, NULL, NULL, NULL, 140.394818, -8.505406, 1, 3, 0, 0),
(702, 'RT090702', 'MARTHA MARIANA GOLLUD', ' JL. PEMBANGUNAN NO. 10 B PERUMAHAN KEHUTANAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 00:57:06', '2018-01-07', 'MERAUKE', '2018-02-07', NULL, NULL, NULL, NULL, NULL, 140.421768, -8.520811, 1, 9, 0, 0),
(703, 'RT090703', 'HERMIN MANGALA', ' JALAN YOBAR I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:04:33', '2018-01-07', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.400939, -8.511069, 1, 9, 0, 0),
(704, 'RT030704', 'YETTI OKTAVIA', ' JL. SASATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:05:25', '2018-01-07', 'TEMBILAHAN', '1981-11-05', NULL, NULL, NULL, NULL, NULL, 140.394158, -8.50284, 1, 3, 0, 0),
(705, 'RT030705', 'MELISA ENOK', ' JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:08:38', '2018-01-07', 'WAMENA', '1984-09-04', NULL, NULL, NULL, NULL, NULL, 140.394159, -8.502816, 1, 3, 0, 0),
(706, 'RT040706', 'MESJID NURUL FALAH', ' JALAN TERNATE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:09:54', '2018-01-07', 'MERAUKE', '2010-02-07', NULL, NULL, NULL, NULL, NULL, 140.386825, -8.498646, 1, 4, 0, 0),
(707, 'RT030707', 'DARMAWAN', 'JL. SASATE, Gg. MUMU ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:11:48', '2018-01-07', 'GUNUNG KUDUL', '1984-12-20', NULL, NULL, NULL, NULL, NULL, 140.394096, -8.502712, 1, 3, 0, 0),
(708, 'RT040708', 'GTATIYEM', ' JALAN TERNATE GANG OKABA I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:16:10', '2018-01-07', 'MERAUKE', '1977-02-06', NULL, NULL, NULL, NULL, NULL, 140.387276, -8.499122, 1, 4, 0, 0),
(709, 'RT030709', 'KASPARINA KASMINI', ' JL. SASATE, Gg. MUMU/Gg. BALUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:16:49', '2018-01-07', 'CILACAP', '1972-10-23', NULL, NULL, NULL, NULL, NULL, 140.393994, -8.502595, 1, 3, 0, 0),
(710, 'TB070710', 'PT. PELNI (KM. TATAMAILAO -KM. SIRIMAU - KM .LEUSER)', ' JL.. SABANG', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:26:18', '2018-01-07', 'MERAUKE', '2018-02-07', NULL, NULL, NULL, NULL, NULL, 140.391259, -8.480422, 2, 7, 0, 0),
(711, 'RT120711', 'FATIMAH', ' JALAN RAYA MANDALA SPADEM GANG KPKN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 01:36:51', '2018-01-07', 'MERAUKE', '1966-02-06', NULL, NULL, NULL, NULL, NULL, 140.408181, -8.517594, 1, 12, 0, 0),
(712, 'RT090712', 'ASEP DWI SAPUTRO', ' JL. MARTADINATA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 02:23:16', '2018-01-07', 'MERAUKE', '1988-10-09', NULL, NULL, NULL, NULL, NULL, 140.409865, -8.513003, 1, 9, 0, 0),
(713, 'RT030713', 'KRISTINA B. RETTOB', ' JL. GAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 02:33:14', '2018-01-07', 'MERAUKE', '2018-02-07', NULL, NULL, NULL, NULL, NULL, 140.390812, -8.503044, 1, 3, 0, 0),
(714, 'RT040714', 'KUSWARA', ' JL. KAMPUNG TIMUR ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 02:44:50', '2018-01-07', 'MERAUKE', '2018-02-07', NULL, NULL, NULL, NULL, NULL, 140.391195, -8.494426, 1, 4, 23, 1),
(715, 'RT020715', 'KRISTIAN WALONG (5KK)', ' JL. BIAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 03:14:54', '2018-01-07', 'MERAUKE', '2018-02-07', NULL, NULL, NULL, NULL, NULL, 140.401009, -8.503522, 1, 2, 0, 0),
(716, 'RT030716', 'MACHBUB NAIM SYAHRI', ' JALAN RAYA MANDALA BAMPEL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 04:38:31', '2018-01-07', 'MERAUKE', '1988-02-06', NULL, NULL, NULL, NULL, NULL, 140.40125, -8.507266, 1, 3, 0, 0),
(717, 'RT040717', 'SRI ISTI MULYANI', ' JL. TERNATE, PAPUA II', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 23:47:43', '2018-01-07', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.388596, -8.499829, 1, 4, 0, 0),
(718, 'RT030718', 'MISYE KAROLIN', ' JALAN SESATE GANG BALUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 23:52:24', '2018-01-07', 'MERAUKE', '1988-02-08', NULL, NULL, NULL, NULL, NULL, 140.396411, -8.50397, 1, 3, 0, 0),
(719, 'RT060719', 'SAHATI', ' JALAN SUMATERA GANG BELAKANG NOTARIS', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-07 23:57:06', '2018-01-07', 'MERAUKE', '1977-02-08', NULL, NULL, NULL, NULL, NULL, 140.389963, -8.490321, 1, 6, 0, 0),
(720, 'RT060720', 'AYU ARIANI', ' JL. SUMATERA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 00:03:39', '2018-01-08', 'MERAUKE', '1990-03-03', NULL, NULL, NULL, NULL, NULL, 140.389523, -8.489302, 1, 6, 0, 0),
(721, 'RT060721', 'AHMAD SODIKIN', ' JALAN SUMATERA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 00:04:06', '2018-01-08', 'MERAUKE', '1988-02-08', NULL, NULL, NULL, NULL, NULL, 140.390049, -8.490453, 1, 6, 0, 0),
(722, 'RT040722', 'GUNARI', ' JL. IRIAN SERINGGU Gg. AT-TAQWA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 00:17:21', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.391936, -8.50241, 1, 4, 0, 0),
(723, 'RT090723', 'MARYATIN (14 KK)', ' JL. MARTADINATA, Gg. ANGKASA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 00:26:55', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.408792, -8.511082, 1, 9, 0, 0),
(724, 'TB090724', 'SD NEGERI CENDERAWASIH', ' JL. KAMIZAUN-SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 00:48:36', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.417467, -8.525917, 2, 9, 0, 0),
(725, 'RT120725', 'WIWIT TUTI NINGSIH', ' JL. RAYA MANDALA Gg. KPKN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 01:02:14', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.407815, -8.520537, 1, 12, 0, 0),
(726, 'RT120726', 'SUPRIANTO', ' JL. RAYA MANDALA, Gg. KPKN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 01:05:36', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.407905, -8.5206, 1, 12, 0, 0),
(727, 'KNTR020727', 'KANTOR J & T (SITI M. SARJONO)', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 01:10:16', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.401249, -8.507255, 7, 2, 0, 0),
(728, 'RT050728', 'NUR MUTAMINAH (8 KK)', ' JL. NDOREMKAI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 01:30:33', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.389795, -8.5139, 1, 5, 0, 0),
(729, 'RT040729', 'NUR LAILA (8 KK)', ' JL. KAMPUNG TIMUR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 01:49:50', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.391552, -8.495527, 1, 4, 0, 0),
(730, 'RT030730', 'NATALIA RAHANGMETAN', ' JALAN RAYA MANDALA DEPAN TOKO 7', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 01:53:41', '2018-01-08', 'MERAUKE', '1973-02-08', NULL, NULL, NULL, NULL, NULL, 140.397892, -8.502453, 1, 3, 0, 0),
(731, 'TB080731', 'SD NEGERI POLDER', ' JL. TMP POLDER', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:11:21', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.397836, -8.485045, 2, 8, 0, 0),
(732, 'RT020732', 'SERVIS SIMANUNGKALIT ', ' JALAN MISI ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:13:27', '2018-01-08', 'MERAUKE', '1955-02-08', NULL, NULL, NULL, NULL, NULL, 140.404148, -8.506529, 1, 2, 23, 1),
(733, 'KNTR080733', 'SD IMPRES POLDER', ' JL. TMP TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:14:23', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.397836, -8.485045, 7, 8, 0, 0),
(734, 'RT070734', 'HERMIN T.', ' JL. POLDER DALAM I', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:21:29', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.398522, -8.484081, 1, 7, 0, 0),
(735, 'RT020735', 'WELEM A SAIYA', ' JALAN PENDIDIKAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:22:43', '2018-01-08', 'MERAUKE', '1967-02-08', NULL, NULL, NULL, NULL, NULL, 140.400306, -8.496164, 1, 2, 0, 0),
(736, 'TS030736', 'YOHANES H SORMIN', ' JALAN RAYA MANDALA BAMPEL  ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:37:21', '2018-01-08', 'MERAUKE', '1978-06-08', NULL, NULL, NULL, NULL, NULL, 140.401246, -8.507323, 3, 3, 23, 1),
(737, 'RT020737', 'ACELINA PATH UNTAJANA', ' JL. ASPOL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:47:37', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.391674, -8.487585, 1, 2, 0, 0),
(738, 'RT020738', 'YOHANES H SORMIN', ' JALAN PENDIDIKAN GANG TEMBUS JALAN BIAK ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:51:10', '2018-01-08', 'MERAUKE', '1988-02-08', NULL, NULL, NULL, NULL, NULL, 140.40212, -8.50159, 1, 2, 23, 1),
(739, 'RT090739', 'IRTAN TIMANG', ' JL. POMPA AIR II', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 02:57:18', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.408605, -8.514837, 1, 9, 0, 0),
(740, 'RT120740', 'DARWATI', ' JALAN GARUDA SPADEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 03:13:19', '2018-01-08', 'MERAUKE', '1945-02-08', NULL, NULL, NULL, NULL, NULL, 140.415242, -8.525197, 1, 12, 0, 0),
(741, 'RT020741', 'ANA WULANDARI', ' JL. PERNDIDIKAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 03:22:10', '2018-01-08', 'MAGENTA', '1959-06-19', NULL, NULL, NULL, NULL, NULL, 140.401899, -8.501157, 1, 2, 0, 0),
(742, 'RT060742', 'JOAN MELANIE', ' JALAN NATUNA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 03:39:37', '2018-01-08', 'MERAUKE', '1966-02-08', NULL, NULL, NULL, NULL, NULL, 140.383713, -8.490503, 1, 6, 0, 0),
(743, 'RT020743', 'WUYI DELTASARI', ' JL. PRAJURIT', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 03:45:17', '2018-01-08', 'MERAUKE', '1981-09-29', NULL, NULL, NULL, NULL, NULL, 140.399209, -8.495189, 1, 2, 0, 0),
(744, 'RT070744', 'SINIKE KAMBUAYA', ' JALAN TRIKORA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 03:51:29', '2018-01-08', 'MERAUKE', '1956-02-08', NULL, NULL, NULL, NULL, NULL, 140.395494, -8.480885, 1, 7, 0, 0),
(745, 'RT030745', 'IDA THENO', ' JALAN GAK SEBELAH KOLAM BUAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 04:08:26', '2018-01-08', 'MERAUKE', '1970-12-08', NULL, NULL, NULL, NULL, NULL, 140.393083, -8.505342, 1, 3, 0, 0),
(746, 'RT080746', 'TRI PUDIARTI OKZOONRINI', ' JL. SUTAN SYAHRIL', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 04:41:20', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.414625, -8.505946, 1, 8, 0, 0),
(747, 'RT080747', 'Hj. SUHARMI', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 06:09:27', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.408117, -8.501211, 1, 8, 0, 0),
(748, 'BGL080748', 'BENGKEL JALIL (Hj. SUHARMI) ', ' JL. BRAWIJAYA, DEPAN SBM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 06:15:20', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.408124, -8.501213, 11, 8, 0, 0),
(749, 'RT040749', 'NUR ICHSANUDDIN', ' JL. TIDORE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-08 07:29:48', '2018-01-08', 'MERAUKE', '2018-02-08', NULL, NULL, NULL, NULL, NULL, 140.388143, -8.495132, 1, 4, 0, 0),
(751, 'KNTR020751', 'KANTOR BUPATI KABUPATEN MERAUKE', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 20:25:27', '2018-01-20', 'MERAUKE', '1912-02-12', NULL, NULL, NULL, NULL, NULL, 140.409596, -8.508663, 7, 2, 0, 0),
(752, 'KNTR020752', 'SMA NEGERI I MERAUKE', ' JL. PENDIDIKAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 20:49:14', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.40078, -8.500842, 7, 2, 0, 0),
(753, 'PRSH120753', 'BANK PAPUA CABANG MERAUKE', ' JL. RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 21:04:57', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.406119, -8.513813, 12, 12, 0, 0),
(754, 'PRSH120754', 'BANK BRI MULI MERAUKE', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 21:08:27', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.404278, -8.512663, 12, 12, 0, 0),
(755, 'PRSH020755', 'BANK BNI CABANG MERAUKE', 'JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 21:12:35', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.396998, -8.501243, 12, 2, 0, 0),
(756, 'KNTR070756', 'LEMBAGA PEMASYARAKATAN MERAUKE', ' JL. ERMASSU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 21:17:32', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.393312, -8.486996, 7, 7, 0, 0),
(757, 'RS060757', 'RUMAH SAKIT UMUM DAERAH (RSUD) MERAUKE', ' JL. SOEKARJO', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 21:24:57', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.388769, -8.481839, 10, 6, 0, 0),
(758, 'KNTR090758', 'KANTOR BANDARA MOPAH MERAUKE', ' JL. MOPAH', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 21:28:23', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.416067, -8.521467, 7, 9, 0, 0),
(759, 'TB020759', 'TOKO SENANG HATI', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 21:31:45', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.404386, -8.511833, 2, 2, 0, 0),
(760, 'KNTR090760', 'KANTOR SATRAD 244', ' JL. RAYA BOKEM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-20 21:43:36', '2018-01-20', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.450538, -8.546945, 7, 9, 0, 0),
(761, 'RT120761', 'SUMADI (20 KK)', 'JL. RAYA MANDALA SPADEM, Gg, BUNTU PAGAR BANDARA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-21 05:11:40', '2018-01-21', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.416899, -8.524696, 1, 12, 0, 0),
(762, 'RT030762', 'MARINUS RETTTOB I', ' JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-21 05:35:58', '2018-01-21', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.395441, -8.502371, 1, 3, 0, 0),
(763, 'RT030763', 'MARINUS RETTTOB II', ' JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-21 05:39:59', '2018-01-21', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.395441, -8.502371, 1, 3, 0, 0),
(764, 'RT030764', 'MARIA RETTOB', ' JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-21 05:48:14', '2018-01-21', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.395441, -8.502371, 1, 3, 0, 0),
(765, 'RT030765', 'MARINUS RETTTOB III', ' JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-21 05:51:12', '2018-01-21', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.395441, -8.502371, 1, 3, 0, 0),
(766, 'RT030766', 'MATIAS TANGKE', ' JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-21 05:53:12', '2018-01-21', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.395442, -8.502372, 1, 3, 0, 0),
(767, 'RT030767', 'YOHANIS PIPIANA', 'JL. SASATE, Gg. MUMU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-21 05:56:01', '2018-01-21', 'MERAUKE', '2018-02-21', NULL, NULL, NULL, NULL, NULL, 140.395442, -8.502372, 1, 3, 0, 0),
(768, 'KI070768', 'TATU UMI ZAKIYAH DERAJAT', ' JL. ERMASSU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-26 23:13:55', '2018-01-26', 'MERAUKE', '2018-02-27', NULL, NULL, NULL, NULL, NULL, 140.383803, -8.489436, 6, 7, 0, 0),
(769, 'RT070769', 'CRIS NOYA', ' JL. ERMASSU', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-02-26 23:17:01', '2018-01-26', 'MERAUKE', '2018-02-27', NULL, NULL, NULL, NULL, NULL, 140.393915, -8.48719, 1, 7, 0, 0),
(770, 'KNTR020770', 'BANK SINAR MAS', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-08 02:53:23', '2018-02-07', 'MERAUKE', '2018-03-08', '081200000000', NULL, NULL, NULL, NULL, 140.4002, -8.505575, 7, 2, 0, 0),
(771, 'PRSH020771', 'PT. INNI HAPPY DREAM (an. FRANSINA TORREY R.)', ' JL. RAYA MANDALA, DEPAN KODIM', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-08 03:55:25', '2018-02-07', 'MERAUKE', '0000-00-00', '081247112515', NULL, NULL, NULL, NULL, 140.393533, -8.495135, 12, 2, 0, 0),
(772, 'RT090772', 'JOSEPH JOLMEN', ' JALAN HARAPAN, STADION KATALPAL GG.PAPUA RINJA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-08 23:21:36', '2018-02-08', 'MERAUKE', '2018-03-09', '089961513865', NULL, NULL, NULL, NULL, 140.413605, -8.527418, 1, 9, 0, 0),
(773, 'RT020773', 'RAMLI KABAU', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-08 23:36:23', '2018-02-08', 'NAMLEA', '1986-01-08', '081200000000000', NULL, NULL, NULL, NULL, 140.396789, -8.488626, 1, 2, 0, 0),
(774, 'TB020774', 'TOKO MULINDAH', ' JL. RAYA MANDALA MULI MERAUKE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-08 23:48:36', '2018-02-08', 'MERAUKE', '1968-05-24', '0816695054', NULL, NULL, NULL, NULL, 140.4026, -8.509893, 2, 2, 0, 0),
(775, 'RM090775', 'SEBASTIANA HETO', ' JALAN HARAPAN STADION KATALPAL GG.PAPUA RINJA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-08 23:56:02', '2018-02-08', 'MERAUKE', '2018-03-09', '081291301052', NULL, NULL, NULL, NULL, 140.430892, -8.525259, 4, 9, 0, 0),
(776, 'RT090776', 'SEBASTIANA HETO', ' JALAN HARAPAN STADION KATALPAL GANG PAPUA RINJA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 00:03:16', '2018-02-08', 'MERAUKE', '2018-03-09', '081291301052', NULL, NULL, NULL, NULL, 140.431801, -8.524836, 1, 9, 0, 0),
(777, 'RT090777', 'HERIBERTUS BOLE MAHUZE', ' JALAN HARAPAN STADION KATALPAL GANG PAPUA RINJA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 00:06:10', '2018-02-08', 'MERAUKE', '2018-03-09', '085254243530', NULL, NULL, NULL, NULL, 140.431832, -8.524867, 1, 9, 0, 0),
(778, 'TB020778', 'MERAUKE TOWN SQUARE', ' JL. RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 00:30:02', '2018-02-08', 'MERAUKE', '2018-03-09', '081111111111111', NULL, NULL, NULL, NULL, 140.391782, -8.489956, 2, 2, 0, 0),
(779, 'KI020779', 'PRISAN CLUB', ' JL. RAYA MANDALA MERAUKE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 01:00:03', '2018-02-08', 'MERAUKE', '2018-03-09', '081328084103', NULL, NULL, NULL, NULL, 140.402752, -8.509403, 6, 2, 0, 0),
(780, 'KI030780', 'BAKSO ZORRO (SUTRISNO)', ' JL. RAYA MANDALA, SAMPING SUZUKI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 01:11:01', '2018-02-08', 'MERAUKE', '2018-03-09', '085244598993', NULL, NULL, NULL, NULL, 140.397997, -8.502115, 6, 3, 0, 0),
(781, 'RT020781', 'NURBAITI', ' JALAN BIAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 01:13:59', '2018-02-08', 'MERAUKE', '2018-03-09', '081248683086', NULL, NULL, NULL, NULL, 140.398476, -8.497439, 1, 2, 0, 0),
(782, 'RT020782', 'PURWADI', ' JALAN BIAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 01:18:05', '2018-02-08', 'MERAUKE', '2018-03-09', '081200000000', NULL, NULL, NULL, NULL, 140.398468, -8.497445, 1, 2, 0, 0),
(783, 'RT020783', 'UUN UNARSIH', ' JALAN BIAK', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 01:20:42', '2018-02-08', 'MERAUKE', '2018-03-09', '082399134920', NULL, NULL, NULL, NULL, 140.398485, -8.49745, 1, 2, 0, 0),
(784, 'RT040784', 'RISANO RIAN APRIANTO', ' JL. TERNATE, Gg. FELUBUN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 01:22:49', '2018-02-08', 'MERAUKE', '2018-03-09', '085243521995', NULL, NULL, NULL, NULL, 140.387463, -8.494549, 1, 4, 0, 0),
(785, 'RT030785', 'WARUNG MAKAN MEDAN SELERA (Slamet Riyadi)', 'JL. RAYA MANDALA, samping Toko Elvasa Merauke', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 02:16:33', '2018-02-08', 'MERAUKE', '2018-03-09', '081240268042', NULL, NULL, NULL, NULL, 140.400952, -8.506932, 1, 3, 0, 0),
(786, 'RM030786', 'WARUNG MAKAN MEDAN SELERA (Slamet Riyadi)', ' JL. RAYA MANDALA, SAMPING ELVASA ', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 02:19:22', '2018-02-08', 'MERAUKE', '2018-03-09', '081240268042', NULL, NULL, NULL, NULL, 140.400955, -8.506944, 4, 3, 23, 1),
(787, 'RT030787', 'SLAMET RIYADI (5 KK)', ' JL. RAYA MANDALA, SAMPING BANK SINAR MAS', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 02:27:35', '2018-02-08', 'MERAUKE', '2018-03-09', '081240268042', NULL, NULL, NULL, NULL, 140.400848, -8.505492, 1, 3, 0, 0),
(788, 'RT070788', 'MARTHA KAINAMA', ' JALAN AMPERA 3', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 04:22:53', '2018-02-08', 'MERAUKE', '2018-03-09', '081200000000', NULL, NULL, NULL, NULL, 140.394829, -8.483999, 1, 7, 0, 0),
(789, 'TS120789', 'FERLIN NURENDAH KIOS BERKAH', ' JALAN RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 05:03:47', '2018-02-08', 'MERAUKE', '2018-03-09', '081234900200', NULL, NULL, NULL, NULL, 140.407586, -8.51615, 3, 12, 0, 0),
(790, 'KI120790', 'FERLIN NURENDAH KIOS BERKAH', ' JALAN RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-09 05:07:01', '2018-02-08', 'MERAUKE', '2018-03-09', '081234900200', NULL, NULL, NULL, NULL, 140.407892, -8.516681, 6, 12, 0, 0),
(791, 'RT040791', 'INGGRID', ' JALAN TERNATE KOMPLEKS PERUM KEHUTANAN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-11 23:56:31', '2018-02-11', 'MERAUKE', '2018-03-12', '081200000000', NULL, NULL, NULL, NULL, 140.384467, -8.495832, 1, 4, 0, 0),
(792, 'KI120792', 'LAUNDRY NAY', ' JALAN RAYA MANDALA MULI, SAMPING BANK MEGA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 00:06:34', '2018-02-11', 'MERAUKE', '2018-03-12', '082238362355', NULL, NULL, NULL, NULL, 140.407221, -8.516105, 6, 12, 0, 0),
(793, 'RT020793', 'DEDI IRAWAN', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 00:24:51', '2018-02-11', 'MERAUKE', '2018-02-12', '085354533388', NULL, NULL, NULL, NULL, 140.399444, -8.489589, 1, 2, 0, 0),
(794, 'RT020794', 'DEDI IRAWAN I', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 00:29:49', '2018-02-11', 'MERAUKE', '2018-03-12', '085354533388', NULL, NULL, NULL, NULL, 140.399366, -8.489526, 1, 2, 0, 0),
(795, 'RT020795', 'DEDI IRAWAN II', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 00:36:58', '2018-02-11', 'MERAUKE', '2018-03-12', '085354433388', NULL, NULL, NULL, NULL, 140.399409, -8.489597, 1, 2, 0, 0),
(796, 'RT020796', 'DEDI IRAWAN III', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 00:41:59', '2018-02-11', 'MERAUKE', '2018-03-12', '085354533388', NULL, NULL, NULL, NULL, 140.398676, -8.488993, 1, 2, 0, 0),
(797, 'RT120797', 'ERFA WITFIATI', ' JALAN RAYA MANDALA SPADEM Gg. KPKN', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 00:50:45', '2018-02-11', 'MERAUKE', '2018-02-12', '081247718891', NULL, NULL, NULL, NULL, 140.408306, -8.51923, 1, 12, 0, 0),
(798, 'RT120798', 'WIWIN ', ' JALAN RAYA MANDALA MULI GG. ROMBE', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 00:58:43', '2018-02-11', 'MERAUKE', '2018-03-12', '081344170965', NULL, NULL, NULL, NULL, 140.402873, -8.511877, 1, 12, 0, 0),
(799, 'TS030799', 'H. SUKANI', ' JALAN RAYA MANDALA, SAMPING BATIK PAPUA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 01:05:34', '2018-02-11', 'MERAUKE', '2018-03-12', '0811484711', NULL, NULL, NULL, NULL, 140.402884, -8.509551, 3, 3, 0, 0),
(800, 'RT070800', 'ANTONI SAFEI', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'nonactive', '2018-03-12 01:13:52', '2018-02-11', 'MERAUKE', '2018-02-12', '085222790041', NULL, NULL, NULL, NULL, 140.389193, -8.483443, 1, 7, 0, 0),
(801, 'RT020801', 'DEDI IRAWAN IV', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-12 23:32:03', '2018-02-12', 'MERAUKE', '2018-03-12', '085354533388', NULL, NULL, NULL, NULL, 140.398657, -8.489009, 1, 2, 0, 0),
(802, 'RT020802', 'DEDI IRAWAN V', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-12 23:37:18', '2018-02-12', 'MERAUKE', '2018-03-12', '085354533388', NULL, NULL, NULL, NULL, 140.398668, -8.489006, 1, 2, 0, 0),
(803, 'RT020803', 'DEDI IRAWAN VI', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-12 23:40:33', '2018-02-12', 'MERAUKE', '2018-03-12', '085354533388', NULL, NULL, NULL, NULL, 140.398648, -8.489001, 1, 2, 0, 0),
(804, 'RT020804', 'BENEDICTUS DUMATUBUN', ' JALAN BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-12 23:46:56', '2018-02-12', 'MERAUKE', '2018-03-12', '082199016700', NULL, NULL, NULL, NULL, 140.408074, -8.501075, 1, 2, 0, 0),
(805, 'TB020805', 'ELDRY R JAUWENA', ' JALAN MISI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-12 23:55:35', '2018-02-12', 'MERAUKE', '2018-03-12', '081240640929', NULL, NULL, NULL, NULL, 140.403328, -8.50649, 2, 2, 0, 0),
(806, 'RT090806', 'TUTIK', ' JALAN BLOREP ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 00:09:45', '2018-02-12', 'MERAUKE', '2018-03-12', '082238053941', NULL, NULL, NULL, NULL, 140.414648, -8.502852, 1, 8, 23, 1),
(807, 'RT080807', 'MIRAJANA WATI', ' JALAN BLOREP', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 00:14:09', '2018-02-12', 'MERAUKE', '2018-03-12', '082397474907', NULL, NULL, NULL, NULL, 140.414671, -8.50287, 1, 8, 0, 0),
(808, 'RT080808', 'LAURENSIUS LABESUBAN', ' JALAN MANGGA DUA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 00:27:17', '2018-02-12', 'MERAUKE', '2018-03-12', '081200000000', NULL, NULL, NULL, NULL, 140.403062, -8.490351, 1, 8, 0, 0),
(809, 'RT120809', 'PHILIP ALFONS', ' BELAKANG SD MULI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 00:47:05', '2018-02-12', 'MERAUKE', '2018-03-12', '081200000000', NULL, NULL, NULL, NULL, 140.40733, -8.514, 1, 12, 0, 0),
(810, 'RT030810', 'HARRY LEGIMAN, ST', ' JALAN GAK GANG LEGIMAN', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 01:12:45', '2018-02-12', 'MERAUKE', '2018-03-12', '081200000000', NULL, NULL, NULL, NULL, 140.392376, -8.503058, 1, 3, 0, 0),
(811, 'RT060811', 'KARTINI KARDI', ' JALAN NOARI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 01:24:11', '2018-02-12', 'MERAUKE', '2018-03-13', '082197961134', NULL, NULL, NULL, NULL, 140.385665, -8.485698, 1, 6, 0, 0),
(812, 'KI030812', 'MUSTARI JUDDA - NIATI SALON ', 'JALAN BAMPEL ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 01:32:57', '2018-02-12', 'MERAUKE', '2018-03-13', '085244066492', NULL, NULL, NULL, NULL, 140.401236, -8.507182, 6, 3, 0, 0),
(813, 'RT060813', 'TRIANI BALAGAIZE', ' JALAN MALUKU', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 01:38:25', '2018-02-12', 'MERAUKE', '2018-02-13', '082199125725', NULL, NULL, NULL, NULL, 140.386848, -8.484728, 1, 6, 0, 0),
(814, 'RT070814', 'HERAD R MANUPUTTY', ' JALAN AMPERA III', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 01:47:54', '2018-02-12', 'MERAUKE', '2018-03-13', '081200000000', NULL, NULL, NULL, NULL, 140.394928, -8.483765, 1, 7, 0, 0),
(815, 'LCLK050815', 'LOKALISASI YOBAR', ' JALAN ARAFURA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 02:41:41', '2018-02-12', 'MERAUKE', '2018-03-13', '081200000000', NULL, NULL, NULL, NULL, 140.405697, -8.532488, 16, 5, 0, 0),
(816, 'RT120816', 'SRI IRIANI', ' JALAN RAYA MANDALA SPADEM GANG KEMBAR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-13 04:02:47', '2018-02-12', 'MERAUKE', '2018-03-13', '08', NULL, NULL, NULL, NULL, 140.412272, -8.521401, 1, 12, 0, 0),
(817, 'RT040817', 'GUNTUR OHOITIMUR', ' JALAN TERNATE ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 00:11:36', '2018-02-13', 'MERAUKE', '2018-01-10', '085254683397', NULL, NULL, NULL, NULL, 140.38772, -8.49987, 1, 4, 0, 0),
(818, 'RT070818', 'YULIUS MASUDI', 'JALAN MUTING POLDER ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 00:52:31', '2018-02-13', 'MERAUKE', '2018-02-14', '081344977474', NULL, NULL, NULL, NULL, 140.398212, -8.480288, 1, 7, 0, 0),
(819, 'RT070819', 'MANTO SILUBUN', 'JALAN AMPERA II ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 00:59:36', '2018-02-13', 'MERAUKE', '2018-03-14', '081344977474', NULL, NULL, NULL, NULL, 140.395278, -8.483324, 1, 7, 0, 0),
(820, 'RT070820', 'AGNES PARIYEM', ' JALAN ALIARKAM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 01:04:06', '2018-02-13', 'MERAUKE', '2018-03-14', '081344977474', NULL, NULL, NULL, NULL, 140.395216, -8.483059, 1, 7, 0, 0),
(821, 'RT070821', 'SATRIA', ' JL SOEKARJO', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 01:50:44', '2018-02-13', 'MERAUKE', '2018-03-14', '085244331395', NULL, NULL, NULL, NULL, 140.387946, -8.480858, 1, 7, 0, 0),
(822, 'RT020822', 'AFANDI ARIFUDDIN', ' JALAN AHMAD GANG AHMAD YANI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 02:06:28', '2018-02-13', 'MERAUKE', '2018-03-14', '081247922922', NULL, NULL, NULL, NULL, 140.405373, -8.510566, 1, 2, 0, 0),
(823, 'RT120823', 'MARTINUS GALA', ' JALAN GG SERUNI BELAKANG SD MULI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 04:16:11', '2018-02-13', 'MERAUKE', '2018-03-14', '082346780282', NULL, NULL, NULL, NULL, 140.407103, -8.514107, 1, 12, 0, 0),
(824, 'RT070824', 'AWALUDIN', ' JL. PINTU AIR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 05:47:02', '2018-02-13', 'MERAUKE', '2018-03-14', '082198318843', NULL, NULL, NULL, NULL, 140.394185, -8.476781, 1, 7, 0, 0),
(825, 'RT070825', 'H. MALUKI', ' JL. GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-14 05:56:05', '2018-02-13', 'MERAUKE', '2018-03-14', '081248024945', NULL, NULL, NULL, NULL, 140.395797, -8.489186, 1, 7, 0, 0),
(826, 'RT090826', 'SRI UTAMI', ' JALAN PEMBANGUNAN GANG WEVAS', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-15 00:32:16', '2018-02-14', 'MERAUKE', '2018-03-15', '082198838111', NULL, NULL, NULL, NULL, 140.420677, -8.516741, 1, 9, 0, 0),
(827, 'RT090827', 'SRI UTAMI / SUPARJA', ' JALAN PEMBANGUNAN GANG WELLPASS', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-15 00:35:15', '2018-02-14', 'MERAUKE', '2018-03-15', '082198838111', NULL, NULL, NULL, NULL, 140.420694, -8.516806, 1, 9, 0, 0),
(828, 'RM020828', 'WARUNG MAKAN RAJA LALAP', ' JALAN MARTADINATA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-15 02:52:33', '2018-02-14', 'MERAUKE', '2018-03-15', '082248220852', NULL, NULL, NULL, NULL, 140.407484, -8.50714, 4, 2, 0, 0),
(829, 'TB070829', 'TOKO SATRIA', ' JALAN TMP', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-15 04:16:39', '2018-02-14', 'MERAUKE', '2018-03-15', '081200000000', NULL, NULL, NULL, NULL, 140.398641, -8.486877, 2, 7, 0, 0),
(830, 'RT070830', 'SUTIMIN', ' JALAN GOR ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 02:33:57', '2018-02-15', 'MERAUKE', '2018-03-16', '081200000000', NULL, NULL, NULL, NULL, 140.397128, -8.488325, 6, 7, 23, 1),
(831, 'RT070831', 'SURATMI', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 02:41:16', '2018-02-15', 'MERAUKE', '2018-03-16', '081200000000', NULL, NULL, NULL, NULL, 140.397169, -8.488336, 1, 7, 0, 0),
(832, 'KI070832', 'M. ARSYAD', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 02:42:57', '2018-02-15', 'MERAUKE', '2018-03-16', '081200000000', NULL, NULL, NULL, NULL, 140.397222, -8.488347, 6, 7, 0, 0),
(833, 'KI070833', 'MUHAMAD RAHMAN', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 02:44:44', '2018-02-15', 'MERAUKE', '2018-03-16', '081200000000', NULL, NULL, NULL, NULL, 140.397296, -8.488302, 6, 7, 0, 0),
(834, 'KI070834', 'SYARIFUDDIN AMULAH', 'JALAN GOR ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 02:46:45', '2018-02-15', 'MERAUKE', '2018-03-16', '081200000000', NULL, NULL, NULL, NULL, 140.397228, -8.488314, 6, 7, 0, 0),
(835, 'KI070835', 'BAHARUDDIN', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 02:48:14', '2018-02-15', 'MERAUKE', '2018-03-16', '081200000000', NULL, NULL, NULL, NULL, 140.397293, -8.488264, 6, 7, 0, 0),
(836, 'KI070836', 'MARJAN', 'JALAN GOR ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 02:50:00', '2018-02-15', 'MERAUKE', '2018-03-16', '081200000000', NULL, NULL, NULL, NULL, 140.397407, -8.488269, 6, 7, 0, 0),
(837, 'KI070837', 'SUTIMIN', ' JALAN GOR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 02:58:14', '2018-02-15', 'MERAUKE', '2018-03-15', '081200000000', NULL, NULL, NULL, NULL, 140.397373, -8.488269, 6, 7, 0, 0),
(838, 'RT080838', 'TUTIK', ' JL. BLOREP', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 04:12:45', '2018-02-15', 'MERAUKE', '2016-03-16', '082238053941', NULL, NULL, NULL, NULL, 140.416326, -8.502257, 1, 8, 0, 0),
(839, 'RT030839', 'MUSNAYATI', ' JL. GAK', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 05:19:10', '2018-02-15', 'MERAUKE', '2015-03-16', '085197120851', NULL, NULL, NULL, NULL, 140.392231, -8.50664, 1, 3, 0, 0),
(840, 'TS030840', 'RUMAH BATIK SELIS', ' JALAN RAYA MANDALA BAMPEL', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 05:55:38', '2018-02-15', 'MERAUKE', '2018-03-16', '085244834858', NULL, NULL, NULL, NULL, 140.400925, -8.506876, 3, 3, 0, 0),
(841, 'TB020841', 'MERAUKE TOWN SQUARE', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-16 06:08:59', '2018-02-15', 'MERAUKE', '2018-03-16', '081200000000', NULL, NULL, NULL, NULL, 140.391757, -8.489993, 2, 2, 0, 0),
(842, 'KI030842', 'KIOS CAHAYA (WIYATMOKO)', ' JL. RAYA MANDALA, DEPAN TOKO TUJUH', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 00:46:55', '2018-02-18', 'MERAUKE', '2018-03-19', '081247631999', NULL, NULL, NULL, NULL, 140.39835, -8.502619, 6, 3, 0, 0),
(843, 'KI060843', 'ASRI CELL', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 01:33:28', '2018-02-18', 'MERAUKE', '2018-03-18', '082248111788', NULL, NULL, NULL, NULL, 140.387724, -8.491657, 6, 6, 0, 0),
(844, 'RT060844', 'MARGONO', ' JL. NUSA BARONG (ASRI CELL)', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 01:38:19', '2018-02-18', 'MERAUKE', '2018-03-18', '082248111788', NULL, NULL, NULL, NULL, 140.387666, -8.491694, 1, 6, 0, 0),
(845, 'PRSH060845', 'PT. WIJAYA KARYA (PERSERO) tbk', ' JL. PELABUHAN PERIKANAN SAMUDERA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 01:53:13', '2018-02-18', 'MERAUKE', '2018-03-18', '081266063333', NULL, NULL, NULL, NULL, 140.373556, -8.478065, 12, 6, 23, 1),
(846, 'RT070846', 'KOE TJAI SIN', ' JL.. RAYA MANDALA NO. 16', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 02:15:58', '2018-02-18', 'MERAUKE', '2018-03-19', '085244156197', NULL, NULL, NULL, NULL, 140.389692, -8.485103, 1, 7, 0, 0),
(847, 'RT030847', 'MEGA ASHARY AL MAHDALI', ' JL. GANG GEREJA BAMPEL', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 02:24:50', '2018-02-18', 'MERAUKE', '2018-03-19', '082248083216', NULL, NULL, NULL, NULL, 140.395996, -8.507406, 1, 3, 0, 0),
(848, 'RS090848', 'PUSKESMAS RIMBA JAYA', ' JL. GARUDA MOPAH LAMA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 02:31:27', '2018-02-18', 'MERAUKE', '2018-03-19', '082189352351', NULL, NULL, NULL, NULL, 140.441884, -8.542007, 10, 9, 0, 0),
(849, 'RT060849', 'BENYAMIN ISAK LATUMAHINA', 'JL. NOARI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 02:42:44', '2018-02-18', 'MERAUKE', '2018-03-19', '085210173077', NULL, NULL, NULL, NULL, 140.388845, -8.48509, 1, 6, 0, 0),
(850, 'RT060850', 'ISABELA DWI YUNITA DUMATUBUN', ' JL. HABE', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 03:19:45', '2018-02-18', 'MERAUKE', '2018-12-03', '081243392689', NULL, NULL, NULL, NULL, 140.386718, -8.48704, 1, 6, 0, 0),
(851, 'TS060851', 'HERI SETIAWAN', ' JL. NUSABARONG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 04:49:44', '2018-02-18', 'MERAUKE', '2018-12-03', '08534933615', NULL, NULL, NULL, NULL, 140.383931, -8.493606, 3, 6, 0, 0),
(852, 'BGL030852', 'BENGKEL CAHAYA MOTOR (WARDI)', ' JL. RAYA MANDALA BAMPEL', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 05:54:57', '2018-02-18', 'MERAUKE', '2018-03-19', '082248521475', NULL, NULL, NULL, NULL, 140.398221, -8.502476, 11, 3, 0, 0),
(853, 'BGL030853', 'BENGKEL M. RIZAL', ' JL. RAYA MANDALA BAMPEL, SAMPING SWISSBELL HOTEL (RENTAL MUTIARA)', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 06:05:47', '2018-02-18', 'MERAUKE', '2018-03-19', '082248521475', NULL, NULL, NULL, NULL, 140.399135, -8.504475, 11, 3, 0, 0),
(854, 'RT030854', 'HASANTO', ' JL. GAK, Gg. SAMPING KOLAM BUAYA ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 06:43:57', '2018-02-18', 'MERAUKE', '2018-07-19', '081343101981', NULL, NULL, NULL, NULL, 140.390833, -8.506218, 1, 3, 23, 1),
(855, 'RT120855', 'AGNITA ARITANGGINOP', 'JALAN POMPA AIR II MULI ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 23:40:45', '2018-02-19', 'MERAUKE', '2018-03-20', '082238828934', NULL, NULL, NULL, NULL, 140.406968, -8.512825, 1, 12, 0, 0),
(856, 'RT070856', 'MUHAMMAD JHONI', ' JALAN AMPERA IV', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-19 23:51:59', '2018-02-19', 'MERAUKE', '2018-03-20', '085254055701', NULL, NULL, NULL, NULL, 140.394459, -8.483889, 1, 7, 0, 0),
(857, 'RT120857', 'HADJIDJAH BODONG', ' JL RAYA MANDALA SPADEM GG.ASFAAT KPKN NO.6', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-20 01:04:08', '2018-02-19', 'MERAUKE', '2018-03-20', '085243525171', NULL, NULL, NULL, NULL, 140.406806, -8.516722, 1, 12, 0, 0),
(858, 'RT120858', 'RITA AMIRUDIN S', ' JL RAYA MANDALA SPADEM Gg. KEMBAR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-20 01:59:29', '2018-02-19', 'MERAUKE', '2018-03-20', '081200000000', NULL, NULL, NULL, NULL, 140.410989, -8.521882, 1, 12, 0, 0),
(859, 'KNTR020859', 'KANTOR PERWAKILAN BANDARA OKABA', ' JALAN RAYA MANDALA KOMPLEKS RUKO SENANG HATI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-20 02:12:07', '2018-02-19', 'MERAUKE', '2018-03-20', '08114830333', NULL, NULL, NULL, NULL, 140.404624, -8.511825, 7, 2, 0, 0),
(860, 'RT030860', 'ADI SAPTONO', ' JALAN GAK GANG LEGIMIN', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-20 02:24:40', '2018-02-19', 'MERAUKE', '2018-03-20', '081200000000', NULL, NULL, NULL, NULL, 140.398065, -8.505916, 1, 3, 0, 0),
(861, 'RT090861', 'NOVA PUSPITASARI', ' JALAN PEMUDA SAMPING SMP NEG 2 PERUM BANDARA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-20 02:30:20', '2018-02-19', 'MERAUKE', '2018-03-20', '08524070046', NULL, NULL, NULL, NULL, 140.409453, -8.504012, 1, 9, 0, 0),
(862, 'TS120862', 'HOKI MART (VINSEN D. LUKKEY)', ' JL. HUSEN PALELA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-20 03:12:25', '2018-02-19', 'MERAUKE', '2018-03-19', '081283067730', NULL, NULL, NULL, NULL, 140.404572, -8.517496, 3, 12, 0, 0),
(863, 'RT060863', 'NELLY YUNUS', ' JALAN SUMATERA GG.MANGGARAI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-20 04:26:26', '2018-02-19', 'MERAUKE', '2018-03-20', '085254460785', NULL, NULL, NULL, NULL, 140.39004, -8.49083, 1, 6, 0, 0),
(864, 'KI120864', 'MINCE LELANG', ' JL. RAYA MANDALA DEPAN KANTOR BPJS', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-20 05:52:12', '2018-02-19', 'MERAUKE', '2018-03-20', '082239102389', NULL, NULL, NULL, NULL, 140.411481, -8.52118, 6, 12, 0, 0),
(865, 'RT020865', 'MOCH DEBI', ' JALAN BIAK RUMAH SEWA BIAK PERMAI NO.2', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 00:39:47', '2018-02-20', 'MERAUKE', '2018-03-21', '081343396809', NULL, NULL, NULL, NULL, 140.39813, -8.497585, 1, 2, 0, 0),
(866, 'RT050866', 'KRISNAWATI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:16:42', '2018-02-20', 'MERAUKE', '1989-10-05', '085244323392', NULL, NULL, NULL, NULL, 140.371681, -8.500857, 1, 5, 0, 0),
(867, 'RT050867', 'HERMIN SRI WAHYUNUI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:30:32', '2018-02-20', 'MALANG', '1988-02-28', '08120000090', NULL, NULL, NULL, NULL, 140.371769, -8.501083, 1, 5, 0, 0),
(868, 'RT050868', 'HANI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:32:51', '2018-02-20', 'MERAUKE', '1985-04-17', '08120000000', NULL, NULL, NULL, NULL, 140.371762, -8.5012, 1, 5, 0, 0),
(869, 'RT050869', 'DARMIYANTI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:35:20', '2018-02-20', 'MERAUKE', '1986-08-29', '081200000000', NULL, NULL, NULL, NULL, 140.371799, -8.501206, 1, 5, 0, 0),
(870, 'RT060870', 'YARMAN', ' JALAN NATUNA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:36:18', '2018-02-20', 'MERAUKE', '2018-03-21', '082238252224', NULL, NULL, NULL, NULL, 140.386511, -8.488827, 1, 6, 0, 0),
(871, 'RT050871', 'HASBULLAH', 'JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:37:51', '2018-02-20', 'SAPULUNGAN', '1987-05-05', '085254273188', NULL, NULL, NULL, NULL, 140.371533, -8.500929, 1, 5, 0, 0),
(872, 'RT050872', 'DARMAWATI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:40:27', '2018-02-20', 'UJUNG PANDANG', '1981-04-12', '082238873085', NULL, NULL, NULL, NULL, 140.371585, -8.501059, 1, 5, 0, 0);
INSERT INTO `re_costumer` (`cost_id`, `cost_id_pel`, `cost_name`, `cost_address`, `cost_prov`, `cost_kab`, `cost_kec`, `cost_status`, `cost_date`, `cost_periode_terakhir`, `cost_tmp_lahir`, `cost_tgl_lahir`, `cost_phone`, `cost_foto`, `cost_lokasi`, `cost_denah`, `cost_ktp`, `cost_lang`, `cost_latt`, `cost_type_id`, `cost_desa_id`, `cost_user_id`, `cost_jadwal_id`) VALUES
(873, 'RT050873', 'JUFRI TANDILINO', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:43:52', '2018-02-20', 'PANGA', '1982-06-19', '085244054888', NULL, NULL, NULL, NULL, 140.371629, -8.501192, 1, 5, 0, 0),
(874, 'RT050874', 'AGUS TANIFAN', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:46:35', '2018-02-20', 'ROMEAN', '1969-12-22', '0813997870', NULL, NULL, NULL, NULL, 140.371652, -8.501204, 1, 5, 0, 0),
(875, 'RT050875', 'HUSNI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:48:57', '2018-02-20', 'UJUNG PANDANG', '1982-05-14', '0812000000', NULL, NULL, NULL, NULL, 140.371881, -8.501311, 1, 5, 0, 0),
(876, 'RT050876', 'NURHAYATI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:51:06', '2018-02-20', 'JAWA TIMUR', '1978-06-26', '081200000', NULL, NULL, NULL, NULL, 140.371818, -8.5012, 1, 5, 0, 0),
(877, 'RT050877', 'ANI ABBAS', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:53:39', '2018-02-20', 'MERAUKE', '1985-05-01', '0812000000', NULL, NULL, NULL, NULL, 140.371639, -8.501208, 1, 5, 0, 0),
(878, 'RT050878', 'BACHTIAR', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:56:06', '2018-02-20', 'MAKASSAR', '1988-10-10', '081200000', NULL, NULL, NULL, NULL, 140.371983, -8.501452, 1, 5, 0, 0),
(879, 'RT050879', 'RUKINI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:58:08', '2018-02-20', 'LAMPUNG', '1980-09-10', '08120000000', NULL, NULL, NULL, NULL, 140.371999, -8.50144, 1, 5, 0, 0),
(880, 'RT050880', 'JUMRIA', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 01:59:54', '2018-02-20', 'MAROS', '1978-07-05', '081200000', NULL, NULL, NULL, NULL, 140.371547, -8.501248, 1, 5, 0, 0),
(881, 'RT050881', 'HASAN', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 02:01:41', '2018-02-20', 'GOA', '1978-08-17', '081200000', NULL, NULL, NULL, NULL, 140.371833, -8.50131, 1, 5, 0, 0),
(882, 'RT050882', 'SANTI SRIYANTI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 02:04:34', '2018-02-20', 'MAKASSAR', '1984-11-03', '0812000000', NULL, NULL, NULL, NULL, 140.371951, -8.501128, 1, 5, 0, 0),
(883, 'KI050883', 'SITI FATIMAH', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 02:06:36', '2018-02-20', 'MERAUKE', '1980-03-18', '08120000', NULL, NULL, NULL, NULL, 140.371709, -8.501638, 6, 5, 0, 0),
(884, 'KI050884', 'RAMLI', ' JL. MENARA LAMPU I', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 02:08:42', '2018-02-20', 'UJUNG PANDANG', '1977-07-29', '081200000', NULL, NULL, NULL, NULL, 140.371628, -8.501481, 6, 5, 0, 0),
(885, 'RT120885', 'RIYADI', ' JALAN RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 05:20:29', '2018-02-20', 'MERAUKE', '2018-03-21', '081200000000', NULL, NULL, NULL, NULL, 140.399391, -8.508233, 1, 12, 0, 0),
(886, 'RT120886', 'RIYADI', ' JALAN RAYA MANDALA MULI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-21 05:23:02', '2018-02-20', 'MERAUKE', '2018-03-21', '081200000000', NULL, NULL, NULL, NULL, 140.399405, -8.508269, 1, 12, 0, 0),
(887, 'RT020887', 'BUNYAMIN LAITUPA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 01:18:00', '2018-02-21', 'MERAUKE', '2018-03-22', '081200000000', NULL, NULL, NULL, NULL, 140.392373, -8.493374, 1, 2, 0, 0),
(888, 'RT020888', 'ATIKA LAITUPA', ' JALAN RAYA MANDALA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 01:23:57', '2018-02-21', 'MERAUKE', '2018-03-22', '081200000000', NULL, NULL, NULL, NULL, 140.392352, -8.493427, 1, 2, 0, 0),
(889, 'RT020889', 'JONI TUWING', ' ASRAMA POLRES', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 01:28:16', '2018-02-21', 'MERAUKE', '2018-03-22', '081344019174', NULL, NULL, NULL, NULL, 140.391175, -8.487661, 1, 2, 0, 0),
(890, 'RT060890', 'LIANE STEPHANIE SAHERTIAN', ' JALAN NOWARI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 01:59:20', '2018-02-21', 'MERAUKE', '2018-03-22', '085254245230', NULL, NULL, NULL, NULL, 140.38321, -8.488052, 1, 6, 0, 0),
(891, 'RT070891', 'BURHANUDIN', ' JALAN ALIARKAM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 02:45:42', '2018-02-21', 'MERAUKE', '2018-03-22', '081344388743', NULL, NULL, NULL, NULL, 140.395001, -8.483028, 1, 7, 0, 0),
(892, 'KI120892', 'SANTI', ' JALAN RAYA MANDALA GG.DIRGANTARA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 02:55:33', '2018-02-21', 'MERAUKE', '2018-03-22', '081200000000', NULL, NULL, NULL, NULL, 140.407309, -8.517994, 6, 12, 0, 0),
(893, 'RT120893', 'ROSLIANA', ' JALAN RAYA MANDALA GG.DIRGANTARA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 02:59:37', '2018-02-21', 'MERAUKE', '2018-03-22', '081200000000', NULL, NULL, NULL, NULL, 140.407334, -8.517999, 1, 12, 0, 0),
(894, 'RT120894', 'HERY', ' JL.RAYA MANDALA GG.DIRGANTARA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 03:02:21', '2018-02-21', 'MERAUKE', '2018-03-22', '081200000000', NULL, NULL, NULL, NULL, 140.407312, -8.517975, 1, 12, 0, 0),
(895, 'RT120895', 'SUTILA RANI', ' JALAN RAYA MANDALA GG.DIRGANTARA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 03:05:28', '2018-02-21', 'M', '2018-03-22', '081200000000', NULL, NULL, NULL, NULL, 140.407327, -8.517984, 1, 12, 0, 0),
(896, 'RT120896', 'ORPA P.TULAK', ' JL.RAYA MANDALA GG.DIRGANTARA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-22 03:09:11', '2018-02-21', 'MERAUKE', '2018-03-22', '085254052839', NULL, NULL, NULL, NULL, 140.407377, -8.518016, 1, 12, 0, 0),
(897, 'RT060897', 'LATUPEIRISSA SIMON', ' JL. Gg. KELINCI II ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-27 04:17:03', '2018-02-26', 'HARIA', '1953-03-16', '081212346220', NULL, NULL, NULL, NULL, 140.389177, -8.48824, 1, 6, 23, 1),
(898, 'RT020898', 'JUMARI (3 KK) RUMAH SEWA', ' JL. BRAWIJAYA, Gg. ROMANUS ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-03-27 05:05:05', '2018-02-26', 'MERAUKE', '2018-03-27', '085244670943', NULL, NULL, NULL, NULL, 140.406604, -8.500558, 1, 2, 0, 0),
(899, 'KI030899', 'MULYONO (KIOS)_Gg. SAYUR', 'GANG GEREJA GANG SAYUR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-05 00:39:08', '2018-03-04', 'MERAUKE', '2018-04-05', '085344453717', NULL, NULL, NULL, NULL, 140.383803, -8.489436, 6, 3, 0, 0),
(900, 'RT030900', 'MULYONO (10 KK)_Gg. SAYUR', ' JL. GANG GEREJA, GG. SAYUR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-05 00:44:51', '2018-03-04', 'MERAUKE', '2018-04-05', '085344453717', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 3, 0, 0),
(901, 'RT110901', 'IRFAN', ' JL. PEMBANGUNAN', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-05 01:27:00', '2018-03-04', 'MERAUKE', '2018-04-05', '08124877872', NULL, NULL, NULL, NULL, 140.398399, -8.504475, 1, 11, 0, 0),
(902, 'RT040902', 'SUTIYAM MADI', ' JL. IRIAN SERINGGU, Gg. AT-TAQWWA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-05 03:03:14', '2018-03-04', 'MERAUKE', '2018-04-05', '085254607083', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 4, 0, 0),
(903, 'RT040903', 'CAKRA ABDI ROMADHAN', ' JL. TERNATE, SAMPING KANTOR/RUMAH KERUCUT', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-05 03:06:58', '2018-03-04', 'MERAUKE', '2018-04-05', '08113670628', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 4, 0, 0),
(904, 'RT070904', 'CORNELIA TETHOL', ' JL. ERMASSU, KOMPLEKS PU', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-05 05:07:45', '2018-03-04', 'SOFYANIN', '1985-03-16', '081344087066', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 7, 0, 0),
(905, 'KI070905', 'FERRY IRWAN', ' JALAN TMP POLDER', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-05 06:14:13', '2018-03-04', 'MERAUKE', '2018-04-04', '085354539111', NULL, NULL, NULL, NULL, 140.394185, -8.488269, 6, 7, 0, 0),
(906, 'KI070906', 'FERRY IRWAN a', ' JALAN TMP POLDER', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-05 06:19:29', '2018-03-04', 'MERAUKE', '2018-04-05', '085354539111', NULL, NULL, NULL, NULL, 140.394185, -8.488269, 6, 7, 0, 0),
(907, 'RT060907', 'ANAS', ' JL. NUSA BARONG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-06 00:32:00', '2018-03-05', 'MERAUKE', '2018-04-06', '081344766042', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 6, 0, 0),
(908, 'RT030908', 'MUHAMAD ARAS', ' JALAN GAK', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-06 01:29:34', '2018-03-05', 'MERAUKE', '2018-04-06', '082239438377', NULL, NULL, NULL, NULL, 140.394185, -8.488269, 1, 3, 0, 0),
(909, 'RT070909', 'SULAIMAN SAMAD', ' JALAN BHAKTI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-08 23:20:47', '2018-03-08', 'MERAUKE', '2018-04-09', '081200000000', NULL, NULL, NULL, NULL, 140.394185, -8.488269, 1, 7, 0, 0),
(910, 'RT020910', 'YOAKIM MALOMPO', ' JALAN MISSI SIMPANG PENDIDIKAN', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 00:12:59', '2018-03-09', 'MERAUKE', '2018-04-10', '081200000000', NULL, NULL, NULL, NULL, 140.394185, -8.488269, 1, 2, 0, 0),
(911, 'RT030911', 'SRI WAHYUNI', ' JALAN SERINGGU', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 00:28:40', '2018-03-09', 'MERAUKE', '2018-04-10', '081200000000', NULL, NULL, NULL, NULL, 140.394185, -8.488269, 1, 3, 0, 0),
(912, 'TS020912', 'KORNELIA KONYOPAT', ' JALAN BRAWIJAYA DEPAN MEGARAYA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 00:34:28', '2018-03-09', 'MERAUKE', '2018-04-10', '082238391077', NULL, NULL, NULL, NULL, 140.394185, -8.488269, 3, 2, 0, 0),
(913, 'RT030913', 'OKTON R. PAGAYA', ' JL. GAK PERUM BTN BELAKANG KELURAHAN BAMPEL', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 01:01:15', '2018-03-09', 'MERAUKE', '2018-04-10', '082348375853', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 3, 0, 0),
(914, 'RT070914', 'UMAR TONO', ' JALAN AMPERA IV, RUMAH SEWA HJ. BADARIAH', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 01:30:18', '2018-03-09', 'MERAUKE', '2018-04-10', '082199265641', NULL, NULL, NULL, NULL, 140.394185, -8.488269, 1, 7, 0, 0),
(915, 'RT040915', 'ERFAN CHANDRA NUGROHO', ' JL. IRIAN SERINGGU KALI WEDA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 01:54:48', '2018-03-09', 'MERAUKE', '2018-04-09', '08112115454', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 4, 0, 0),
(916, 'KI020916', 'SITTI RAHMADANIA WATI (KIOS RAHMAT)', ' JL. PENDIDIKAN BLOK C NO. 10', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 02:01:54', '2018-03-09', 'MERAUKE', '2018-04-10', '085243384567', NULL, NULL, NULL, NULL, 140.383803, -8.504475, 6, 2, 0, 0),
(917, 'RT030917', 'REHAN', ' JL. GAK PERUMAHAN BTN INBUTIKAY', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 02:22:21', '2018-03-09', 'MERAUKE', '2018-04-10', '081111111111111', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 3, 0, 0),
(918, 'RT030918', 'HENGKI', ' JL. GAK PERUMAHAN BTN INBUTIKAY', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 02:25:19', '2018-03-09', 'MERAUKE', '2018-04-10', '081111111111111111111', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 3, 0, 0),
(919, 'RT030919', 'DIAH', ' JL. GAK. PERUMAHAN BTN INBUTIKAY', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 02:27:29', '2018-03-09', 'MERAUKE', '2018-04-10', '08111111111111111111111', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 3, 0, 0),
(920, 'PRSH020920', 'SAM SADO', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 02:51:15', '2018-03-09', 'MERAUKE', '2018-04-10', '0811111111111111', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 12, 2, 0, 0),
(921, 'RT060921', 'SUKIRNO (5 KK)', ' JL. NATUNA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-10 23:46:43', '2018-03-10', 'MERAUKE', '2018-04-11', '08110000000000000000', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 6, 0, 0),
(922, 'RT020922', 'RONI ROBERT WALINAULIK', ' JL. BRAWIJAYA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-11 00:21:49', '2018-03-10', 'MERAUKE', '2018-04-11', '081235416321', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 2, 0, 0),
(923, 'RT120923', 'JOHANES SIRUPANG', ' JL. POMPA AIR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-11 00:53:54', '2018-03-10', 'MERAUKE', '2018-04-11', '085244448881', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 12, 0, 0),
(924, 'RT080924', 'AZIS', ' JL. BRAWIJAYA DEPAN TOKO HARI-HARI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-11 01:01:17', '2018-03-10', 'MERAUKE', '2018-04-11', '085243225303', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 8, 0, 0),
(925, 'RT040925', 'SAMRONI', ' JL. TERNATE, Gg. TIGA TANJUNG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-11 01:11:57', '2018-03-10', 'MERAUKE', '2018-04-11', '08120000000', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 4, 0, 0),
(926, 'RT040926', 'TRIYONO', ' JL. TERNATE, Gg. TIGA TANJUNG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-11 01:14:20', '2018-03-10', 'MERAUKE', '2018-04-11', '08120000000', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 4, 0, 0),
(927, 'HTL040927', 'HOTEL COREINN', ' JL. KAMPUNG TIMUR', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-11 01:17:52', '2018-03-10', 'MERAUKE', '2018-04-11', '085244409990', NULL, NULL, NULL, NULL, 140.383803, -8.504475, 15, 4, 0, 0),
(928, 'RT040928', 'AL-AMIN (14 PINTU)', ' JL. TERNATE, (BEKAS KEBAKARAN)', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-11 01:28:27', '2018-03-10', 'MERAUKE', '2018-04-11', '081248635812', NULL, NULL, NULL, NULL, 140.383803, -8.504475, 1, 4, 0, 0),
(929, 'RT020929', 'FINCE RENYUT', ' JL. RAYA MANDALA, Gg. TOKO CITRA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 00:08:37', '2018-03-11', 'MERAUKE', '2018-04-12', '085354679602', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 2, 0, 0),
(930, 'RT090930', 'DANIEL J. TARANENO', ' JL. GARUDA SPADEM', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 00:19:24', '2018-03-11', 'MERAUKE', '2018-04-12', '0812 4093 0000', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 9, 0, 0),
(931, 'RT030931', 'MUHAMAD FATURAHMAN', ' JL. GAK', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 01:05:23', '2018-03-11', 'KEBUMEN', '1976-01-02', '08120000000', NULL, NULL, NULL, NULL, 140.399763, -8.507692, 1, 3, 0, 0),
(932, 'RT120932', 'BIBIANA KIRWELAKUBUN (4 KK)', ' JL.. RAYA MANDALA MULI SAMPING BANK MEGA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 01:24:44', '2018-03-11', 'MERAUKE', '2018-04-12', '081344930984', NULL, NULL, NULL, NULL, 140.406764, -8.515984, 1, 12, 0, 0),
(933, 'RT040933', 'SUJADI', ' JL. TERNATE, Gg. TIGA TANJUNG', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 01:52:39', '2018-03-11', 'MERAUKE', '2018-04-12', '082397993328', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 4, 0, 0),
(934, 'TS120934', 'APRI KRISTIYANI (TOKO HERBAL NUTRISI)', ' JL. RAYA MANDALA DEPAN DEALER TOYOTA', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 02:06:27', '2018-03-11', 'MERAUKE', '2018-04-12', '081200000000', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 3, 12, 0, 0),
(935, 'RT030935', 'SUKARNO', ' JL. RAYA MANDALA BAMPEL', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 02:09:15', '2018-03-11', 'MERAUKE', '2018-04-12', '081200000000', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 3, 0, 0),
(936, 'RT060936', 'SUKMAWATI IRIANY', 'JL. MALUKU ', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 02:16:49', '2018-03-11', 'MERAUKE', '2018-04-12', '081344233649', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 6, 0, 0),
(937, 'RT040937', 'SAMSURI (17 KK)', ' JL. IRIAN SERINGGU, Gg. LAHARI (BELAKANG DEALER KAWASAKI)', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 02:47:29', '2018-03-11', 'MERAUKE', '1993-06-02', '081111111111111', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 1, 4, 0, 0),
(938, 'KI040938', 'SAMSURI (KIOS)', ' JL. IRIAN SERINGGU, BELAKANG DEALER KAWASAKI', 'Papua', 'Merauke', 'Merauke', 'active', '2018-04-12 02:54:06', '2018-03-11', 'MERAUKE', '1993-06-02', '081111111111111', NULL, NULL, NULL, NULL, 140.398399, -8.489436, 6, 4, 0, 0);

--
-- Triggers `re_costumer`
--
DELIMITER $$
CREATE TRIGGER `setDefaultDate` BEFORE INSERT ON `re_costumer` FOR EACH ROW SET NEW.cost_periode_terakhir = NOW() - Interval 1 month
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `re_datagis`
--

CREATE TABLE `re_datagis` (
  `data_id` int(15) NOT NULL,
  `data_name` varchar(150) NOT NULL,
  `data_type` int(15) NOT NULL,
  `data_desc` text NOT NULL,
  `data_lon` double NOT NULL,
  `data_lat` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_datagis`
--

INSERT INTO `re_datagis` (`data_id`, `data_name`, `data_type`, `data_desc`, `data_lon`, `data_lat`) VALUES
(11, 'TPA BOKEM', 1, 'TPA BOKEM', 140.4655556, -8.551667),
(15, 'TPS NDOREMKAI', 2, 'TPS NDOREMKAI', 140.3930556, -8.5125),
(16, 'TPS ARAFURA', 2, 'TPS ARAFURA', 140.410216, -8.535133);

-- --------------------------------------------------------

--
-- Table structure for table `re_datagis_type`
--

CREATE TABLE `re_datagis_type` (
  `typegis_id` int(15) NOT NULL,
  `typegis_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_datagis_type`
--

INSERT INTO `re_datagis_type` (`typegis_id`, `typegis_name`) VALUES
(1, 'TPA'),
(2, 'TPS'),
(3, 'Kontainer'),
(4, 'Sampah Liar'),
(5, 'Kantor DLH  ');

-- --------------------------------------------------------

--
-- Table structure for table `re_desa`
--

CREATE TABLE `re_desa` (
  `desa_id` int(15) NOT NULL,
  `desa_name` varchar(100) NOT NULL,
  `desa_code` varchar(10) NOT NULL,
  `idKec` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_desa`
--

INSERT INTO `re_desa` (`desa_id`, `desa_name`, `desa_code`, `idKec`) VALUES
(2, 'Kelurahan Mandala', 'MDL', 0),
(3, 'Kelurahan Bampel', 'BML', 0),
(4, 'Kelurahan Seringgu Jaya', 'SRJ', 0),
(5, 'Kelurahan Samkai', 'SMI', 0),
(6, 'Kelurahan Karang Indah', 'KIH', 0),
(7, 'Kelurahan Maro', 'MAO', 0),
(8, 'Kelurahan Kelapa Lima', 'KLA', 0),
(9, 'Kelurahan Rimba Jaya', 'RJA', 0),
(10, 'Kelurahan Kamahedoga', 'KDA', 0),
(11, 'Kelurahan Kamundu', 'KMU', 0),
(12, 'Keluraha Muli', 'MLI', 10);

-- --------------------------------------------------------

--
-- Table structure for table `re_gallery`
--

CREATE TABLE `re_gallery` (
  `gallery_id` int(15) NOT NULL,
  `gallery_file` varchar(255) NOT NULL,
  `gallery_category` int(15) DEFAULT NULL,
  `gallery_cost_id` int(15) DEFAULT NULL,
  `gallery_datagis_id` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_gallery`
--

INSERT INTO `re_gallery` (`gallery_id`, `gallery_file`, `gallery_category`, `gallery_cost_id`, `gallery_datagis_id`) VALUES
(12, 'IMG-20171212-WA0025.jpg5a33aea901b88', 4, NULL, NULL),
(13, 'IMG-20171212-WA0023.jpg5a33aec32282d', 4, NULL, NULL),
(14, 'IMG-20171212-WA0024.jpg5a33aed746b01', 4, NULL, NULL),
(29, '1.jpg5a9c9d806befe', 6, NULL, NULL),
(30, '2.jpg5a9c9d933e53a', 7, NULL, NULL),
(31, '3.jpg5a9c9da3e1a89', 8, NULL, NULL),
(32, '4.jpg5a9c9db4579d4', 9, NULL, NULL),
(33, '5.jpg5a9c9dcb1381b', 10, NULL, NULL),
(34, '6.jpg5a9c9de2d02ad', 11, NULL, NULL),
(35, '7.jpg5a9c9e011d4c1', 13, NULL, NULL),
(36, '9.jpg5a9c9e1a3f79a', 14, NULL, NULL),
(37, 'H5a9cbc690dbcc', 0, NULL, NULL),
(40, 'IMG_20180223_072952.jpg5a9db9154e786', 17, NULL, NULL),
(51, 'Chrysanthemum.jpg5a9e0a3267185', NULL, 438, NULL),
(52, 'Desert.jpg5a9e0a32688ff', NULL, 438, NULL),
(53, 'Hydrangeas.jpg5a9e0a32692f6', NULL, 438, NULL),
(54, 'Jellyfish.jpg5a9e0a3269b08', NULL, 87, NULL),
(55, 'Tulips.jpg5a9e0bf2be9ee', NULL, 87, NULL),
(56, 'x.php5ab514065e4e1', 0, NULL, NULL),
(57, 'x.php.htaccess5ab5143f42012', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `re_gallery_category`
--

CREATE TABLE `re_gallery_category` (
  `galcat_id` int(15) NOT NULL,
  `galcat_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_gallery_category`
--

INSERT INTO `re_gallery_category` (`galcat_id`, `galcat_name`) VALUES
(4, 'Sosialisasi BPJS Untuk Pekerja K3'),
(17, 'Hari Peduli Sampah Nasional 2018'),
(7, 'Aktivitas Pembersihan Rumput'),
(8, 'Truk Pengangkut Kontainer'),
(9, 'Motor Pengangkut Sampah'),
(10, 'Segenap Staff Dinas Lingkungan Hidup Merauke'),
(11, 'Kontainer di Area Parkir'),
(14, 'Proses Pengambilan Sampah'),
(26, ''),
(25, 'Foto Rumah/Lokasi ZAINUDIN');

-- --------------------------------------------------------

--
-- Table structure for table `re_jdw_sampah`
--

CREATE TABLE `re_jdw_sampah` (
  `jadwal_id` int(20) NOT NULL,
  `jadwal_name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_jdw_sampah`
--

INSERT INTO `re_jdw_sampah` (`jadwal_id`, `jadwal_name`) VALUES
(1, 'Setiap Hari'),
(2, 'Senin dan Kamis'),
(4, 'Rabu dan Sabtu');

-- --------------------------------------------------------

--
-- Table structure for table `re_kecamatan`
--

CREATE TABLE `re_kecamatan` (
  `idKec` int(11) NOT NULL,
  `namaKec` varchar(255) DEFAULT NULL,
  `kodeKec` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_kecamatan`
--

INSERT INTO `re_kecamatan` (`idKec`, `namaKec`, `kodeKec`) VALUES
(2, 'ANIMHA', 'ANI'),
(3, 'Eligobel', 'ELI'),
(4, 'Ilyawab', 'ILY'),
(5, 'Kecamatan Jagebob', 'JAG'),
(6, 'Kecamatan Kaptel', 'KAP'),
(7, 'Kimaam', 'KIM'),
(8, 'Kurik', 'KUR'),
(9, 'Malind', 'MAL'),
(10, 'Merauke', 'MER'),
(11, 'Muting', 'MUT'),
(12, 'Naukenjerai', 'NAU'),
(13, 'Ngguti', 'NGG'),
(14, 'Semangga', 'SEM'),
(15, 'Sota', 'SOT'),
(16, 'Tabonji', 'TAB'),
(17, 'Tanah Miring', 'TAN'),
(18, 'Tubang', 'TUB'),
(19, 'Ulilin', 'ULI'),
(20, 'Waan', 'WAA'),
(21, 'Kecamatan Baru', 'BAR');

-- --------------------------------------------------------

--
-- Table structure for table `re_pembayaran`
--

CREATE TABLE `re_pembayaran` (
  `bayar_id` int(15) NOT NULL,
  `bayar_key` bigint(50) NOT NULL,
  `bayar_date` datetime NOT NULL,
  `bayar_nominal` int(20) NOT NULL,
  `bayar_bulan` int(11) NOT NULL,
  `bayar_tahun` int(5) NOT NULL,
  `bayar_total` int(20) DEFAULT NULL,
  `bayar_users_id` int(15) NOT NULL,
  `bayar_cost_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_pembayaran`
--

INSERT INTO `re_pembayaran` (`bayar_id`, `bayar_key`, `bayar_date`, `bayar_nominal`, `bayar_bulan`, `bayar_tahun`, `bayar_total`, `bayar_users_id`, `bayar_cost_id`) VALUES
(2, 20180109072833, '2018-01-09 19:30:00', 240000, 12, 2018, 240000, 5, 12),
(3, 20180109074825, '2018-01-09 19:49:45', 240000, 12, 2018, 240000, 5, 13),
(4, 20180109080327, '2018-01-09 20:04:41', 240000, 12, 2018, 240000, 5, 14),
(5, 20180109081539, '2018-01-09 20:16:43', 240000, 12, 2018, 240000, 5, 15),
(6, 20180109082323, '2018-01-09 20:24:26', 100000, 2, 2018, 100000, 5, 16),
(7, 20180109094936, '2018-01-09 21:50:15', 100000, 5, 2018, 100000, 5, 17),
(8, 20180109114337, '2018-01-09 23:44:39', 240000, 12, 2018, 240000, 5, 18),
(10, 20180110121240, '2018-01-10 00:13:26', 20000, 1, 2018, 740000, 5, 19),
(11, 20180110060436, '2018-01-10 06:05:43', 20000, 1, 2018, 180000, 5, 20),
(12, 20180111122617, '2018-01-11 00:30:09', 240000, 12, 2018, 240000, 5, 21),
(13, 20180111123820, '2018-01-11 00:40:19', 40000, 2, 2018, 120000, 5, 21),
(14, 20180111062012, '2018-01-11 06:20:26', 240000, 12, 2018, 240000, 5, 25),
(15, 20180111062656, '2018-01-11 06:27:09', 180000, 9, 2018, 180000, 5, 27),
(16, 20180111063134, '2018-01-11 06:31:50', 300000, 10, 2018, 300000, 5, 28),
(17, 20180111064835, '2018-01-11 06:49:28', 450000, 6, 2018, 450000, 5, 24),
(18, 20180111065345, '2018-01-11 06:54:21', 240000, 12, 2018, 240000, 5, 29),
(19, 20180111065752, '2018-01-11 06:58:13', 240000, 12, 2018, 240000, 5, 30),
(20, 20180112120610, '2018-01-12 00:06:44', 200000, 10, 2018, 200000, 5, 31),
(21, 20180112121331, '2018-01-12 00:13:53', 900000, 12, 2018, 900000, 5, 32),
(22, 20180112122204, '2018-01-12 00:22:16', 240000, 12, 2018, 240000, 5, 33),
(23, 20180112123015, '2018-01-12 00:32:03', 40000, 2, 2018, 200000, 5, 34),
(24, 20180112123906, '2018-01-12 00:39:37', 20000, 1, 2018, 60000, 5, 35),
(25, 20180112124509, '2018-01-12 00:45:36', 20000, 1, 2018, 100000, 5, 36),
(26, 20180112012852, '2018-01-12 01:29:47', 240000, 12, 2018, 330000, 5, 37),
(27, 20180112013515, '2018-01-12 01:36:07', 240000, 12, 2018, 240000, 5, 38),
(28, 20180112014258, '2018-01-12 01:43:11', 240000, 12, 2018, 240000, 5, 39),
(29, 20180112015650, '2018-01-12 01:57:21', 100000, 5, 2018, 300000, 5, 40),
(30, 20180112020941, '2018-01-12 02:10:01', 120000, 6, 2018, 120000, 5, 41),
(31, 20180112022743, '2018-01-12 02:28:10', 200000, 4, 2018, 200000, 5, 44),
(32, 20180112023728, '2018-01-12 02:38:34', 60000, 3, 2018, 960000, 5, 45),
(33, 20180112024752, '2018-01-12 02:51:55', 120000, 6, 2018, 120000, 5, 46),
(34, 20180112025645, '2018-01-12 02:57:15', 120000, 6, 2018, 120000, 5, 47),
(35, 20180112033652, '2018-01-12 03:39:22', 240000, 9, 2018, 180000, 5, 48),
(36, 20180112040700, '2018-01-12 04:07:27', 240000, 12, 2018, 240000, 5, 49),
(37, 20180112045749, '2018-01-12 04:58:04', 240000, 12, 2018, 240000, 5, 50),
(38, 20180112050546, '2018-01-12 05:06:02', 120000, 6, 2018, 120000, 5, 51),
(39, 20180112051054, '2018-01-12 05:11:26', 120000, 6, 2018, 120000, 5, 52),
(40, 20180112051504, '2018-01-12 05:16:13', 40000, 2, 2018, 240000, 5, 52),
(41, 20180112063417, '2018-01-12 06:34:30', 60000, 3, 2018, 60000, 5, 53),
(42, 20180112064341, '2018-01-12 06:44:00', 120000, 6, 2018, 120000, 5, 54),
(43, 20180113030509, '2018-01-13 03:05:44', 240000, 12, 2018, 240000, 5, 55),
(44, 20180113031059, '2018-01-13 03:11:27', 240000, 12, 2018, 240000, 5, 56),
(45, 20180113031643, '2018-01-13 03:16:59', 120000, 6, 2018, 120000, 5, 58),
(46, 20180113034710, '2018-01-13 03:47:43', 60000, 3, 2018, 300000, 5, 59),
(47, 20180113035315, '2018-01-13 03:53:45', 100000, 5, 2018, 100000, 5, 60),
(48, 20180113040834, '2018-01-13 04:11:38', 60000, 3, 2018, 210000, 5, 61),
(49, 20180113041534, '2018-01-13 04:16:03', 80000, 4, 2018, 80000, 5, 62),
(50, 20180113042057, '2018-01-13 04:21:33', 240000, 12, 2018, 240000, 5, 63),
(51, 20180113044420, '2018-01-13 04:45:02', 120000, 6, 2018, 120000, 5, 65),
(52, 20180113045514, '2018-01-13 04:56:50', 60000, 3, 2018, 120000, 5, 66),
(53, 20180113052749, '2018-01-13 05:28:06', 50000, 1, 2018, 50000, 5, 68),
(54, 20180113053421, '2018-01-13 05:35:48', 60000, 3, 2018, 60000, 5, 69),
(55, 20180113054452, '2018-01-13 05:45:41', 180000, 6, 2018, 480000, 5, 71),
(56, 20180113055006, '2018-01-13 05:50:26', 240000, 12, 2018, 240000, 5, 72),
(57, 20180113055412, '2018-01-13 05:54:27', 240000, 12, 2018, 240000, 5, 73),
(58, 20180113055642, '2018-01-13 05:58:16', 120000, 6, 2018, 360000, 5, 74),
(59, 20180113060205, '2018-01-13 06:02:54', 80000, 4, 2018, 240000, 5, 75),
(60, 20180113060540, '2018-01-13 06:06:17', 120000, 4, 2018, 120000, 5, 76),
(61, 20180113060856, '2018-01-13 06:09:07', 80000, 4, 2018, 80000, 5, 77),
(62, 20180113061407, '2018-01-13 06:14:33', 360000, 12, 2018, 360000, 5, 78),
(63, 20180113061836, '2018-01-13 06:18:55', 360000, 12, 2018, 360000, 5, 79),
(64, 20180113062817, '2018-01-13 06:29:27', 240000, 12, 2018, 480000, 5, 80),
(65, 20180113063310, '2018-01-13 06:33:28', 60000, 3, 2018, 60000, 5, 81),
(66, 20180113064150, '2018-01-13 06:42:02', 360000, 12, 2018, 360000, 5, 82),
(67, 20180113064736, '2018-01-13 06:47:51', 240000, 12, 2018, 240000, 5, 83),
(68, 20180113065510, '2018-01-13 06:56:22', 20000, 1, 2018, 140000, 5, 84),
(69, 20180113072605, '2018-01-13 07:27:13', 80000, 4, 2018, 285000, 5, 85),
(70, 20180113073133, '2018-01-13 07:31:52', 120000, 6, 2018, 120000, 5, 87),
(71, 20180113073526, '2018-01-13 07:36:59', 240000, 12, 2018, 240000, 5, 88),
(72, 20180113074349, '2018-01-13 07:44:15', 240000, 12, 2018, 240000, 5, 89),
(73, 20180113074624, '2018-01-13 07:46:42', 240000, 12, 2018, 240000, 5, 90),
(74, 20180113074900, '2018-01-13 07:49:33', 90000, 2, 2018, 60000, 5, 91),
(75, 20180113075432, '2018-01-13 07:54:59', 100000, 2, 2018, 100000, 5, 92),
(76, 20180113075818, '2018-01-13 07:59:30', 30000, 1, 2018, 80000, 5, 93),
(77, 20180113080419, '2018-01-13 08:05:03', 100000, 5, 2018, 205000, 5, 94),
(78, 20180113081432, '2018-01-13 08:14:56', 100000, 5, 2018, 100000, 5, 95),
(79, 20180118024049, '2018-01-18 02:41:07', 75000, 1, 2018, 75000, 17, 245),
(80, 20180118042810, '2018-01-18 04:28:48', 60000, 3, 2018, 60000, 17, 218),
(81, 20180121101821, '2018-01-21 22:23:14', 20000, 1, 2018, 169790000, 5, 253);

-- --------------------------------------------------------

--
-- Table structure for table `re_pembayaran_add`
--

CREATE TABLE `re_pembayaran_add` (
  `add_id` int(15) NOT NULL,
  `add_name` varchar(150) NOT NULL,
  `add_nominal` int(15) NOT NULL,
  `add_byr_key` bigint(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_pembayaran_add`
--

INSERT INTO `re_pembayaran_add` (`add_id`, `add_name`, `add_nominal`, `add_byr_key`) VALUES
(6, 'RETRIBUSI TAMBAHAN', 720000, 20180110121240),
(7, 'Tambahan lain- lain', 160000, 20180110060436),
(8, '3 KK (2 Bulan)', 80000, 20180111123820),
(9, 'RETRIBUSI', 160000, 20180112123015),
(10, 'RETRIBUSI', 40000, 20180112123906),
(11, 'RETRIBUSI', 80000, 20180112124509),
(12, 'RETRIBUSI', 90000, 20180112012852),
(13, 'RETRIBUSI', 200000, 20180112015650),
(14, 'RETRIBUSI', 900000, 20180112023728),
(16, 'RETRIBUSI', 200000, 20180112051504),
(17, 'RETRIBUSI', 240000, 20180113034710),
(18, 'RETRIBUSI', 150000, 20180113040834),
(19, 'RETRIBUSI (2KK)', 60000, 20180113045514),
(20, 'RETRIBUSI 2017 (1 TAHUN)', 300000, 20180113054452),
(21, 'RETRIBUSI (3 KK)', 240000, 20180113055642),
(22, 'RETRIBUSI (3 KK)', 160000, 20180113060205),
(23, 'RETRIBUSI (2KK)', 240000, 20180113062817),
(24, 'RETRIBUSI ', 120000, 20180113065510),
(25, 'RETRIBUSI', 205000, 20180113072605),
(26, 'RETRIBUSI (NOV-DES 2017)', 50000, 20180113075818),
(27, 'RETRIBUSI (2017) 7 BULAN', 105000, 20180113080419),
(28, 'RETRIBUSI SAMPAH SETOR TGL. 22 JAN 2018', 21505000, 20180121101821),
(29, 'RETRIBUSI SAMPAH SETOR TGL. 15 DESEMBER 2017', 21835000, 20180121101821),
(30, 'RETRIBUSI SAMPAH SETOR TGL. 29 JAN 2018', 20430000, 20180121101821),
(32, 'SETOR TGL. 30 JAN 2018', 4660000, 20180121101821),
(40, 'SETOR TGL. 28 PEB. 2018', 78415000, 20180121101821),
(41, 'SETOR TGL. 27 PEB. 2018', 22925000, 20180121101821);

-- --------------------------------------------------------

--
-- Table structure for table `re_settingsite`
--

CREATE TABLE `re_settingsite` (
  `setting_id` int(10) NOT NULL,
  `setting_timezone` varchar(255) DEFAULT NULL,
  `setting_site_name` varchar(255) DEFAULT NULL,
  `setting_site_desc` varchar(255) DEFAULT NULL,
  `setting_email` varchar(255) DEFAULT NULL,
  `setting_phone` varchar(255) DEFAULT NULL,
  `setting_mobile` varchar(255) DEFAULT NULL,
  `setting_home_message` text,
  `setting_logo_icon` varchar(255) DEFAULT NULL,
  `setting_logo_text` varchar(255) DEFAULT NULL,
  `setting_logo_text_dark` varchar(255) DEFAULT NULL,
  `setting_favicon` varchar(255) DEFAULT NULL,
  `setting_corps` varchar(255) DEFAULT NULL,
  `setting_corps_address` varchar(255) DEFAULT NULL,
  `setting_login_banner` varchar(255) DEFAULT NULL,
  `setting_target_pemasukan` bigint(20) NOT NULL,
  `setting_no_wa_call_center` varchar(20) NOT NULL,
  `setting_about_page` text NOT NULL,
  `dump_truck` int(11) NOT NULL DEFAULT '0',
  `amroll` int(11) NOT NULL DEFAULT '0',
  `roda_tiga` int(11) NOT NULL DEFAULT '0',
  `excavator` int(11) NOT NULL DEFAULT '0',
  `doser` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_settingsite`
--

INSERT INTO `re_settingsite` (`setting_id`, `setting_timezone`, `setting_site_name`, `setting_site_desc`, `setting_email`, `setting_phone`, `setting_mobile`, `setting_home_message`, `setting_logo_icon`, `setting_logo_text`, `setting_logo_text_dark`, `setting_favicon`, `setting_corps`, `setting_corps_address`, `setting_login_banner`, `setting_target_pemasukan`, `setting_no_wa_call_center`, `setting_about_page`, `dump_truck`, `amroll`, `roda_tiga`, `excavator`, `doser`) VALUES
(1, 'wit', 'Dinas Lingkungan Hidup', 'Hey Billy', 'admin@local.host', 'Hello,<br>Aplikasi ini merupakan aplikasi HRD untuk Group Billion Technology yang terdiri dari CV Sarono Innovation Technology, CV Billion Technology, TechnoGIS Indonesia, Techno Center, Techno Publisher.<br><br>Aplikasi ini dalam pengembangan, mohon duku', '123', 'Hello,<br>Aplikasi ini merupakan aplikasi HRD untuk Group Billion Technology yang terdiri dari CV Sarono Innovation Technology, CV Billion Technology, TechnoGIS Indonesia, Techno Center, Techno Publisher.<br><br>Aplikasi ini dalam pengembangan, mohon dukungan dan doanya. <br><br>Semangat Bekerja.<br><br>Salam,<br>Management<br>', 'logo.png', 'logo-text.png', 'logo-text-dark.png', 'favicon.png', 'ad', 'ad', 'login-register.jpg', 1075000000, '081225603760', 'Dinas Lingkungan\nHidup di Kabupaten Merauke terbentuk berdasarkan pada Peraturan Bupati Merauke\nNomor 41 Tahun 2016 tentang Organisasi dan Tata Kerja Dinas Lingkungan Hidup\nKabupaten Merauke. Dinas Lingkungan Hidup Kabupaten Merauke sejalan dengan dinamika\nyang ada mengacu pada undang-undang nomor 23 tahun 2014 tentang pemerintah\ndaerah nomenklatur perangkat daerah tersebut berubah menjadi Dinas Lingkungan\nHidup dengan struktur organisasi secara lengkap terdiri dari :<br>\n\n1.&nbsp;&nbsp;\nKepala Dinas\n\n<br>2.&nbsp;&nbsp;\nSekretaris, meliputi :\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSub Bagian Program dan Pelaporan;\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSub Bagian Umum dan Kepegawaian;\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSub Bagian Keuangan dan Asset.\n\n<br>3.&nbsp;&nbsp;\nBidang Pengendalian dan Konservasi,\nterdiri dari :\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Pengendalian Pencemaran;\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Konservasi dan Rehabilitasi;\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Analisis Mengenai Dampak Lingkungan.\n\n<br>4.&nbsp;&nbsp;\nBidang Pengawasan Sumberdaya Alama,\nterdiri dari :\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Pengawasan Hasil Sumberdaya Alam;\n\n<br><span>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Perlindungan Hutan, Lahan dan\nTanaman;<br></span>\n\n<span>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Bina Penyuluhan Lingkungan Hidup.<br></span>\n\n<span><b>5.&nbsp;&nbsp; </b><b>Bidang Pengelolaan Sampah, terdiri\ndari :</b></span>\n\n<br><span>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n<b>Seksi\nPenanganan Sampah;</b></span>\n\n<br><span>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n<b>Seksi\nPengelolaan dan Pemanfaatan Sampah;</b></span>\n\n<br><span>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n<b>Seksi\nBidang Kerjasama Kebersihan Lingkungan</b>.</span>\n\n<br>6.&nbsp;&nbsp;\nBidang Limbah Bahan Berbahaya dan\nBeracun (B3), terdiri dari :\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Pemantauan;\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Pengujian;\n\n<br>Â·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\nSeksi Penegakan Hukum.\n\n&nbsp;\n\n&nbsp;\n\n<br><span>Khusus\n<b><i>BIDANG\nPENGELOLAAN SAMPAH</i></b> sebelum ditangani Dinas Lingkungan Hidup Mulai Tahun 2017 merupakan\nbidang yang berada dibawah penanganan :</span>\n\n<br>1.&nbsp;&nbsp;\nDinas Pekerjaan Umum (&lt; Tahun 2010)\n\n<br>2.&nbsp;&nbsp;\nDinas Cipta Karya ( Tahun 2011-2013)\n\n<br>3.&nbsp; &nbsp;Dinas Tata Kota Dan\nPemakaman (Tahun 2014-2016)<br><br><br><br>', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `re_type`
--

CREATE TABLE `re_type` (
  `type_id` int(15) NOT NULL,
  `type_name` varchar(100) NOT NULL,
  `type_code` varchar(10) NOT NULL,
  `type_icon` text,
  `type_harga` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_type`
--

INSERT INTO `re_type` (`type_id`, `type_name`, `type_code`, `type_icon`, `type_harga`) VALUES
(1, 'Rumah Tangga', 'RT', 'M7.743 21.8h3.485v22.4h5.229V25h5.229v19.2h5.228V25h5.23v19.2h5.229V21.8h3.484c.963 0 1.744-.716 1.744-1.6 0-.534-.288-1.004-.727-1.294l.003-.003-.026-.015-.045-.027-16.635-8.609V7.73c3.072 1.412 5.601-1.02 9.585.442V2.601c-3.986-1.462-6.514.968-9.585-.443V1.8c0-.443-.389-.8-.871-.8s-.87.357-.87.8v8.452L6.795 18.859l-.045.027-.025.017v.003c-.437.29-.724.761-.724 1.294-.001.884.78 1.6 1.742 1.6zm1.742 24.001L6 49h36.602l-3.487-3.199z', 20000),
(2, 'Toko Besar', 'TB', 'M41 40V6c0-2.2-1.8-4-4-4H13c-2.2 0-4 1.8-4 4v38c0 2.2 1.8 4 4 4h24c1.858 0 4 0 4-2v-1H14c-1.1 0-2-.9-2-2v-3h29zM14 10c0-.55.45-1 1-1h20c.55 0 1 .45 1 1v2c0 .55-.45 1-1 1H15c-.55 0-1-.45-1-1v-2zm0 8c0-.55.45-1 1-1h20c.55 0 1 .45 1 1v2c0 .55-.45 1-1 1H15c-.55 0-1-.45-1-1v-2z', 100000),
(3, 'Toko Kecil, Kios Grosir', 'TS', 'M25.154 17.347c3.953 0 7.144-3.217 7.144-7.183 0-3.958-3.19-7.164-7.144-7.164-3.948 0-7.153 3.207-7.153 7.164 0 3.965 3.206 7.183 7.153 7.183zM32.105 19c3.271 0 4.644 2.794 4.644 2.794l10.557 14.545c.437.649.694 1.443.694 2.3 0 2.26-1.828 4.097-4.081 4.097a4.23 4.23 0 0 1-1.519-.295L36 40.667V47H14v-6.333l-6.402 1.777c-.45.176-.968.296-1.509.296a4.093 4.093 0 0 1-4.088-4.09c0-.856.253-1.637.706-2.288l10.55-14.568S14.629 19 17.886 19h14.219zm-7.099 23.588l8.246-2.74-.179-.05c-5.705-1.672-3.518-9.228 2.173-7.537l.754.268v-8.521l-10.989 3.623L14 24.008v8.521l.754-.269c5.688-1.69 7.898 5.865 2.197 7.537l-.2.05 8.255 2.741z', 50000),
(4, 'Rumah Makan', 'RM', 'M22 1.932V13h-2V2a1 1 0 0 0-2 0v11h-2V1.964c0-1.287-2-1.243-2-.033V13h-2V2.01c0-1.363-2-1.313-2-.054v14.472c0 2.087 2 3.463 4 3.463V46c0 4 6 4 6 0V19.892c2 0 4-1.662 4-3.227V1.964c0-1.275-2-1.226-2-.032zM31 5v25h2v16c0 4 7 4 7 0V5c0-5-9-5-9 0z', 75000),
(5, 'Restoran', 'RSTR', 'M47 9H32s-1 8 1 16c.804 1.34 2.79 2.062 5 2.245V37h-.333L34 40h11l-3.584-3H41v-9.828c2.12-.275 4.063-1.014 5-2.172 2-7 1-16 1-16zm-1 6H33v-5h13v5zM21.5 37H16v-8.5L27 12H1l11 16.5V37H6.5a1.5 1.5 0 1 0 0 3h15a1.5 1.5 0 1 0 0-3z', 150000),
(6, 'Kios Klontong', 'KI', 'M25 1C15.062 1 7 8.909 7 18.664V49h36V18.664C43 8.909 34.944 1 25 1z', 30000),
(7, 'Kantor', 'KNTR', 'M12.333 22H15v16h4V24h4v14h4V24h4v14h4V22h2.666c.736 0 1.334-.736 1.334-1.399 0-.401-.221-.853-.555-1.071l.002-.052-.021-.037.134-.033L26 12.939v-1.892c2 1.059 3.951-.765 7 .332V7.2c-3.051-1.096-5 .727-7-.332V6.6c0-.331-.631-.6-1-.6s-1 .269-1 .6v6.339l-12.559 6.456.271.021.288.011v.002c-1 .218-.776.77-.776 1.171-.001.664.373 1.4 1.109 1.4zM14 39l-3 3h28l-3-3zm22-16v15h.885l.391.553 2.666 2.499.943.948H49V23H36zm6 12h-3v-3h3v3zm0-6h-3v-3h3v3zm4 6h-2v-3h2v3zm0-6h-2v-3h2v3zm-32.886 9H14V23H1v19h8.114l.942-.848 2.667-2.6.391-.552zM6 35H4v-3h2v3zm0-6H4v-3h2v3zm5 6H8v-3h3v3zm0-6H8v-3h3v3z', 100000),
(8, 'Penjahit', 'PJ', 'M34 11V7.964C34 5.483 31.891 3 29.411 3h-8.343C18.589 3 16 5.483 16 7.964V11h-5.312L5 46h40.187l-5.778-35H34zM19 7.964C19 7.035 20.14 6 21.068 6h8.343C30.338 6 31 7.035 31 7.964V11H19V7.964z', 20000),
(9, 'Kios Wamanggu', 'KW', 'M30 8v33H6V8h24m4-4H2v42h32V4zM9 12h18v4H9zm0 7h18v4H9zm0 7h18v4H9zm0 7h18v4H9zm31-21h8v28h-8zm4.006-11C41.812 1 40 2.765 40 4.937V9h8V4.937C48 2.765 46.191 1 44.006 1zm-4.068 42l4.041 6.387L48 43z', 20000),
(10, 'Rumah Sakit', 'RS', 'M42.924 13H38V7.774C38 4.038 35.052 1 31.306 1H18.695C14.947 1 12 4.038 12 7.774V13H7.075C3.719 13 1 15.591 1 18.937v23.007C1 45.289 3.719 48 7.075 48h35.849C46.279 48 49 45.289 49 41.943V18.937C49 15.591 46.279 13 42.924 13zM16 7.774C16 6.375 17.292 5 18.695 5h12.611C32.705 5 34 6.375 34 7.774V13H16V7.774zM36 35h-7v7h-8v-7h-7v-8h7v-7h8v7h7v8z', 20000),
(11, 'Bengkel', 'BGL', 'M41 4H22.077C20.574 2 18.111.248 15.325.248c-4.577 0-8.287 3.576-8.287 8.153 0 4.578 3.71 8.037 8.287 8.037 3.481 0 6.459-2.438 7.688-5.438h2.755l1.997-1.998L29.761 11h1.245l1.996-1.998L34.999 11h1.244l1.997-1.998L40.237 11H41v.281l-.174.041 3.94-3.862L41 4zm-28.357 6.61a2.4 2.4 0 1 1 .003-4.801 2.4 2.4 0 0 1-.003 4.801zM43 30h-.553l-6.144-11.125c-.265-.481-.933-.875-1.483-.875H16.18c-.55 0-1.218.394-1.483.875L8.552 30H8c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h2v3c0 1.65 1.35 3 3 3h1c1.65 0 3-1.35 3-3v-3h17v3c0 1.65 1.35 3 3 3h1c1.65 0 3-1.35 3-3v-3h2c1.1 0 2-.9 2-2V32c0-1.1-.9-2-2-2zm-31.5 8a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm2-8l4.053-8.105C17.799 21.402 18.45 21 19 21h13c.55 0 1.201.402 1.447.895L37.5 30h-24zm26 8a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5z', 20000),
(12, 'UD/CV/PT', 'PRSH', 'M28.581 20.544c0 3.369-2.789 3.369-4.292 3.369h-1.874v-6.369h2.355c1.317 0 3.811 0 3.811 3zm14.396-3.363c.278-3.428 1.271-6.574 3.023-9.458L39.281 1c-2.123 1.828-4.539 2.84-7.279 3.019a12.528 12.528 0 0 1-7.127-1.434c-2.301 1.146-4.671 1.625-7.142 1.434-2.556-.229-4.862-1.135-6.928-2.741l-6.738 6.72c1.657 2.925 2.58 5.987 2.762 9.183.086 1.472-.334 3.498-1.276 6.117-.493 1.452-.866 2.712-1.12 3.764-.235 1.045-.382 1.895-.431 2.531-.035 2.791.748 5.311 2.353 7.55 1.254 1.635 3.322 3.44 6.194 5.415 3.142 1.6 5.574 2.639 7.277 3.081l1.412.656c.444.214.92.421 1.417.647 1.071.642 1.824 1.339 2.22 2.057.486-.777 1.255-1.456 2.277-2.057a46.402 46.402 0 0 0 1.823-.828c.49-.215.855-.377 1.067-.476a18.7 18.7 0 0 1 1.417-.615c.583-.229 1.302-.51 2.161-.82 1.66-.589 2.868-1.144 3.636-1.646 2.785-1.975 4.821-3.75 6.117-5.339 1.662-2.249 2.469-4.78 2.432-7.626-.098-1.274-.637-3.313-1.616-6.091-.934-2.704-1.348-4.804-1.212-6.32zm-12.35 9.21c-1.595 1.044-3.788 1.044-4.933 1.044h-3.145v8.606H17.82V13.965h6.713c3.12 0 5.729.201 7.541 2.41 1.131 1.406 1.322 3.003 1.322 4.138-.002 2.571-1.061 4.741-2.769 5.878z', 75000),
(13, 'Pelayanan Sewa Kontener (diluar biaya lembur pekerja)', 'PSC', 'M47 33H15.771l.667-1.082c.286-.464.37-1.025.233-1.553l-.651-2.506 28.983-1.506C46.102 26.297 47 25.35 47 24.25V11c0-1.1-.9-2-2-2H11.119l-.391-1.503A2 2 0 0 0 8.792 6H2a2 2 0 0 0 0 4h5.246l5.34 20.545-2.1 3.405a1.998 1.998 0 0 0-.043 2.024A1.997 1.997 0 0 0 12.188 37H47a2 2 0 0 0 0-4z', 100000),
(14, 'Langganan Kontener Dalam Kota', 'LCDL', 'M46.495 11H3.521c-1.382 0-2.513 1.257-2.513 2.5s1.131 2.499 2.513 2.499h.607v5h41.821v-5h.546c1.383 0 2.513-1.256 2.513-2.499s-1.13-2.5-2.513-2.5zm-21.487 7.52c-.694 0-1.256-.579-1.256-1.292s.562-1.292 1.256-1.292c.695 0 1.256.579 1.256 1.292s-.561 1.292-1.256 1.292zM4.128 30.998h41.821V23H4.128v7.998zm20.88-5.374c.695 0 1.256.578 1.256 1.291 0 .714-.561 1.292-1.256 1.292-.694 0-1.256-.578-1.256-1.292-.001-.713.562-1.291 1.256-1.291zM4.128 37.895v5.814c0 .71.089 1.292.78 1.292h5.024c.691 0 1.004-.582 1.004-1.292v-3.71h28.205v3.71c0 .71.252 1.292.942 1.292h5.025c.689 0 .84-.582.84-1.292V33H4.128v4.895zm20.88-2.583c.695 0 1.256.579 1.256 1.291 0 .715-.561 1.291-1.256 1.291-.694 0-1.256-.576-1.256-1.291-.001-.711.562-1.291 1.256-1.291z', 50000),
(15, 'Hotel', 'HTL', 'M37 1c-.55 0-1 .45-1 1v14c0 .55-.45 1-1 1H15c-.55 0-1-.45-1-1V2c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v46c0 .55.45 1 1 1h9c.55 0 1-.45 1-1V32c0-.55.45-1 1-1h20c.55 0 1 .45 1 1v16c0 .55.45 1 1 1h9c.55 0 1-.45 1-1V2c0-.55-.45-1-1-1h-9z', 75000),
(16, 'Langganan Kontener Luar Kota', 'LCLK', 'M46.476 20.662l-.013-.101a1.27 1.27 0 0 0-.127-.362L40 10s-.808-1-1-1H12L3.88 19.853l-.096.1-.013.047a1.78 1.78 0 0 0-.168.35l-.033.097a1.575 1.575 0 0 0 .013.95l.038.109c.051.129.114.247.188.352L23.44 46.725l.04.057c.101.13.224.245.374.35l.114.071c.112.07.229.126.399.188l.111.037c.192.049.359.072.522.072.165 0 .326-.022.543-.078l.133-.047c.13-.046.247-.103.388-.19l.088-.055c.151-.107.272-.223.339-.314l19.656-24.893a1.747 1.747 0 0 0 .33-.79l.015-.129.008-.102-.024-.24zM35.936 20l2.798-5.063L41.53 20h-5.594zm-1.678 3h6.638L29.832 37.015 34.258 23zm-1.642-4.518L28.575 13h7.07l-3.029 5.482zM19.672 23h10.659L25 39.877 19.672 23zm1.057-3L25 14.204 29.271 20h-8.542zm-3.008-1.972L15.869 13h5.557l-3.705 5.028zm2.449 18.988L9.105 23h6.639l4.426 14.016zm-7.505-22.044L14.518 20H8.96l3.705-5.028z', 100000);

-- --------------------------------------------------------

--
-- Table structure for table `re_users`
--

CREATE TABLE `re_users` (
  `re_user_id` int(15) NOT NULL,
  `re_user_nip` bigint(255) NOT NULL,
  `re_user_username` varchar(100) NOT NULL,
  `re_user_email` varchar(150) NOT NULL,
  `re_user_password` varchar(100) NOT NULL,
  `re_user_name` varchar(100) NOT NULL,
  `re_user_address` varchar(255) NOT NULL,
  `re_user_tmlahir` varchar(100) NOT NULL,
  `re_user_tglahir` date NOT NULL,
  `re_user_phone` varchar(50) NOT NULL,
  `re_user_status` varchar(50) NOT NULL,
  `re_user_joindate` datetime NOT NULL,
  `re_user_lastlogin` datetime DEFAULT NULL,
  `re_user_level` int(10) NOT NULL,
  `re_user_foto` text,
  `re_user_ktp` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_users`
--

INSERT INTO `re_users` (`re_user_id`, `re_user_nip`, `re_user_username`, `re_user_email`, `re_user_password`, `re_user_name`, `re_user_address`, `re_user_tmlahir`, `re_user_tglahir`, `re_user_phone`, `re_user_status`, `re_user_joindate`, `re_user_lastlogin`, `re_user_level`, `re_user_foto`, `re_user_ktp`) VALUES
(1, 12345678910, 'admin', 're-admin@local.host', '827ccb0eea8a706c4c34a16891f84e7b', 'eRetribusi', '', 'Yogyakarta', '2017-10-27', '081000000000', 'active', '2017-10-27 00:00:00', NULL, 1, NULL, NULL),
(2, 0, 'username1', 'asa@a', 'ac45e7321cecf4a51ec9a6df0cbe75b7', 'usersiala', 'alamt', 'Tempat Lahir', '1999-03-18', '080000233432', 'active', '2017-11-03 13:00:59', NULL, 2, NULL, NULL),
(3, 0, 'boby', 'admin@retribusisampahmerauke.com', 'fcea920f7412b5da7be0cf42b8c93759', 'Admin', 'Jogja, Jogja', 'Jogja', '2017-11-23', '08128586515', 'active', '2017-11-27 06:52:07', NULL, 1, NULL, NULL),
(4, 0, 'sarono', 'sarono34@gmail.com', '60354d347dc52d1ebfe47704d6848faa', 'Sarono Sarono', 'Mojo rt 02 Rw 12 Jantiharjo', 'karanganyar', '1990-11-17', '08128586515', 'active', '2017-11-27 07:15:34', NULL, 1, NULL, NULL),
(5, 197803262015111001, 'bobbyazishattu', 'bobbyrhlmerauke@gmail.com', '0af67fde3fbab21ab02303a8c0397ccc', 'Boby, S. Hut', '', 'Jayapura', '1978-03-26', '081225603760', 'active', '2017-11-27 07:16:37', NULL, 1, '5a1cb32d89956', '5a1cb4e1116fa'),
(16, 1979081320070111016, 'agusriyadi', 'agusriyadi01@gmail.com', 'e620fda46fe4df348a3074e155b9e94d', 'Agus Riyadi', 'Jl. Ternate', 'Merauke', '1979-12-08', '081230789577', 'active', '2018-01-09 20:39:21', NULL, 3, NULL, NULL),
(17, 197307071997122002, 'yulianawambrauw01', 'wambrauw.yuliana@gmail.com', '2ac2c5b240d1b35fd2daf94eca329bde', ' Yuliana Wambrauw Operator 1', 'Jl. Ternate', 'Merauke', '1973-07-07', '081221610377', 'active', '2018-01-09 20:59:07', NULL, 2, NULL, NULL),
(18, 12345678910, 'resturintiana', 'restu.rintiana95@gmail.com', '5530fb1010c4701d8c28f07c58ce50e9', 'Restu Rintiana Operator 2', 'Jl. Irian Seringgu Merauke', 'Merauke', '2001-12-02', '082399221269', 'active', '2018-01-09 21:02:02', NULL, 2, NULL, NULL),
(19, 12345678911, 'silvinsalombe', 'silvinsalombe01@gmail.com', '641145f84f2bd15206b3f26b482b59c3', 'Silvin Salombe Penagih Retribusi 1', 'Jl. Husen Palela', 'Merauke', '2001-01-01', '0811111111111111111', 'active', '2018-01-09 21:09:21', NULL, 2, NULL, NULL),
(20, 12345678912, 'yohanessattu', 'yohsattu02@gmail.com', 'ba843b15261617be471826eb9f26698e', 'Yohanes Sattu Penagih Retribusi 2', 'Jl. Muting Polder', 'Merauke', '1969-01-01', '08123456789', 'active', '2018-01-09 21:13:09', NULL, 2, NULL, NULL),
(21, 12345678913, 'jacksonmsimatupang', 'jackson.mandor01@gmail.com', 'ff8191e10d96d1b498811749c5e109d4', 'Jackson M. Simatupang', 'Jl. Irian Seringgu Merauke', 'Merauke', '1977-01-01', '08123456991', 'active', '2018-01-09 21:16:02', NULL, 4, NULL, NULL),
(22, 12345678915, 'harryirawan', 'harry.mandor02@gmail.com', 'ff95f34e48fb64868c9388da7a2e2fde', 'Harry Irawan', 'Jl. Jawa', 'Merauke', '2001-01-01', '08123467992', 'active', '2018-01-09 21:18:08', NULL, 4, NULL, NULL),
(23, 1234778899, 'agussutrimo', 'agussutrimo01@gmail.com', '17e610969a90582f58f8a13cc481e8b5', 'Agus Sutrimo', 'Jl. Ternate', 'Merauke', '1977-10-01', '08128769018', 'active', '2018-01-09 21:20:02', NULL, 5, NULL, NULL),
(24, 19201010101, 'dadan', 'dadan02@gmail.com', '2c4d9d04b44320cf9c1a73e6fe8a2830', 'Dadan', 'Jl. Gor', 'Merauke', '1970-11-12', '08238811991', 'active', '2018-01-09 21:23:14', NULL, 5, NULL, NULL),
(25, 19201010101, 'ajam', 'ajam02@gmail.com', '365e0de604b494d0f38574bc8202cca0', 'Ajam', 'Jl. Jawa', 'Merauke', '1972-10-19', '08125677819', 'active', '2018-01-09 21:26:18', NULL, 5, NULL, NULL),
(26, 19789245671, 'amir', 'amir02@gmail.com', '74223995d353842ef8fa313374c44190', 'Amir', 'Jl. Raya Mandala Muli Merauke', 'Jayapura', '1970-01-06', '08989010287', 'active', '2018-01-09 21:28:36', NULL, 5, NULL, NULL),
(27, 19780925678190, 'purwandi', 'purwandi02@gmail.com', '8166135fad0d2020b78e78c663b39153', 'Purwandi', 'Jl. sasate', 'Merauke', '1975-06-09', '08918265512', 'active', '2018-01-09 21:30:32', NULL, 5, NULL, NULL),
(28, 19789201912, 'rikko', 'rikko02@gmail.com', '05dbbfb9a48473c7576cc15b8f8acc72', 'Rikko ', 'Jl. Prajurit', 'Malang', '1977-09-08', '081918171615', 'active', '2018-01-09 21:32:50', NULL, 5, NULL, NULL),
(29, 198709027839, 'asep', 'asep02@gmail.com', '4f01ef22ac08616336fd41f26851da36', 'Asep Haryanto', 'Jl. Gak', 'Merauke', '1969-01-01', '08918272611', 'active', '2018-01-09 21:35:03', NULL, 5, NULL, NULL),
(30, 197903242009041002, 'hendrix', 'hendrix02@gmail.com', 'f0b59563104d1c6cc4fd3bff6ce35847', 'Hendriks H. Prasetyo', 'Jl. Kali Weda', 'Jayapura', '1980-01-04', '0812789101817', 'active', '2018-01-09 21:38:34', NULL, 5, NULL, NULL),
(31, 1989775437281, 'moses', 'moses02@gmail.com', '07a36a4e78c9ff667a9e74bbf1fbc69a', 'Moses', 'Jl. Noari', 'Merauke', '1980-03-12', '081452781910', 'active', '2018-01-09 21:41:22', NULL, 5, NULL, NULL),
(32, 1989367482, 'junaedi', 'junaedi02@gmail.com', '2e60fbc23558011f3905c561b0d8e6a7', 'Junaedi', 'Jl. Parakomando', 'Jayapura', '1977-01-02', '08127881101', 'active', '2018-01-09 21:43:47', NULL, 5, NULL, NULL),
(33, 197903242009041002, 'hendrix', 'hendrix.prasetyo2018@gmail.com', 'c034ab9c07c03a24f2661637d2bc3651', 'Hendriks H. Prasetyo', 'Jl. Irian Seringgu Kompleks BTN Seringgu Permai Kali Weda', 'Banyuwangi', '1979-03-24', '081248042264', 'active', '2018-02-01 11:38:28', NULL, 5, NULL, NULL),
(34, 1970000000, 'agussusyanto', 'agus.susyanto2018@gmail.com', '2d887c17d9241019a631eb1af4ad64fd', 'Agus Susyanto', 'Jl. Peternakan Mopah Lama', 'Merauke', '1970-02-03', '081248042751', 'active', '2018-02-01 12:54:57', NULL, 5, NULL, NULL),
(35, 197000000011, 'subur', 'subur.subur2018@gmail.com', 'c4c2ab52938f9f0f8302c80b36225d87', 'Subur', 'Jl. Peternakan Mopah Lama', 'Merauke', '1970-02-04', '081248043206', 'active', '2018-02-01 13:03:31', NULL, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `re_users_level`
--

CREATE TABLE `re_users_level` (
  `level_id` int(15) NOT NULL,
  `level_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `re_users_level`
--

INSERT INTO `re_users_level` (`level_id`, `level_name`) VALUES
(1, 'Administrator'),
(2, 'Operator'),
(3, 'Pengawas'),
(4, 'Mandor'),
(5, 'Pengangkut');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `re_costumer`
--
ALTER TABLE `re_costumer`
  ADD PRIMARY KEY (`cost_id`);

--
-- Indexes for table `re_datagis`
--
ALTER TABLE `re_datagis`
  ADD PRIMARY KEY (`data_id`),
  ADD KEY `data_type` (`data_type`);

--
-- Indexes for table `re_datagis_type`
--
ALTER TABLE `re_datagis_type`
  ADD PRIMARY KEY (`typegis_id`);

--
-- Indexes for table `re_desa`
--
ALTER TABLE `re_desa`
  ADD PRIMARY KEY (`desa_id`);

--
-- Indexes for table `re_gallery`
--
ALTER TABLE `re_gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `re_gallery_category`
--
ALTER TABLE `re_gallery_category`
  ADD PRIMARY KEY (`galcat_id`);

--
-- Indexes for table `re_jdw_sampah`
--
ALTER TABLE `re_jdw_sampah`
  ADD PRIMARY KEY (`jadwal_id`);

--
-- Indexes for table `re_kecamatan`
--
ALTER TABLE `re_kecamatan`
  ADD PRIMARY KEY (`idKec`);

--
-- Indexes for table `re_pembayaran`
--
ALTER TABLE `re_pembayaran`
  ADD PRIMARY KEY (`bayar_id`);

--
-- Indexes for table `re_pembayaran_add`
--
ALTER TABLE `re_pembayaran_add`
  ADD PRIMARY KEY (`add_id`);

--
-- Indexes for table `re_settingsite`
--
ALTER TABLE `re_settingsite`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `re_type`
--
ALTER TABLE `re_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `re_users`
--
ALTER TABLE `re_users`
  ADD PRIMARY KEY (`re_user_id`);

--
-- Indexes for table `re_users_level`
--
ALTER TABLE `re_users_level`
  ADD PRIMARY KEY (`level_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `re_costumer`
--
ALTER TABLE `re_costumer`
  MODIFY `cost_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=939;

--
-- AUTO_INCREMENT for table `re_datagis`
--
ALTER TABLE `re_datagis`
  MODIFY `data_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `re_datagis_type`
--
ALTER TABLE `re_datagis_type`
  MODIFY `typegis_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `re_desa`
--
ALTER TABLE `re_desa`
  MODIFY `desa_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `re_gallery`
--
ALTER TABLE `re_gallery`
  MODIFY `gallery_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `re_gallery_category`
--
ALTER TABLE `re_gallery_category`
  MODIFY `galcat_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `re_jdw_sampah`
--
ALTER TABLE `re_jdw_sampah`
  MODIFY `jadwal_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `re_kecamatan`
--
ALTER TABLE `re_kecamatan`
  MODIFY `idKec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `re_pembayaran`
--
ALTER TABLE `re_pembayaran`
  MODIFY `bayar_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `re_pembayaran_add`
--
ALTER TABLE `re_pembayaran_add`
  MODIFY `add_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `re_settingsite`
--
ALTER TABLE `re_settingsite`
  MODIFY `setting_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `re_type`
--
ALTER TABLE `re_type`
  MODIFY `type_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `re_users`
--
ALTER TABLE `re_users`
  MODIFY `re_user_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `re_users_level`
--
ALTER TABLE `re_users_level`
  MODIFY `level_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
