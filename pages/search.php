<?php
	include("inc/inc.session.php");
	include("inc/themes/header.php");
	include("inc/themes/navbar.php");	
?> 
<?php if (!$page) {header("location:../index.php?page=404");} else {};?>
 <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?php echo $pageName;?></h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                          <ol class="breadcrumb">
                            <li><a href="index.php?page=home">Dashboard</a></li>
                            <li class="active"><?php echo $pageName;?></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Masukkan Kata Kunci</h3>
                            <form class="form-group" role="search" action="index.php" method="get">
                                <div class="input-group">
									<input type="hidden" name="page" value="search">
                                    <input type="text" id="example-input1-group2" name="keywords" class="form-control" placeholder="<?php echo $_GET["keywords"];?>">
                                    <span class="input-group-btn"><button type="submit" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></button></span>
                                </div>
                            </form>
                            <h2 class="m-t-40">Hasil Pencarian "<?php echo $_GET["keywords"];?>"</h2>
                            <small>About 14,700 result ( 0.10 seconds)</small>
                            <hr>
                            <ul class="search-listing">
                            <?php
                            $keywords = $_GET["keywords"];
                                $query = pg_query("SELECT * From zp_corps WHERE lower(corps_name) LIKE '%$keywords%' AND corps_status='active' OR lower(corps_address) LIKE '%$keywords%' AND corps_status='active' OR lower(corps_prov) LIKE '%$keywords%' AND corps_status='active' OR lower(corps_kab) LIKE '%$keywords%' AND corps_status='active' ORDER BY corps_id ASC ");
                                while($data = pg_fetch_array($query))
                                {
                            ?>
                                <li>
                                    <h3><a href="index.php?page=viewperusahaan&id=<?php echo $data['corps_id'];?>"><?php echo $data['corps_name'];?></a></h3>
                                    <a href="javascript:void(0)" class="search-links"><?php echo $data['corps_address'];?></a>
                                    <p><?php echo $data['corps_desc'];?></p></br>
                                    <form class="form-horizontal" action="index.php?page=addanalisis-step3" method="post">
                                    <input type="hidden" name="type" value="single">
                                      <input type="hidden" name="corps" value="<?php echo $data['corps_id'];?>">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Buat Analisis</button>
                                    </form>
                                </li>
                               <?php } ;?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
    <?php include("inc/themes/footer.php");?>
 </body>
</html>