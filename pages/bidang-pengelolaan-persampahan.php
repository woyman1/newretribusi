<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
   -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Bidang Pengelolaan Persampahan - Aplikasi Retribusi Pengelolaan Sampah Kabupaten Merauke Provinsi Papua.</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/frontend/old/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- This is Sidebar menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- This is a Animation CSS -->
    <link href="assets/frontend/old/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/frontend/old/css/style.css" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (megna.css) for this starter
         page. However, you can choose any other skin from folder css / colors .
         -->
    <link href="assets/frontend/old/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Bookman Old Style";
	panose-1:2 5 6 4 5 5 5 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

    <style type="text/css">
        #logo {
            height: 55px;
        }

        .home {
            margin: auto;
        }

        .left-picture {

            background-image: url("assets/frontend/old/img/518169-backgrounds.jpg");

            display: block;
            position: absolute;
            height: auto;
            bottom: 0;
            top: 0;
            left: 0;
            right: 0;
        }

        .back {
            text-align: center;
        }

        .content-right {
            margin-left: 15px;
        }
    </style>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <!-- Toggle icon for mobile view -->
                <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="ti-menu"></i>
                </a>
                <!-- Logo -->
                <div class="top-left-part">
                    <a class="logo" href="index.php?page=home">
                        <!-- Logo icon image, you can use font-icon also -->
                        <b>
                            <img id="logo" src="uploads/images/assets/logo-app-bg.png" alt="home" style="height: 50px;;" />
                        </b>
                        <!-- Logo text image you can use text also -->

                    </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->

                <!-- This is the message dropdown -->
                <ul class="nav navbar-top-links navbar-right pull-right">

                    <!-- .Task dropdown -->

                    <!-- /.Task dropdown -->
                    <!-- .user dropdown -->
                    <!-- <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="../plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">Steave</b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.user dropdown-user -->
                    <!-- </li>  -->
                    <!-- /.user dropdown -->
                    <!-- .Megamenu -->

                    <!-- /.Megamenu -->

                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="index.php?page=home" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Home
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=about" class="waves-effect ">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">About
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=gis" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">WebGIS
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=contact" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Kontak
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=data" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Data Lain
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                     <li>
                        <a href="index.php?page=gallery" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Gallery
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>

                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <br />
                <!-- .row -->
                <div class="row">
                <img src="uploads/images/assets/visi-misi.jpg" style="margin-bottom: 20px; width:100%;" />
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default left">
                        
  


<img src="uploads/images/assets/bidang.jpg" style="width: 100%;"/>
                            
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="panel panel-default content-right" style="padding:10px;">
<p>
<b>TUGAS</b><br />
Membantu Kepala Dinas dalam merumuskan kebijakan, mengkoordinasikan, membina, mengawasi dan mengendalikan program di bidang Pengelolaan Persampahan<br />
<br />
<b>FUNGSI</b><br /><br />
1.	Penyusunan Informasi Pengelolaan Sampah Tingkat Kabupaten;<br />
2.	Penetapan target pengurangan sampah dan prioritas jenis sampah untuk setiap kurun waktu tertentu;<br />
3.	Perumusan kebijakan pengurangan sampah;<br />
4.	Pembinaan pembatasan timbunan sampah kepada produsen/industry;<br />
5.	Pembinaan penggunaan bahan baku produksi dan kemasan yang mampu diurai oleh proses alami;<br />
6.	Pembinaan pendaur pendaur ulangan sampah;<br />
7.	Penyediaan fasilitas pendaur ulangan sampah;<br />
8.	Pembinaan pemanfaatan kembali sampah dari produk dan kemasan produk;<br />
9.	Perumusan kebijakan penanganan sampah di kabupaten;<br />
10.	Koordinasi pemilahan, pengumpulan pengangkutan dan pemrosesan akhir sampah;<br />
11.	 Penyediaan sapras penanganan sampah;<br />
12.	Pemungutan retribusi atas jasa layanan pengelolaan sampah;<br />
13.	Penetapan lokasi tempat TPS, TPST dan TPA sampah;<br />
14.	Pengawasan terhadap tempat pemrosesan akhir dengan sistem pembuangan open dumping;<br />
15.	Penyusunan dan pelaksanaan sistem tanggap darurat pengelolaan sampah;<br />
16.	Pemberian kompensasi dampak negatif kegiatan pemrosesan akhir sampah,<br />
17.	Pelaksanaan kerjasama dengan kabupaten/kota lain dan kemitraan dengan badan usaha pengelola sampah dalam menyelenggrakan pengelolaan sampah;<br />
18.	Pengembangan investasi dalam usaha pengelolaan sampah;<br />
19.	Penyusunan kebijakan perizinan pengelolaan sampah, pengangkutan sampah dan pemrosesan akhir yang diselenggarakan oleh swasta;<br />
20.	Perumusan kebijakan pembinaan dan pengawasan kinerja pengelolaan sampah yang dilakankan pihak lain (badan usaha);<br />
21.	Pelaksanaan pembinaan dan pengawasan kinerja pengelolaan sampah yang dilaksankan oleh pihak lain (badan usaha).<br />
<br />
<br />
<b>SDM</b><br /><br />
<b>I.Kepala Bidang (1 orang)</b><br /><br />
<b>II.Kepala Seksi (3 orang)</b><br /><br />
<b>I. Seksi Penanganan Sampah</b><br />
1.	Staf Kantor Penanganan Sampah (7 orang)<br />
2.	Pengawas Penanganan Sampah (2 orang)<br />
3.	Petugas Pembersihan Jalan (52 orang) :<br />
-	Tenaga Mandor Pembersihan Jalan (1 orang) <br />
-	Tenaga Pekerja Pembersihan Jalan (51 orang) :<br />
4.	Petugas Pengangkutan Sampah (66 orang):<br />
-	Mandor Pengangkutan Sampah (1 orang)<br />
-	Tenaga Sopir Pengangkutan Sampah (9 orang)<br />
-	Tenaga Mekanik Sapras Pengangkutan Sampah (2 orang)<br />
-	Tenaga Administrasi Pengangkutan Sampah (2 orang)<br />
-	Tenaga Penagih Retribusi Sampah (2 orang)<br />
-	Tenaga Pekerja Pengangkutan Sampah (50 orang)<br /><br />
 <b>II.Seksi Kerjasama Kebersihan Lingkungan</b><br />
1.	Staf Kantor Kerjasama Kebersihan Lingkungan (5 orang)<br /><br />
<b>III.Seksi Pengelolaan dan Pemanfaatan Sampah</b><br />
1.	Staf Kantor Pengelolaan dan Pemanfaatan Sampah (3 orang)<br />
2.	Tenaga Pekerja di TPA (7 Orang) :<br />
- Tenaga Operator di TPA (2 orang)<br />
- Tenaga Pekerja di TPA (5 orang)<br />
<br /><br />
<b>TOTAL  : 155 orang

</p>

</div>



                        </div>
                    </div>
                </div>
                <!-- .row -->
                <!-- .right-sidebar -->
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Dinas Lingkungan Hidup Kabupaten Merauke <small>Support by <a href="http://technogis.co.id" target="_blank">TechnoGIS Indonesia</a></small> </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/frontend/old/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/frontend/old/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="assets/frontend/old/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="assets/frontend/old/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="assets/frontend/old/js/custom.min.js"></script>
</body>

</html>
