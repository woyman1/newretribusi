<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
   -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Kontak - Aplikasi Retribusi Pengelolaan Sampah Kabupaten Merauke Provinsi Papua.</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/frontend/old/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- This is Sidebar menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- This is a Animation CSS -->
    <link href="assets/frontend/old/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/frontend/old/css/style.css" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (megna.css) for this starter
         page. However, you can choose any other skin from folder css / colors .
         -->
    <link href="assets/frontend/old/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <style type="text/css">
        #logo {
            height: 55px;
        }

        .home {
            margin: auto;
        }

        .left-picture {

            background-image: url("assets/frontend/img/518169-backgrounds.jpg");

            display: block;
            position: absolute;
            height: auto;
            bottom: 0;
            top: 0;
            left: 0;
            right: 0;
        }

        .back {
            text-align: center;
        }

        .content-right {
            margin-left: 15px;
            height: 70vh;

            display: flex;
            align-items: center;
        }

        .content-right > dl:nth-child(1) {
         margin-left: 50px;   
        }
    </style>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <!-- Toggle icon for mobile view -->
                <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="ti-menu"></i>
                </a>
                <!-- Logo -->
                <div class="top-left-part">
                    <a class="logo" href="index.php?page=home">
                        <!-- Logo icon image, you can use font-icon also -->
                        <b>
                            <img id="logo" src="uploads/images/assets/logo-app-bg.png" style="height: 50px;" alt="home" />
                        </b>
                        <!-- Logo text image you can use text also -->

                    </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->

                <!-- This is the message dropdown -->
                <ul class="nav navbar-top-links navbar-right pull-right">

                    <!-- .Task dropdown -->

                    <!-- /.Task dropdown -->
                    <!-- .user dropdown -->
                    <!-- <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="../plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">Steave</b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.user dropdown-user -->
                    <!-- </li>  -->
                    <!-- /.user dropdown -->
                    <!-- .Megamenu -->

                    <!-- /.Megamenu -->

                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="index.php?page=home" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Home
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=about" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">About
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=gis" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">WebGIS
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=contact" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Kontak
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=data" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Data Lain
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    
                     <li>
                        <a href="index.php?page=gallery" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Gallery
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>

                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <br />
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default left">
                             <img src="uploads/images/assets/kontak1..jpg" style="width: 100%;" />
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="panel panel-default" style="padding:0px;">
                            <div class="content-right">
                                <dl style="margin-top: -125px;">
                                 <dt><i class="ti-home" style="font-size:1.5em; text-shadow: 1px 1px 1px #ccc;"></i></dt>
                                    <dd class="contact-detail">
                                        <address>
                                            Jl. Peternakan Mopah Lama, Merauke , Papua
                                            <br/> Kota Merauke
                                            <br/> Provinsi Papua
                                            <br/> Indonesia
                                        </address>
                                    </dd>
                                    <dt><i class="ti-email" style="font-size:1.5em; text-shadow: 1px 1px 1px #ccc;"></i></dt>
                                    <dd class="contact-detail">
                                        <a href="" title="Klik untuk mengirim email pada kami">dlh.meraukepapua@gmail.com</a>
                                    </dd>

                                    <dt><i class="fa fa-phone" style="font-size:1.5em; text-shadow: 1px 1px 1px #ccc;"></i></dt>
                                    <dd class="contact-detail">
                                       (0971) 312220
                                    </dd>
                                     <dt><i class="fa fa-mobile" style="font-size:1.5em; text-shadow: 1px 1px 1px #ccc;"></i></dt>
                                    <dd class="contact-detail">
                                      081225603760
                                    </dd>

                                    
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
                <!-- .right-sidebar -->
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Dinas Lingkungan Hidup Kabupaten Merauke <small>Support by <a href="http://technogis.co.id" target="_blank">TechnoGIS Indonesia</a></small> </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/frontend/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/frontend/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="assets/frontend/old/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="assets/frontend/old/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="assets/frontend/old/js/custom.min.js"></script>
</body>

</html>