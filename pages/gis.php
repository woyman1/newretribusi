<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
   -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>WebGIS | Aplikasi Retribusi Pengelolaan Sampah Kabupaten Merauke Provinsi Papua.</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/frontend/old/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- This is Sidebar menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- This is a Animation CSS -->
    <link href="assets/frontend/old/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/frontend/old/css/style.css" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (megna.css) for this starter
         page. However, you can choose any other skin from folder css / colors .
         -->
    <link href="assets/frontend/old/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

    <style>
         #legend {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
        height: 300px;
        width:300px

      }

      .isi{
        height: 280px;
        overflow: auto;
      }

      .kaki{
        height: 20px;
      }

      #markerz{
        height: 300px;
        width: 200px;
      }

      .isiMarks{
        height: 280px;
        overflow: auto;
      }

      #legend h3 {
        margin-top: 0;
      }
      #legend img {
        vertical-align: middle;
      }

       #control {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
      }
      #control h3 {
        margin-top: 0;
      }
      #control img {
        vertical-align: middle;
      }

      .col-md-12{
        margin-left: -25px;
      }

      #googleMap{
        width: 104%;
      }
      #logo{
        height: 55px;
      }

      #imgDesc {
        background: #fff;
        padding: 10px;
        margin: 10px;
        margin-left: 40px;
        border: 3px solid #000;
        height: 70%;

      }

      #iw-container {
	margin-bottom: 10px;
}
#iw-container .iw-title {
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 22px;
	font-weight: 400;
	padding: 10px;
	background-color: #48b5e9;
	color: white;
	margin: 0;
	border-radius: 2px 2px 0 0;
}
#iw-container .iw-content {
	font-size: 13px;
	line-height: 18px;
	font-weight: 400;
	margin-right: 1px;
	padding: 15px 5px 20px 15px;
	max-height: 140px;
	overflow-y: auto;
	overflow-x: hidden;
}
.iw-content img {
	float: right;
	margin: 0 5px 5px 10px;
}
.iw-subTitle {
	font-size: 16px;
	font-weight: 700;
	padding: 5px 0;
}
.iw-bottom-gradient {
	position: absolute;
	width: 326px;
	height: 25px;
	bottom: 10px;
	right: 18px;
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
}
    </style>
</head>

<body>
    <!-- Preloader -->
   <!-- <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div> -->
    <div id="wrapper">
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <!-- Toggle icon for mobile view -->
                <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <!-- Logo -->
                <div class="top-left-part">
                    <a class="logo" href="index.php?page=home">
                        <!-- Logo icon image, you can use font-icon also -->
                        <b><img id="logo" src="http://retribusisampahmerauke.com/uploads/images/assets/logo-app-bg.png" alt="home" style="height: 50px;" /></b>
                        <!-- Logo text image you can use text also -->
                        <!-- <span class="hidden-xs"><img src="../plugins/images/eliteadmin-text.png" alt="home" /></span> -->
                    </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->
                 <center>
                    <h2> WebGIS Data Pelanggan Retribusi Sampah Kab Merauke</h2>
                    </center>
                <!-- This is the message dropdown -->
                <ul class="nav navbar-top-links navbar-right pull-right">

                    <!-- .Task dropdown -->

                    <!-- /.Task dropdown -->
                    <!-- .user dropdown -->

                    <!-- /.user dropdown -->
                    <!-- .Megamenu -->

                    <!-- /.Megamenu -->

                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->

        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                         <div id="googleMap" style="height:650px;border: 2px solid #3872ac; margin-bottom:0px;"></div>
                         <div id="conLegend" text-align= "center" vertical-align= "middle">
                            <div id="legend" text-align= "center" vertical-align= "middle"></div>
                         </div>

                         <div id="control" text-align= "center" vertical-align= "middle">
                            <div text-align="center">
                                    <button class="btn btn-default btn-block"  type="button" id="sembunyi">Legenda</button>
                            </div>
                            <br />
                            <div text-align="center">
                                    <button class="btn btn-default btn-block"  type="button" id="sembunyi_marker">Kontrol Marker</button>
                            </div>
                        </div>

                            <script>
                            function myMap() {
                                var mapProp= {
                                    center:new google.maps.LatLng(-4.9488732732152005,133.14749157187498),
                                    zoom:7,
                                    mapTypeControl: true,
                                    overviewMapControl: true,
                                    overviewMapControlOptions: {
                                      opened: false,
                                    },

                                    zoomControl: true,
                                    zoomControlOptions: {
                                          position: google.maps.ControlPosition.LEFT_CENTER
                                      },

                                      scaleControl: true,
                                      streetViewControl: true,
                                      streetViewControlOptions: {
                                          position: google.maps.ControlPosition.LEFT_TOP
                                      },


                                };

                                var icon3 = {

                                    path: "M7.167 615l620-608q8-7 18.5-7t17.5 7l608 608q8 8 5.5 13t-12.5 5h-175v575q0 10-7.5 17.5t-17.5 7.5h-250q-10 0-17.5-7.5t-7.5-17.5V833h-300v375q0 10-7.5 17.5t-17.5 7.5h-250q-10 0-17.5-7.5t-7.5-17.5V633h-175q-10 0-12.5-5t5.5-13z",
                                    fillColor: '#000000',
                                    fillOpacity: 1,
                                    anchor: new google.maps.Point(0,0),
                                    strokeWeight: 0,
                                    scale: 0.014,
                                    ask: "assets/icon/home.svg"
                                }

                                var tps = {
                                    path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                                    fillColor: '#0000ff',
                                    fillOpacity: 1,
                                    anchor: new google.maps.Point(0,0),
                                    strokeWeight: 0,
                                    scale: 0.5,
                                    ask: "assets/icon/trash.svg"
                                    }

                                var tpa = {
                                    path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                                    fillColor: '#008000',
                                    fillOpacity: 1,
                                    anchor: new google.maps.Point(0,0),
                                    strokeWeight: 0,
                                    scale: 0.014,
                                    ask: "assets/icon/trash.svg"
                                    }

                                var kontainer = {
                                    path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                                    fillColor: '#ffa500',
                                    fillOpacity: 1,
                                    anchor: new google.maps.Point(0,0),
                                    strokeWeight: 0,
                                    scale: 0.014,
                                    ask: "assets/icon/trash.svg"
                                    }

                                var sampahLiar = {
                                    path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                                    fillColor: '#ff0000',
                                    fillOpacity: 1,
                                    anchor: new google.maps.Point(0,0),
                                    strokeWeight: 0,
                                    scale: 0.014,
                                    ask: "assets/icon/trash.svg"
                                    }

                                <?php
                                    $query_type = mysqli_query($re_connect, "SELECT * FROM re_type");
                                    while($data_type = mysqli_fetch_array($query_type)) :
                                ?>

                                var <?php echo str_replace(str_split('\\/:*?"<>|,() '), "",  $data_type["type_name"])?> = {
                                                path: "<?=$data_type["type_icon"]?>",
                                                fillColor: '#000000',
                                                fillOpacity: 1,
                                                anchor: new google.maps.Point(0,0),
                                                strokeWeight: 0,
                                                scale: 0.5,
                                            }
                                <?php endwhile ?>

                                var icons = {

                                <?php
                                    $query_type = mysqli_query($re_connect, "SELECT * FROM re_type");
                                    while($data_type = mysqli_fetch_array($query_type)) :
                                ?>

                                <?php echo str_replace( str_split('\\/:*?"<>|,() ') , "",  $data_type["type_name"])?>: {
                                    name: '<?php echo str_replace(str_split('\\/:*?"<>|,() '), "",  $data_type["type_name"])?>',
                                    icon: <?php echo str_replace(str_split('\\/:*?"<>|,() '), "",  $data_type["type_name"])?>,
                                    fill: '#000000',
                                    nama: '<?=$data_type["type_name"]?>'
                                },

                                <?php endwhile ?>

                                  tps: {
                                    name: 'TPS',
                                    nama: 'TPS',
                                    icon: tps,
                                    fill:'#0000ff',
                                    h : '24px',
                                    width: '50px',
                                  },
                                  tpa: {
                                    name: 'TPA',
                                    nama: 'TPA',
                                    icon: tps,
                                    fill:'#ff0066',
                                    h : '24px',
                                    width: '50px',
                                  },
                                  kontainer: {
                                    name: 'Kontainer',
                                    nama: 'Kontainer',
                                    icon: tps,
                                    fill:'#ffa500',
                                    h : '24px',
                                    width: '50px',
                                  },
                                  sampahLiar: {
                                    name: 'Sampah Liar',
                                    nama: 'Sampah Liar',
                                    icon: tps,
                                    fill:'#000000',
                                    h : '24px',
                                    width: '50px',
                                  },
                                };

                                var myCenter = new google.maps.LatLng(-4.2699,138.0804);

                                var features = [
                                    {
                                        position:new google.maps.LatLng(-4.0699,138.1804),
                                        type: 'sampah'
                                    }, {
                                        position: myCenter,
                                        type: 'rumah'
                                    }
                                ];


                                var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

                                var markers = ["test"];

                                 var legend = document.getElementById('legend');
                                 var isi = document.createElement('div');
                                 isi.className = "isi";
                                 legend.appendChild(isi);
                                 var kaki = document.createElement('div');
                                 kaki.className = "kaki";
                                 legend.appendChild(kaki);
                                    for (var key in icons) {
                                      var type = icons[key];
                                      var nama = type.nama;
                                      var fill = type.fill;
                                      var iconing = type.icon;
                                      var h = type.h;
                                      var width = type.width;
                                      var div = document.createElement('div');

                                      //div.innerHTML = '<img src="' + icon.path + '"> ' + name;
                                      div.innerHTML = '<div class="row" style="border-bottom:1px solid black">'+
                                       '<div class="col-md-4">'+
                                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 50" height="24px" width="50px" style="fill: '+fill+'"><path d="'+iconing.path+'"/></svg>' +
                                      '</div>'+
                                      '<div class="col-md-1"></div>'+
                                       '<div class="col-md-6" style="vertical-align: middle;line-height: 27px;">'+
                                         '<span>'+nama+'</span>'+
                                         '</div></div>';
                                      isi.appendChild(div);
                                    };

                                    var control = document.getElementById('control');

                            }
                            </script>

                            <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwx2LQQM6zmHj7oqZfI_oDrAuuXXN3tBk"></script> -->
                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwx2LQQM6zmHj7oqZfI_oDrAuuXXN3tBk&callback=myMap&libraries=geometry"></script>

                    </div>
                </div>
                <!-- .row -->
                <!-- .right-sidebar -->
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Dinas Lingkungan Hidup Kabupaten Merauke <small>Support by <a href="http://technogis.co.id" target="_blank">TechnoGIS Indonesia</a></small> </footer>
          </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->

    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/frontend/old/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/frontend/old/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="assets/frontend/old/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="assets/frontend/old/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="assets/frontend/old/js/custom.min.js"></script>
    <script src="assets/frontend/old/js/MeasureTool.js"></script>

    <?php
        // include("inc\inc.gmaps.gis.php");
    ?>

<script type="text/javascript">
        $(document).ready(function () {
                    $("#sembunyi").click(function () {
                        $('#legend').toggle("slow");
                    });

                    $("#legend").hide();

                    var tps = {
                        path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                        fillColor: '#0000ff',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.5,
                    }

                    var tpa = {
                        path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                        fillColor: '#ff0066',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.5,
                    }

                    var kontainer = {
                        path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                        fillColor: '#ffa500',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.5,
                    }

                    var sampahLiar = {
                        path: "M12 38c0 2.21 1.79 4 4 4h16c2.21 0 4-1.79 4-4V14H12v24zM38 8h-7l-2-2H19l-2 2h-7v4h28V8z",
                        fillColor: '#000000',
                        fillOpacity: 1,
                        anchor: new google.maps.Point(0, 0),
                        strokeWeight: 0,
                        scale: 0.5,
                    }



            <?php
                $query_type = mysqli_query($re_connect, "SELECT * FROM re_type");
                while($data_type = mysqli_fetch_array($query_type)) :
            ?>

            var <?php echo str_replace(str_split('\\/:*?"<>|,() '), "", $data_type["type_name"])."active"?> = {
                path: "<?=$data_type["type_icon"]?>",
                fillColor: '#008000',
                fillOpacity: 1,
                anchor: new google.maps.Point(0,0),
                strokeWeight: 0,
                scale: 0.5,
            }

            var <?php echo str_replace(str_split('\\/:*?"<>|,() '), "", $data_type["type_name"])."nonactive"?> = {
                path: "<?=$data_type["type_icon"]?>",
                fillColor: '#ff0000',
                fillOpacity: 1,
                anchor: new google.maps.Point(0,0),
                strokeWeight: 0,
                scale: 0.5,
            }
            <?php endwhile ?>

        var myCenter = new google.maps.LatLng(-4.2699,138.0804);
        var bounds = new google.maps.LatLngBounds();

        var desSatu =
            '<div id="iw-container">'+
                '<div class="iw-title">';
        var desDua = '</div>' +
                '<div class="iw-content">' +
                    '<img src="assets/frontend/img/Lambang_Kabupaten_Merauke.png" alt="gambar" height= "100" width= "100">'+
                    '<img src="assets/frontend/img/Lambang_Kabupaten_Merauke.png" alt="gambar" height= "100" width= "100"> <br> ';


        var desAkhir = '<div class="iw-bottom-gradient"></div>' +
        '</div></div>';

        var places={

            <?php
                $query_cost = mysqli_query($re_connect, "SELECT * FROM re_type");

                while($data_cost = mysqli_fetch_array($query_cost)) :
            ?>
                <?php
                    $type_name = $data_cost["type_name"];
                ?>
                <?php echo str_replace(str_split('\\/:*?"<>|,() '), "",  $data_cost["type_name"])."active"?> : {
                    label : ' <?=$data_cost["type_name"]?>',
                    checked: true,
                    icon:<?=str_replace(str_split('\\/:*?"<>|,() '), '', $data_cost["type_name"])."active";?>,
                    items:[
                         <?php
                        $people = mysqli_query($re_connect, "SELECT * FROM re_costumer inner join re_type on re_costumer.cost_type_id=re_type.type_id where re_type.type_name = '$type_name' AND re_costumer.cost_status = 'active'");

                        while($data_people = mysqli_fetch_array($people)) : ?>
                            [desSatu+'<?=$data_people["cost_name"]?>'+desDua+desAkhir, <?=$data_people["cost_latt"]?>  , <?=$data_people["cost_lang"]?>,  '<?=$data_people["cost_name"]?>'],

                         <?php endwhile ?>
                    ]
                },

                <?php echo str_replace(str_split('\\/:*?"<>|,() '), "",  $data_cost["type_name"])."nonactive"?> : {
                    label : ' <?=$data_cost["type_name"]?>',
                    checked: true,
                    icon:<?=str_replace(str_split('\\/:*?"<>|,() '), '', $data_cost["type_name"])."nonactive";?>,
                    items:[
                         <?php
                        $people = mysqli_query($re_connect, "SELECT * FROM re_costumer inner join re_type on re_costumer.cost_type_id=re_type.type_id where re_type.type_name = '$type_name' AND re_costumer.cost_status = 'nonactive'");

                        while($data_people = mysqli_fetch_array($people)) : ?>
                            [desSatu+'<?=$data_people["cost_name"]?>'+desDua+desAkhir, <?=$data_people["cost_latt"]?>  , <?=$data_people["cost_lang"]?>,  '<?=$data_people["cost_name"]?>'],

                         <?php endwhile ?>
                    ]
                },

            <?php endwhile ?>

            tps:{
                label:' TPS',
                checked:true,
                icon:tps,
                items:[
                    <?php $query = mysqli_query($re_connect, "SELECT * from re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type=re_datagis_type.typegis_id WHERE data_type = 1");
                        while($data = mysqli_fetch_array($query)) :?>

                        ['<?=$data["data_desc"]?>' , <?=$data["data_lat"]?>  , <?=$data["data_lon"]?> ],

                    <?php endwhile ?>
                ]
            },

            tpa:{
                label:' TPA',
                checked:true,
                icon:tpa,
                items:[
                    <?php $query = mysqli_query($re_connect, "SELECT * from re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type=re_datagis_type.typegis_id WHERE data_type = 2");
                        while($data = mysqli_fetch_array($query)) :?>

                        ['<?=$data["data_desc"]?>' , <?=$data["data_lat"]?>  , <?=$data["data_lon"]?> ],

                    <?php endwhile ?>
                ]
            },

            kontainer:{
                label:' kontainer',
                checked:true,
                icon:kontainer,
                items:[
                    <?php $query = mysqli_query($re_connect, "SELECT * from re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type=re_datagis_type.typegis_id WHERE data_type = 3");
                    while($data = mysqli_fetch_array($query)) :?>

                    ['<?=$data["data_desc"]?>' , <?=$data["data_lat"]?>  , <?=$data["data_lon"]?> ],

                <?php endwhile ?>
                ]
            },

            sampahLiar:{
                label:' Sampah Liar',
                checked:true,
                icon:sampahLiar,
                items:[
                    <?php $query = mysqli_query($re_connect, "SELECT * from re_datagis INNER JOIN re_datagis_type ON re_datagis.data_type=re_datagis_type.typegis_id WHERE data_type = 4");
                    while($data = mysqli_fetch_array($query)) :?>

                    ['<a class="btn btn-primary" href="index.php?page=gis"><?=$data["data_desc"]?></a>' , <?=$data["data_lat"]?>  , <?=$data["data_lon"]?> ],

                <?php endwhile ?>
                ]
            }
        };

        var mapProp = {
            center: new google.maps.LatLng(-2.4991, 140.4050),
            zoom: 7,
            mapTypeControl: true,
            overviewMapControl: true,
            overviewMapControlOptions: {
                opened: false,
            },

            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },

            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            fullscreenControl: false
        };

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        infowindow = new google.maps.InfoWindow();

        // control area
        ctrlMarks = $('<div/>').css({
            background: '#fff',
            border: '1px solid #000',
            padding: '4px',
            margin: '2px',
            textAlign: 'center',
            display: 'none',
        }).attr('id', 'markerz');

        // var ctrl = document.createElement('div');
        // ctrl.className = "isiMarks";
        // ctrlCont.appendChild(ctrl);
        ctrl = $('<div/>').attr('class', 'isiMarks');



        ctrl.append($('<input>', {
                type: 'button',
                value: 'Tampilkan semua',
                    class: 'btn btn-info btn-sm'
            })
            .click(function () {
                $(this).parent().find('input[type="checkbox"]')
                    .prop('checked', true).trigger('change');
            }));
        ctrl.append($('<br/><br/>'));

        //clear all-button
        ctrl.append($('<input>', {
                type: 'button',
                value: 'Sembunyikan semua',
                class: 'btn btn-info btn-sm'
            })
            .click(function () {
                $(this).parent().find('input[type="checkbox"]')
                    .prop('checked', false).trigger('change');
            }));

        ctrl.append($('<br/><br/>'));

        ctrlMarks.append(ctrl);


        $.each(places, function (c, category) {

            //a checkbox fo the category
            var cat = $('<input>', {
                    type: 'checkbox'
                }).change(function () {
                    $(this).data('goo').set('map', (this.checked) ? map : null);
                })
                //create a data-property with a google.maps.MVCObject
                //this MVC-object will do all the show/hide for the category
                .data('goo', new google.maps.MVCObject)
                .prop('checked', !!category.checked)

                //this will initialize the map-property of the MVCObject
                .trigger('change')

                //label for the checkbox
                .appendTo($('<div/>').css({
                    whiteSpace: 'nowrap',
                    textAlign: 'left'
                }).appendTo(ctrl))
                .after(category.label);

            //loop over the items(markers)
            $.each(category.items, function (m, item) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(item[1], item[2]),
                    title: item[3],
                    icon: category.icon
                });

                var position = new google.maps.LatLng(item[1], item[2]);

                bounds.extend(position);

                //bind the map-property of the marker to the map-property
                //of the MVCObject that has been stored as checkbox-data
                marker.bindTo('map', cat.data('goo'), 'map');


                google.maps.event.addListener(map, 'dragend', function() {
                    google.maps.event.addListener(marker, 'click', function () {
                        infowindow.setContent(item[0]);
                        infowindow.open(map, this);
                    });

                    google.maps.event.addListener(map, 'click', function () {
                        infowindow.close();
                    });
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent(item[0]);
                    infowindow.open(map, this);
                });

                google.maps.event.addListener(map, 'click', function () {
                        infowindow.close();
                    });
            });

        });

        map.fitBounds(bounds);
        map.panToBounds(bounds);

        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(ctrlMarks[0]);
        // end control area


        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(control);
        //legend area
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);
        // var imgDesc = $("#imgDesc")[0];
        // map.controls[google.maps.ControlPosition.LEFT_TOP].push(imgDesc);
        //end legend area

        measureTool = new MeasureTool(map, {
            unit: MeasureTool.UnitTypeId.METRIC
        });

        $("#sembunyi_marker").click(function () {
            $('#markerz').toggle("slow");
        });


        $("#markerz").hide();
        });

    </script>
