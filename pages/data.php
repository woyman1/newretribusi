<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
   -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Data Lainnya - Aplikasi Retribusi Pengelolaan Sampah Kabupaten Merauke Provinsi Papua.</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/frontend/old/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- This is Sidebar menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- This is a Animation CSS -->
    <link href="assets/frontend/old/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/frontend/old/css/style.css" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <!-- We have chosen the skin-blue (megna.css) for this starter
         page. However, you can choose any other skin from folder css / colors .
         -->
    <link href="assets/frontend/old/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Bookman Old Style";
	panose-1:2 5 6 4 5 5 5 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

    <style type="text/css">
        #logo {
            height: 55px;
        }

        .home {
            margin: auto;
        }

        .left-picture {

            background-image: url("assets/frontend/old/img/518169-backgrounds.jpg");

            display: block;
            position: absolute;
            height: auto;
            bottom: 0;
            top: 0;
            left: 0;
            right: 0;
        }

        .back {
            text-align: center;
        }

        .content-right {
            margin-left: 15px;
        }
    </style>

</head>

<body>

  <?php
    $q_setting = "SELECT setting_about_page FROM re_settingsite";
    $r_setting = $re_connect->query($q_setting);

    if ($r_setting->num_rows >0) {
        $row = $r_setting->fetch_assoc();
    }
  ?>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <!-- Toggle icon for mobile view -->
                <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="ti-menu"></i>
                </a>
                <!-- Logo -->
                <div class="top-left-part">
                    <a class="logo" href="index.php?page=home">
                        <!-- Logo icon image, you can use font-icon also -->
                        <b>
                            <img id="logo" src="uploads/images/assets/logo-app-bg.png" alt="home" style="height: 50px;;" />
                        </b>
                        <!-- Logo text image you can use text also -->

                    </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->

                <!-- This is the message dropdown -->
                <ul class="nav navbar-top-links navbar-right pull-right">

                    <!-- .Task dropdown -->

                    <!-- /.Task dropdown -->
                    <!-- .user dropdown -->
                    <!-- <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="../plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">Steave</b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.user dropdown-user -->
                    <!-- </li>  -->
                    <!-- /.user dropdown -->
                    <!-- .Megamenu -->

                    <!-- /.Megamenu -->

                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="index.php?page=home" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Home
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=about" class="waves-effect ">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">About
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=gis" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">WebGIS
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=contact" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Kontak
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                    <li>
                        <a href="index.php?page=data" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Data Lain
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>
                     <li>
                        <a href="index.php?page=gallery" class="waves-effect">
                            <i class="linea-icon linea-basic fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Gallery
                                <span class="fa arrow"></span>
                            </span>
                        </a>

                    </li>

                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <br />
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default left">
                         <div class="container">
                           <br>
                            <?= $row['setting_about_page'] ?>
                         </div>

                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default content-right" style="padding:10px;">
<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center;line-height:150%'><b><span
style='font-size:14.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Data
Penunjang Pengelolaan Kebersihan Kota dan Persampahan</span></b></p>

<div align=center>

<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=535
 style='width:401.4pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Luas
  Wilayah</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>46.791, 63 M<sup>2</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Jumlah
  Penduduk (2015)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>216.585 Jiwa/Orang</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Jumlah
  Penduduk Perkotaan</span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>(Jangkauan
  Penanganan Sampah)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>98.478 Jiwa/Orang</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Jumlah
  Kecamatan/Distrik</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>20 Kecamatan/Distrik</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Jumlah
  Desa/Kelurahan</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>11 Kelurahan, 160 Kampung</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Jumlah
  RW/RT</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>388/1.196</span></p>
  </td>
 </tr>
 <tr>
  <td width=535 colspan=3 valign=top style='width:401.4pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Data
  Teknis Persampahan</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Timbulan
  Sampah Domestik (Skala Kabupaten)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>433 M<sup>3/</sup>Hari</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Timbulan
  Sampah Perkotaan (Skala IKK)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>197 M<sup>3/</sup>Hari</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Pelayanan
  Pengangkutan Sampah (Skala IKK)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>32 M<sup>3/</sup>Hari</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Persentase
  Pelayanan (Skala IKK)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>32,99 %</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Persentase
  Pelayanan </span></p>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>(Skala
  Kabupaten)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>15,02 %</span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Persentase
  Sampah Organik (Perkotaan)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>137,0 M<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=286 valign=top style='width:214.2pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Persentase
  Sampah An-Organik (Perkotaan)</span></p>
  </td>
  <td width=28 valign=top style='width:21.3pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
  justify;line-height:150%'><span style='font-size:12.0pt;line-height:150%;
  font-family:"Bookman Old Style",serif'>:</span></p>
  </td>
  <td width=221 valign=top style='width:165.9pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>59,1 M<sup>3</sup></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal style='text-align:justify;line-height:150%'><span
style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>&nbsp;</span></p>

<span style='font-size:12.0pt;line-height:115%;font-family:"Bookman Old Style",serif'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><span style='font-size:12.0pt;line-height:115%;font-family:
"Bookman Old Style",serif'>&nbsp;</span></p>

<p class=MsoNormal align=center style='text-align:center;line-height:150%'><b><span
style='font-size:14.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Data
Sarana dan Prasarana Bidang Pengelolaan sampah</span></b></p>

<div align=center>
<?php

function tpa_count() {
  global $re_connect;
  $query = "SELECT COUNT(data_name) as jumlah from re_datagis WHERE data_type = 2";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      return $row['jumlah'];
    }
  }else{
    return "0";
  }
}

function tpa_items(){
  global $re_connect;
  $query = "SELECT data_name from re_datagis WHERE data_type = 2";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;
  $item = array();

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      $item[] = $row['data_name'];
    }
    return implode(", ", $item);
  }else{
    return "Tidak ada";
  }
}

function tps_count(){
  global $re_connect;
  $query = "SELECT COUNT(data_name) as jumlah from re_datagis WHERE data_type = 1";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      return $row['jumlah'];
    }
  } else{
    return "0";
  }
}
function tps_items(){
  global $re_connect;
  $query = "SELECT data_name from re_datagis WHERE data_type = 1";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;
  $item = array();

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      $item[] = $row['data_name'];
    }
    return implode(", ", $item);
  }else{
    return "Tidak ada";
  }
}

function kont_count(){
  global $re_connect;
  $query = "SELECT COUNT(data_name) as jumlah from re_datagis WHERE data_type = 3";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      return $row['jumlah'];
    }
  }else{
    return "0";
  }
}

function kont_items(){
  global $re_connect;
  $query = "SELECT data_name from re_datagis WHERE data_type = 3";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;
  $item = array();

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      $item[] = $row['data_name'];
    }
    return implode(", ", $item);
  }else{
    return "Tidak ada";
  }
}

function count_kendaraan($jenis){
  global $re_connect;
  $query = "SELECT $jenis from re_settingsite";
  $result = $re_connect->query($query);
  $num_rows = $result->num_rows;

  if ($num_rows>0){
    while ($row = $result->fetch_assoc()){
      return $row[$jenis];
    }
  }else{
    return "0";
  }
}
 ?>


<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=589
 style='width:441.45pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:12.0pt;
  line-height:150%;font-family:"Bookman Old Style",serif'>No.</span></b></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:none;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:12.0pt;
  line-height:150%;font-family:"Bookman Old Style",serif'>Uraian</span></b></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:12.0pt;
  line-height:150%;font-family:"Bookman Old Style",serif'>Jumlah</span></b></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><b><span style='font-size:12.0pt;
  line-height:150%;font-family:"Bookman Old Style",serif'>Keterangan</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>1.</span></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Kendaraan
  Dump Truck</span></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?=count_kendaraan("dump_truck") ?> Unit</span></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>2.</span></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Kendaraan
  Amroll</span></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?=count_kendaraan("amroll") ?> Unit</span></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>3.</span></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Kendaraan
  Roda 3</span></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?=count_kendaraan("roda_tiga") ?> Unit</span></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>4.</span></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Kendaraan
  Exavator</span></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?=count_kendaraan("excavator") ?> Unit</span></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>5.</span></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Kendaraan
  Dozer</span></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?=count_kendaraan("doser") ?> Unit</span></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'>&nbsp;</span></p>
  </td>
 </tr>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>6.</span></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>Container/Bak
  Sampah</span></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?=kont_count()?> Unit</span></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?=kont_items()?></span></p>
  </td>
 </tr>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>7.</span></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>TPA</span></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?php echo tpa_count() ?> Lokasi</span></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?php echo tpa_items() ?></span></p>
  </td>
 </tr>
 <tr>
  <td width=29 valign=top style='width:21.75pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>8.</span></p>
  </td>
  <td width=279 valign=top style='width:209.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:12.0pt;line-height:150%;font-family:"Bookman Old Style",serif'>TPS</span></p>
  </td>
  <td width=124 valign=top style='width:93.25pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=right style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:right;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?php echo tps_count() ?> Lokasi </span></p>
  </td>
  <td width=156 valign=top style='width:116.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center;line-height:150%'><span style='font-size:12.0pt;line-height:
  150%;font-family:"Bookman Old Style",serif'><?=tps_items()?></span></p>
  </td>
 </tr>
</table>

</div>

<h5 style="alignment-adjust: right;">Nb: Update terakhir Desember 2017</h5>
<br />
<br />
<br />


                        </div>
                    </div>
                </div>
                <!-- .row -->
                <!-- .right-sidebar -->
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Dinas Lingkungan Hidup Kabupaten Merauke <small>Support by <a href="http://technogis.co.id" target="_blank">TechnoGIS Indonesia</a></small> </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/frontend/old/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/frontend/old/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="assets/frontend/old/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="assets/frontend/old/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="assets/frontend/old/js/custom.min.js"></script>
</body>

</html>
