
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Aplikasi E-REMerauke, Merupakan aplikasi managemen pengelolaan sampah, aplikasi ini diharapkan mampu meningkatkan kesadaran masyarakat mengenai kebersihan dan meningkatkan pendapatan pemerintah daerah terutama Kabupaten Merauke">
    <meta name="author" content="">
    <meta name="keyword" content="retribusi sampah merauker, Retribusi Sampah Merauke, sampah merauke, Sampah Merauke, retribusi merauke, Retribusi Merauke, Merauke, merauke">
    <link rel="icon" type="image/png" sizes="16x16" href="../demos/plugins/uploads/images/favicon.png">
    <title>Aplikasi Retribusi Pengelolaan Sampah Kabupaten Merauke Provinsi Papua.</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/frontend/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!--My admin Custom CSS -->
    <link href="plugins/bower_components/owl.carousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="plugins/bower_components/owl.carousel/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="assets/frontend/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/frontend/css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-19175540-9', 'auto');
    ga('send', 'pageview');
    </script>
    <!--End of Zopim Live Chat Script-->
</head>

<body class="">
    <!-- Preloader -->
    <div id="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 back">
                    <center class="p-10 text-white"><b>Selamat Datang di Aplikasi Retribusi Pengelolaan Sampah Kabupaten Merauke, Provinsi Papua </b></center>
                </div>
            </div>
            <!-- Row -->
            <div class="row">
                <div class="col-md-12 navbar-fixed">
                    <div class="fix-width">
                        <nav class="navbar navbar-default">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="index.php?page=home"><img src="uploads/images/assets/logo-app.png" alt="Eliteadmin" style="width: 250px;" /></a>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav custom-nav navbar-right">
                                    <li class="dropdown cst-dwn">
                                        <a href="index.php?page=home" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                                        
                                    </li>
                                    <li><a href="index.php?page=about">Tentang</a></li>
                                    <li><a href="index.php?page=gis">WebGIS</a></li>
                                    <li><a href="index.php?page=contact">Kontak</a></li>
                                    <li><a href="index.php?page=data" >Data Lain</a></li>
                                    <li><a href="index.php?page=gallery" >Gallery</a></li>
                                    <li><a href="/login/" class="btn btn-danger btn-rounded custom-btn" target="_blank">Login</a></li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </nav>
                    </div>
                </div>
            </div>
            <!-- /Row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="fix-width text-center banner-part">
                        <div class="min-h">
                            <h1 class="banner-title">Aplikasi Retribusi Pengelolaan Sampah Kabupaten Merauke</h1></div>
                        <span class="banner-small-text"><b>Aplikasi E-REMerauke</b>, Merupakan aplikasi managemen pengelolaan sampah, aplikasi ini diharapkan mampu meningkatkan kesadaran masyarakat mengenai kebersihan dan meningkatkan pendapatan pemerintah daerah terutama Kabupaten Merauke...
                </span>
                        <div class="btn-box"><a href="#choose-demo" class="left-btn">SELENGKAPNYA</a><a href="index.php?page=gis" target="_blank" class="right-btn">DATA WEBGIS</a></div>
                    </div>
                </div>
            </div>
            <!-- Row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <a href="#choose-demo"><img src="uploads/images/assets/home-merauke.jpg" class="img-responsive"></a>
                    </div>
                </div>
            </div>
            <!-- .row -->
            <div class="row" id="choose-demo">
                <div class="col-lg-12">
                    <div class="light-blue-bg">
                        <center>
                            <small class="text-danger">Dokumentasi Kegiatan </small>
                            <h2 class="font-500">Galeri & Dokumentasi Kegiatan</h2>
                            <p>Untuk mewujudkan Kabupaten Merauke tercinta menjadi bersih dan nyaman.
                                <br/>Segenap Tim Dinas Lingkungan Hidup akan bekerja keras, cerdas, tuntas dan iklas.</p>
                        </center>
                        <div class="fix-width demo-boxes">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 m-t-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/1.jpg" class="img-responsive" />
                                        <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                    </div>
                                    <h5 class="m-t-20 font-500">Memperingati Hari Lingkungan Hidup</h5>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 m-t-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/2.jpg" class="img-responsive" />
                                        <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                    </div>
                                    <h5 class="m-t-20 font-500">Aktifitas Pembersihan Rumput</h5>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 m-t-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/3.jpg" class="img-responsive" />
                                        <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                    </div>
                                    <h5 class="m-t-20 font-500">Truk Pengangkut Kontainer </h5>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/4.jpg" class="img-responsive" />
                                        <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                    </div>
                                    <h5 class="m-t-20 font-500">Motor Pengangkut Sampah</h5>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/5.jpg" class="img-responsive" />
                                      <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                   </div>
                                    <h5 class="m-t-20 font-500">Segenap Staff Dinas Lingkungan Hidup Merauke</h5>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/6.jpg" class="img-responsive" />
                                    <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                    </div>
                                    <h5 class="m-t-20 font-500">Kontainer di Area Parkir</h5>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/7.jpg" class="img-responsive" />
                                        <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                   </div>
                                    <h5 class="m-t-20 font-500">Salah Satu Kontainer Sampah</h5>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/8.jpg" class="img-responsive" />
                                      <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                   </div>
                                    <h5 class="m-t-20 font-500">Kondisi Jalan di Merauke</h5>
                                </div>
                                
                                 <div class="col-md-4 col-sm-6 col-xs-12 m-b-40 text-center">
                                    <div class="white-box">
                                        <img src="uploads/images/assets/9.jpg" class="img-responsive" />
                                       <div class="img-ovrly"><a class="btn btn-rounded btn-danger" href="index.php?page=gallery" target="_blank">Live Preview</a></div>
                                   </div>
                                    <h5 class="m-t-20 font-500">Proses Pengambilan Sampah</h5>
                                </div>
                                
                               
                                <div class="col-md-12 col-sm-12 col-xs-12 p-t-10 text-center demo-text">
                                    <a class="btn btn-outline btn-rounded btn-default" href="index.php?page=gallery">Galeri Lainnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            
             <!-- .row -->
            <div class="row m-t-60">
                <div class="col-md-12">
                    <div class="fix-width">
                        <div class="row">
                            <div class="col-md-7 auto-img "><img src="uploads/images/assets/home1.jpg" class="pull-right" /></div>
                            <div class="col-md-5 demo-text">
                                <small class="text-danger">Tim Kami</small>
                                <h2 class="font-500">Bidang Pengelolaan Persampahan</h2>
                                <p class="m-t-30 m-b-30">Bertugas Membantu Kepala Dinas dalam merumuskan kebijakan, mengkoordinasikan, membina, mengawasi dan mengendalikan program di bidang Pengelolaan Persampahan.</p>
                                <a class="btn btn-outline btn-rounded btn-default" href="http://retribusisampahmerauke.com/index.php?page=bidang-pengelolaan-persampahan" target="_blank">Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            
            <!-- .row -->
            <div class="row m-t-60" id="myfeatures">
                <div class="col-md-12">
                    <div class="fix-width">
                        <div class="row">
                            <div class="col-md-5 demo-text">
                                <small class="text-danger">Dinas Lingkungan Hidup Merauke</small>
                                <h2 class="font-500">VISI & MISI </h2>
                                <p class="m-t-30 m-b-30">Terwujudnya peningkatan kualitas dan fungsi lingkungan hidup dalam pengelolaan sumberdaya alam berkelanjutan.</p>
                                <a class="btn btn-outline btn-rounded btn-default" href="index.php?page=visi-misi" target="_blank">Selengkapnya</a>
                            </div>
                            <div class="col-md-7 auto-img"><img src="uploads/images/assets/home2.jpg" /></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
           
            
            <div class="row m-t-60"></div>
            <!-- .row -->
            <div class="row m-t-60">
                <div class="col-md-12">
                    <div class="fix-width">
                        <center class="col-md-7 offset-md-2">
                            <small class="text-danger">Sumber Daya Manusia</small>
                            <h2 class="font-500">Struktur Organisasi & Tim Kerja</h2>
                            <p class="m-t-10 m-b-30">Seluruh jajaran Dinas Lingkungan Hidup Kabupaten Merauke merupakan induk terciptanya program ini, Divisi Pengelolaan Sampah sebagai inisiator dan pelaksanaan program e-REMerauke.
                            </p>
                        </center>
                        <div class="col-md-12 m-t-40"><img src="uploads/images/assets/home5.png" style="width:100%; align:center;"/></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            
             <!-- .row -->
            <div class="row m-t-60">
                <div class="col-md-12">
                    <div class="fix-width">
                        <center class="col-md-7 offset-md-2">
                            <small class="text-danger">Statistik</small>
                            <h2 class="font-500">Pemasukan & Pencapaian</h2>
                            <p class="m-t-10 m-b-30">Berikut Statistik laporan pemasukan retribusi dan target secara terbuka dan realtime.
                            </p>
                            
                        </center>
                        <div class="col-md-12 m-t-40">
                        <?php include("widget/widget.statistik.php");?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
   
            <!-- .row -->
            <div class="row">
                <div class="col-md-12 call-to-action bg-success">
                    <div class="fix-width">
                        <div class="row">
                           
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!-- .row -->
            <footer class="footer">
                <div class="fix-width">
                    <div class="row">
                        <div class="col-md-3 col-sm-6"><img src="uploads/images/assets/logo-app-white.png" style="height: 75px;" />
                            <p class="m-t-30">
                                <font class="text-white">E-REMerauke</font> merupakan aplikasi retribusi pengelolaan sampah di Kabupaten Merauke dikembangkan oleh Dinas Lingkungan Hidup Kab Merauke. </p>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <ul class="footer-link list-icons">
                                 <li><a href="index.php?page=about"></i> Tentang Aplikasi </a></li>
                                 <li><a href="index.php?page=gis"></i> Data WebGIS </a></li>
                                 <li><a href="index.php?page=data"></i> Data Penunjang Lain </a></li>
                                 <li><a href="index.php?page=gallery"></i> Kumpulan Gallery </a></li>
                                 <li><a href="index.php?page=contact"></i> Layanan Pengaduan </a></li>
                                
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-6">
                             <ul class="footer-link list-icons">
                                 <li><a href="http://www.technogis.co.id/" target="_blank"></i> TechnoGIS Indonesia </a></li>
                                 <li><a href="#" target="_blank"></i> CV Alam Makmur </a></li>
                                
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <ul class="footer-link list-icons">
                                 <li><a href="http://www.merauke.go.id/" target="_blank"></i> Pemerintah Kabupaten Merauke </a></li>
                                 <li><a href="https://papua.go.id/" target="_blank"></i> Pemerintah Provinsi Papua </a></li>
                                 <li><a href="http://www.menlhk.go.id/" target="_blank"></i> Kementrian Lingkungan Hidup & Kehutanan </a></li>
                                 <li><a href="https://www.facebook.com/pages/Badan-Lingkungan-hidup-kabMerauke/1393042744291381" target="_blank"></i> Dinas Lingkungan Hidup Merauke </a></li>
                                
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 sub-footer">
                            <span>Copyright 2017. Aplikasi e-REMERAUKE <a class="text-white" href="https://www.facebook.com/pages/Badan-Lingkungan-hidup-kabMerauke/1393042744291381" target="_blank">Dinas Lingkungan Hidup Merauke</a></span>
                            <span class="pull-right">Design & Developed by <a class="text-white" href="http://technogis.co.id" target="_blank">TechnoGIS Indonesia</a></span>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- /.row -->
        </div>
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/frontend/bootstrap/dist/js/tether.min.js"></script>
    <script src="assets/frontend/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- jQuery for carousel -->
    <script src="plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="assets/frontend/js/custom.js"></script>
    <!-- jQuery for typing -->
    <script src="plugins/bower_components/typed.js-master/dist/typed.min.js"></script>
    <script>
    $(function() {
        $(".banner-title").typed({
            strings: ["Aplikasi Retribusi Pengelolaan Sampah Kabupaten Merauke", "Bersama Mewujudkan Kabupaten Merauke Bersih dan Sehat", "Mewujudkan Merauke Menjadi Primadona Untuk Semua Orang"],
            typeSpeed: 100,
            loop: true
        });
    });
    </script>
</body>

</html>
